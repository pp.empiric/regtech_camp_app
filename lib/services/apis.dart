import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:regtech_camp_app/model/custom_field_response.dart';
import 'package:regtech_camp_app/model/folder_response.dart';
import 'package:regtech_camp_app/model/item_invoice_model.dart';
import 'package:regtech_camp_app/model/note_model.dart';
import 'package:regtech_camp_app/model/offer_code_response.dart';
import 'package:regtech_camp_app/model/payment_model.dart';

import '../model/StoreModel.dart';
import '../model/add_note_model.dart';
import '../model/comments_response.dart';
import '../model/contact_response.dart';
import '../model/course_curriculam_response.dart';
import '../model/course_response.dart';
import '../model/courses_curriculum_details_response.dart';
import '../model/general_user_data_response.dart';
import '../model/item_category_response.dart';
import '../model/log_in_response.dart';
import '../model/ping_response.dart';
import '../model/quiz_new_response.dart';
import '../model/quiz_question_response.dart';
import '../model/quote_response.dart';
import '../model/section_response.dart';
import '../modules/home/controllers/home_controller.dart';
import '../resources/app_constants.dart';
import '../resources/clients.dart';
import '../utils.dart';
import 'app_preference.dart';

Future<PingModel> getGeneralData() async {
  const String url = '$BASE/public/ping?name=$ORG_ID';
  final response = await http.get(Uri.parse(url));

  print(url);
  print(response.body);

  if (response.statusCode == 200 || response.statusCode == 201) {
    try {
      pingModel = PingModel.fromJson(jsonDecode(response.body));
      return pingModel;
    } catch (e) {
      return PingModel();
    }
  } else {
    return PingModel();
  }
}

Future<http.Response> callStripePaymentIntentCreateApi(dynamic amount) async {
  var body = <String, dynamic>{};
  body['amount'] = amount;
  body['currency'] = pingModel.csymbol == "\$" ? "usd" : "inr";
  if (pingModel.stripePublishableKey != null &&
      pingModel.stripePublishableKey!.isNotEmpty) {
    body['key'] = pingModel.stripePublishableKey!;
  }
  if (pingModel.stripeAuthCode != null &&
      pingModel.stripeAuthCode!.toString().isNotEmpty) {
    body['_stripe_account'] = pingModel.stripeAuthCode!;
  }
  //body['confirm'] = true;

  var parts = [];
  body.forEach((key, value) {
    parts.add('${Uri.encodeQueryComponent(key)}='
        '${Uri.encodeQueryComponent(value.toString())}');
  });
  var formData = parts.join('&');

  var response = await http.post(
      Uri.parse('https://api.stripe.com/v1/payment_intents'),
      body: formData,
      headers: <String, String>{
        HttpHeaders.authorizationHeader: "Bearer ${pingModel.stripeApiKey}",
        "Content-Type": "application/x-www-form-urlencoded"
      });
  print(response.body);
  return response;
}

Future<http.Response> callStripePaymentIntentCreateApiOld(String cardNumber,
    String cardMonth, String cardYear, String cardCvv) async {
  var body = <String, String>{};
  body['type'] = 'card';
  body['card[number]'] = cardNumber.replaceAll(" ", "").trim();
  body['card[exp_month]'] = cardMonth;
  body['card[exp_year]'] = cardYear;
  body['card[cvc]'] = cardCvv;

  if (AppSharedPreferences.getStringFromLocalStorage(KEY_USER_EMAIL) == null ||
      AppSharedPreferences
          .getStringFromLocalStorage(KEY_USER_EMAIL)
          .toString()
          .isEmpty) {} else {
    body['billing_details[email]'] =
        AppSharedPreferences.getStringFromLocalStorage(KEY_USER_EMAIL)
            .toString();
  }

  if (AppSharedPreferences.getStringFromLocalStorage(KEY_USER_PHONE) == null ||
      AppSharedPreferences
          .getStringFromLocalStorage(KEY_USER_PHONE)
          .toString()
          .isEmpty) {} else {
    body['billing_details[phone]'] =
        AppSharedPreferences.getStringFromLocalStorage(KEY_USER_PHONE)
            .toString();
  }

  if (AppSharedPreferences.getStringFromLocalStorage(KEY_USER_NAME) == null ||
      AppSharedPreferences
          .getStringFromLocalStorage(KEY_USER_NAME)
          .toString()
          .isEmpty) {} else {
    body['billing_details[name]'] =
        AppSharedPreferences.getStringFromLocalStorage(KEY_USER_NAME)
            .toString();
  }

  if (pingModel.stripePublishableKey != null &&
      pingModel.stripePublishableKey!.isNotEmpty) {
    body['key'] = pingModel.stripePublishableKey!;
  }
  if (pingModel.stripeAuthCode != null &&
      pingModel.stripeAuthCode!.toString().isNotEmpty) {
    body['_stripe_account'] = pingModel.stripeAuthCode!;
  }

  var parts = [];
  body.forEach((key, value) {
    parts.add('${Uri.encodeQueryComponent(key)}='
        '${Uri.encodeQueryComponent(value.toString())}');
  });
  var formData = parts.join('&');

  print(formData);
  var response = await http.post(
      Uri.parse('https://api.stripe.com/v1/payment_methods'),
      body: formData,
      headers: <String, String>{
        HttpHeaders.authorizationHeader: "Bearer ${pingModel.stripeApiKey}",
        "Content-Type": "application/x-www-form-urlencoded"
      });
  print(response.body);
  return response;
}

Future<LoginResponse> getGoogleUserData(String orgId, String refreshToken,
    String accessToken) async {
  const url = '$BASE_URL/authToken';
  final body =
      '{"orgId":"$orgId","email":"$accessToken","accessToken":"$refreshToken","authType":"google", "redirectUrl": "https://$orgId.wajooba.$baseApiDomain"}';
  final response = await http.post(
    Uri.parse(url),
    body: body,
  );

  print(response.body);

  if (response.statusCode == 200) {
    try {
      final LoginResponse user =
      LoginResponse.fromJson(jsonDecode(response.body));
      return user;
    } catch (e) {
      return LoginResponse();
    }
  } else {
    return LoginResponse();
  }
}

Future updateContactData(String body) async {
  final String accessToken =
  await AppSharedPreferences.getStringFromLocalStorage(KEY_ACCESS_TOKEN);
  final String id =
  await AppSharedPreferences.getStringFromLocalStorage(KEY_GUID);
  final String url = '$BASE_URL/contact/$id';
  final response = await http.put(
    Uri.parse(url),
    body: body,
    headers: <String, String>{
      'Authorization': 'Bearer $accessToken',
      'Content-Type': 'application/json',
    },
  );

  print(url);
  print(response.body);

  if (response.statusCode == 200 || response.statusCode == 201) {
    try {
      contactResponse = ContactResponse.fromJson(jsonDecode(response.body));
      await AppSharedPreferences.saveStringToLocalStorage(
          KEY_USER_EMAIL, contactResponse.email ?? '');
      await AppSharedPreferences.saveStringToLocalStorage(
          KEY_USER_NAME, contactResponse.name ?? contactResponse.fullName);
      await AppSharedPreferences.saveStringToLocalStorage(
          KEY_USER_PHONE, contactResponse.phone ?? '');
      Get.find<HomeController>().callApis();
      return contactResponse;
    } catch (e) {
      return ContactResponse();
    }
  } else {
    return ContactResponse();
  }
}

Future<ContactResponse> getContactResponse() async {
  final id = await AppSharedPreferences.getStringFromLocalStorage(KEY_GUID);
  if (id == null) {
    return ContactResponse();
  }

  final String accessToken =
  await AppSharedPreferences.getStringFromLocalStorage(KEY_ACCESS_TOKEN);
  final String url = '$BASE_URL/contact/$id';
  final response = await http.get(Uri.parse(url), headers: {
    'Authorization': 'Bearer $accessToken',
  });
  print(url);
  print(response.body);

  if (response.statusCode == 200 || response.statusCode == 201) {
    try {
      final ContactResponse user =
      ContactResponse.fromJson(jsonDecode(response.body));
      return user;
    } catch (e) {
      return ContactResponse();
    }
  } else {
    return ContactResponse();
  }
}

Future<CourseModel> getCourses() async {
  final String accessToken = await getAccessToken();
  final String id =
  await AppSharedPreferences.getStringFromLocalStorage(KEY_GUID);
  final String url = '$BASE_URL/contact/$id/courses?include=stats,purchase';
  final response = await http.get(Uri.parse(url), headers: {
    'Authorization': 'Bearer $accessToken',
  });

  print(accessToken);
  print(url);
  print(response.body);

  if (response.statusCode == 200) {
    final value = CourseModel.fromJson(jsonDecode(response.body));
    return value;
  } else {
    return CourseModel();
  }
}

Future<GeneralUserDataResponse> getGeneralUserData() async {
  const String url = '$BASE_URL/tenant';
  final accessToken = await getAccessToken();
  final response = await http.get(
    Uri.parse(url),
    headers: {'Authorization': 'Bearer $accessToken'},
  );

  print(accessToken);
  print(response.body);

  if (response.statusCode == 200) {
    final GeneralUserDataResponse general =
    GeneralUserDataResponse.fromJson(jsonDecode(response.body));
    return general;
  } else {
    return GeneralUserDataResponse();
  }
}

Future<ItemInvoice> getItemInvoiceStripe(StoreModel element, String cardId,
    String offerId) async {
  var paymentData = {};
  double txnAmount = 0;
  double subAmount = 0;
  double oneAmount = 0;
  double taxPercent = pingModel.taxPercent ?? 0;

  var amount = element.subscriptionInfo != null
      ? element.registrationFees +
      element.regisstrationcardfees -
      element.discountSecondary
      : element.price + element.normalcardfees - element.discountPrimary;

  if (element.isTaxable == true) {
    amount += (amount * taxPercent) / 100;
  }

  var sub = element.subscriptionInfo != null
      ? element.price + element.normalcardfees - element.discountPrimary
      : 0;

  if (element.isTaxable == true && element.subscriptionInfo != null) {
    sub += (sub * taxPercent) / 100;
  }

  txnAmount = txnAmount + amount + sub;
  subAmount = subAmount + sub;
  oneAmount = oneAmount + amount;
  var list = <PaymentModel>[];
  PaymentModel itemClass = PaymentModel(
      guId: element.guId ?? '0',
      billingDay: element.subscriptionInfo != null
          ? 0
          : element.billingDay != null
          ? int.parse(element.billingDay!)
          : 0,
      noOfBillingCycles: element.subscriptionInfo != null ? element
          .subscriptionInfo!.numberOfBillingCycles ?? 0 : 0,
      subscriptionAmount: element.subscriptionInfo != null
          ? element.price.toStringAsFixed(2)
          : element.registrationFees != 0
          ? element.registrationFees.toStringAsFixed(2)
          : '0.0',
      oneTimePayment: element.registrationFees != 0
          ? element.registrationFees.toStringAsFixed(2)
          : element.price != 0
          ? element.price.toStringAsFixed(2)
          : '0',
      finalOneTimePayment: amount.toStringAsFixed(2),
      finalSubscriptionAmount: sub.toStringAsFixed(2),
      txnAmount: (amount + sub).toStringAsFixed(2),
      memberShipExpire: element.subscriptionInfo != null
          ? element.subscriptionInfo!.billingFrequency != null
          ? element.subscriptionInfo!.billingFrequency!.contains('mont')
          ? DateTime(
          DateTime
              .now()
              .month == 12
              ? DateTime
              .now()
              .year + 1
              : DateTime
              .now()
              .year,
          DateTime
              .now()
              .month == 12
              ? element.subscriptionInfo!.numberOfBillingCycles != null
              ? element.subscriptionInfo!.numberOfBillingCycles == 1
              ? 1
              : (element.subscriptionInfo!.numberOfBillingCycles! - 1)
              : 1
              : DateTime
              .now()
              .month + (element.subscriptionInfo!.numberOfBillingCycles ?? 1),
          DateTime
              .now()
              .day - 1,
          DateTime
              .now()
              .hour,
          DateTime
              .now()
              .minute,
          DateTime
              .now()
              .second)
          .millisecondsSinceEpoch
          .toString()
          .substring(0, 10)
          : DateTime(
          DateTime
              .now()
              .year + (element.subscriptionInfo!.numberOfBillingCycles ?? 1),
          DateTime
              .now()
              .month,
          DateTime
              .now()
              .day - 1,
          DateTime
              .now()
              .hour,
          DateTime
              .now()
              .minute,
          DateTime
              .now()
              .second)
          .millisecondsSinceEpoch
          .toString()
          .substring(0, 10)
          : DateTime(DateTime
          .now()
          .month == 12
          ? DateTime
          .now()
          .year + 1
          : DateTime
          .now()
          .year, DateTime
          .now()
          .month == 12 ? 1 : DateTime
          .now()
          .month + 1, DateTime
          .now()
          .day - 1,
          DateTime
              .now()
              .hour,
          DateTime
              .now()
              .minute,
          DateTime
              .now()
              .second)
          .millisecondsSinceEpoch
          .toString()
          .substring(0, 10)
          : DateTime(DateTime
          .now()
          .month == 12
          ? DateTime
          .now()
          .year + 1
          : DateTime
          .now()
          .year, DateTime
          .now()
          .month == 12 ? 1 : DateTime
          .now()
          .month + 1, DateTime
          .now()
          .day - 1,
          DateTime
              .now()
              .hour,
          DateTime
              .now()
              .minute,
          DateTime
              .now()
              .second).millisecondsSinceEpoch.toString().substring(0, 10));
  list.add(itemClass);

  paymentData.addAll({
    "type": "s_card",
    "nonce": cardId,
    "isSaveCard": false,
    "paidAmount": txnAmount.toStringAsFixed(2),
    "txnAmount": txnAmount.toStringAsFixed(2),
    "subscriptionAmount": subAmount.toStringAsFixed(2),
    "oneTimePayment": oneAmount.toStringAsFixed(2)
  });

  try {
    String accessToken = await getAccessToken();
    String id = await AppSharedPreferences.getStringFromLocalStorage(KEY_GUID);
    var body = <String, dynamic>{};
    body['provider'] = "STRIPE";
    body['user'] = {"guId": id};
    body['payment'] = paymentData;
    body['itemList'] = list;
    if (offerId.isNotEmpty) {
      body['offerId'] = offerId;
    }

    final response = await http.post(Uri.parse('$BASE_URL/cart'),
        body: jsonEncode(body),
        headers: <String, String>{
          "Authorization": "Bearer $accessToken",
        });

    print(jsonEncode(body));
    print(response.body);
    if (response.statusCode == 200 || response.statusCode == 201) {
      if (jsonDecode(response.body)['error'] != null &&
          jsonDecode(response.body)['error']['message'] != null) {
        showToast(jsonDecode(response.body)['error']['message']);
        return ItemInvoice();
      }
      return ItemInvoice.fromJson(jsonDecode(response.body));
    } else if (response.statusCode == 503) {
      showToast(jsonDecode(response.body)['error']['message'] ??=
      "Failed to made payment.");
      return ItemInvoice();
    } else {
      showToast(jsonDecode("Failed to made payment."));
      return ItemInvoice();
    }
  } catch (e) {
    print(e.toString());
    return ItemInvoice();
  }
}

Future<List<Quote>> getQuote() async {
  const String url = '$BASE/edtest/noticeboard';
  final String accessToken = await getAccessToken();
  final response = await http.get(
    Uri.parse(url),
    headers: {'Authorization': 'Bearer $accessToken'},
  );

  print(url);
  print(response.body);

  if (response.statusCode == 200 || response.statusCode == 201) {
    final QuoteResponse general =
    QuoteResponse.fromJson(jsonDecode(response.body));
    if (general.data != null && general.data!.isNotEmpty) {
      return general.data!;
    } else {
      return <Quote>[];
    }
  } else {
    return <Quote>[];
  }
}

Future<OfferCodeResponse> applyOfferApi(String offerCode) async {
  String errorMessage = "Failed to apply offer code";
  try {
    String accessToken = await getAccessToken();
    String url = '$BASE_URL/itemOffer?offerCode=$offerCode';
    final response = await http.get(
      Uri.parse(url),
      headers: <String, String>{
        HttpHeaders.authorizationHeader: "Bearer $accessToken",
      },
    );

    if (response.statusCode == 200 || response.statusCode == 201) {
      return OfferCodeResponse.fromJson(jsonDecode(response.body));
    } else if (response.statusCode == 503) {
      return OfferCodeResponse();
    }
    return OfferCodeResponse();
  } catch (e) {
    return OfferCodeResponse();
  }
}

Future<FeaturedEventDescription> getFeaturedEventsDescription(
    String eventId) async {
  String url = '$BASE/unlayertemplate/categories?guid=$eventId';
  final response = await http.get(Uri.parse(url));
  if (response.statusCode == 200) {
    try {
      return FeaturedEventDescription.fromJson(jsonDecode(response.body).first);
    } catch (e) {
      return FeaturedEventDescription();
    }
  } else {
    return FeaturedEventDescription();
  }
}

Future<List<ItemCategoryModel>> getItemCategory() async {
  try {
    final String accessToken = await getAccessToken();
    const String url = '$BASE_URL/itemCategory?include=stats&showAll=true';
    final response = await http.get(Uri.parse(url), headers: {
      'Authorization': 'Bearer $accessToken',
    });

    print(url);
    print(response.body);

    if (response.statusCode == 200) {
      final ItemCategoryResponse itemCategoryModel =
      ItemCategoryResponse.fromJson(jsonDecode(response.body));
      final List<ItemCategoryModel> list = await getDonationItems();
      if (list.isNotEmpty) {
        itemCategoryModel.data?.addAll(list);
      }
      return itemCategoryModel.data!;
    } else {
      return <ItemCategoryModel>[];
    }
  } catch (e) {
    print(e);
    return <ItemCategoryModel>[];
  }
}

Future deleteFileApi(String id) async {
  final String accessToken = await getAccessToken();
  await http.delete(Uri.parse('$BASE/edcourse/assets/$id'), headers: {
    'Authorization': 'Bearer $accessToken',
  });
}

Future<List<ItemCategoryModel>> getDonationItems() async {
  try {
    final String accessToken = await getAccessToken();
    const String url = '$BASE_URL/donation?include=stats&showAll=true';
    final response = await http.get(Uri.parse(url), headers: {
      'Authorization': 'Bearer $accessToken',
    });

    print(url);
    print(response.body);

    if (response.statusCode == 200) {
      final ItemCategoryResponse itemCategoryModel =
      ItemCategoryResponse.fromJson(jsonDecode(response.body));
      final List<ItemCategoryModel> list = [];
      itemCategoryModel.data?.forEach((element) {
        element.setPaymentType(KEY_DONATION);
        list.add(element);
      });
      return list;
    } else {
      return <ItemCategoryModel>[];
    }
  } catch (e) {
    print(e);
    return <ItemCategoryModel>[];
  }
}

Future<CourseCurriculumResponse> getCoursesCurriculum(String guid) async {
  try {
    final String accessToken = await getAccessToken();
    final String url = '$BASE_URL/itemCategory/$guid?include=details,template';
    final response = await http.get(Uri.parse(url), headers: {
      'Authorization': 'Bearer $accessToken',
    });

    print(url);
    print(response.body);

    if (response.statusCode == 200) {
      final value =
      CourseCurriculumResponse.fromJson(jsonDecode(response.body));
      return value;
    } else {
      return CourseCurriculumResponse();
    }
  } catch (e) {
    print(e);
    throw Exception('Failed to get upcoming events');
  }
}

Future<CoursesCurriculumDetailsResponse> getCoursesCurriculumDetails(
    String courseId, String chapterId) async {
  final String accessToken = await getAccessToken();
  final String url =
      '$BASE/edcourse/assets?ownerId=$chapterId&chapterId=$courseId';

  final response = await http.get(Uri.parse(url), headers: {
    'Authorization': 'Bearer $accessToken',
  });

  print(url);
  print(response.body);

  if (response.statusCode == 200) {
    final value =
    CoursesCurriculumDetailsResponse.fromJson(jsonDecode(response.body));
    return value;
  } else {
    return CoursesCurriculumDetailsResponse();
  }
}

Future<List<SectionResponse>> getSections(String? guid) async {
  final String accessToken = await getAccessToken();
  final String url = '$BASE_URL/itemCategory/$guid/section';
  final response = await http.get(Uri.parse(url), headers: {'Authorization': 'Bearer $accessToken'});

  print(accessToken);
  print(url);
  print(response.body);

  if (response.statusCode == 200) {
    final list = (jsonDecode(response.body) as List)
        .map((dynamic i) => SectionResponse.fromJson(i as Map<String, dynamic>))
        .toList();
    return list;
  } else {
    return <SectionResponse>[];
  }
}

Future<List<NoteModel>> getNotes(String courseId,
    String chapterId,
    String courseGuId,) async {
  final String accessToken = await getAccessToken();
  final String url =
      '$BASE_URL/itemCategory/$courseId/section/$courseGuId/chapter/$chapterId';
  final response = await http.get(Uri.parse(url), headers: {
    'Authorization': 'Bearer $accessToken',
  });

  if (response.statusCode == 200 || response.statusCode == 201) {
    try {
      final list = (jsonDecode(response.body) as List)
          .map((dynamic i) => NoteModel.fromJson(i as Map<String, dynamic>))
          .toList();
      return list;
    } catch (e) {
      return <NoteModel>[];
    }
  } else {
    return <NoteModel>[];
  }
}

Future<dynamic> addCourseNote(String courseId,String sectionId,
    String chapterId, String noteText) async {
  final String accessToken = await getAccessToken();
  final String id =
  await AppSharedPreferences.getStringFromLocalStorage(KEY_GUID);
  final body = '{"note":$noteText,"chapter":$chapterId,"contact":"$id"}';
  final response = await http.post(
      Uri.parse(
          '$BASE_URL/itemCategory/$courseId/section/$sectionId/chapter/$chapterId/note'),
      body: body,
      headers: {
        'Authorization': 'Bearer $accessToken',
      });
  print(
      '$BASE_URL/itemCategory/$courseId/section/$sectionId/chapter/$chapterId/note');
  print(response.body);

  if (response.statusCode == 200 || response.statusCode == 201) {
    return jsonDecode(response.body);
  } else {
    return '';
  }
}

Future<QuizQuestionResponse> initQuizAPI(body) async {
  final String accessToken = await getAccessToken();
  const String url = '$BASE/edtest/paper';

  final response = await http.post(Uri.parse(url),
      headers: {
        'Authorization': 'Bearer $accessToken',
        'Content-Type': 'application/json'
      },
      body: body);

  final json = jsonDecode(response.body);

  print(url);
  print(response.body);

  if (json['data'] != null) {
    final value = QuizQuestionResponse.fromJson(jsonDecode(response.body));
    return value;
  } else {
    return QuizQuestionResponse();
  }
}

Future<String> forgetPasswordApi(String email, {String? franchise}) async {
  final String? ordid = pingModel != null ? pingModel.orgId ?? ORG_ID : ORG_ID;
  try {
    final Map user = pingModel.isMasterFranchise != null &&
        pingModel.isMasterFranchise == true
        ? {'email': email, 'orgId': ordid, 'franchiseId': franchise ?? ""}
        : {'email': email, 'orgId': ordid, 'franchiseId': franchise ?? ""};

    final String body = json.encode(user);
    const String url = '$BASE_URL/user/forgotPassword';
    final response = await http.post(Uri.parse(url), body: body);

    print(body);
    print(url);

    if (response.statusCode == 200) {
      showToast(jsonDecode(response.body)['msg']);
    } else {
      showToast(jsonDecode(response.body)['msg']);
    }
  } catch (e) {
    showToast('Something went wrong');
  }

  return '';
}

 getQuizNewApi(
    {String? courseId, String? chapterId, String? type}) async {
  final accessToken = await getAccessToken();
  final String url =
      '$BASE/edtest/activity?courseId=$courseId&chapterId=$chapterId&type=$type';

  final response = await http.get(Uri.parse(url), headers: {
    'Authorization': 'Bearer $accessToken',
  });

  print(url);
  print(response.body);

  if (response.statusCode == 200 || response.statusCode == 201) {
    final QuizNewResponse value =
    QuizNewResponse.fromJson(jsonDecode(response.body));
    if (value.data != null && value.data!.isNotEmpty) {
      return value;
    } else {
      return value;
    }
  } else {
    return QuizNewResponse();
  }
}

Future<List<StoreModel>> getStoreItems(String itemType) async {
  String accessToken = await getAccessToken();
  String url = '$BASE_URL/itemCategory/$itemType/items';
  final response = await http.get(Uri.parse(url), headers: {
    "Authorization": "Bearer $accessToken",
  });

  if (response.statusCode == 200) {
    List<StoreModel> value = [];

    if (jsonDecode(response.body)['memberships'] != null &&
        jsonDecode(response.body)['memberships'] is List) {
      value = (jsonDecode(response.body)['memberships'] as List)
          .map((dynamic i) => StoreModel.fromJson(i as Map<String, dynamic>))
          .toList();
    }
    List<StoreModel> value2 = [];
    if (jsonDecode(response.body)['products'] != null &&
        jsonDecode(response.body)['products'] is List) {
      value2 = (jsonDecode(response.body)['products'] as List)
          .map((dynamic i) => StoreModel.fromJson(i as Map<String, dynamic>))
          .toList();
      value.addAll(value2);
    }
    return value;
  } else {
    return [];
  }
}

Future<AddNoteModel> addNote(String? noteText, String chapterId) async {
  String accessToken = await getAccessToken();

  String url =
      '$BASE_URL/itemCategory/gYVAJEl0M1/section/nD7dzgZGbJ/chapter/$chapterId/note';
  String id = await AppSharedPreferences.getStringFromLocalStorage(KEY_GUID);

  var body = '{"note":$noteText,"chapter":$chapterId,"contact":"$id"}';

  final response = await http.post(Uri.parse(url), body: body, headers: {
    "Authorization": "Bearer $accessToken",
  });
  print(url);
  print(response.body);

  if (response.statusCode == 200) {
    var value = AddNoteModel.fromJson(jsonDecode(response.body));
    return value;
  } else {
    return AddNoteModel();
  }
}

Future deleteFolderVideo(String id) async {
  String accessToken = await getAccessToken();

  String url = '$BASE/edcourse/assets/$id?file-name=';

  final response = await http.delete(Uri.parse(url), headers: {
    "Authorization": "Bearer $accessToken",
  });

  print(url);
  print(response.body);

  if (response.statusCode == 200) {
    var value = jsonDecode(response.body);
    return value;
  } else {
    throw Exception('Failed to get upcoming events');
  }
}

Future<List<ContactAssets>> getFolders() async {
  final String accessToken = await getAccessToken();
  final String id =
  await AppSharedPreferences.getStringFromLocalStorage(KEY_GUID);
  final response =
  await http.get(Uri.parse('$BASE_URL/contact/$id/asset'), headers: {
    'Authorization': 'Bearer $accessToken',
  });

  if (response.statusCode == 200 || response.statusCode == 201) {
    try {
      final temp = FolderResponse.fromJson(jsonDecode(response.body));
      if (temp.contactAssets != null && temp.contactAssets!.isNotEmpty) {
        return temp.contactAssets!;
      } else {
        return [];
      }
    } catch (e) {
      return [];
    }
  } else {
    return [];
  }
}

Future<ContactAssets> deleteFolderAPI(String folderId) async {
  final String accessToken = await getAccessToken();
  final String id =
  await AppSharedPreferences.getStringFromLocalStorage(KEY_GUID);
  final response = await http
      .delete(Uri.parse('$BASE_URL/contact/$id/asset/$folderId'), headers: {
    'Authorization': 'Bearer $accessToken',
  });

  if (response.statusCode == 200 || response.statusCode == 201) {
    try {
      return ContactAssets.fromJson(jsonDecode(response.body));
    } catch (e) {
      return ContactAssets();
    }
  } else {
    return ContactAssets();
  }
}

Future<CommentsResponse> getFolderNotesAPI(String guid) async {
  final String accessToken = await getAccessToken();
  final String id =
  await AppSharedPreferences.getStringFromLocalStorage(KEY_GUID);

  final response =
  await http.get(Uri.parse('$BASE_URL/contact/$id/asset/$guid'), headers: {
    'Authorization': 'Bearer $accessToken',
  });

  if (response.statusCode == 200 || response.statusCode == 201) {
    try {
      final value = CommentsResponse.fromJson(jsonDecode(response.body));
      return value;
    } catch (e) {
      return CommentsResponse(notes: []);
    }
  } else {
    return CommentsResponse(notes: []);
  }
}

Future<CoursesCurriculumDetailsResponse> getFolderContents(
    String chapterId) async {
  final String accessToken = await getAccessToken();
  final String id =
  await AppSharedPreferences.getStringFromLocalStorage(KEY_GUID);

  final response = await http.get(
      Uri.parse('$BASE/edcourse/assets?chapterId=$chapterId&ownerId=$id'),
      headers: {
        'Authorization': 'Bearer $accessToken',
      });
  print("$BASE/edcourse/assets?chapterId=$chapterId&ownerId=$id");
  print(response.body);

  if (response.statusCode == 200) {
    try {
      return CoursesCurriculumDetailsResponse.fromJson(
          jsonDecode(response.body));
    } catch (e) {
      return CoursesCurriculumDetailsResponse();
    }
  } else {
    return CoursesCurriculumDetailsResponse();
  }
}

Future<dynamic> addFolderContents(String chapterId, String link) async {
  final String accessToken = await getAccessToken();
  final String uName =
  await AppSharedPreferences.getStringFromLocalStorage(KEY_USER_NAME);
  final String id =
  await AppSharedPreferences.getStringFromLocalStorage(KEY_GUID);

  final map = <String, dynamic>{};
  map['ownerId'] = id;
  map['chapterId'] = chapterId;
  map['fileName'] = link;
  map['ownerName'] = uName;
  map['folderName'] = 'contacts/$uName-$id';
  map['fileType'] = 'link';
  map['sequence'] = 0;

  final response = await http.post(Uri.parse('$BASE/edcourse/assets/'),
      body: jsonEncode(map),
      headers: {
        'Authorization': 'Bearer $accessToken',
        'Content-Type': 'application/json'
      });

  print(response);
  print(response.body);
  if (response.statusCode == 200 || response.statusCode == 201) {
    try {
      final value = jsonDecode(response.body);
      return value;
    } catch (e) {
      return '';
    }
  } else {
    return '';
  }
}

Future<ContactAssets> addFolder([String folderName = 'New Folder']) async {
  final String accessToken = await getAccessToken();
  final String id =
  await AppSharedPreferences.getStringFromLocalStorage(KEY_GUID);
  final body = '{"title":"$folderName","contact":"$id","author":"$id"}';
  final response = await http
      .post(Uri.parse('$BASE_URL/contact/$id/asset'), body: body, headers: {
    'Authorization': 'Bearer $accessToken',
  });

  if (response.statusCode == 200 || response.statusCode == 201) {
    try {
      return ContactAssets.fromJson(jsonDecode(response.body));
    } catch (e) {
      return ContactAssets();
    }
  } else {
    return ContactAssets();
  }
}

Future<ContactAssets> changeFolderName(String folderName, String guid) async {
  final String accessToken = await getAccessToken();
  final String id =
  await AppSharedPreferences.getStringFromLocalStorage(KEY_GUID);

  final response = await http.put(
      Uri.parse('$BASE_URL/contact/$id/asset/$guid'),
      body: '{"title":"$folderName","isPublished":${true},"guId":"$guid"}',
      headers: {
        'Authorization': 'Bearer $accessToken',
      });

  if (response.statusCode == 200 || response.statusCode == 201) {
    try {
      return ContactAssets.fromJson(jsonDecode(response.body));
    } catch (e) {
      return ContactAssets();
    }
  } else {
    return ContactAssets();
  }
}

Future<dynamic> addFolderNote(String noteText, String noteGuid) async {
  final String accessToken = await getAccessToken();
  final String id =
  await AppSharedPreferences.getStringFromLocalStorage(KEY_GUID);

  final body = '{"note":"$noteText","asset":"$noteGuid","contact":"$id"}';
  final response = await http.post(
      Uri.parse('$BASE_URL/contact/$id/asset/$noteGuid/note'),
      body: body,
      headers: {
        'Authorization': 'Bearer $accessToken',
      });

  if (response.statusCode == 200 || response.statusCode == 201) {
    return jsonDecode(response.body);
  } else {
    throw Exception('Failed to add Folder');
  }
}

Future uploadFile(File file, String mimeType, String url) async {
  String accessToken = await getAccessToken();

  try {
    await http.put(
      Uri.parse(url),
      body: file.readAsBytesSync(),
      headers: {
        "Authorization": "Bearer $accessToken",
      },
    );
  } catch (e) {
    throw Exception("Something went wrong");
  }
}

Future getS3Url(String fileName, String mimeType) async {
  String uName =
  await AppSharedPreferences.getStringFromLocalStorage(KEY_USER_NAME);
  String accessToken = await getAccessToken();

  String id = await AppSharedPreferences.getStringFromLocalStorage(KEY_GUID);

  String url = '$BASE/edcourse/assets/url';

  var body = '{"fileName":"$fileName","folderName":"contacts/$uName-$id"}';

  final response = await http.put(Uri.parse(url), body: body, headers: {
    "Authorization": "Bearer $accessToken",
    "Origin": BASE_S3_ORIGIN,
    "Referer": "$BASE_S3_ORIGIN/allfiles",
    'Content-type': 'application/json',
    'Accept': 'application/json'
  });

  print(url);
  print(accessToken);
  print(response.body);
  if (response.statusCode == 200) {
    final value = jsonDecode(response.body);
    return value;
  } else {
    throw Exception('Failed to get info stats items');
  }
}

Future<dynamic> fileUpload(String s3url, String mimeType, String chapterId,
    bool fileExtension, String fileName,
    {Map<String, dynamic>? data}) async {
  final String accessToken = await getAccessToken();
  final String uName =
  await AppSharedPreferences.getStringFromLocalStorage(KEY_USER_NAME);
  final String id =
  await AppSharedPreferences.getStringFromLocalStorage(KEY_GUID);
  final map = data ?? <String, dynamic>{};
  map['ownerId'] = id;
  map['chapterId'] = chapterId;
  map['notes'] = '';
  map['ownerName'] = uName;
  map['folderName'] = 'contacts/$uName-$id';
  map['fileType'] = fileExtension ? 'video' : 'file';
  map['fileName'] = fileName;
  map['assetLink'] = '';
  map['sequence'] = 0;

  print(jsonEncode(map));

  final response = await http.post(Uri.parse('$BASE/edcourse/assets'),
      body: jsonEncode(map),
      headers: <String, String>{
        'Authorization': 'Bearer $accessToken',
        'Content-type': 'application/json',
        'Accept': 'application/json',
        'Origin': '$BASE_S3_ORIGIN',
        'Referer': '$BASE_S3_ORIGIN/allfiles',
      });

  if (response.statusCode == 200 || response.statusCode == 201) {
    try {
      final value = jsonDecode(response.body);
      return value;
    } catch (e) {
      return '';
    }
  } else {
    return '';
  }
}

Future<http.Response> setActivityApi(body) async {
  final String accessToken = await getAccessToken();
  final response = await http.post(Uri.parse('$BASE/edtest/activityresult'),
      headers: {
        'Authorization': 'Bearer $accessToken',
        'Content-Type': 'application/json'
      },
      body: body);
  print(response);
  print(response.body);

  return response;
}

Future<dynamic> callCertificateAPI(body) async {
  String accessToken = await getAccessToken();

  String url = '$BASE/edtest/certificate/';

  final response = await http.post(Uri.parse(url),
      headers: {
        "Authorization": "Bearer $accessToken",
        "Content-Type": "application/json"
      },
      body: body);
  var json = jsonDecode(response.body);
  return json;
}

Future<List<CustomField>> customFieldListApi() async {
  final String accessToken = await getAccessToken();
  final response = await http.get(Uri.parse('$BASE_URL/customField'), headers: {
    'Authorization': 'Bearer $accessToken',
  });

  try {
    return (jsonDecode(response.body) as List)
        .map((dynamic i) => CustomField.fromJson(i as Map<String, dynamic>))
        .toList();
  } catch (e) {
    return <CustomField>[];
  }
}

Future<List<CustomField>> updateCustomFieldApi() async {
  final String accessToken = await getAccessToken();
  final String id =
  await AppSharedPreferences.getStringFromLocalStorage(KEY_GUID);

  String url = '$BASE_URL/contact/$id/customField';
  final response = await http.get(Uri.parse(url), headers: {
    'Authorization': 'Bearer $accessToken',
  });

  print(url);
  print(response.body);
  if (response.statusCode == 200) {
    final list = (jsonDecode(response.body)['customFieldsVals'] as List)
        .map((dynamic i) => CustomField.fromJson(i as Map<String, dynamic>))
        .toList();
    return list;
  } else {
    return <CustomField>[];
  }
}


