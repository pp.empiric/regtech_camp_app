class Routes {
  static const SPLASH = '/splash';
  static const LOGIN = '/login';
  static const HOME = '/home';
  static const PROFILE = '/profile';
  static const WELCOME = '/welcome';
  static const FORGET = '/forget';
  static const PROFILETWO='/profiletwo';
  static const COURSE='/course';
  static const CHECKOUT='/checkout';
  static const FILES='/files';
  static const EVENTSDETAILS='/eventdetails';

}
