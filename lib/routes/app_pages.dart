import 'package:get/get.dart';
import 'package:regtech_camp_app/modules/checkout/views/checkout.dart';
import 'package:regtech_camp_app/modules/course/views/courses_screen.dart';
import 'package:regtech_camp_app/modules/forget_password/views/forgetPassword.dart';
import 'package:regtech_camp_app/modules/login/views/welcome_screen.dart';
import 'package:regtech_camp_app/modules/splash/views/splash_screen.dart';
import 'package:regtech_camp_app/routes/app_routes.dart';

import '../modules/files/file_one.dart';
import '../modules/forget_password/views/forgetPassword.dart';
import '../modules/home/views/home_screen.dart';
import '../modules/login/views/login_screen.dart';
import '../modules/login/views/welcome_screen.dart';
import '../modules/profile/views/profile_screen.dart';

class AppPages {
  static const initial = Routes.SPLASH;

  static final routes = [
    GetPage(name: Routes.SPLASH, page: () => SplashScreen()),
    GetPage(name: Routes.LOGIN, page: () => LoginScreen()),
    GetPage(name: Routes.HOME, page: () => HomeScreen()),
    GetPage(name: Routes.WELCOME, page: () => WelcomeScreen()),
    GetPage(name: Routes.FORGET, page: () => ForgetPasswordScreen()),
    GetPage(name: Routes.PROFILE, page: () => ProfileScreen()),
    GetPage(name: Routes.FILES, page: () => const FilesOne()),
    GetPage(name: Routes.COURSE, page: () => CoursesScreen()),
    GetPage(name: Routes.CHECKOUT, page: () => Checkout()),
  ];
}
