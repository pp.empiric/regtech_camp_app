import 'package:get/get_rx/src/rx_types/rx_types.dart';

import 'activity_response.dart';

/// data : [{"_id":"61961ff4d013c300125e7dfa","name":"new act","additionalInfo":"new act","activityQuestions":[],"isTrackActivity":true,"isActive":false,"isRepeatActivity":false,"isAllowUserToUploadFiles":false,"course":"2EAVqrm0zK","chapter":"zjd58eyAvO","section":"8EjAP5jd9q","orgId":"m300","createdAt":"2021-11-18T09:42:12.737Z","updatedAt":"2021-11-18T09:42:12.737Z","__v":0,"hasCompleted":false}]
/// total : 1
/// assetData : [{"id":"61961ff4843ebf00137b81c0","orgId":"m300","chapterId":"zjd58eyAvO","sectionId":"8EjAP5jd9q","fileType":"text","fileName":"text","folderName":"courses/Grade1-2EAVqrm0zK","fileSize":"0.00","sequence":0,"isVideo":false,"ownerId":"2EAVqrm0zK","ownerName":"Grade 1","updatedAt":"2021-11-18T09:42:12.727Z","status":"COMPLETED","jobId":"","notes":"<p>new act</p>"}]
/// assetCount : 1

class QuizNewResponse {
  QuizNewResponse({
    List<QuizNewData>? data,
    int? total,
    List<ContactAssetData>? contactAssetData,
    List<AssetData>? assetData,
    int? assetCount,
  }) {
    _data = data;
    _total = total;
    _contactAssetData = contactAssetData;
    _assetData = assetData;
    _assetCount = assetCount;
  }

  QuizNewResponse.fromJson(dynamic json) {
    if (json['data'] != null) {
      _data = [];
      json['data'].forEach((v) {
        _data?.add(QuizNewData.fromJson(v));
      });
    }
    _total = json['total'];
    if (json['contactAssetData'] != null) {
      _contactAssetData = [];
      json['contactAssetData'].forEach((v) {
        _contactAssetData?.add(ContactAssetData.fromJson(v));
      });
    }
    if (json['assetData'] != null) {
      _assetData = [];
      json['assetData'].forEach((v) {
        _assetData?.add(AssetData.fromJson(v));
      });
    }
    _assetCount = json['assetCount'];
  }

  List<QuizNewData>? _data;
  int? _total;
  List<AssetData>? _assetData;
  int? _assetCount;
  List<ContactAssetData>? _contactAssetData;

  List<QuizNewData>? get data => _data;

  int? get total => _total;

  List<AssetData>? get assetData => _assetData;

  int? get assetCount => _assetCount;

  List<ContactAssetData>? get contactAssetData => _contactAssetData;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_data != null) {
      map['data'] = _data?.map((v) => v.toJson()).toList();
    }
    map['total'] = _total;
    if (_assetData != null) {
      map['assetData'] = _assetData?.map((v) => v.toJson()).toList();
    }
    map['assetCount'] = _assetCount;
    return map;
  }
}

/// id : "61961ff4843ebf00137b81c0"
/// orgId : "m300"
/// chapterId : "zjd58eyAvO"
/// sectionId : "8EjAP5jd9q"
/// fileType : "text"
/// fileName : "text"
/// folderName : "courses/Grade1-2EAVqrm0zK"
/// fileSize : "0.00"
/// sequence : 0
/// isVideo : false
/// ownerId : "2EAVqrm0zK"
/// ownerName : "Grade 1"
/// updatedAt : "2021-11-18T09:42:12.727Z"
/// status : "COMPLETED"
/// jobId : ""
/// notes : "<p>new act</p>"

/// id : "61c4083e55aa9900149f3b96"
/// orgId : "regtechdemo"
/// chapterId : "9kd9YoWdny"
/// sectionId : ""
/// fileType : "file"
/// fileName : "new.pdf"
/// folderName : "contacts/Test-Test-YM0aWaEdbJ"
/// fileSize : "70.263 kB"
/// sequence : 0
/// isVideo : false
/// ownerId : "YM0aWaEdbJ"
/// ownerName : "Test Test"
/// updatedAt : "2021-12-23T05:25:18.150Z"
/// status : "COMPLETED"
/// jobId : ""
/// notes : ""
/// s3url : "https://wjfiles.s3.us-west-1.wasabisys.com/us/staging/post/regtechdemo/contacts/Test-Test-YM0aWaEdbJ/new.pdf?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=4PSIZZTBLWA6EPOUK147%2F20211223%2Fus-west-1%2Fs3%2Faws4_request&X-Amz-Date=20211223T052705Z&X-Amz-Expires=1200&X-Amz-Signature=fd8d9bec179d1c0eeec0eb4912dc3292fcd0f60eb8cefdd8f9b50991736bab11&X-Amz-SignedHeaders=host"
/// downloadUrl : "https://wjfiles.s3.us-west-1.wasabisys.com/us/staging/post/regtechdemo/contacts/Test-Test-YM0aWaEdbJ/new.pdf?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=4PSIZZTBLWA6EPOUK147%2F20211223%2Fus-west-1%2Fs3%2Faws4_request&X-Amz-Date=20211223T052705Z&X-Amz-Expires=1200&X-Amz-Signature=fd8d9bec179d1c0eeec0eb4912dc3292fcd0f60eb8cefdd8f9b50991736bab11&X-Amz-SignedHeaders=host"
/// videoUrl : ""

class ContactAssetData {
  ContactAssetData({
    String? id,
    String? orgId,
    String? chapterId,
    String? sectionId,
    String? fileType,
    String? fileName,
    String? folderName,
    String? fileSize,
    int? sequence,
    bool? isVideo,
    String? ownerId,
    String? ownerName,
    String? updatedAt,
    String? status,
    String? jobId,
    String? notes,
    String? s3url,
    String? downloadUrl,
    String? videoUrl,
  }) {
    _id = id;
    _orgId = orgId;
    _chapterId = chapterId;
    _sectionId = sectionId;
    _fileType = fileType;
    _fileName = fileName;
    _folderName = folderName;
    _fileSize = fileSize;
    _sequence = sequence;
    _isVideo = isVideo;
    _ownerId = ownerId;
    _ownerName = ownerName;
    _updatedAt = updatedAt;
    _status = status;
    _jobId = jobId;
    _notes = notes;
    _s3url = s3url;
    _downloadUrl = downloadUrl;
    _videoUrl = videoUrl;
  }

  ContactAssetData.fromJson(dynamic json) {
    _id = json['id'];
    _orgId = json['orgId'];
    _chapterId = json['chapterId'];
    _sectionId = json['sectionId'];
    _fileType = json['fileType'];
    _fileName = json['fileName'];
    _folderName = json['folderName'];
    _fileSize = json['fileSize'];
    _sequence = json['sequence'];
    _isVideo = json['isVideo'];
    _ownerId = json['ownerId'];
    _ownerName = json['ownerName'];
    _updatedAt = json['updatedAt'];
    _status = json['status'];
    _jobId = json['jobId'];
    _notes = json['notes'];
    _s3url = json['s3url'];
    _downloadUrl = json['downloadUrl'];
    _videoUrl = json['videoUrl'];
  }

  String? _id;
  String? _orgId;
  String? _chapterId;
  String? _sectionId;
  String? _fileType;
  String? _fileName;
  String? _folderName;
  String? _fileSize;
  int? _sequence;
  bool? _isVideo;
  String? _ownerId;
  String? _ownerName;
  String? _updatedAt;
  String? _status;
  String? _jobId;
  String? _notes;
  String? _s3url;
  String? _downloadUrl;
  String? _videoUrl;

  String? get id => _id;

  String? get orgId => _orgId;

  String? get chapterId => _chapterId;

  String? get sectionId => _sectionId;

  String? get fileType => _fileType;

  String? get fileName => _fileName;

  String? get folderName => _folderName;

  String? get fileSize => _fileSize;

  int? get sequence => _sequence;

  bool? get isVideo => _isVideo;

  String? get ownerId => _ownerId;

  String? get ownerName => _ownerName;

  String? get updatedAt => _updatedAt;

  String? get status => _status;

  String? get jobId => _jobId;

  String? get notes => _notes;

  String? get s3url => _s3url;

  String? get downloadUrl => _downloadUrl;

  String? get videoUrl => _videoUrl;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['orgId'] = _orgId;
    map['chapterId'] = _chapterId;
    map['sectionId'] = _sectionId;
    map['fileType'] = _fileType;
    map['fileName'] = _fileName;
    map['folderName'] = _folderName;
    map['fileSize'] = _fileSize;
    map['sequence'] = _sequence;
    map['isVideo'] = _isVideo;
    map['ownerId'] = _ownerId;
    map['ownerName'] = _ownerName;
    map['updatedAt'] = _updatedAt;
    map['status'] = _status;
    map['jobId'] = _jobId;
    map['notes'] = _notes;
    map['s3url'] = _s3url;
    map['downloadUrl'] = _downloadUrl;
    map['videoUrl'] = _videoUrl;
    return map;
  }
}

class AssetData {
  String? id;
  String? orgId;
  String? chapterId;
  String? sectionId;
  String? fileType;
  String? fileName;
  String? folderName;
  String? fileSize;
  int? sequence;
  bool? isVideo;
  String? ownerId;
  String? ownerName;
  String? updatedAt;
  String? status;
  String? jobId;
  String? s3url;
  String? notes;
  bool? completed;
  var isDownloading = false.obs;
  var isDeleting = false.obs;
  RxString? title = "".obs;

  AssetData(
      {this.id,
      this.orgId,
      this.chapterId,
      this.sectionId,
      this.fileType,
      this.fileName,
      this.folderName,
      this.fileSize,
      this.sequence,
      this.isVideo,
      this.ownerId,
      this.ownerName,
      this.updatedAt,
      this.status,
      this.jobId,
      this.notes,
      this.s3url});

  AssetData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    orgId = json['orgId'];
    chapterId = json['chapterId'];
    sectionId = json['sectionId'];
    fileType = json['fileType'];
    fileName = json['fileName'];
    folderName = json['folderName'];
    fileSize = json['fileSize'];
    sequence = json['sequence'];
    isVideo = json['isVideo'];
    ownerId = json['ownerId'];
    ownerName = json['ownerName'];
    updatedAt = json['updatedAt'];
    status = json['status'];
    jobId = json['jobId'];
    notes = json['notes'];
    s3url = json['s3url'];
    isDownloading.value = false;
    isDeleting.value = false;
    title?.value = "";
  }

  void setFileName(String fileName) {
    fileName = fileName;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['orgId'] = orgId;
    data['chapterId'] = chapterId;
    data['sectionId'] = sectionId;
    data['fileType'] = fileType;
    data['fileName'] = fileName;
    data['folderName'] = folderName;
    data['fileSize'] = fileSize;
    data['sequence'] = sequence;
    data['isVideo'] = isVideo;
    data['ownerId'] = ownerId;
    data['ownerName'] = ownerName;
    data['updatedAt'] = updatedAt;
    data['status'] = status;
    data['jobId'] = jobId;
    data['s3url'] = s3url;
    data['notes'] = notes;
    return data;
  }
}

// class AssetData {
//   AssetData(
//       {String? sectionId,
//       bool? completed,
//       bool? isDownloading,
//       bool? isDeleting}) {
//     _id = id;
//     _orgId = orgId;
//     _chapterId = chapterId;
//     _sectionId = sectionId;
//     _fileType = fileType;
//     _fileName = fileName;
//     _folderName = folderName;
//     _fileSize = fileSize;
//     _sequence = sequence;
//     _isVideo = isVideo;
//     _ownerId = ownerId;
//     _ownerName = ownerName;
//     _updatedAt = updatedAt;
//     _status = status;
//     _jobId = jobId;
//     _notes = notes;
//     _s3url = s3url;
//     _isDownloading = isDownloading;
//     _isDeleting = isDeleting;
//   }
//
//   AssetData.fromJson(dynamic json) {
//     _id = json['id'];
//     _orgId = json['orgId'];
//     _chapterId = json['chapterId'];
//     _sectionId = json['sectionId'];
//     _fileType = json['fileType'];
//     _fileName = json['fileName'];
//     _folderName = json['folderName'];
//     _fileSize = json['fileSize'];
//     _sequence = json['sequence'];
//     _isVideo = json['isVideo'];
//     _ownerId = json['ownerId'];
//     _ownerName = json['ownerName'];
//     _updatedAt = json['updatedAt'];
//     _status = json['status'];
//     _jobId = json['jobId'];
//     _notes = json['notes'];
//     _s3url = json['s3url'];
//     _isDownloading = false;
//     _isDeleting = false;
//   }
//
//   String? _id;
//   String? _orgId;
//   String? _chapterId;
//   String? _sectionId;
//   String? _fileType;
//   String? _fileName;
//   String? _folderName;
//   String? _fileSize;
//   int? _sequence;
//   bool? _isVideo;
//   String? _ownerId;
//   String? _ownerName;
//   String? _updatedAt;
//   String? _status;
//   String? _jobId;
//   String? _notes;
//   bool? _isDeleting;
//   bool? _isDownloading;
//   String? _s3url;
//
//   String? get id => _id;
//
//   String? get orgId => _orgId;
//
//   String? get chapterId => _chapterId;
//
//   String? get sectionId => _sectionId;
//
//   String? get fileType => _fileType;
//
//   String? get fileName => _fileName;
//
//   String? get folderName => _folderName;
//
//   String? get fileSize => _fileSize;
//
//   int? get sequence => _sequence;
//
//   bool? get isVideo => _isVideo;
//
//   String? get ownerId => _ownerId;
//
//   String? get ownerName => _ownerName;
//
//   String? get updatedAt => _updatedAt;
//
//   String? get status => _status;
//
//   String? get jobId => _jobId;
//
//   String? get notes => _notes;
//
//   String? get s3url => _s3url;
//
//   bool? get isDeleting => _isDeleting;
//
//   bool? get isDownloading => _isDownloading;
//
//   void setFileName(String fileName) {
//     _fileName = fileName;
//   }
//
//   Map<String, dynamic> toJson() {
//     final map = <String, dynamic>{};
//     map['id'] = _id;
//     map['orgId'] = _orgId;
//     map['chapterId'] = _chapterId;
//     map['sectionId'] = _sectionId;
//     map['fileType'] = _fileType;
//     map['fileName'] = _fileName;
//     map['folderName'] = _folderName;
//     map['fileSize'] = _fileSize;
//     map['sequence'] = _sequence;
//     map['isVideo'] = _isVideo;
//     map['ownerId'] = _ownerId;
//     map['ownerName'] = _ownerName;
//     map['updatedAt'] = _updatedAt;
//     map['status'] = _status;
//     map['jobId'] = _jobId;
//     map['notes'] = _notes;
//     return map;
//   }
// }

/// _id : "619b5ffff117be0013209329"
/// name : "vinodv1"
/// additionalInfo : "vinodv1"
/// activityQuestions : []
/// isTrackActivity : true
/// isActive : false
/// isRepeatActivity : false
/// isAllowUserToUploadFiles : false
/// course : "2EAVqrm0zK"
/// chapter : "eZd891mGPx"
/// section : "8EjAP5jd9q"
/// orgId : "m300"
/// paragraph : "<p>vinodv1</p>"
/// createdAt : "2021-11-22T09:16:47.772Z"
/// updatedAt : "2021-11-22T09:16:47.772Z"
/// __v : 0
/// hasCompleted : false

class QuizNewData {
  QuizNewData({
    List<QbChapterDistribution>? qbChapterDistribution,
    String? id,
    String? difficultyLevel,
    bool? displayResults,
    String? imageUrl,
    bool? isGenerateCertificate,
    String? name,
    String? additionalInfo,
    int? passingPercent,
    String? questionBank,
    int? noOfQuestions,
    bool? showAnswersInResult,
    bool? showAllChoiceInResult,
    int? enforceTime,
    int? timeAlloted,
    String? paragraph,
    bool? isShowExplanation,
    bool? isMandatory,
    bool? isRepeatTest,
    bool? isAllowUserToUploadFiles,
    bool? isSkipContent,
    bool? isNoQuestions,
    bool? isShowAnswerAfterEachQuestion,
    String? negativeMarkingPattern,
    String? scoreType,
    String? instructions,
    String? course,
    String? chapter,
    String? section,
    String? orgId,
    String? createdAt,
    String? updatedAt,
    int? v,
    bool? hasCompleted,
    List<ActivityQuestions>? activityQuestions,
    bool? completed,
  }) {
    _activityQuestions = activityQuestions;
  }

  QuizNewData.fromJson(dynamic json) {
    if (json['qbChapterDistribution'] != null) {
      _qbChapterDistribution = [];
      json['qbChapterDistribution'].forEach((v) {
        _qbChapterDistribution?.add(QbChapterDistribution.fromJson(v));
      });
    }
    _id = json['_id'];
    _difficultyLevel = json['difficultyLevel'];
    _displayResults = json['displayResults'];
    _imageUrl = json['imageUrl'];
    _isGenerateCertificate = json['isGenerateCertificate'];
    _name = json['name'];
    _additionalInfo = json['additionalInfo'];
    _passingPercent = json['passingPercent'];
    _questionBank = json['questionBank'];
    _noOfQuestions = json['noOfQuestions'];
    _showAnswersInResult = json['showAnswersInResult'];
    _showAllChoiceInResult = json['showAllChoiceInResult'];
    _enforceTime = json['enforceTime'];
    _timeAlloted = json['timeAlloted'];
    _paragraph = json['paragraph'];
    _isShowExplanation = json['isShowExplanation'];
    _isMandatory = json['isMandatory'];
    _isRepeatTest = json['isRepeatTest'];
    _isAllowUserToUploadFiles = json['isAllowUserToUploadFiles'];
    _isSkipContent = json['isSkipContent'];
    _isNoQuestions = json['isNoQuestions'];
    _isShowAnswerAfterEachQuestion = json['isShowAnswerAfterEachQuestion'];
    _negativeMarkingPattern = json['negativeMarkingPattern'];
    _scoreType = json['scoreType'];
    _instructions = json['instructions'];
    _course = json['course'];
    _chapter = json['chapter'];
    _section = json['section'];
    _orgId = json['orgId'];
    _createdAt = json['createdAt'];
    _updatedAt = json['updatedAt'];
    _v = json['__v'];
    _hasCompleted = json['hasCompleted'];
    if (json['activityQuestions'] != null) {
      _activityQuestions = [];
      json['activityQuestions'].forEach((v) {
        _activityQuestions?.add(ActivityQuestions.fromJson(v));
      });
    }
    _isRepeatActivity = json['isRepeatActivity'];
  }

  List<QbChapterDistribution>? _qbChapterDistribution;
  String? _id;
  String? _difficultyLevel;
  bool? _displayResults;
  String? _imageUrl;
  bool? _isGenerateCertificate;
  String? _name;
  String? _additionalInfo;
  int? _passingPercent;
  String? _questionBank;
  int? _noOfQuestions;
  bool? _showAnswersInResult;
  bool? _showAllChoiceInResult;
  int? _enforceTime;
  int? _timeAlloted;
  String? _paragraph;
  bool? _isShowExplanation;
  bool? _isMandatory;
  bool? _isRepeatTest;
  bool? _isAllowUserToUploadFiles;
  bool? _isSkipContent;
  bool? _isNoQuestions;
  bool? _isShowAnswerAfterEachQuestion;
  String? _negativeMarkingPattern;
  String? _scoreType;
  String? _instructions;
  String? _course;
  String? _chapter;
  String? _section;
  String? _orgId;
  String? _createdAt;
  String? _updatedAt;
  int? _v;
  bool? _hasCompleted;
  List<ActivityQuestions>? _activityQuestions;
  bool? _isRepeatActivity;

  List<QbChapterDistribution>? get qbChapterDistribution =>
      _qbChapterDistribution;

  String? get id => _id;

  String? get difficultyLevel => _difficultyLevel;

  bool? get displayResults => _displayResults;

  String? get imageUrl => _imageUrl;

  bool? get isGenerateCertificate => _isGenerateCertificate;

  String? get name => _name;

  String? get additionalInfo => _additionalInfo;

  int? get passingPercent => _passingPercent;

  String? get questionBank => _questionBank;

  int? get noOfQuestions => _noOfQuestions;

  bool? get showAnswersInResult => _showAnswersInResult;

  bool? get showAllChoiceInResult => _showAllChoiceInResult;

  int? get enforceTime => _enforceTime;

  int? get timeAlloted => _timeAlloted;

  String? get paragraph => _paragraph;

  bool? get isShowExplanation => _isShowExplanation;

  bool? get isMandatory => _isMandatory;

  bool? get isRepeatTest => _isRepeatTest;

  bool? get isAllowUserToUploadFiles => _isAllowUserToUploadFiles;

  bool? get isSkipContent => _isSkipContent;

  bool? get isNoQuestions => _isNoQuestions;

  bool? get isShowAnswerAfterEachQuestion => _isShowAnswerAfterEachQuestion;

  String? get negativeMarkingPattern => _negativeMarkingPattern;

  String? get scoreType => _scoreType;

  String? get instructions => _instructions;

  String? get course => _course;

  String? get chapter => _chapter;

  String? get section => _section;

  String? get orgId => _orgId;

  String? get createdAt => _createdAt;

  String? get updatedAt => _updatedAt;

  int? get v => _v;

  bool? get hasCompleted => _hasCompleted;

  List<ActivityQuestions>? get activityQuestions => _activityQuestions;

  bool? get isRepeatActivity => _isRepeatActivity;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_qbChapterDistribution != null) {
      map['qbChapterDistribution'] =
          _qbChapterDistribution?.map((v) => v.toJson()).toList();
    }
    map['_id'] = _id;
    map['difficultyLevel'] = _difficultyLevel;
    map['displayResults'] = _displayResults;
    map['imageUrl'] = _imageUrl;
    map['isGenerateCertificate'] = _isGenerateCertificate;
    map['name'] = _name;
    map['additionalInfo'] = _additionalInfo;
    map['passingPercent'] = _passingPercent;
    map['questionBank'] = _questionBank;
    map['noOfQuestions'] = _noOfQuestions;
    map['showAnswersInResult'] = _showAnswersInResult;
    map['showAllChoiceInResult'] = _showAllChoiceInResult;
    map['enforceTime'] = _enforceTime;
    map['timeAlloted'] = _timeAlloted;
    map['paragraph'] = _paragraph;
    map['isShowExplanation'] = _isShowExplanation;
    map['isMandatory'] = _isMandatory;
    map['isRepeatTest'] = _isRepeatTest;
    map['isAllowUserToUploadFiles'] = _isAllowUserToUploadFiles;
    map['isSkipContent'] = _isSkipContent;
    map['isNoQuestions'] = _isNoQuestions;
    map['isShowAnswerAfterEachQuestion'] = _isShowAnswerAfterEachQuestion;
    map['negativeMarkingPattern'] = _negativeMarkingPattern;
    map['scoreType'] = _scoreType;
    map['instructions'] = _instructions;
    map['course'] = _course;
    map['chapter'] = _chapter;
    map['section'] = _section;
    map['orgId'] = _orgId;
    map['createdAt'] = _createdAt;
    map['updatedAt'] = _updatedAt;
    map['__v'] = _v;
    map['hasCompleted'] = _hasCompleted;
    if (_activityQuestions != null) {
      map['activityQuestions'] =
          _activityQuestions?.map((v) => v.toJson()).toList();
    }
    map['isRepeatActivity'] = _isRepeatActivity;

    return map;
  }
}

/// index : 0
/// qbChapterId : "613f9504a6202900121a4c1d"
/// percentage : 1

class QbChapterDistribution {
  QbChapterDistribution({
    int? index,
    String? qbChapterId,
    int? percentage,
  }) {
    _index = index;
    _qbChapterId = qbChapterId;
    _percentage = percentage;
  }

  QbChapterDistribution.fromJson(dynamic json) {
    _index = json['index'];
    _qbChapterId = json['qbChapterId'];
    _percentage = json['percentage'];
  }

  int? _index;
  String? _qbChapterId;
  int? _percentage;

  int? get index => _index;

  String? get qbChapterId => _qbChapterId;

  int? get percentage => _percentage;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['index'] = _index;
    map['qbChapterId'] = _qbChapterId;
    map['percentage'] = _percentage;
    return map;
  }
}
