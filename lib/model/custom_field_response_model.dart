// import 'package:flutter/material.dart';
// import 'package:get/get.dart';
//
// /// guId : "ylPAZ8ZGMJ"
// /// isDisabled : false
// /// isListOnWebsite : false
// /// isMandatory : false
// /// name : "Current Job Title"
// /// option1 : null
// /// option2 : null
// /// option3 : null
// /// option4 : null
// /// placeholder : "Current Job Title"
// /// sequence : 0
// /// type : "text"
// ///
//
// import 'package:flutter/material.dart';
// import 'package:get/get.dart';
//
// /// guId : "ylPAZ8ZGMJ"
// /// isDisabled : false
// /// isListOnWebsite : false
// /// isMandatory : false
// /// name : "Current Job Title"
// /// option1 : null
// /// option2 : null
// /// option3 : null
// /// option4 : null
// /// placeholder : "Current Job Title"
// /// sequence : 0
// /// type : "text"
// ///
//
// // class CustomFieldResponseModel {
// //   CustomFieldResponseModel(
// //       {String? guId,
// //         bool? isDisabled,
// //         bool? isListOnWebsite,
// //         bool? isMandatory,
// //         String? name,
// //         dynamic option1,
// //         dynamic option2,
// //         dynamic option3,
// //         dynamic option4,
// //         String? placeholder,
// //         int? sequence,
// //         String? type,
// //         RxString? value}) {
// //     _guId = guId;
// //     _isDisabled = isDisabled;
// //     _isListOnWebsite = isListOnWebsite;
// //     _isMandatory = isMandatory;
// //     _name = name;
// //     _option1 = option1;
// //     _option2 = option2;
// //     _option3 = option3;
// //     _option4 = option4;
// //     _placeholder = placeholder;
// //     _sequence = sequence;
// //     _type = type;
// //     _value = value;
// //   }
// //
// //   CustomFieldResponseModel.fromJson(dynamic json) {
// //     _guId = json['guId'];
// //     _isDisabled = json['isDisabled'];
// //     _isListOnWebsite = json['isListOnWebsite'];
// //     _isMandatory = json['isMandatory'];
// //     _name = json['name'];
// //     _option1 = json['option1'];
// //     _option2 = json['option2'];
// //     _option3 = json['option3'];
// //     _option4 = json['option4'];
// //     _placeholder = json['placeholder'];
// //     _sequence = json['sequence'];
// //     _type = json['type'];
// //     _value?.value = json['value'];
// //   }
// //
// //   String? _guId;
// //   bool? _isDisabled;
// //   bool? _isListOnWebsite;
// //   bool? _isMandatory;
// //   String? _name;
// //   dynamic _option1;
// //   dynamic _option2;
// //   dynamic _option3;
// //   dynamic _option4;
// //   String? _placeholder;
// //   int? _sequence;
// //   String? _type;
// //   RxString? _value = "".obs;
// //   final TextEditingController controller = TextEditingController();
// //
// //   String? get guId => _guId;
// //
// //   bool? get isDisabled => _isDisabled;
// //
// //   bool? get isListOnWebsite => _isListOnWebsite;
// //
// //   bool? get isMandatory => _isMandatory;
// //
// //   String? get name => _name;
// //
// //   dynamic get option1 => _option1;
// //
// //   dynamic get option2 => _option2;
// //
// //   dynamic get option3 => _option3;
// //
// //   dynamic get option4 => _option4;
// //
// //   String? get placeholder => _placeholder;
// //
// //   int? get sequence => _sequence;
// //
// //   String? get type => _type;
// //
// //   RxString? get value => _value;
// //
// //   Map<String, dynamic> toJson() {
// //     final map = <String, dynamic>{};
// //     map['guId'] = _guId;
// //     map['isDisabled'] = _isDisabled;
// //     map['isListOnWebsite'] = _isListOnWebsite;
// //     map['isMandatory'] = _isMandatory;
// //     map['name'] = _name;
// //     map['option1'] = _option1;
// //     map['option2'] = _option2;
// //     map['option3'] = _option3;
// //     map['option4'] = _option4;
// //     map['placeholder'] = _placeholder;
// //     map['sequence'] = _sequence;
// //     map['type'] = _type;
// //     map['value'] = value?.value;
// //     return map;
// //   }
// // }
// import 'package:flutter/material.dart';
// import 'package:get/get.dart';
//
// /// guId : "ylPAZ8ZGMJ"
// /// isDisabled : false
// /// isListOnWebsite : false
// /// isMandatory : false
// /// name : "Current Job Title"
// /// option1 : null
// /// option2 : null
// /// option3 : null
// /// option4 : null
// /// placeholder : "Current Job Title"
// /// sequence : 0
// /// type : "text"
// ///
//
// class CustomFieldResponse {
//   CustomFieldResponse(
//       {String? guId,
//       bool? isDisabled,
//       bool? isListOnWebsite,
//       bool? isMandatory,
//       String? name,
//       dynamic option1,
//       dynamic option2,
//       dynamic option3,
//       dynamic option4,
//       String? placeholder,
//       int? sequence,
//       String? type,
//      }) {
//     _guId = guId;
//     _isDisabled = isDisabled;
//     _isListOnWebsite = isListOnWebsite;
//     _isMandatory = isMandatory;
//     _name = name;
//     _option1 = option1;
//     _option2 = option2;
//     _option3 = option3;
//     _option4 = option4;
//     _placeholder = placeholder;
//     _sequence = sequence;
//     _type = type;
//   }
//
//   CustomFieldResponse.fromJson(dynamic json) {
//     _guId = json['guId'];
//     _isDisabled = json['isDisabled'];
//     _isListOnWebsite = json['isListOnWebsite'];
//     _isMandatory = json['isMandatory'];
//     _name = json['name'];
//     _option1 = json['option1'];
//     _option2 = json['option2'];
//     _option3 = json['option3'];
//     _option4 = json['option4'];
//     _placeholder = json['placeholder'];
//     _sequence = json['sequence'];
//     _type = json['type'];
//   }
//
//   String? _guId;
//   bool? _isDisabled;
//   bool? _isListOnWebsite;
//   bool? _isMandatory;
//   String? _name;
//   dynamic _option1;
//   dynamic _option2;
//   dynamic _option3;
//   dynamic _option4;
//   String? _placeholder;
//   int? _sequence;
//   String? _type;
//
//   final TextEditingController controller = TextEditingController();
//
//   String? get guId => _guId;
//
//   bool? get isDisabled => _isDisabled;
//
//   bool? get isListOnWebsite => _isListOnWebsite;
//
//   bool? get isMandatory => _isMandatory;
//
//   String? get name => _name;
//
//   dynamic get option1 => _option1;
//
//   dynamic get option2 => _option2;
//
//   dynamic get option3 => _option3;
//
//   dynamic get option4 => _option4;
//
//   String? get placeholder => _placeholder;
//
//   int? get sequence => _sequence;
//
//   String? get type => _type;
//
//   Map<String, dynamic> toJson() {
//     final map = <String, dynamic>{};
//     map['guId'] = _guId;
//     map['isDisabled'] = _isDisabled;
//     map['isListOnWebsite'] = _isListOnWebsite;
//     map['isMandatory'] = _isMandatory;
//     map['name'] = _name;
//     map['option1'] = _option1;
//     map['option2'] = _option2;
//     map['option3'] = _option3;
//     map['option4'] = _option4;
//     map['placeholder'] = _placeholder;
//     map['sequence'] = _sequence;
//     map['type'] = _type;
//     return map;
//   }
// }
