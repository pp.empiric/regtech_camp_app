class CommentsResponse {
  String? dateCreated;
  String? guId;
  bool? isActive;
  bool? isNote;
  bool? isPublished;
  String? lastUpdated;
  List<Notes>? notes;
  String? section;
  String? title;

  CommentsResponse({
    this.dateCreated,
    this.guId,
    this.isActive,
    this.isNote,
    this.isPublished,
    this.lastUpdated,
    this.notes,
    this.section,
    this.title,
  });

  CommentsResponse.fromJson(Map<String, dynamic> json) {
    dateCreated = json['dateCreated'];
    guId = json['guId'];
    isActive = json['isActive'];
    isNote = json['isNote'];
    isPublished = json['isPublished'];
    lastUpdated = json['lastUpdated'];
    if (json['notes'] != null) {
      notes = <Notes>[];
      json['notes'].forEach((v) {
        notes?.add(Notes.fromJson(v));
      });
    }
    section = json['section'];
    title = json['title'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['dateCreated'] = dateCreated;
    data['guId'] = guId;
    data['isActive'] = isActive;
    data['isNote'] = isNote;
    data['isPublished'] = isPublished;
    data['lastUpdated'] = lastUpdated;
    if (notes != null) {
      data['notes'] = notes?.map((v) => v.toJson()).toList();
    }
    data['section'] = section;
    data['title'] = title;
    return data;
  }
}

class Notes {
  Contact? contact;
  String? contactId;
  String? dateCreated;
  String? dateCreatedStr;
  String? guId;
  String? note;
  bool? isDeleting;

  Notes(
      {this.contact,
      this.contactId,
      this.dateCreated,
      this.dateCreatedStr,
      this.guId,
      this.note,
      this.isDeleting});

  Notes.fromJson(Map<String, dynamic> json) {
    contact =
        json['contact'] != null ? Contact.fromJson(json['contact']) : null;
    contactId = json['contactId'];
    dateCreated = json['dateCreated'];
    dateCreatedStr = json['dateCreatedStr'];
    guId = json['guId'];
    note = json['note'];
    isDeleting = false;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (contact != null) {
      data['contact'] = contact?.toJson();
    }
    data['contactId'] = contactId;
    data['dateCreated'] = dateCreated;
    data['dateCreatedStr'] = dateCreatedStr;
    data['guId'] = guId;
    data['note'] = note;
    return data;
  }
}

class Contact {
  dynamic balance;
  String? children;
  String? description;
  String? email;
  String? fullName;
  bool? hasAcceptedTerms;
  bool? hasParent;
  String? id;
  String? imageUrl;
  bool? isAdminVerified;
  bool? isEmailVerified;
  bool? isFirstLogin;
  bool? isLocalPicture;
  bool? isParent;
  String? lastName;
  String? name;
  String? phone;
  String? picture;

  Contact(
      {this.balance,
      this.children,
      this.description,
      this.email,
      this.fullName,
      this.hasAcceptedTerms,
      this.hasParent,
      this.id,
      this.imageUrl,
      this.isAdminVerified,
      this.isEmailVerified,
      this.isFirstLogin,
      this.isLocalPicture,
      this.isParent,
      this.lastName,
      this.name,
      this.phone,
      this.picture});

  Contact.fromJson(Map<String, dynamic> json) {
    balance = json['balance'];
    children = json['children'];
    description = json['description'];
    email = json['email'];
    fullName = json['fullName'];
    hasAcceptedTerms = json['hasAcceptedTerms'];
    hasParent = json['hasParent'];
    id = json['id'];
    imageUrl = json['imageUrl'];
    isAdminVerified = json['isAdminVerified'];
    isEmailVerified = json['isEmailVerified'];
    isFirstLogin = json['isFirstLogin'];
    isLocalPicture = json['isLocalPicture'];
    isParent = json['isParent'];
    lastName = json['lastName'];
    name = json['name'];
    phone = json['phone'];
    picture = json['picture'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['balance'] = balance;
    data['children'] = children;
    data['description'] = description;
    data['email'] = email;
    data['fullName'] = fullName;
    data['hasAcceptedTerms'] = hasAcceptedTerms;
    data['hasParent'] = hasParent;
    data['id'] = id;
    data['imageUrl'] = imageUrl;
    data['isAdminVerified'] = isAdminVerified;
    data['isEmailVerified'] = isEmailVerified;
    data['isFirstLogin'] = isFirstLogin;
    data['isLocalPicture'] = isLocalPicture;
    data['isParent'] = isParent;
    data['lastName'] = lastName;
    data['name'] = name;
    data['phone'] = phone;
    data['picture'] = picture;
    return data;
  }
}
