
import 'quiz_result_request_body.dart';

/// data : {"_id":"61b1d5bc38062d00136d270e","name":"Quiz One 9 Dec","startTime":1639026445239,"course":"zjd58YWAvO","chapter":"2EAVqO80zK","section":"nD7dzxZ0bJ","contact":"YEAoo4BAL5","quiz":"61b189cf38062d00136d1e81","orgId":"rayatdemo","timeAlloted":30,"questions":[{"_id":"61b1d5bc38062d00136d28c2","name":"Which of the following is not Constraint in SQL?","type":"image","questionType":"SingleChoice","imageUrl":null,"orgId":"rayatdemo","questionBank":"61b188e038062d00136d1c77","qBankChapter":"61b188ee38062d00136d1c78","qBankChapterName":"Chapter 1","explanation":"Constraint specifies the rule to allow or restrict what data will be stored in a table. The PRIMARY KEY, NOT NULL, and CHECK are the constraints that specify rules for data insertion.\n\nUNION is an operator that combines two or more results from multiple SELECT queries into a single result set.","parentExplaination":"Constraint specifies the rule to allow or restrict what data will be stored in a table. The PRIMARY KEY, NOT NULL, and CHECK are the constraints that specify rules for data insertion.\n\nUNION is an operator that combines two or more results from multiple SELECT queries into a single result set.","assetId":null,"folderName":null,"paperQuestionChoices":[{"_id":"61b1d5bc38062d00136d28c3","choice":"Primary Key","imageUrl":"","choiceAnswer":false,"isAnswer":false},{"_id":"61b1d5bc38062d00136d28c4","choice":"Not Null","imageUrl":"","choiceAnswer":false,"isAnswer":false},{"_id":"61b1d5bc38062d00136d28c5","choice":"Check","imageUrl":"","choiceAnswer":false,"isAnswer":false},{"_id":"61b1d5bc38062d00136d28c6","choice":"Union","imageUrl":"","choiceAnswer":false,"isAnswer":true}],"createdAt":"2021-12-09T10:09:00.524Z","updatedAt":"2021-12-09T10:09:00.524Z"},{"_id":"61b1d5bc38062d00136d28d6","name":"Which operator is used to compare a value to a specified list of values?","type":"image","questionType":"SingleChoice","imageUrl":null,"orgId":"rayatdemo","questionBank":"61b188e038062d00136d1c77","qBankChapter":"61b188ee38062d00136d1c78","qBankChapterName":"Chapter 1","explanation":"The IN operator easily tests the expression if it matches any value in a specified list of values. It reduces the use of multiple OR conditions.\n\nThe WHERE or HAVING clause uses the ANY and ALL operators. ANY gives the result when any subquery value matches the specified condition. The ALL give the result when all subquery values match the specified condition.\n\nThe BETWEEN operator selects values only in the given range.","parentExplaination":"The IN operator easily tests the expression if it matches any value in a specified list of values. It reduces the use of multiple OR conditions.\n\nThe WHERE or HAVING clause uses the ANY and ALL operators. ANY gives the result when any subquery value matches the specified condition. The ALL give the result when all subquery values match the specified condition.\n\nThe BETWEEN operator selects values only in the given range.","assetId":null,"folderName":null,"paperQuestionChoices":[{"_id":"61b1d5bc38062d00136d28d7","choice":"ANY","imageUrl":"","choiceAnswer":false,"isAnswer":false},{"_id":"61b1d5bc38062d00136d28d8","choice":"BETWEEN","imageUrl":"","choiceAnswer":false,"isAnswer":false},{"_id":"61b1d5bc38062d00136d28d9","choice":"ALL","imageUrl":"","choiceAnswer":false,"isAnswer":false},{"_id":"61b1d5bc38062d00136d28da","choice":"IN","imageUrl":"","choiceAnswer":false,"isAnswer":true}],"createdAt":"2021-12-09T10:09:00.525Z","updatedAt":"2021-12-09T10:09:00.525Z"},{"_id":"61b1d5bc38062d00136d28e5","name":"In which of the following cases a DML statement is not executed?","type":"image","questionType":"SingleChoice","imageUrl":null,"orgId":"rayatdemo","questionBank":"61b188e038062d00136d1c77","qBankChapter":"61b188ee38062d00136d1c78","qBankChapterName":"Chapter 1","explanation":"The DML statement is used to access and manipulate the data in an existing table. Therefore, it cannot be used in table deletion.","parentExplaination":"The DML statement is used to access and manipulate the data in an existing table. Therefore, it cannot be used in table deletion.","assetId":null,"folderName":null,"paperQuestionChoices":[{"_id":"61b1d5bc38062d00136d28e6","choice":"When existing rows are modified.","imageUrl":"","choiceAnswer":false,"isAnswer":false},{"_id":"61b1d5bc38062d00136d28e7","choice":"When a table is deleted.","imageUrl":"","choiceAnswer":false,"isAnswer":true},{"_id":"61b1d5bc38062d00136d28e8","choice":"When some rows are deleted.","imageUrl":"","choiceAnswer":false,"isAnswer":false},{"_id":"61b1d5bc38062d00136d28e9","choice":"All of the above","imageUrl":"","choiceAnswer":false,"isAnswer":false}],"createdAt":"2021-12-09T10:09:00.525Z","updatedAt":"2021-12-09T10:09:00.525Z"},{"_id":"61b1d5bc38062d00136d2854","name":"Which of the following is also called an INNER JOIN?","type":"image","questionType":"SingleChoice","imageUrl":null,"orgId":"rayatdemo","questionBank":"61b188e038062d00136d1c77","qBankChapter":"61b188ee38062d00136d1c78","qBankChapterName":"Chapter 1","explanation":"The INNER JOIN returns data from the two or more tables that match the specified condition and hides other records. EQUI JOIN is similar to INNER JOIN that returns records for equality or matching column(s) values of the relative tables.\n\nNON-EQUI JOIN is returned those records that are not matching in the relative tables.\n\nSELF JOIN returns records from the tables by joining itself.","parentExplaination":"The INNER JOIN returns data from the two or more tables that match the specified condition and hides other records. EQUI JOIN is similar to INNER JOIN that returns records for equality or matching column(s) values of the relative tables.\n\nNON-EQUI JOIN is returned those records that are not matching in the relative tables.\n\nSELF JOIN returns records from the tables by joining itself.","assetId":null,"folderName":null,"paperQuestionChoices":[{"_id":"61b1d5bc38062d00136d2855","choice":"SELF JOIN","imageUrl":"","choiceAnswer":false,"isAnswer":false},{"_id":"61b1d5bc38062d00136d2856","choice":"EQUI JOIN","imageUrl":"","choiceAnswer":false,"isAnswer":true},{"_id":"61b1d5bc38062d00136d2857","choice":"NON-EQUI JOIN","imageUrl":"","choiceAnswer":false,"isAnswer":false},{"_id":"61b1d5bc38062d00136d2858","choice":"None of the above","imageUrl":"","choiceAnswer":false,"isAnswer":false}],"createdAt":"2021-12-09T10:09:00.520Z","updatedAt":"2021-12-09T10:09:00.520Z"},{"_id":"61b1d5bc38062d00136d28e0","name":"SQL Views are also known as","type":"image","questionType":"SingleChoice","imageUrl":null,"orgId":"rayatdemo","questionBank":"61b188e038062d00136d1c77","qBankChapter":"61b188ee38062d00136d1c78","qBankChapterName":"Chapter 1","explanation":"A view is also known as a virtual table because it contains rows and columns similar to a real table. It shows the table interface but cannot be stored in a database.","parentExplaination":"A view is also known as a virtual table because it contains rows and columns similar to a real table. It shows the table interface but cannot be stored in a database.","assetId":null,"folderName":null,"paperQuestionChoices":[{"_id":"61b1d5bc38062d00136d28e1","choice":"Simple tables","imageUrl":"","choiceAnswer":false,"isAnswer":false},{"_id":"61b1d5bc38062d00136d28e2","choice":"Virtual tables","imageUrl":"","choiceAnswer":false,"isAnswer":true},{"_id":"61b1d5bc38062d00136d28e3","choice":"Complex tables","imageUrl":"","choiceAnswer":false,"isAnswer":false},{"_id":"61b1d5bc38062d00136d28e4","choice":"Actual Tables","imageUrl":"","choiceAnswer":false,"isAnswer":false}],"createdAt":"2021-12-09T10:09:00.525Z","updatedAt":"2021-12-09T10:09:00.525Z"}],"updatedAt":"2021-12-09T10:09:00.529Z","createdAt":"2021-12-09T10:09:00.529Z"}

class QuizQuestionResponse {
  QuizQuestionResponse({
    QuizQ? data,
  }) {
    _data = data;
  }

  QuizQuestionResponse.fromJson(dynamic json) {
    _data = json['data'] != null ? QuizQ.fromJson(json['data']) : null;
  }

  QuizQ? _data;

  QuizQ? get data => _data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_data != null) {
      map['data'] = _data?.toJson();
    }
    return map;
  }
}

/// _id : "61b1d5bc38062d00136d270e"
/// name : "Quiz One 9 Dec"
/// startTime : 1639026445239
/// course : "zjd58YWAvO"
/// chapter : "2EAVqO80zK"
/// section : "nD7dzxZ0bJ"
/// contact : "YEAoo4BAL5"
/// quiz : "61b189cf38062d00136d1e81"
/// orgId : "rayatdemo"
/// timeAlloted : 30
/// questions : [{"_id":"61b1d5bc38062d00136d28c2","name":"Which of the following is not Constraint in SQL?","type":"image","questionType":"SingleChoice","imageUrl":null,"orgId":"rayatdemo","questionBank":"61b188e038062d00136d1c77","qBankChapter":"61b188ee38062d00136d1c78","qBankChapterName":"Chapter 1","explanation":"Constraint specifies the rule to allow or restrict what data will be stored in a table. The PRIMARY KEY, NOT NULL, and CHECK are the constraints that specify rules for data insertion.\n\nUNION is an operator that combines two or more results from multiple SELECT queries into a single result set.","parentExplaination":"Constraint specifies the rule to allow or restrict what data will be stored in a table. The PRIMARY KEY, NOT NULL, and CHECK are the constraints that specify rules for data insertion.\n\nUNION is an operator that combines two or more results from multiple SELECT queries into a single result set.","assetId":null,"folderName":null,"paperQuestionChoices":[{"_id":"61b1d5bc38062d00136d28c3","choice":"Primary Key","imageUrl":"","choiceAnswer":false,"isAnswer":false},{"_id":"61b1d5bc38062d00136d28c4","choice":"Not Null","imageUrl":"","choiceAnswer":false,"isAnswer":false},{"_id":"61b1d5bc38062d00136d28c5","choice":"Check","imageUrl":"","choiceAnswer":false,"isAnswer":false},{"_id":"61b1d5bc38062d00136d28c6","choice":"Union","imageUrl":"","choiceAnswer":false,"isAnswer":true}],"createdAt":"2021-12-09T10:09:00.524Z","updatedAt":"2021-12-09T10:09:00.524Z"},{"_id":"61b1d5bc38062d00136d28d6","name":"Which operator is used to compare a value to a specified list of values?","type":"image","questionType":"SingleChoice","imageUrl":null,"orgId":"rayatdemo","questionBank":"61b188e038062d00136d1c77","qBankChapter":"61b188ee38062d00136d1c78","qBankChapterName":"Chapter 1","explanation":"The IN operator easily tests the expression if it matches any value in a specified list of values. It reduces the use of multiple OR conditions.\n\nThe WHERE or HAVING clause uses the ANY and ALL operators. ANY gives the result when any subquery value matches the specified condition. The ALL give the result when all subquery values match the specified condition.\n\nThe BETWEEN operator selects values only in the given range.","parentExplaination":"The IN operator easily tests the expression if it matches any value in a specified list of values. It reduces the use of multiple OR conditions.\n\nThe WHERE or HAVING clause uses the ANY and ALL operators. ANY gives the result when any subquery value matches the specified condition. The ALL give the result when all subquery values match the specified condition.\n\nThe BETWEEN operator selects values only in the given range.","assetId":null,"folderName":null,"paperQuestionChoices":[{"_id":"61b1d5bc38062d00136d28d7","choice":"ANY","imageUrl":"","choiceAnswer":false,"isAnswer":false},{"_id":"61b1d5bc38062d00136d28d8","choice":"BETWEEN","imageUrl":"","choiceAnswer":false,"isAnswer":false},{"_id":"61b1d5bc38062d00136d28d9","choice":"ALL","imageUrl":"","choiceAnswer":false,"isAnswer":false},{"_id":"61b1d5bc38062d00136d28da","choice":"IN","imageUrl":"","choiceAnswer":false,"isAnswer":true}],"createdAt":"2021-12-09T10:09:00.525Z","updatedAt":"2021-12-09T10:09:00.525Z"},{"_id":"61b1d5bc38062d00136d28e5","name":"In which of the following cases a DML statement is not executed?","type":"image","questionType":"SingleChoice","imageUrl":null,"orgId":"rayatdemo","questionBank":"61b188e038062d00136d1c77","qBankChapter":"61b188ee38062d00136d1c78","qBankChapterName":"Chapter 1","explanation":"The DML statement is used to access and manipulate the data in an existing table. Therefore, it cannot be used in table deletion.","parentExplaination":"The DML statement is used to access and manipulate the data in an existing table. Therefore, it cannot be used in table deletion.","assetId":null,"folderName":null,"paperQuestionChoices":[{"_id":"61b1d5bc38062d00136d28e6","choice":"When existing rows are modified.","imageUrl":"","choiceAnswer":false,"isAnswer":false},{"_id":"61b1d5bc38062d00136d28e7","choice":"When a table is deleted.","imageUrl":"","choiceAnswer":false,"isAnswer":true},{"_id":"61b1d5bc38062d00136d28e8","choice":"When some rows are deleted.","imageUrl":"","choiceAnswer":false,"isAnswer":false},{"_id":"61b1d5bc38062d00136d28e9","choice":"All of the above","imageUrl":"","choiceAnswer":false,"isAnswer":false}],"createdAt":"2021-12-09T10:09:00.525Z","updatedAt":"2021-12-09T10:09:00.525Z"},{"_id":"61b1d5bc38062d00136d2854","name":"Which of the following is also called an INNER JOIN?","type":"image","questionType":"SingleChoice","imageUrl":null,"orgId":"rayatdemo","questionBank":"61b188e038062d00136d1c77","qBankChapter":"61b188ee38062d00136d1c78","qBankChapterName":"Chapter 1","explanation":"The INNER JOIN returns data from the two or more tables that match the specified condition and hides other records. EQUI JOIN is similar to INNER JOIN that returns records for equality or matching column(s) values of the relative tables.\n\nNON-EQUI JOIN is returned those records that are not matching in the relative tables.\n\nSELF JOIN returns records from the tables by joining itself.","parentExplaination":"The INNER JOIN returns data from the two or more tables that match the specified condition and hides other records. EQUI JOIN is similar to INNER JOIN that returns records for equality or matching column(s) values of the relative tables.\n\nNON-EQUI JOIN is returned those records that are not matching in the relative tables.\n\nSELF JOIN returns records from the tables by joining itself.","assetId":null,"folderName":null,"paperQuestionChoices":[{"_id":"61b1d5bc38062d00136d2855","choice":"SELF JOIN","imageUrl":"","choiceAnswer":false,"isAnswer":false},{"_id":"61b1d5bc38062d00136d2856","choice":"EQUI JOIN","imageUrl":"","choiceAnswer":false,"isAnswer":true},{"_id":"61b1d5bc38062d00136d2857","choice":"NON-EQUI JOIN","imageUrl":"","choiceAnswer":false,"isAnswer":false},{"_id":"61b1d5bc38062d00136d2858","choice":"None of the above","imageUrl":"","choiceAnswer":false,"isAnswer":false}],"createdAt":"2021-12-09T10:09:00.520Z","updatedAt":"2021-12-09T10:09:00.520Z"},{"_id":"61b1d5bc38062d00136d28e0","name":"SQL Views are also known as","type":"image","questionType":"SingleChoice","imageUrl":null,"orgId":"rayatdemo","questionBank":"61b188e038062d00136d1c77","qBankChapter":"61b188ee38062d00136d1c78","qBankChapterName":"Chapter 1","explanation":"A view is also known as a virtual table because it contains rows and columns similar to a real table. It shows the table interface but cannot be stored in a database.","parentExplaination":"A view is also known as a virtual table because it contains rows and columns similar to a real table. It shows the table interface but cannot be stored in a database.","assetId":null,"folderName":null,"paperQuestionChoices":[{"_id":"61b1d5bc38062d00136d28e1","choice":"Simple tables","imageUrl":"","choiceAnswer":false,"isAnswer":false},{"_id":"61b1d5bc38062d00136d28e2","choice":"Virtual tables","imageUrl":"","choiceAnswer":false,"isAnswer":true},{"_id":"61b1d5bc38062d00136d28e3","choice":"Complex tables","imageUrl":"","choiceAnswer":false,"isAnswer":false},{"_id":"61b1d5bc38062d00136d28e4","choice":"Actual Tables","imageUrl":"","choiceAnswer":false,"isAnswer":false}],"createdAt":"2021-12-09T10:09:00.525Z","updatedAt":"2021-12-09T10:09:00.525Z"}]
/// updatedAt : "2021-12-09T10:09:00.529Z"
/// createdAt : "2021-12-09T10:09:00.529Z"

class QuizQ {
  QuizQ({
    String? id,
    String? name,
    int? startTime,
    String? course,
    String? chapter,
    String? section,
    String? contact,
    String? quiz,
    String? orgId,
    int? timeAlloted,
    List<Questions>? questions,
    String? updatedAt,
    String? createdAt,
  }) {
    _id = id;
    _name = name;
    _startTime = startTime;
    _course = course;
    _chapter = chapter;
    _section = section;
    _contact = contact;
    _quiz = quiz;
    _orgId = orgId;
    _timeAlloted = timeAlloted;
    _questions = questions;
    _updatedAt = updatedAt;
    _createdAt = createdAt;
  }

  QuizQ.fromJson(dynamic json) {
    _id = json['_id'];
    _name = json['name'];
    _startTime = json['startTime'];
    _course = json['course'];
    _chapter = json['chapter'];
    _section = json['section'];
    _contact = json['contact'];
    _quiz = json['quiz'];
    _orgId = json['orgId'];
    _timeAlloted = json['timeAlloted'];
    if (json['questions'] != null) {
      _questions = [];
      json['questions'].forEach((v) {
        _questions?.add(Questions.fromJson(v));
      });
    }
    _updatedAt = json['updatedAt'];
    _createdAt = json['createdAt'];
  }

  String? _id;
  String? _name;
  int? _startTime;
  String? _course;
  String? _chapter;
  String? _section;
  String? _contact;
  String? _quiz;
  String? _orgId;
  int? _timeAlloted;
  List<Questions>? _questions;
  String? _updatedAt;
  String? _createdAt;

  String? get id => _id;

  String? get name => _name;

  int? get startTime => _startTime;

  String? get course => _course;

  String? get chapter => _chapter;

  String? get section => _section;

  String? get contact => _contact;

  String? get quiz => _quiz;

  String? get orgId => _orgId;

  int? get timeAlloted => _timeAlloted;

  List<Questions>? get questions => _questions;

  String? get updatedAt => _updatedAt;

  String? get createdAt => _createdAt;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['_id'] = _id;
    map['name'] = _name;
    map['startTime'] = _startTime;
    map['course'] = _course;
    map['chapter'] = _chapter;
    map['section'] = _section;
    map['contact'] = _contact;
    map['quiz'] = _quiz;
    map['orgId'] = _orgId;
    map['timeAlloted'] = _timeAlloted;
    if (_questions != null) {
      map['questions'] = _questions?.map((v) => v.toJson()).toList();
    }
    map['updatedAt'] = _updatedAt;
    map['createdAt'] = _createdAt;
    return map;
  }
}

/// _id : "61b1d5bc38062d00136d28c2"
/// name : "Which of the following is not Constraint in SQL?"
/// type : "image"
/// questionType : "SingleChoice"
/// imageUrl : null
/// orgId : "rayatdemo"
/// questionBank : "61b188e038062d00136d1c77"
/// qBankChapter : "61b188ee38062d00136d1c78"
/// qBankChapterName : "Chapter 1"
/// explanation : "Constraint specifies the rule to allow or restrict what data will be stored in a table. The PRIMARY KEY, NOT NULL, and CHECK are the constraints that specify rules for data insertion.\n\nUNION is an operator that combines two or more results from multiple SELECT queries into a single result set."
/// parentExplaination : "Constraint specifies the rule to allow or restrict what data will be stored in a table. The PRIMARY KEY, NOT NULL, and CHECK are the constraints that specify rules for data insertion.\n\nUNION is an operator that combines two or more results from multiple SELECT queries into a single result set."
/// assetId : null
/// folderName : null
/// paperQuestionChoices : [{"_id":"61b1d5bc38062d00136d28c3","choice":"Primary Key","imageUrl":"","choiceAnswer":false,"isAnswer":false},{"_id":"61b1d5bc38062d00136d28c4","choice":"Not Null","imageUrl":"","choiceAnswer":false,"isAnswer":false},{"_id":"61b1d5bc38062d00136d28c5","choice":"Check","imageUrl":"","choiceAnswer":false,"isAnswer":false},{"_id":"61b1d5bc38062d00136d28c6","choice":"Union","imageUrl":"","choiceAnswer":false,"isAnswer":true}]
/// createdAt : "2021-12-09T10:09:00.524Z"
/// updatedAt : "2021-12-09T10:09:00.524Z"

// class Questions {
//   Questions(
//       {String? id,
//       String? name,
//       String? type,
//       String? questionType,
//       dynamic imageUrl,
//       String? orgId,
//       String? questionBank,
//       String? qBankChapter,
//       String? qBankChapterName,
//       String? explanation,
//       String? parentExplaination,
//       dynamic assetId,
//       dynamic folderName,
//       List<PaperQuestionChoices>? paperQuestionChoices,
//       String? createdAt,
//       String? updatedAt,
//       bool? isAttempted,
//       bool? isConfirmed,
//       AssetData? assetData}) {
//     _id = id;
//     _name = name;
//     _type = type;
//     _questionType = questionType;
//     _imageUrl = imageUrl;
//     _orgId = orgId;
//     _questionBank = questionBank;
//     _qBankChapter = qBankChapter;
//     _qBankChapterName = qBankChapterName;
//     _explanation = explanation;
//     _parentExplaination = parentExplaination;
//     _assetId = assetId;
//     _folderName = folderName;
//     _paperQuestionChoices = paperQuestionChoices;
//     _createdAt = createdAt;
//     _updatedAt = updatedAt;
//     _isAttempted = isAttempted;
//     _isConfirmed = isConfirmed;
//     _assetData = assetData;
//   }
//
//   void setAttempted(bool value) {
//     _isAttempted = value;
//   }
//
//   void setCurriculum(AssetData value) {
//     _assetData = value;
//   }
//   void setConfirmed(bool value) {
//     _isConfirmed = value;
//   }
//
//   Questions.fromJson(dynamic json) {
//     _id = json['_id'];
//     _name = json['name'];
//     _type = json['type'];
//     _questionType = json['questionType'];
//     _imageUrl = json['imageUrl'];
//     _orgId = json['orgId'];
//     _questionBank = json['questionBank'];
//     _qBankChapter = json['qBankChapter'];
//     _qBankChapterName = json['qBankChapterName'];
//     _explanation = json['explanation'];
//     _parentExplaination = json['parentExplaination'];
//     _assetId = json['assetId'];
//     _folderName = json['folderName'];
//     if (json['paperQuestionChoices'] != null) {
//       _paperQuestionChoices = [];
//       json['paperQuestionChoices'].forEach((v) {
//         _paperQuestionChoices?.add(PaperQuestionChoices.fromJson(v));
//       });
//     }
//     _createdAt = json['createdAt'];
//     _updatedAt = json['updatedAt'];
//   }
//
//   String? _id;
//   String? _name;
//   String? _type;
//   String? _questionType;
//   dynamic _imageUrl;
//   String? _orgId;
//   String? _questionBank;
//   String? _qBankChapter;
//   String? _qBankChapterName;
//   String? _explanation;
//   String? _parentExplaination;
//   dynamic _assetId;
//   dynamic _folderName;
//   List<PaperQuestionChoices>? _paperQuestionChoices;
//   String? _createdAt;
//   String? _updatedAt;
//   bool? _isAttempted;
//   bool? _isConfirmed;
//   AssetData? _assetData;
//
//   String? get id => _id;
//
//   String? get name => _name;
//
//   String? get type => _type;
//
//   String? get questionType => _questionType;
//
//   dynamic get imageUrl => _imageUrl;
//
//   String? get orgId => _orgId;
//
//   String? get questionBank => _questionBank;
//
//   String? get qBankChapter => _qBankChapter;
//
//   String? get qBankChapterName => _qBankChapterName;
//
//   String? get explanation => _explanation;
//
//   String? get parentExplaination => _parentExplaination;
//
//   dynamic get assetId => _assetId;
//
//   dynamic get folderName => _folderName;
//
//   List<PaperQuestionChoices>? get paperQuestionChoices => _paperQuestionChoices;
//
//   String? get createdAt => _createdAt;
//
//   String? get updatedAt => _updatedAt;
//
//   bool? get isAttempted => _isAttempted;
//
//   bool? get isConfirmed => _isConfirmed;
//
//   AssetData? get assetData => _assetData;
//
//   Map<String, dynamic> toJson() {
//     final map = <String, dynamic>{};
//     map['_id'] = _id;
//     map['name'] = _name;
//     map['type'] = _type;
//     map['questionType'] = _questionType;
//     map['imageUrl'] = _imageUrl;
//     map['orgId'] = _orgId;
//     map['questionBank'] = _questionBank;
//     map['qBankChapter'] = _qBankChapter;
//     map['qBankChapterName'] = _qBankChapterName;
//     map['explanation'] = _explanation;
//     map['parentExplaination'] = _parentExplaination;
//     map['assetId'] = _assetId;
//     map['folderName'] = _folderName;
//     if (_paperQuestionChoices != null) {
//       map['paperQuestionChoices'] =
//           _paperQuestionChoices?.map((v) => v.toJson()).toList();
//     }
//     map['createdAt'] = _createdAt;
//     map['updatedAt'] = _updatedAt;
//     return map;
//   }
// }

/// _id : "61b1d5bc38062d00136d28c3"
/// choice : "Primary Key"
/// imageUrl : ""
/// choiceAnswer : false
/// isAnswer : false

class PaperQuestionChoices {
  PaperQuestionChoices({
    String? id,
    String? choice,
    String? imageUrl,
    bool? choiceAnswer,
    bool? isAnswer,
    bool? selectedAnswer,
  }) {
    _id = id;
    _choice = choice;
    _imageUrl = imageUrl;
    _choiceAnswer = choiceAnswer;
    _isAnswer = isAnswer;
    _selectedAnswer = selectedAnswer;
  }

  void setAnswer(bool value) {
    _selectedAnswer = value;
  }

  PaperQuestionChoices.fromJson(dynamic json) {
    _id = json['_id'];
    _choice = json['choice'];
    _imageUrl = json['imageUrl'];
    _choiceAnswer = json['choiceAnswer'];
    _isAnswer = json['isAnswer'];
  }

  String? _id;
  String? _choice;
  String? _imageUrl;
  bool? _choiceAnswer;
  bool? _isAnswer;
  bool? _selectedAnswer = false;

  String? get id => _id;

  String? get choice => _choice;

  String? get imageUrl => _imageUrl;

  bool? get choiceAnswer => _choiceAnswer;

  bool? get isAnswer => _isAnswer;

  bool? get selectedAnswer => _selectedAnswer;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['_id'] = _id;
    map['choice'] = _choice;
    map['imageUrl'] = _imageUrl;
    map['choiceAnswer'] = _choiceAnswer;
    map['isAnswer'] = _isAnswer;
    return map;
  }
}
