import 'quiz_new_response.dart';

/// _id : "6101560e66c6a06170f25fbb"
/// quiz : "610144936aadba45405f360b"
/// contact : "Z4dRZ5ld31"
/// startTime : 1626939255
/// duration : 30
/// orgId : "m300"
/// course : "4JAq4r90gW"
/// chapter : "wyLA62bAoM"
/// section : ""
/// paperQuestionList : [{"_id":"6101560e66c6a06170f25fbc","name":"Who developed Yahoo?","type":"text","questionType":"SingleChoice","imageUrl":null,"difficultyLevel":"Easy","orgId":"m300","questionBank":"60f918696f1aac1790f45623","isAttempted":true,"paperQuestionChoices":[{"_id":"6101560e66c6a06170f25fbd","choice":"Dennis Ritchie & Ken Thompson","explanation":"","imageUrl":"","choiceAnswer":false,"isAnswer":false}]}]

class QuizResultRequestBody {
  String? _id;
  String? _paper;
  String? _quiz;
  String? _contact;
  int? _startTime;
  int? _duration;
  String? _orgId;
  String? _course;
  String? _chapter;
  String? _section;
  List<Questions>? _paperQuestionList;

  String? get id => _id;

  String? get paper => _paper;

  String? get quiz => _quiz;

  String? get contact => _contact;

  int? get startTime => _startTime;

  int? get duration => _duration;

  String? get orgId => _orgId;

  String? get course => _course;

  String? get chapter => _chapter;

  String? get section => _section;

  List<Questions>? get paperQuestionList => _paperQuestionList;

  QuizResultRequestBody(
      {String? id,
      String? quiz,
      String? contact,
      int? startTime,
      int? duration,
      String? orgId,
      String? course,
      String? chapter,
      String? section,
      List<Questions>? paperQuestionList,
      String? paper}) {
    _id = id;
    _quiz = quiz;
    _contact = contact;
    _startTime = startTime;
    _duration = duration;
    _orgId = orgId;
    _course = course;
    _chapter = chapter;
    _section = section;
    _paperQuestionList = paperQuestionList;
    _paper = paper;
  }

  QuizResultRequestBody.fromJson(dynamic json) {
    _id = json['_id'];
    _quiz = json['quiz'];
    _contact = json['contact'];
    _startTime = json['startTime'];
    _duration = json['duration'];
    _orgId = json['orgId'];
    _course = json['course'];
    _chapter = json['chapter'];
    _section = json['section'];
    _paper = json['paper'];
    if (json['paperQuestionList'] != null) {
      _paperQuestionList = [];
      json['paperQuestionList'].forEach((v) {
        _paperQuestionList!.add(Questions.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['_id'] = _id;
    map['quiz'] = _quiz;
    map['contact'] = _contact;
    map['startTime'] = _startTime;
    map['duration'] = _duration;
    map['orgId'] = _orgId;
    map['paper'] = _paper;
    map['course'] = _course;
    map['chapter'] = _chapter;
    map['section'] = _section;
    if (_paperQuestionList != null) {
      map['paperQuestionList'] =
          _paperQuestionList!.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// _id : "6101560e66c6a06170f25fbc"
/// name : "Who developed Yahoo?"
/// type : "text"
/// questionType : "SingleChoice"
/// imageUrl : null
/// difficultyLevel : "Easy"
/// orgId : "m300"
/// questionBank : "60f918696f1aac1790f45623"
/// isAttempted : true
/// paperQuestionChoices : [{"_id":"6101560e66c6a06170f25fbd","choice":"Dennis Ritchie & Ken Thompson","explanation":"","imageUrl":"","choiceAnswer":false,"isAnswer":false}]

class Questions {
  String? _assetId;
  String? _id;
  String? _name;
  String? _type;
  String? _questionType;
  dynamic _explanation;
  dynamic _parentExplanation;
  dynamic _imageUrl;
  String? _difficultyLevel;
  String? _orgId;
  String? _questionBank;
  bool? _isAttempted;
  bool? _isConfirmed;
  List<PaperQuestionChoices>? _paperQuestionChoices;
  AssetData _curriculamData = AssetData();

  String? get assetId => _assetId;

  AssetData? get curriculamData => _curriculamData;

  String? get id => _id;

  String? get name => _name;

  String? get type => _type;

  String? get questionType => _questionType;

  dynamic get imageUrl => _imageUrl;

  dynamic get explanation => _explanation;

  dynamic get parentExplanation => _parentExplanation;

  String? get difficultyLevel => _difficultyLevel;

  String? get orgId => _orgId;

  String? get questionBank => _questionBank;

  bool? get isAttempted => _isAttempted;

  bool? get isConfirmed => _isConfirmed;

  List<PaperQuestionChoices>? get paperQuestionChoices => _paperQuestionChoices;

  void setAttempted(bool value) {
    _isAttempted = value;
  }

  void setCurriculum(AssetData value) {
    _curriculamData = value;
  }

  void setConfirmed(bool value) {
    _isConfirmed = value;
  }

  Questions.fromJson(dynamic json) {
    _id = json['_id'];
    _assetId = json['assetId'];
    _name = json['name'];
    _type = json['type'];
    _questionType = json['questionType'];
    _imageUrl = json['imageUrl'];
    _difficultyLevel = json['difficultyLevel'];
    _orgId = json['orgId'];
    _explanation = json['explanation'];
    _parentExplanation = json['parentExplaination'];
    _questionBank = json['questionBank'];
    _isAttempted = json['isAttempted'];
    if (json['paperQuestionChoices'] != null) {
      _paperQuestionChoices = [];
      json['paperQuestionChoices'].forEach((v) {
        _paperQuestionChoices!.add(PaperQuestionChoices.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['_id'] = _id;
    map['assetId'] = _assetId;
    map['name'] = _name;
    map['type'] = _type;
    map['questionType'] = _questionType;
    map['imageUrl'] = _imageUrl;
    map['explanation'] = _explanation;
    map['parentExplaination'] = _parentExplanation;
    map['difficultyLevel'] = _difficultyLevel;
    map['orgId'] = _orgId;
    map['questionBank'] = _questionBank;
    map['isAttempted'] = _isAttempted;
    if (_paperQuestionChoices != null) {
      map['paperQuestionChoices'] =
          _paperQuestionChoices!.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// _id : "6101560e66c6a06170f25fbd"
/// choice : "Dennis Ritchie & Ken Thompson"
/// explanation : ""
/// imageUrl : ""
/// choiceAnswer : false
/// isAnswer : false

class PaperQuestionChoices {
  String? _id;
  String? _choice;
  String? _imageUrl;
  bool? _choiceAnswer;
  bool? _isAnswer;
  bool? _selectedAnswer = false;

  String? get id => _id;

  String? get choice => _choice;

  String? get imageUrl => _imageUrl;

  bool? get choiceAnswer => _choiceAnswer;

  bool? get isAnswer => _isAnswer;

  bool? get selectedAnswer => _selectedAnswer;

  void setAnswer(bool value) {
    _selectedAnswer = value;
  }

  PaperQuestionChoices(
      {String? id,
      String? choice,
      String? explanation,
      String? imageUrl,
      bool? choiceAnswer,
      bool? selectedAnswer,
      bool? isAnswer}) {
    _id = id;
    _choice = choice;
    _imageUrl = imageUrl;
    _selectedAnswer = selectedAnswer;
    _choiceAnswer = choiceAnswer;
    _isAnswer = isAnswer;
  }

  PaperQuestionChoices.fromJson(dynamic json) {
    _id = json['_id'];
    _choice = json['choice'];
    _imageUrl = json['imageUrl'];
    _choiceAnswer = json['choiceAnswer'];
    _isAnswer = json['isAnswer'];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['_id'] = _id;
    map['choice'] = _choice;
    map['imageUrl'] = _imageUrl;
    map['choiceAnswer'] = _selectedAnswer;
    return map;
  }
}
