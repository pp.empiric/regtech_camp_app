import 'dart:convert';

import 'StoreModel.dart';

/// draw : 1
/// recordsTotal : 17
/// recordsFiltered : 17
/// data : [{"attendeesCount":0,"classColor":"bg-color-red txt-color-white","dateCreated":1635321704,"dateCreatedStr":"27/10/2021","dateUpdated":1637672267,"dateUpdatedStr":"23/11/2021","donationGuId":null,"externalLink":null,"guId":"DKEG2NPGRW","id":"DKEG2NPGRW","image1":"undefined/","isOnlineCourse":true,"isShowOnWebsite":true,"learningPathName":null,"membershipCount":1,"name":"Combine Pre Super 60 Batch 2021","paymentType":"PAID","productCount":0,"programCount":0,"shortDescription":null,"title":"Combine Pre Super 60 Batch 2021","value":"Combine Pre Super 60 Batch 2021","videoUrl":null},{"attendeesCount":0,"classColor":"bg-color-red txt-color-white","dateCreated":1634629897,"dateCreatedStr":"19/10/2021","dateUpdated":1634632191,"dateUpdatedStr":"19/10/2021","donationGuId":null,"externalLink":null,"guId":"ZzyGMQBG4N","id":"ZzyGMQBG4N","image1":"v1634629839/xgcxcplluj22kwadswuk.jpg","isOnlineCourse":false,"isShowOnWebsite":false,"learningPathName":null,"membershipCount":0,"name":"Mulakhat Margadarshan","paymentType":"PRODUCT","productCount":1,"programCount":0,"shortDescription":null,"title":"Mulakhat Margadarshan","value":"Mulakhat Margadarshan","videoUrl":null},{"attendeesCount":0,"classColor":"bg-color-greenLight txt-color-white","dateCreated":1633763766,"dateCreatedStr":"09/10/2021","dateUpdated":1637732060,"dateUpdatedStr":"24/11/2021","donationGuId":null,"externalLink":null,"guId":"jq4Glgvd8w","id":"jq4Glgvd8w","image1":"v1633763572/d2q0vf9ad7gw7nesduiv.jpg","isOnlineCourse":true,"isShowOnWebsite":true,"learningPathName":null,"membershipCount":1,"name":"राज्यसेवा पूर्व परीक्षा - 2021 (Super 75 Batch)","paymentType":"PAID","productCount":0,"programCount":0,"shortDescription":"अद्यावत नोट्स सहित \n\n75 दिवसांचे (450 तास) नियोजन \n\nटेस्ट सिरिज सहित  \n\nराज्यसेवा पूर्व परीक्षेची  संपूर्ण तयारी.\n\n\n\n\n\n\n","title":"राज्यसेवा पूर्व परीक्षा - 2021 (Super 75 Batch)","value":"राज्यसेवा पूर्व परीक्षा - 2021 (Super 75 Batch)","videoUrl":null},{"attendeesCount":0,"classColor":"bg-color-purple txt-color-white","dateCreated":1626260988,"dateCreatedStr":"14/07/2021","dateUpdated":1632123495,"dateUpdatedStr":"20/09/2021","donationGuId":null,"externalLink":null,"guId":"wyLA6nRAoM","id":"wyLA6nRAoM","image1":"v1626260884/il65e2qgihkvvrgggp5x.jpg","isOnlineCourse":false,"isShowOnWebsite":true,"learningPathName":null,"membershipCount":2,"name":"INDUCTION LECTURE","paymentType":"PAID","productCount":0,"programCount":0,"shortDescription":"INDUCTION LECTURE","title":"INDUCTION LECTURE","value":"INDUCTION LECTURE","videoUrl":null},{"attendeesCount":0,"classColor":"bg-color-blue txt-color-white","dateCreated":1623821220,"dateCreatedStr":"16/06/2021","dateUpdated":1626242963,"dateUpdatedStr":"14/07/2021","donationGuId":null,"externalLink":null,"guId":"Q9xABw7dvl","id":"Q9xABw7dvl","image1":"v1626179394/zevuqbboo36sllmchgtl.jpg","isOnlineCourse":false,"isShowOnWebsite":true,"learningPathName":null,"membershipCount":1,"name":"मराठी लेखन व आकलन","paymentType":"PAID","productCount":0,"programCount":0,"shortDescription":"मराठी लेखन व आकलन, Vishal Sutar, Rayat, Rayat Prabodhini Pune, Umesh Kudale, Marathi Grammer","title":"मराठी लेखन व आकलन","value":"मराठी लेखन व आकलन","videoUrl":null},{"attendeesCount":0,"classColor":"bg-color-darken txt-color-white","dateCreated":1622355471,"dateCreatedStr":"30/05/2021","dateUpdated":1626242928,"dateUpdatedStr":"14/07/2021","donationGuId":null,"externalLink":null,"guId":"nD7dzzDdbJ","id":"nD7dzzDdbJ","image1":"v1626242900/rdh8a4lkoxhtukwnqzvn.jpg","isOnlineCourse":false,"isShowOnWebsite":true,"learningPathName":null,"membershipCount":1,"name":"Marathi Grammar (Basic Batch)","paymentType":"PAID","productCount":0,"programCount":0,"shortDescription":"Marathi Grammar (Basic Batch - 4)","title":"Marathi Grammar (Basic Batch)","value":"Marathi Grammar (Basic Batch)","videoUrl":null},{"attendeesCount":0,"classColor":"bg-color-orange txt-color-white","dateCreated":1619419321,"dateCreatedStr":"26/04/2021","dateUpdated":1626693078,"dateUpdatedStr":"19/07/2021","donationGuId":null,"externalLink":null,"guId":"Or6dwJNA4b","id":"Or6dwJNA4b","image1":"v1626242679/qapojp6pptc37kwge3wk.jpg","isOnlineCourse":false,"isShowOnWebsite":true,"learningPathName":null,"membershipCount":1,"name":"Agriculture Economics ","paymentType":"PAID","productCount":0,"programCount":0,"shortDescription":"Agriculture Economics (Batch 3)","title":"Agriculture Economics ","value":"Agriculture Economics ","videoUrl":null},{"attendeesCount":0,"classColor":"bg-color-orange txt-color-white","dateCreated":1619418998,"dateCreatedStr":"26/04/2021","dateUpdated":1626243049,"dateUpdatedStr":"14/07/2021","donationGuId":null,"externalLink":null,"guId":"aNZAW7YAPY","id":"aNZAW7YAPY","image1":"v1626242469/ez1muvoo2xnmfk7lvpax.jpg","isOnlineCourse":false,"isShowOnWebsite":true,"learningPathName":null,"membershipCount":1,"name":"Agriculture Geography","paymentType":"PAID","productCount":0,"programCount":0,"shortDescription":"Agriculture Geography (Batch 3)","title":"Agriculture Geography","value":"Agriculture Geography","videoUrl":null},{"attendeesCount":0,"classColor":"bg-color-greenLight txt-color-white","dateCreated":1619260088,"dateCreatedStr":"24/04/2021","dateUpdated":1637671626,"dateUpdatedStr":"23/11/2021","donationGuId":null,"externalLink":null,"guId":"K9kd9vZGny","id":"K9kd9vZGny","image1":"v1626180151/veocpvyoz6y906clhxbp.jpg","isOnlineCourse":false,"isShowOnWebsite":true,"learningPathName":null,"membershipCount":1,"name":"Marathi Grammar","paymentType":"PAID","productCount":0,"programCount":0,"shortDescription":"Marathi Grammar (Batch - 2)","title":"Marathi Grammar","value":"Marathi Grammar","videoUrl":null},{"attendeesCount":0,"classColor":"bg-color-blueLight txt-color-white","dateCreated":1615442056,"dateCreatedStr":"11/03/2021","dateUpdated":1635162189,"dateUpdatedStr":"25/10/2021","donationGuId":null,"externalLink":null,"guId":"DKEG2rKGRW","id":"DKEG2rKGRW","image1":"v1626244096/kcfnwtkxw7geqzxaet2p.jpg","isOnlineCourse":false,"isShowOnWebsite":false,"learningPathName":null,"membershipCount":1,"name":"Maths and Reasoning","paymentType":"PAID","productCount":0,"programCount":0,"shortDescription":"Maths and Reasoning (Combine - 2020)","title":"Maths and Reasoning","value":"Maths and Reasoning","videoUrl":null},{"attendeesCount":0,"classColor":"bg-color-greenDark txt-color-white","dateCreated":1615027191,"dateCreatedStr":"06/03/2021","dateUpdated":1635162223,"dateUpdatedStr":"25/10/2021","donationGuId":null,"externalLink":null,"guId":"ngB0NXnd5e","id":"ngB0NXnd5e","image1":"v1626250560/flc4plsn8jt7gudzmgcz.jpg","isOnlineCourse":false,"isShowOnWebsite":false,"learningPathName":null,"membershipCount":1,"name":"SUPER - 60 2021 BATCH","paymentType":"PAID","productCount":0,"programCount":0,"shortDescription":"SUPER - 60 2021 BATCH","title":"SUPER - 60 2021 BATCH","value":"SUPER - 60 2021 BATCH","videoUrl":null},{"attendeesCount":0,"classColor":"bg-color-red txt-color-white","dateCreated":1615024233,"dateCreatedStr":"06/03/2021","dateUpdated":1634216836,"dateUpdatedStr":"14/10/2021","donationGuId":null,"externalLink":null,"guId":"Xzjd5DQAvO","id":"Xzjd5DQAvO","image1":"v1626240803/v1vmolvg3wx6ony2lli9.jpg","isOnlineCourse":false,"isShowOnWebsite":true,"learningPathName":null,"membershipCount":1,"name":"COMBINE 2021 Integrated Batch","paymentType":"PAID","productCount":0,"programCount":0,"shortDescription":null,"title":"COMBINE 2021 Integrated Batch","value":"COMBINE 2021 Integrated Batch","videoUrl":null},{"attendeesCount":0,"classColor":"bg-color-red txt-color-white","dateCreated":1615017801,"dateCreatedStr":"06/03/2021","dateUpdated":1626179718,"dateUpdatedStr":"13/07/2021","donationGuId":null,"externalLink":null,"guId":"Vo8G7a6AkB","id":"Vo8G7a6AkB","image1":"v1626179704/sb7ptlg46kd4egqugfaj.jpg","isOnlineCourse":false,"isShowOnWebsite":true,"learningPathName":null,"membershipCount":1,"name":"MPSC 2021 Integrated Batch","paymentType":"PAID","productCount":0,"programCount":0,"shortDescription":null,"title":"MPSC 2021 Integrated Batch","value":"MPSC 2021 Integrated Batch","videoUrl":null},{"attendeesCount":0,"classColor":"bg-color-greenLight txt-color-white","dateCreated":1610620780,"dateCreatedStr":"14/01/2021","dateUpdated":1634617740,"dateUpdatedStr":"19/10/2021","donationGuId":null,"externalLink":null,"guId":"M4JAqPO0gW","id":"M4JAqPO0gW","image1":"v1611927695/rayat/fbbnkksw8sobhwnuklmu.png","isOnlineCourse":false,"isShowOnWebsite":false,"learningPathName":null,"membershipCount":1,"name":"Marathi Grammer","paymentType":"PAID","productCount":0,"programCount":0,"shortDescription":null,"title":"Marathi Grammer","value":"Marathi Grammer","videoUrl":null},{"attendeesCount":0,"classColor":"bg-color-red txt-color-white","dateCreated":1609851472,"dateCreatedStr":"05/01/2021","dateUpdated":1618556726,"dateUpdatedStr":"16/04/2021","donationGuId":null,"externalLink":null,"guId":"DKEG2XOARW","id":"DKEG2XOARW","image1":"v1615454798/rayat/rcqfgcagskd5syudopys.png","isOnlineCourse":false,"isShowOnWebsite":true,"learningPathName":null,"membershipCount":2,"name":"Agriculture Geography & Economics (Batch 2)","paymentType":"PAID","productCount":0,"programCount":0,"shortDescription":null,"title":"Agriculture Geography & Economics (Batch 2)","value":"Agriculture Geography & Economics (Batch 2)","videoUrl":null},{"attendeesCount":0,"classColor":"bg-color-red txt-color-white","dateCreated":1599832121,"dateCreatedStr":"11/09/2020","dateUpdated":1626245217,"dateUpdatedStr":"14/07/2021","donationGuId":null,"externalLink":null,"guId":"gYVAJ8xGM1","id":"gYVAJ8xGM1","image1":"v1615546919/rayat/g5npjb3hcgzwcvzgxlaw.png","isOnlineCourse":false,"isShowOnWebsite":true,"learningPathName":null,"membershipCount":1,"name":"Agriculture Geography (Batch 1)","paymentType":"PAID","productCount":0,"programCount":1,"shortDescription":null,"title":"Agriculture Geography (Batch 1)","value":"Agriculture Geography (Batch 1)","videoUrl":null},{"attendeesCount":0,"classColor":null,"dateCreated":1599801580,"dateCreatedStr":"11/09/2020","dateUpdated":1599801580,"dateUpdatedStr":"11/09/2020","donationGuId":null,"externalLink":null,"guId":"1oVAL3b0rQ","id":"1oVAL3b0rQ","image1":null,"isOnlineCourse":false,"isShowOnWebsite":false,"learningPathName":null,"membershipCount":0,"name":"FREE","paymentType":null,"productCount":0,"programCount":0,"shortDescription":null,"title":null,"value":"FREE","videoUrl":null}]

class ItemCategoryResponse {
  ItemCategoryResponse({
    int? draw,
    int? recordsTotal,
    int? recordsFiltered,
    List<ItemCategoryModel>? data,
  }) {
    _draw = draw;
    _recordsTotal = recordsTotal;
    _recordsFiltered = recordsFiltered;
    _data = data;
  }

  ItemCategoryResponse.fromJson(dynamic json) {
    _draw = json['draw'];
    _recordsTotal = json['recordsTotal'];
    _recordsFiltered = json['recordsFiltered'];
    if (json['data'] != null) {
      _data = [];
      json['data'].forEach((v) {
        _data?.add(ItemCategoryModel.fromJson(v));
      });
    }
  }

  int? _draw;
  int? _recordsTotal;
  int? _recordsFiltered;
  List<ItemCategoryModel>? _data;

  int? get draw => _draw;

  int? get recordsTotal => _recordsTotal;

  int? get recordsFiltered => _recordsFiltered;

  List<ItemCategoryModel>? get data => _data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['draw'] = _draw;
    map['recordsTotal'] = _recordsTotal;
    map['recordsFiltered'] = _recordsFiltered;
    if (_data != null) {
      map['data'] = _data?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// attendeesCount : 0
/// classColor : "bg-color-red txt-color-white"
/// dateCreated : 1635321704
/// dateCreatedStr : "27/10/2021"
/// dateUpdated : 1637672267
/// dateUpdatedStr : "23/11/2021"
/// donationGuId : null
/// externalLink : null
/// guId : "DKEG2NPGRW"
/// id : "DKEG2NPGRW"
/// image1 : "undefined/"
/// isOnlineCourse : true
/// isShowOnWebsite : true
/// learningPathName : null
/// membershipCount : 1
/// name : "Combine Pre Super 60 Batch 2021"
/// paymentType : "PAID"
/// productCount : 0
/// programCount : 0
/// shortDescription : null
/// title : "Combine Pre Super 60 Batch 2021"
/// value : "Combine Pre Super 60 Batch 2021"
/// videoUrl : null
///
///

class LongDescription {
  String? guId;

  LongDescription({this.guId});

  LongDescription.fromJson(Map<String, dynamic> json) {
    guId = json['guId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['guId'] = guId;
    return data;
  }
}

class ItemCategoryModel {
  ItemCategoryModel(
      {int? attendeesCount,
      String? classColor,
      int? dateCreated,
      String? dateCreatedStr,
      int? dateUpdated,
      String? dateUpdatedStr,
      dynamic donationGuId,
      dynamic externalLink,
      String? guId,
      String? id,
      String? image1,
      bool? isOnlineCourse,
      bool? isShowOnWebsite,
      dynamic learningPathName,
      int? membershipCount,
      String? name,
      String? paymentType,
      int? productCount,
      int? programCount,
      dynamic shortDescription,
      String? title,
      String? value,
      dynamic videoUrl}) {
    _attendeesCount = attendeesCount;
    _classColor = classColor;
    _dateCreated = dateCreated;
    _dateCreatedStr = dateCreatedStr;
    _dateUpdated = dateUpdated;
    _dateUpdatedStr = dateUpdatedStr;
    _donationGuId = donationGuId;
    _externalLink = externalLink;
    _guId = guId;
    _id = id;
    _image1 = image1;
    _isOnlineCourse = isOnlineCourse;
    _isShowOnWebsite = isShowOnWebsite;
    _learningPathName = learningPathName;
    _membershipCount = membershipCount;
    _name = name;
    _paymentType = paymentType;
    _productCount = productCount;
    _programCount = programCount;
    _shortDescription = shortDescription;
    _title = title;
    _value = value;
    _videoUrl = videoUrl;
  }

  setPaymentType(String paymentType) {
    _videoUrl = paymentType;
  }

  setStoreModel(List<StoreModel> storeModel) {
    _storeModel.clear();
    _storeModel.addAll(storeModel);
  }

  ItemCategoryModel.fromJson(dynamic json) {
    _attendeesCount = json['attendeesCount'];
    _classColor = json['classColor'];
    _dateCreated = json['dateCreated'];
    _dateCreatedStr = json['dateCreatedStr'];
    _dateUpdated = json['dateUpdated'];
    _dateUpdatedStr = json['dateUpdatedStr'];
    _donationGuId = json['donationGuId'];
    _externalLink = json['externalLink'];
    _guId = json['guId'];
    _id = json['id'];
    _image1 = json['image1'];
    _isOnlineCourse = json['isOnlineCourse'];
    _isShowOnWebsite = json['isShowOnWebsite'];
    _learningPathName = json['learningPathName'];
    _membershipCount = json['membershipCount'];
    _name = json['name'];
    _paymentType = json['paymentType'];
    _productCount = json['productCount'];
    _programCount = json['programCount'];
    _shortDescription = json['shortDescription'];
    _title = json['title'];
    _value = json['value'];
    _videoUrl = json['videoUrl'];
    longDescription = json['longDescription'] != null
        ? LongDescription.fromJson(json['longDescription'])
        : null;
  }

  int? _attendeesCount;
  String? _classColor;
  int? _dateCreated;
  String? _dateCreatedStr;
  int? _dateUpdated;
  String? _dateUpdatedStr;
  dynamic _donationGuId;
  dynamic _externalLink;
  String? _guId;
  String? _id;
  String? _image1;
  bool? _isOnlineCourse;
  bool? _isShowOnWebsite;
  dynamic _learningPathName;
  int? _membershipCount;
  String? _name;
  String? _paymentType;
  int? _productCount;
  int? _programCount;
  dynamic _shortDescription;
  String? _title;
  String? _value;
  dynamic _videoUrl;
  final List<StoreModel> _storeModel = <StoreModel>[];
  LongDescription? longDescription;

  int? get attendeesCount => _attendeesCount;

  String? get classColor => _classColor;

  int? get dateCreated => _dateCreated;

  String? get dateCreatedStr => _dateCreatedStr;

  int? get dateUpdated => _dateUpdated;

  String? get dateUpdatedStr => _dateUpdatedStr;

  dynamic get donationGuId => _donationGuId;

  dynamic get externalLink => _externalLink;

  String? get guId => _guId;

  String? get id => _id;

  String? get image1 => _image1;

  bool? get isOnlineCourse => _isOnlineCourse;

  bool? get isShowOnWebsite => _isShowOnWebsite;

  dynamic get learningPathName => _learningPathName;

  int? get membershipCount => _membershipCount;

  String? get name => _name;

  String? get paymentType => _paymentType;

  int? get productCount => _productCount;

  int? get programCount => _programCount;

  dynamic get shortDescription => _shortDescription;

  String? get title => _title;

  String? get value => _value;

  dynamic get videoUrl => _videoUrl;

  List<StoreModel> get storeModel => _storeModel;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['attendeesCount'] = _attendeesCount;
    map['classColor'] = _classColor;
    map['dateCreated'] = _dateCreated;
    map['dateCreatedStr'] = _dateCreatedStr;
    map['dateUpdated'] = _dateUpdated;
    map['dateUpdatedStr'] = _dateUpdatedStr;
    map['donationGuId'] = _donationGuId;
    map['externalLink'] = _externalLink;
    map['guId'] = _guId;
    map['id'] = _id;
    map['image1'] = _image1;
    map['isOnlineCourse'] = _isOnlineCourse;
    map['isShowOnWebsite'] = _isShowOnWebsite;
    map['learningPathName'] = _learningPathName;
    map['membershipCount'] = _membershipCount;
    map['name'] = _name;
    if (longDescription != null) {
      map['longDescription'] = longDescription!.toJson();
    }
    map['paymentType'] = _paymentType;
    map['productCount'] = _productCount;
    map['programCount'] = _programCount;
    map['shortDescription'] = _shortDescription;
    map['title'] = _title;
    map['value'] = _value;
    map['videoUrl'] = _videoUrl;
    return map;
  }
}

// class FeaturedEventDescription {
//   String? description;
//   String? guId;
//
//   FeaturedEventDescription({this.description, this.guId});
//
//   FeaturedEventDescription.fromJson(Map<String, dynamic> json) {
//     description = json['description'];
//     guId = json['guId'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = <String, dynamic>{};
//     data['description'] = description;
//     data['guId'] = guId;
//     return data;
//   }
// }

// To parse this JSON data, do
//
//     final genralDataResponse = genralDataResponseFromJson(jsonString);

class FeaturedEventDescription {
  String? description;


  FeaturedEventDescription({this.description});

  FeaturedEventDescription.fromJson(Map<String, dynamic> json) {
    description = json['unhtml'];

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['unhtml'] = description;
    return data;
  }
}
