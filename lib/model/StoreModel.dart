class StoreModel {
  Category? category;
  String? categoryId;
  dynamic donationCategory;
  String? guId;
  bool? isBillFromNextMonth;
  bool? isChargeCreditCardFees;
  bool? isDiscountSubscriptionFees;
  bool? isDonationItem;
  bool? isOtherPrice;
  bool? isTaxable;
  String? offerId;
  String? itemType;
  String? name;
  num price = 0;
  num tax =0;
  num cardfees = 0;
  num discount = 0;
  num discountPercent = 0;
  num discountAmount = 0;
  num discountPrimary = 0;
  num discountSecondary = 0;
  num normalcardfees = 0;
  num regisstrationcardfees = 0;
  num registrationFees = 0;
  SubscriptionInfo? subscriptionInfo;
  String? billingDay;
  String? quantity;
  String? membershipType;

  StoreModel();

  StoreModel.fromJson(Map<String?, dynamic> json) {
    category = json['category'] != null ? Category.fromJson(json['category']) : null;
    donationCategory = json['donationCategory'];
    guId = json['guId'];

    discountPrimary = json['discountPrimary'] ?? 0;
    discountSecondary = json['discountSecondary'] ?? 0;
    categoryId = json['categoryId'];

    normalcardfees = json['normalcardfees'] ?? 0;
    regisstrationcardfees = json['regisstrationcardfees'] ?? 0;

    offerId = json['offerId'];
    membershipType = json['membershipType'];
    isBillFromNextMonth = json['isBillFromNextMonth'];
    isChargeCreditCardFees = json['isChargeCreditCardFees'];
    isDiscountSubscriptionFees = json['isDiscountSubscriptionFees'];
    isDonationItem = json['isDonationItem'];
    isOtherPrice = json['isOtherPrice'];
    isTaxable = json['isTaxable'];
    itemType = json['itemType'];
    name = json['name'];
    price = json['price'] ?? 0;
    cardfees = json['cardfees'] ?? 0;
    registrationFees = json['registrationFees'] ?? 0;
    if (json['subscriptionInfo'] != null) {
      subscriptionInfo = SubscriptionInfo.fromJson(json['subscriptionInfo']);
    } else {
      subscriptionInfo = null;
    }
    billingDay = "0";
    quantity = "1";
    discount = json['discount'] ?? 0;
    discountPercent = json['discountPercent'] ?? 0;
    discountAmount = json['discountAmount'] ?? 0;
  }

  Map<String?, dynamic> toJson() {
    final Map<String?, dynamic> data = <String?, dynamic>{};
    if (category != null) {
      data['category'] = category?.toJson();
    }
    data['membershipType'] = membershipType;
    data['categoryId'] = categoryId;
    data['donationCategory'] = donationCategory;
    data['guId'] = guId;
    data['discountPrimary'] = discountPrimary;
    data['discountSecondary'] = discountSecondary;
    data['normalcardfees'] = normalcardfees;
    data['regisstrationcardfees'] = regisstrationcardfees;
    data['isBillFromNextMonth'] = isBillFromNextMonth;
    data['isChargeCreditCardFees'] = isChargeCreditCardFees;
    data['isDiscountSubscriptionFees'] = isDiscountSubscriptionFees;
    data['isDonationItem'] = isDonationItem;
    data['isOtherPrice'] = isOtherPrice;
    data['isTaxable'] = isTaxable;
    data['itemType'] = itemType;
    data['name'] = name;
    data['price'] = price;
    data['cardfees'] = cardfees;
    data['registrationFees'] = registrationFees;
    if (subscriptionInfo != null) {
      data['subscriptionInfo'] = subscriptionInfo?.toJson();
    }
    data['discount'] = discount;
    data['discountPercent'] = discountPercent;
    data['discountAmount'] = discountAmount;
    data['offerId'] = offerId;
    return data;
  }
}

class Category {
  String? classColor;
  dynamic donationId;
  String? externalLink;
  String? guId;
  String? imageUrl;
  String? name;
  String? paymentType;
  String? videoUrl;

  Category({classColor, donationId, externalLink, guId, imageUrl, name, paymentType, videoUrl});

  Category.fromJson(Map<String?, dynamic> json) {
    classColor = json['classColor'];
    donationId = json['donationId'];
    externalLink = json['externalLink'];
    guId = json['guId'];
    imageUrl = json['imageUrl'];
    name = json['name'];
    paymentType = json['paymentType'];
    videoUrl = json['videoUrl'];
  }

  Map<String?, dynamic> toJson() {
    final Map<String?, dynamic> data = <String?, dynamic>{};
    data['classColor'] = classColor;
    data['donationId'] = donationId;
    data['externalLink'] = externalLink;
    data['guId'] = guId;
    data['imageUrl'] = imageUrl;
    data['name'] = name;
    data['paymentType'] = paymentType;
    data['videoUrl'] = videoUrl;
    return data;
  }
}

class MembershipInfo {
  dynamic activePeriod;
  String? activePeriodType;
  dynamic credits;
  dynamic expiryPeriod;
  String? expiryPeriodType;

  MembershipInfo({activePeriod, activePeriodType, credits, expiryPeriod, expiryPeriodType});

  MembershipInfo.fromJson(Map<String?, dynamic> json) {
    activePeriod = json['activePeriod'];
    activePeriodType = json['activePeriodType'];
    credits = json['credits'];
    expiryPeriod = json['expiryPeriod'];
    expiryPeriodType = json['expiryPeriodType'];
  }

  Map<String?, dynamic> toJson() {
    final Map<String?, dynamic> data = <String?, dynamic>{};
    data['activePeriod'] = activePeriod;
    data['activePeriodType'] = activePeriodType;
    data['credits'] = credits;
    data['expiryPeriod'] = expiryPeriod;
    data['expiryPeriodType'] = expiryPeriodType;
    return data;
  }
}

class SubscriptionInfo {
  String? billingDayOfMonth;
  String? billingFreqText;
  String? billingFrequency;
  dynamic calculatedBillingCycles;
  bool? isNeverExpires;
  int? numberOfBillingCycles;
  String? subscriptionPlanId;

  SubscriptionInfo(
      {billingDayOfMonth,
        billingFreqText,
        billingFrequency,
        calculatedBillingCycles,
        isNeverExpires,
        numberOfBillingCycles,
        subscriptionPlanId});

  SubscriptionInfo.fromJson(Map<String?, dynamic> json) {
    billingDayOfMonth = json['billingDayOfMonth'];
    billingFreqText = json['billingFreqText'];
    billingFrequency = json['billingFrequency'];
    calculatedBillingCycles = json['calculatedBillingCycles'];
    isNeverExpires = json['isNeverExpires'];
    numberOfBillingCycles = json['numberOfBillingCycles'];
    subscriptionPlanId = json['subscriptionPlanId'];
  }

  Map<String?, dynamic> toJson() {
    final Map<String?, dynamic> data = <String?, dynamic>{};
    data['billingDayOfMonth'] = billingDayOfMonth;
    data['billingFreqText'] = billingFreqText;
    data['billingFrequency'] = billingFrequency;
    data['calculatedBillingCycles'] = calculatedBillingCycles;
    data['isNeverExpires'] = isNeverExpires;
    data['numberOfBillingCycles'] = numberOfBillingCycles;
    data['subscriptionPlanId'] = subscriptionPlanId;
    return data;
  }
}
