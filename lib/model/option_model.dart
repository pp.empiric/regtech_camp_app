class RadioModel {
  String guId;
  List<OptionModel> list = [];
  RadioModel(this.guId,this.list);
}

class OptionModel {
  int? index;
  String? value;
  bool? selected;

  OptionModel(this.index, this.value, this.selected);
}
