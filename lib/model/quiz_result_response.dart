import 'quiz_result_request_body.dart';

class QuizResultResponse {
  QuizResultResponse({
      Data? data,}){
    _data = data;
}

  QuizResultResponse.fromJson(dynamic json) {
    _data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }
  Data? _data;

  Data? get data => _data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_data != null) {
      map['data'] = _data?.toJson();
    }
    return map;
  }
}


class Data {
  Data({
      String? id, 
      int? score, 
      int? attempts, 
      int? totalQuestions, 
      String? percentage, 
      bool? isPass, 
      int? correct, 
      int? unattempts, 
      int? incorect, 
      String? paper, 
      String? grade, 
      String? quiz, 
      String? quizName, 
      int? quizPassingPerc, 
      String? quizType, 
      String? quizTypePattern, 
      String? course, 
      String? chapter, 
      String? section, 
      String? contact, 
      String? contactName, 
      String? contactEmail, 
      String? courseName, 
      String? chapterName, 
      String? sectionName, 
      String? orgId, 
      String? createdAt, 
      String? updatedAt, 
      int? v, 
      List<Questions>? questions, 
      bool? showAllChoiceInResult, 
      bool? showAnswersInResult, 
      String? name,}){
    _id = id;
    _score = score;
    _attempts = attempts;
    _totalQuestions = totalQuestions;
    _percentage = percentage;
    _isPass = isPass;
    _correct = correct;
    _unattempts = unattempts;
    _incorect = incorect;
    _paper = paper;
    _grade = grade;
    _quiz = quiz;
    _quizName = quizName;
    _quizPassingPerc = quizPassingPerc;
    _quizType = quizType;
    _quizTypePattern = quizTypePattern;
    _course = course;
    _chapter = chapter;
    _section = section;
    _contact = contact;
    _contactName = contactName;
    _contactEmail = contactEmail;
    _courseName = courseName;
    _chapterName = chapterName;
    _sectionName = sectionName;
    _orgId = orgId;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
    _v = v;
    _questions = questions;
    _showAllChoiceInResult = showAllChoiceInResult;
    _showAnswersInResult = showAnswersInResult;
    _name = name;
}

  Data.fromJson(dynamic json) {
    _id = json['_id'];
    _score = json['score'];
    _attempts = json['attempts'];
    _totalQuestions = json['totalQuestions'];
    _percentage = json['percentage'];
    _isPass = json['isPass'];
    _correct = json['correct'];
    _unattempts = json['unattempts'];
    _incorect = json['incorect'];
    _paper = json['paper'];
    _grade = json['grade'];
    _quiz = json['quiz'];
    _quizName = json['quizName'];
    _quizPassingPerc = json['quizPassingPerc'];
    _quizType = json['quizType'];
    _quizTypePattern = json['quizTypePattern'];
    _course = json['course'];
    _chapter = json['chapter'];
    _section = json['section'];
    _contact = json['contact'];
    _contactName = json['contactName'];
    _contactEmail = json['contactEmail'];
    _courseName = json['courseName'];
    _chapterName = json['chapterName'];
    _sectionName = json['sectionName'];
    _orgId = json['orgId'];
    _createdAt = json['createdAt'];
    _updatedAt = json['updatedAt'];
    _v = json['__v'];
    if (json['questions'] != null) {
      _questions = [];
      json['questions'].forEach((v) {
        _questions?.add(Questions.fromJson(v));
      });
    }
    _showAllChoiceInResult = json['showAllChoiceInResult'];
    _showAnswersInResult = json['showAnswersInResult'];
    _name = json['name'];
  }
  String? _id;
  num? _score;
  int? _attempts;
  int? _totalQuestions;
  String? _percentage;
  bool? _isPass;
  int? _correct;
  int? _unattempts;
  int? _incorect;
  String? _paper;
  String? _grade;
  String? _quiz;
  String? _quizName;
  num? _quizPassingPerc;
  String? _quizType;
  String? _quizTypePattern;
  String? _course;
  String? _chapter;
  String? _section;
  String? _contact;
  String? _contactName;
  String? _contactEmail;
  String? _courseName;
  String? _chapterName;
  String? _sectionName;
  String? _orgId;
  String? _createdAt;
  String? _updatedAt;
  int? _v;
  List<Questions>? _questions;
  bool? _showAllChoiceInResult;
  bool? _showAnswersInResult;
  String? _name;

  String? get id => _id;
  num? get score => _score;
  int? get attempts => _attempts;
  int? get totalQuestions => _totalQuestions;
  String? get percentage => _percentage;
  bool? get isPass => _isPass;
  int? get correct => _correct;
  int? get unattempts => _unattempts;
  int? get incorect => _incorect;
  String? get paper => _paper;
  String? get grade => _grade;
  String? get quiz => _quiz;
  String? get quizName => _quizName;
  num? get quizPassingPerc => _quizPassingPerc;
  String? get quizType => _quizType;
  String? get quizTypePattern => _quizTypePattern;
  String? get course => _course;
  String? get chapter => _chapter;
  String? get section => _section;
  String? get contact => _contact;
  String? get contactName => _contactName;
  String? get contactEmail => _contactEmail;
  String? get courseName => _courseName;
  String? get chapterName => _chapterName;
  String? get sectionName => _sectionName;
  String? get orgId => _orgId;
  String? get createdAt => _createdAt;
  String? get updatedAt => _updatedAt;
  int? get v => _v;
  List<Questions>? get questions => _questions;
  bool? get showAllChoiceInResult => _showAllChoiceInResult;
  bool? get showAnswersInResult => _showAnswersInResult;
  String? get name => _name;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['_id'] = _id;
    map['score'] = _score;
    map['attempts'] = _attempts;
    map['totalQuestions'] = _totalQuestions;
    map['percentage'] = _percentage;
    map['isPass'] = _isPass;
    map['correct'] = _correct;
    map['unattempts'] = _unattempts;
    map['incorect'] = _incorect;
    map['paper'] = _paper;
    map['grade'] = _grade;
    map['quiz'] = _quiz;
    map['quizName'] = _quizName;
    map['quizPassingPerc'] = _quizPassingPerc;
    map['quizType'] = _quizType;
    map['quizTypePattern'] = _quizTypePattern;
    map['course'] = _course;
    map['chapter'] = _chapter;
    map['section'] = _section;
    map['contact'] = _contact;
    map['contactName'] = _contactName;
    map['contactEmail'] = _contactEmail;
    map['courseName'] = _courseName;
    map['chapterName'] = _chapterName;
    map['sectionName'] = _sectionName;
    map['orgId'] = _orgId;
    map['createdAt'] = _createdAt;
    map['updatedAt'] = _updatedAt;
    map['__v'] = _v;
    if (_questions != null) {
      map['questions'] = _questions?.map((v) => v.toJson()).toList();
    }
    map['showAllChoiceInResult'] = _showAllChoiceInResult;
    map['showAnswersInResult'] = _showAnswersInResult;
    map['name'] = _name;
    return map;
  }

}

