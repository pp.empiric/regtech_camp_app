import 'package:regtech_camp_app/model/section_response.dart';

class CourseCurriculumResponse {
  String? classColor;
  List<SectionResponse>? courseSections;
  String? guId;
  String? name;
  String? title;

  CourseCurriculumResponse({
    this.classColor,
    this.courseSections,
    this.guId,
    this.name,
    this.title,
  });

  CourseCurriculumResponse.fromJson(Map<String, dynamic> json) {
    classColor = json['classColor'];
    if (json['courseSections'] != null) {
      courseSections = [];
      json['courseSections'].forEach((v) {
        courseSections?.add(new SectionResponse.fromJson(v));
      });
    }
    guId = json['guId'];
    name = json['name'];
    title = json['title'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['classColor'] = this.classColor;
    if (this.courseSections != null) {
      data['courseSections'] =
          this.courseSections?.map((v) => v.toJson()).toList();
    }
    data['name'] = this.name;
    data['title'] = this.title;
    return data;
  }
}


