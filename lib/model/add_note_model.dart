class AddNoteModel {
  String? chapterId;
  String? dateCreated;
  String? dateCreatedStr;
  String? guId;
  String? note;

  AddNoteModel(
      {this.chapterId,
      this.dateCreated,
      this.dateCreatedStr,
      this.guId,
      this.note});

  AddNoteModel.fromJson(Map<String?, dynamic> json) {
    chapterId = json['chapterId'];
    dateCreated = json['dateCreated'];
    dateCreatedStr = json['dateCreatedStr'];
    guId = json['guId'];
    note = json['note'];
  }

  Map<String?, dynamic> toJson() {
    final Map<String?, dynamic> data = new Map<String?, dynamic>();
    data['chapterId'] = this.chapterId;
    data['dateCreated'] = this.dateCreated;
    data['dateCreatedStr'] = this.dateCreatedStr;
    data['guId'] = this.guId;
    data['note'] = this.note;
    return data;
  }
}
