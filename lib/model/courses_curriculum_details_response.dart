import 'package:get/get_rx/src/rx_types/rx_types.dart';

class CoursesCurriculumDetailsResponse {
  dynamic status;
  List<Data>? curriculumdata;

  CoursesCurriculumDetailsResponse({this.status, this.curriculumdata});

  CoursesCurriculumDetailsResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['data'] != null) {
      curriculumdata = [];
      json['data'].forEach((v) {
        curriculumdata?.add(Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['status'] = status;
    if (curriculumdata != null) {
      data['data'] = curriculumdata?.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  String? id;
  String? orgId;
  String? chapterId;
  String? fileType;
  String? fileName;
  String? folderName;
  String? fileSize;
  int? sequence;
  bool? isVideo;
  String? ownerId;
  String? ownerName;
  String? updatedAt;
  String? status;
  String? jobId;
  String? s3url;
  String? notes;
  var isDownloading = false.obs;
  var isDeleting= false.obs;

  Data(
      {   this.id,
        this.orgId,
        this.chapterId,
        this.fileType,
        this.fileName,
        this.folderName,
        this.fileSize,
        this.sequence,
        this.isVideo,
        this.ownerId,
        this.ownerName,
        this.updatedAt,
        this.status,
        this.jobId,
        this.notes,
        this.s3url
      });

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    orgId = json['orgId'];
    chapterId = json['chapterId'];
    fileType = json['fileType'];
    fileName = json['fileName'];
    folderName = json['folderName'];
    fileSize = json['fileSize'];
    sequence = json['sequence'];
    isVideo = json['isVideo'];
    ownerId = json['ownerId'];
    ownerName = json['ownerName'];
    updatedAt = json['updatedAt'];
    status = json['status'];
    jobId = json['jobId'];
    notes = json['notes'];
    s3url = json['s3url'];
    isDownloading.value = false;
    isDeleting.value = false;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['orgId'] = orgId;
    data['chapterId'] = chapterId;
    data['fileType'] = fileType;
    data['fileName'] = fileName;
    data['folderName'] = folderName;
    data['fileSize'] = fileSize;
    data['sequence'] = sequence;
    data['isVideo'] = isVideo;
    data['ownerId'] = ownerId;
    data['ownerName'] = ownerName;
    data['updatedAt'] = updatedAt;
    data['status'] = status;
    data['jobId'] = jobId;
    data['s3url'] = s3url;
    data['notes'] = notes;
    return data;
  }
}

// class Data {
//   String orgId;
//   bool isPublic;
//   String courseId;
//   String studentId;
//   String ruleId;
//   String s3url;
//   String cid;
//   String hlsurl;
//   String hlsAllurl;
//   String tid;
//   String createdBy;
//   String chapterId;
//   String videoTitle;
//   String notes;
//   String studentName;
//   String courseName;
//   String ruleName;
//   bool isVideo;
//   String jobId;
//   String fileType;
//   String deletedAt;
//   String deletedBy;
//   String assetType;
//   String assetLink;
//   dynamic sequence;
//   String sId;
//   String status;
//   String createdAt;
//   String updatedAt;
//   bool isDownloading;
//   dynamic iV;
//
//   Data(
//       {this.orgId,
//       this.isPublic,
//       this.courseId,
//       this.studentId,
//       this.ruleId,
//       this.s3url,
//       this.cid,
//       this.hlsurl,
//       this.hlsAllurl,
//       this.tid,
//       this.createdBy,
//       this.chapterId,
//       this.videoTitle,
//       this.notes,
//       this.studentName,
//       this.courseName,
//       this.ruleName,
//       this.isVideo,
//       this.jobId,
//       this.fileType,
//       this.deletedAt,
//       this.deletedBy,
//       this.assetType,
//       this.assetLink,
//       this.sequence,
//       this.sId,
//       this.status,
//       this.createdAt,
//       this.updatedAt,
//       this.iV,
//       this.isDownloading});
//
//   Data.fromJson(Map<String, dynamic> json) {
//     orgId = json['orgId'];
//     isPublic = json['isPublic'];
//     courseId = json['courseId'];
//     studentId = json['studentId'];
//     ruleId = json['ruleId'];
//     s3url = json['s3url'];
//     cid = json['cid'];
//     hlsurl = json['hlsurl'];
//     hlsAllurl = json['hlsAllurl'];
//     tid = json['tid'];
//     createdBy = json['createdBy'];
//     chapterId = json['chapterId'];
//     videoTitle = json['videoTitle'];
//     notes = json['notes'];
//     studentName = json['studentName'];
//     courseName = json['courseName'];
//     ruleName = json['ruleName'];
//     isVideo = json['isVideo'];
//     jobId = json['jobId'];
//     fileType = json['fileType'];
//     deletedAt = json['deletedAt'];
//     deletedBy = json['deletedBy'];
//     assetType = json['assetType'];
//     assetLink = json['assetLink'];
//     sequence = json['sequence'];
//     sId = json['_id'];
//     status = json['status'];
//     createdAt = json['createdAt'];
//     updatedAt = json['updatedAt'];
//     iV = json['__v'];
//     isDownloading = false;
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['orgId'] = this.orgId;
//     data['isPublic'] = this.isPublic;
//     data['courseId'] = this.courseId;
//     data['studentId'] = this.studentId;
//     data['ruleId'] = this.ruleId;
//     data['s3url'] = this.s3url;
//     data['hlsurl'] = this.hlsurl;
//     data['hlsAllurl'] = this.hlsAllurl;
//     data['cid'] = this.cid;
//     data['tid'] = this.tid;
//     data['createdBy'] = this.createdBy;
//     data['chapterId'] = this.chapterId;
//     data['videoTitle'] = this.videoTitle;
//     data['notes'] = this.notes;
//     data['studentName'] = this.studentName;
//     data['courseName'] = this.courseName;
//     data['ruleName'] = this.ruleName;
//     data['isVideo'] = this.isVideo;
//     data['jobId'] = this.jobId;
//     data['fileType'] = this.fileType;
//     data['deletedAt'] = this.deletedAt;
//     data['deletedBy'] = this.deletedBy;
//     data['assetType'] = this.assetType;
//     data['assetLink'] = this.assetLink;
//     data['sequence'] = this.sequence;
//     data['_id'] = this.sId;
//     data['status'] = this.status;
//     data['createdAt'] = this.createdAt;
//     data['updatedAt'] = this.updatedAt;
//     data['__v'] = this.iV;
//     return data;
//   }
// }
