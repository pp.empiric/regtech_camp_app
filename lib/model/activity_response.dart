import 'package:get/get.dart';

/// data : [{"_id":"6156d6f863a1a1001301f8c1","name":"Tuesday-CBT","course":"1oVALEpdrQ","chapter":"zjd58mJAvO","section":"e2x0eER0Yb","orgId":"mindsynckids","additionalInfo":"Positive Psychology","isRepeatActivity":false,"isAllowUserToUploadFiles":true,"isActive":true,"isTrackActivity":true,"activityQuestions":[{"answerCount":3,"_id":"6156d6f863a1a1001301f8c2","title":"Three words or statements that make me angry","type":"text"},{"answerCount":3,"_id":"6156d6f863a1a1001301f8c3","title":"Three actions that make me angry","type":"text"},{"answerCount":3,"_id":"6156d6f863a1a1001301f8c4","title":"Three things that I do while I am angry","type":"text"},{"answerCount":3,"_id":"6156d6f863a1a1001301f8c5","title":"Put an ‘X’ or color the area in ‘RED’ where you feel anger the most in your body","type":"body"},{"answerCount":3,"_id":"6156d6f863a1a1001301f8c6","title":"Three things that can help me ‘RELAX’ when I am angry","type":"text"}],"createdAt":"2021-10-01T09:38:00.639Z","updatedAt":"2021-10-12T06:09:23.316Z","__v":0}]
/// total : 1

class ActivityResponse {
  ActivityResponse1({
    List<Activity>? data,
    int? total,
  }) {
    _data = data;
    _total = total;
  }

  ActivityResponse.fromJson(dynamic json) {
    if (json['data'] != null) {
      _data = [];
      json['data'].forEach((v) {
        _data?.add(Activity.fromJson(v));
      });
    }
    _total = json['total'];
  }

  List<Activity>? _data;
  int? _total;

  List<Activity>? get data => _data;

  int? get total => _total;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_data != null) {
      map['data'] = _data?.map((v) => v.toJson()).toList();
    }
    map['total'] = _total;
    return map;
  }
}

/// _id : "6156d6f863a1a1001301f8c1"
/// name : "Tuesday-CBT"
/// course : "1oVALEpdrQ"
/// chapter : "zjd58mJAvO"
/// section : "e2x0eER0Yb"
/// orgId : "mindsynckids"
/// additionalInfo : "Positive Psychology"
/// isRepeatActivity : false
/// isAllowUserToUploadFiles : true
/// isActive : true
/// isTrackActivity : true
/// activityQuestions : [{"answerCount":3,"_id":"6156d6f863a1a1001301f8c2","title":"Three words or statements that make me angry","type":"text"},{"answerCount":3,"_id":"6156d6f863a1a1001301f8c3","title":"Three actions that make me angry","type":"text"},{"answerCount":3,"_id":"6156d6f863a1a1001301f8c4","title":"Three things that I do while I am angry","type":"text"},{"answerCount":3,"_id":"6156d6f863a1a1001301f8c5","title":"Put an ‘X’ or color the area in ‘RED’ where you feel anger the most in your body","type":"body"},{"answerCount":3,"_id":"6156d6f863a1a1001301f8c6","title":"Three things that can help me ‘RELAX’ when I am angry","type":"text"}]
/// createdAt : "2021-10-01T09:38:00.639Z"
/// updatedAt : "2021-10-12T06:09:23.316Z"
/// __v : 0

class Activity {
  Activity(
      {String? id,
      String? name,
      String? course,
      String? chapter,
      String? section,
      String? orgId,
      String? additionalInfo,
      bool? isRepeatActivity,
      bool? isAllowUserToUploadFiles,
      bool? isActive,
      bool? isTrackActivity,
      List<ActivityQuestions>? activityQuestions,
      String? createdAt,
      String? updatedAt,
      int? v,
      bool? completed}) {
    _id = id;
    _name = name;
    _course = course;
    _chapter = chapter;
    _section = section;
    _orgId = orgId;
    _additionalInfo = additionalInfo;
    _isRepeatActivity = isRepeatActivity;
    _isAllowUserToUploadFiles = isAllowUserToUploadFiles;
    _isActive = isActive;
    _isTrackActivity = isTrackActivity;
    _activityQuestions = activityQuestions;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
    _v = v;
  }


  Activity.fromJson(dynamic json) {
    _id = json['_id'];
    _name = json['name'];
    _course = json['course'];
    _chapter = json['chapter'];
    _section = json['section'];
    _orgId = json['orgId'];
    _additionalInfo = json['additionalInfo'];
    _isRepeatActivity = json['isRepeatActivity'];
    _isAllowUserToUploadFiles = json['isAllowUserToUploadFiles'];
    _isActive = json['isActive'];
    _isTrackActivity = json['isTrackActivity'];
    if (json['activityQuestions'] != null) {
      _activityQuestions = [];
      json['activityQuestions'].forEach((v) {
        _activityQuestions?.add(ActivityQuestions.fromJson(v));
      });
    }
    _createdAt = json['createdAt'];
    _updatedAt = json['updatedAt'];
    _v = json['__v'];
  }

  String? _id;
  String? _name;
  String? _course;
  String? _chapter;
  String? _section;
  String? _orgId;
  String? _additionalInfo;
  bool? _isRepeatActivity;
  bool? _isAllowUserToUploadFiles;
  bool? _isActive;
  bool? _isTrackActivity;
  List<ActivityQuestions>? _activityQuestions;
  String? _createdAt;
  String? _updatedAt;
  int? _v;

  String? get id => _id;

  String? get name => _name;

  String? get course => _course;

  String? get chapter => _chapter;

  String? get section => _section;

  String? get orgId => _orgId;

  String? get additionalInfo => _additionalInfo;

  bool? get isRepeatActivity => _isRepeatActivity;

  bool? get isAllowUserToUploadFiles => _isAllowUserToUploadFiles;

  bool? get isActive => _isActive;

  bool? get isTrackActivity => _isTrackActivity;

  List<ActivityQuestions>? get activityQuestions => _activityQuestions;

  String? get createdAt => _createdAt;

  String? get updatedAt => _updatedAt;

  int? get v => _v;


  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['_id'] = _id;
    map['name'] = _name;
    map['course'] = _course;
    map['chapter'] = _chapter;
    map['section'] = _section;
    map['orgId'] = _orgId;
    map['additionalInfo'] = _additionalInfo;
    map['isRepeatActivity'] = _isRepeatActivity;
    map['isAllowUserToUploadFiles'] = _isAllowUserToUploadFiles;
    map['isActive'] = _isActive;
    map['isTrackActivity'] = _isTrackActivity;
    if (_activityQuestions != null) {
      map['activityQuestions'] =
          _activityQuestions?.map((v) => v.toJson()).toList();
    }
    map['createdAt'] = _createdAt;
    map['updatedAt'] = _updatedAt;
    map['__v'] = _v;
    return map;
  }
}

class SAnswer {
  SAnswer(String? answer) {
    _answer = answer;
  }

  String? _answer;

  setAnswer(String val) {
    _answer = val;
  }

  String? get answer => _answer;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['activityAnswer'] = _answer;
    return map;
  }
}

/// answerCount : 3
/// _id : "6156d6f863a1a1001301f8c2"
/// title : "Three words or statements that make me angry"
/// type : "text"

class ActivityQuestions {
  ActivityQuestions({
    int? answerCount,
    String? id,
    String? title,
    String? type,
  }) {
    _answerCount = answerCount;
    _id = id;
    _title = title;
    _type = type;
  }

  ActivityQuestions.fromJson(dynamic json) {
    _answerCount = json['answerCount'];
    _id = json['_id'];
    _title = json['title'];
    _type = json['type'];
  }

  int? _answerCount;
  String? _id;
  String? _title;
  String? _type;
  List<SAnswer>? _answer = [];
  RxList<BodyListPart>? _bodyParts = <BodyListPart>[].obs;

  setParts(RxList<BodyListPart> value) {
    _bodyParts = value;
  }

  setAnswer(List<SAnswer> value) {
    _answer = value;
  }

  int? get answerCount => _answerCount;

  String? get id => _id;

  String? get title => _title;

  String? get type => _type;

  List<SAnswer>? get answers => _answer;

  RxList<BodyListPart>? get bodyParts => _bodyParts;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['answerCount'] = _answerCount;
    map['_id'] = _id;
    map['title'] = _title;
    map['type'] = _type;
    map['activityAnswers'] = _answer;
    return map;
  }
}

class BodyListPart {
  String _partName;
  bool _selected;

  BodyListPart(this._partName, this._selected);

  String? get partName => _partName;

  bool? get selected => _selected;

  setSelected(bool value) {
    _selected = value;
  }
}
