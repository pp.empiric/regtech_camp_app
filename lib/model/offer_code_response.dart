class OfferCodeResponse {
  String? activeStatus;
  dynamic availedCount;
  String? id;
  String? offerCode;
  String? offerType;
  PrimaryCourse? primaryCourse;
  dynamic primaryDiscount;
  String? primaryDiscountType;
  PrimaryItem? primaryItem;
  String? primaryItemCategory;
  dynamic secondaryDiscount;
  dynamic secondaryDiscountType;
  dynamic secondaryItem;
  dynamic secondaryItemCategory;
  dynamic secondaryItemCount;
  dynamic startDate;
  dynamic subTitle;
  String? title;

  OfferCodeResponse(
      {this.activeStatus,
        this.availedCount,
        this.id,
        this.offerCode,
        this.offerType,
        this.primaryCourse,
        this.primaryDiscount,
        this.primaryDiscountType,
        this.primaryItem,
        this.primaryItemCategory,
        this.secondaryDiscount,
        this.secondaryDiscountType,
        this.secondaryItem,
        this.secondaryItemCategory,
        this.secondaryItemCount,
        this.startDate,
        this.subTitle,
        this.title});

  OfferCodeResponse.fromJson(Map<String, dynamic> json) {
    activeStatus = json['activeStatus'];
    availedCount = json['availedCount'];
    id = json['id'];
    offerCode = json['offerCode'];
    offerType = json['offerType'];
    primaryCourse = json['primaryCourse'] != null
        ?  PrimaryCourse.fromJson(json['primaryCourse'])
        : null;
    primaryDiscount = json['primaryDiscount'];
    primaryDiscountType = json['primaryDiscountType'];
    primaryItem = json['primaryItem'] != null
        ?  PrimaryItem.fromJson(json['primaryItem'])
        : null;
    primaryItemCategory = json['primaryItemCategory'];
    secondaryDiscount = json['secondaryDiscount'];
    secondaryDiscountType = json['secondaryDiscountType'];
    secondaryItem = json['secondaryItem'];
    secondaryItemCategory = json['secondaryItemCategory'];
    secondaryItemCount = json['secondaryItemCount'];
    startDate = json['startDate'];
    subTitle = json['subTitle'];
    title = json['title'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['activeStatus'] = activeStatus;
    data['availedCount'] = availedCount;
    data['id'] = id;
    data['offerCode'] = offerCode;
    data['offerType'] = offerType;
    if(primaryCourse != null){
      data['primaryCourse'] = primaryCourse!.toJson();
    }
    data['primaryDiscount'] = primaryDiscount;
    data['primaryDiscountType'] = primaryDiscountType;
    if (primaryItem != null) {
      data['primaryItem'] = primaryItem!.toJson();
    }
    data['primaryItemCategory'] = primaryItemCategory;
    data['secondaryDiscount'] = secondaryDiscount;
    data['secondaryDiscountType'] = secondaryDiscountType;
    data['secondaryItem'] = secondaryItem;
    data['secondaryItemCategory'] = secondaryItemCategory;
    data['secondaryItemCount'] = secondaryItemCount;
    data['startDate'] = startDate;
    data['subTitle'] = subTitle;
    data['title'] = title;
    return data;
  }
}

class PrimaryItem {
  Category? category;
  dynamic costPrice;
  String? guId;
  bool? isChargeCreditCardFees;
  bool? isOtherPrice;
  bool? isTaxable;
  String? itemType;
  String? name;
  dynamic price;


  PrimaryItem(
      {
        this.costPrice,
        this.guId,
        this.isChargeCreditCardFees,
        this.isOtherPrice,
        this.isTaxable,
        this.itemType,
        this.name,
        this.price});

  PrimaryItem.fromJson(Map<String, dynamic> json) {
    category = json['category'] != null
        ? Category.fromJson(json['category'])
        : null;
    costPrice = json['costPrice'];
    guId = json['guId'];
    isChargeCreditCardFees = json['isChargeCreditCardFees'];
    isOtherPrice = json['isOtherPrice'];
    isTaxable = json['isTaxable'];
    itemType = json['itemType'];
    name = json['name'];
    price = json['price'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (category != null) {
      data['category'] = category!.toJson();
    }
    data['costPrice'] = costPrice;
    data['guId'] = guId;
    data['isChargeCreditCardFees'] = isChargeCreditCardFees;
    data['isOtherPrice'] = isOtherPrice;
    data['isTaxable'] = isTaxable;
    data['itemType'] = itemType;
    data['name'] = name;
    data['price'] = price;
    return data;
  }
}

class Category {
  dynamic donationId;
  String? guId;
  String? name;
  String? paymentType;

  Category(
      {
        this.donationId,
        this.guId,
        this.name,
        this.paymentType});

  Category.fromJson(Map<String, dynamic> json) {
    donationId = json['donationId'];
    guId = json['guId'];
    name = json['name'];
    paymentType = json['paymentType'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['donationId'] = donationId;
    data['guId'] = guId;
    data['name'] = name;
    data['paymentType'] = paymentType;
    return data;
  }
}


class PrimaryCourse {
  List<CourseSections>? courseSections;
  String? donationId;
  String? guId;
  String? name;
  String? paymentType;
  dynamic products;
  String? title;

  PrimaryCourse(
        this.courseSections,
        this.name,
        this.paymentType,
        this.products,
        this.title);

  PrimaryCourse.fromJson(Map<String, dynamic> json) {
    if (json['courseSections'] != null) {
      courseSections = [];
      json['courseSections'].forEach((v) {
        courseSections!.add(CourseSections.fromJson(v));
      });
    }
    donationId = json['donationId'];
    guId = json['guId'];
    name = json['name'];
    paymentType = json['paymentType'];
    products = json['products'];
    title = json['title'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (courseSections != null) {
      data['courseSections'] =
          courseSections!.map((v) => v.toJson()).toList();
    }
    data['donationId'] = donationId;
    data['guId'] = guId;
    data['name'] = name;
    data['paymentType'] = paymentType;
    data['products'] = products;
    data['title'] = title;
    return data;
  }
}

class CourseSections {
  List<Chapters>? chapters;
  String? course;
  String? guId;
  String? title;

  CourseSections(
      {this.chapters,
        this.course,
        this.guId,
        this.title});

  CourseSections.fromJson(Map<String, dynamic> json) {
    if (json['chapters'] != null) {
      chapters = [];
      json['chapters'].forEach((v) {
        chapters!.add(Chapters.fromJson(v));
      });
    }
    course = json['course'];
    guId = json['guId'];
    title = json['title'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (chapters != null) {
      data['chapters'] = chapters!.map((v) => v.toJson()).toList();
    }
    data['course'] = course;
    data['guId'] = guId;
    data['title'] = title;
    return data;
  }
}

class Chapters {
  String? guId;
  String? title;

  Chapters(this.guId, this.title);

  Chapters.fromJson(Map<String, dynamic> json) {
    guId = json['guId'];
    title = json['title'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['guId'] = guId;
    data['title'] = title;
    return data;
  }
}



