class CustomFieldModel {
  String? _guId;
  String? _value;

  String? get guId => _guId;

  String? get isActive => _value;

  CustomFieldModel(
    String? guId,
    String? value,
  ) {
    _guId = guId;
    _value = value;
  }

  CustomFieldModel.fromJson(dynamic json) {
    _guId = json['guId'];
    _value = json['value'];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['guId'] = _guId;
    map['value'] = _value;
    return map;
  }
}
