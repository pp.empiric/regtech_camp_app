class ItemInvoice {
  num? balanceAmount;
  num? cardFees;
  num? dateCreated;
  String? dateCreatedStr;
  dynamic discountAmount;
  String? guId;
  num? invoiceAmount;
  bool? isDonationInvoice;
  bool? isItemReturned;
  List<Items> items = [];
  String? notes;
  dynamic offerCode;
  num? paidAmount;
  List<Payments> payments = [];
  num? subTotal;
  num? subscriptionAmount;
  num? taxes;
  User? user;

  ItemInvoice();


  ItemInvoice.fromJson(Map<String, dynamic> json) {
    balanceAmount = json['balanceAmount'].toDouble();
    cardFees = json['cardFees'];
    dateCreated = json['dateCreated'];
    dateCreatedStr = json['dateCreatedStr'];
    discountAmount = json['discountAmount'];
    guId = json['guId'];
    invoiceAmount = json['invoiceAmount'];
    isDonationInvoice = json['isDonationInvoice'];
    isItemReturned = json['isItemReturned'];
    if (json['items'] != null) {
      items = [];
      json['items'].forEach((v) {
        items.add(Items.fromJson(v));
      });
    }
    notes = json['notes'];
    offerCode = json['offerCode'];
    paidAmount = json['paidAmount'];
    if (json['payments'] != null) {
      payments = [];
      json['payments'].forEach((v) {
        payments.add(Payments.fromJson(v));
      });
    }
    subTotal = json['subTotal'];
    subscriptionAmount = json['subscriptionAmount'];
    taxes = json['taxes'];
    user = json['user'] != null ? User.fromJson(json['user']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['balanceAmount'] = balanceAmount;
    data['cardFees'] = cardFees;
    data['dateCreated'] = dateCreated;
    data['dateCreatedStr'] = dateCreatedStr;
    data['discountAmount'] = discountAmount;
    data['guId'] = guId;
    data['invoiceAmount'] = invoiceAmount;
    data['isDonationInvoice'] = isDonationInvoice;
    data['isItemReturned'] = isItemReturned;
    if (items.isNotEmpty) {
      data['items'] = items.map((v) => v.toJson()).toList();
    }
    data['notes'] = notes;
    data['offerCode'] = offerCode;
    data['paidAmount'] = paidAmount;
    if (payments.isNotEmpty) {
      data['payments'] = payments.map((v) => v.toJson()).toList();
    }
    data['subTotal'] = subTotal;
    data['subscriptionAmount'] = subscriptionAmount;
    data['taxes'] = taxes;
    if (user != null) {
      data['user'] = user!.toJson();
    }
    return data;
  }
}

class Items {
  String? guId;
  String? invoiceId;
  bool? isItemReturned;
  bool? isSubscription;
  num? itemTotal;
  String? name;
  String? note;
  dynamic offerCode;
  num? price;
  num? qty;

  Items();

  Items.fromJson(Map<String, dynamic> json) {
    guId = json['guId'];
    invoiceId = json['invoiceId'];
    isItemReturned = json['isItemReturned'];
    isSubscription = json['isSubscription'];
    itemTotal = json['itemTotal'];
    name = json['name'];
    note = json['note'];
    offerCode = json['offerCode'];
    price = json['price'];
    qty = json['qty'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['guId'] = guId;
    data['invoiceId'] = invoiceId;
    data['isItemReturned'] = isItemReturned;
    data['isSubscription'] = isSubscription;
    data['itemTotal'] = itemTotal;
    data['name'] = name;
    data['note'] = note;
    data['offerCode'] = offerCode;
    data['price'] = price;
    data['qty'] = qty;
    return data;
  }
}


class User {
  String? email;
  String? guId;
  String? imageUrl;
  dynamic lastName;
  String? name;
  dynamic phone;
  dynamic tenant;



  User.fromJson(Map<String, dynamic> json) {
    email = json['email'];
    guId = json['guId'];
    imageUrl = json['imageUrl'];
    lastName = json['lastName'];
    name = json['name'];
    phone = json['phone'];
    tenant = json['tenant'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['email'] = email;
    data['guId'] = guId;
    data['imageUrl'] = imageUrl;
    data['lastName'] = lastName;
    data['name'] = name;
    data['phone'] = phone;
    data['tenant'] = tenant;
    return data;
  }
}

class Payments {
  num? amount;
  String? cardTransId;
  num? dateCreated;
  String? dateCreatedStr;
  String? guId;
  bool? isRecurring;
  bool? isRefundPayment;
  String? paymentString;
  String? paymentType;
  dynamic referenceInvoiceId;



  Payments.fromJson(Map<String, dynamic> json) {
    amount = json['amount'];
    cardTransId = json['cardTransId'];
    dateCreated = json['dateCreated'];
    dateCreatedStr = json['dateCreatedStr'];
    guId = json['guId'];
    isRecurring = json['isRecurring'];
    isRefundPayment = json['isRefundPayment'];
    paymentString = json['paymentString'];
    paymentType = json['paymentType'];
    referenceInvoiceId = json['referenceInvoiceId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['amount'] = amount;
    data['cardTransId'] = cardTransId;
    data['dateCreated'] = dateCreated;
    data['dateCreatedStr'] = dateCreatedStr;
    data['guId'] = guId;
    data['isRecurring'] = isRecurring;
    data['isRefundPayment'] = isRefundPayment;
    data['paymentString'] = paymentString;
    data['paymentType'] = paymentType;
    data['referenceInvoiceId'] = referenceInvoiceId;
    return data;
  }
}




