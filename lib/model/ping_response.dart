/// apkUrl : "https://drive.google.com/u/0/uc?id=1JFOcY7-F_3q-5tTtwQFM4n3mF9PBS37p&export=download"
/// authMode : "FUSION_AUTH"
/// bankFees : null
/// bannerGradientColor1 : "#dedede"
/// bannerGradientColor2 : "#bdbdbd"
/// bannerImage : "https://res.cloudinary.com/wajooba/image/upload/c_fill/v1611731845/g8bkk4u3jpntdhxoq1py.jpg"
/// bigLogo : "https://res.cloudinary.com/wajooba/image/upload/c_fill/v1630387266/odigod82di3bwbjlbuhh.jpg"
/// buyButtonLabel : "Buy"
/// calendarVisibility : 0
/// cardFees : null
/// cloudName : "wajooba"
/// cssUrl : "https://d3bjn9bo4wzv3f.cloudfront.net/pink_1604053244.css"
/// csymbol : "₹"
/// customDomain : null
/// email : "rayatprabodhini@gmail.com"
/// environmentName : "prod"
/// fAuthApplicationId : "1c62e4e7-d3e6-4152-8b49-a3dd97283e8c"
/// fAuthTenantId : "7e4e37aa-bb0c-4c59-8a01-075611652ac4"
/// fLoginUrl : "https://fauth.wajooba.com/oauth2/authorize?client_id=1c62e4e7-d3e6-4152-8b49-a3dd97283e8c&response_type=code&redirect_uri=https://rayat.wajooba.com"
/// fLogoutUrl : "https://fauth.wajooba.com/oauth2/logout?client_id=1c62e4e7-d3e6-4152-8b49-a3dd97283e8c&post_logout_redirect_uri=https://rayat.wajooba.com"
/// facebookAppId : "1048532435589501"
/// fusionauthApiKey : "hPRxw4AAdxpE2n1aPCANJk8AYS6GkkWHitJaXIVQNPil_Pt5IJ3dZZuL"
/// googleClientId : "1004414895808-1sn9g3p6057fjsi3laqi3f78ifvnk336"
/// isAddressNeededForEvent : false
/// isDonationPublicCheckout : true
/// isExternalWebsite : true
/// isMasterFranchise : false
/// isOtpAllowed : false
/// isRegAndPurchaseOnSamePage : false
/// isShowCalenderView : true
/// isShowCourses : true
/// isShowDonation : false
/// isShowGuardian : false
/// isShowOnlineZoomMeeting : false
/// isShowRegistrationLink : true
/// isShowRoomName : true
/// isShowSchedule : true
/// isShowScheduleMenu : true
/// isShowSecondGuardian : true
/// isShowSidebar : true
/// isShowStoreMenu : true
/// isShowWorkshops : true
/// isSupportGrn : false
/// isTermsAgreed : false
/// isValid : true
/// isWaiverFormToBeSigned : true
/// logo : "v1630387266/odigod82di3bwbjlbuhh.jpg"
/// name : "Rayat Prabodhini"
/// notificationOnesignalAppId : "1d1ec8b6-0b67-494d-a8a3-57bddf674538"
/// orgGuId : "ngB0NkJA5e"
/// orgId : "rayat"
/// page1 : "Home::http://www.rayatprabodhini.com/"
/// page2 : "::"
/// page3 : "::"
/// page4 : "::"
/// page5 : "::"
/// paymentProvider : "PAYTM"
/// phone : "+91-9762131361"
/// plantype : "free_plan"
/// primaryGuardianLabel : "Mothers Name"
/// promotionLabel : "Courses"
/// registerButtonLabel : "Register"
/// registerSuccessMessage : ""
/// registrationLinkName : "Student Registration Portal"
/// restrictWeeklyCalendar : false
/// scheduleLabel : "Schedule"
/// secondaryGuardianLabel : "Fathers Name"
/// seoDescription : null
/// seoKeywords : null
/// smallLogo : "https://res.cloudinary.com/wajooba/image/upload/c_fill,h_23,w_113/v1630387266/odigod82di3bwbjlbuhh.jpg"
/// stripeApiKey : "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"
/// stripeAuthCode : null
/// stripeClientId : "ca_IHljDXQDd2HSe0I0xS9pHu0DSMtkSYMV"
/// stripePublishableKey : "pk_live_mJmuAnCW5jPNjoGnHHTjhd7800PnKb1HRH"
/// student : {"labels":[{"name":"Courses","imageUrl":"https://d3bjn9bo4wzv3f.cloudfront.net/bottom_menu_courses.svg","position":0.0,"type":"Course"},{"name":"Schedule","imageUrl":"https://d3bjn9bo4wzv3f.cloudfront.net/bottom_menu_schedule.svg","position":1.0,"type":"Schedule"},{"name":"Study Material","imageUrl":"https://d3bjn9bo4wzv3f.cloudfront.net/bottom_menu_morefiles.svg","position":2.0,"type":"My Files"},{"name":"Profile","imageUrl":"https://d3bjn9bo4wzv3f.cloudfront.net/bottom_menu_profile.svg","position":3.0,"type":"Others"}],"primaryColor":"#406fbd","welcomeImages":["https://res.cloudinary.com/wajooba/image/upload/v1627119444/uiea9reh1ziromv7duvl.jpg","https://res.cloudinary.com/wajooba/image/upload/v1627119452/e1qprufy6yjwh92fjpjy.jpg","https://res.cloudinary.com/wajooba/image/upload/v1627119452/v37y9pv3kcd0y18o2jym.jpg"]}
/// tabLabel : "Guardians"
/// tagLine : null
/// taxPercent : 0.00
/// tenantAuthViewCmd : {"clientId":"FRX2aOph0m7dXk9F1ZXG2Z7mHFEVCste","domain":"wajooba.auth0.com"}
/// timezone : "Asia/Kolkata"
/// version : "v2.7-112321-prod"
/// waiverFormLink : null
/// website : "http://www.rayatprabodhini.com/"
/// workshopLabel : "Upcoming Events"
/// isAuthValid : false

class PingModel {
  PingModel({
      String? apkUrl, 
      String? authMode, 
      dynamic bankFees, 
      String? bannerGradientColor1, 
      String? bannerGradientColor2, 
      String? bannerImage, 
      String? bigLogo, 
      String? buyButtonLabel, 
      int? calendarVisibility, 
      dynamic cardFees, 
      String? cloudName, 
      String? cssUrl, 
      String? csymbol, 
      dynamic customDomain, 
      String? email, 
      String? environmentName, 
      String? fAuthApplicationId, 
      String? fAuthTenantId, 
      String? fLoginUrl, 
      String? fLogoutUrl, 
      String? facebookAppId, 
      String? fusionauthApiKey, 
      String? googleClientId, 
      bool? isAddressNeededForEvent, 
      bool? isDonationPublicCheckout, 
      bool? isExternalWebsite, 
      bool? isMasterFranchise, 
      bool? isOtpAllowed, 
      bool? isRegAndPurchaseOnSamePage, 
      bool? isShowCalenderView, 
      bool? isShowCourses, 
      bool? isShowDonation, 
      bool? isShowGuardian, 
      bool? isShowOnlineZoomMeeting, 
      bool? isShowRegistrationLink, 
      bool? isShowRoomName, 
      bool? isShowSchedule, 
      bool? isShowScheduleMenu, 
      bool? isShowSecondGuardian, 
      bool? isShowSidebar, 
      bool? isShowStoreMenu, 
      bool? isShowWorkshops, 
      bool? isSupportGrn, 
      bool? isTermsAgreed, 
      bool? isValid, 
      bool? isWaiverFormToBeSigned, 
      String? logo, 
      String? name, 
      String? notificationOnesignalAppId, 
      String? orgGuId, 
      String? orgId, 
      String? page1, 
      String? page2, 
      String? page3, 
      String? page4, 
      String? page5, 
      String? paymentProvider, 
      String? phone, 
      String? plantype, 
      String? primaryGuardianLabel, 
      String? promotionLabel, 
      String? registerButtonLabel, 
      String? registerSuccessMessage, 
      String? registrationLinkName, 
      bool? restrictWeeklyCalendar, 
      String? scheduleLabel, 
      String? secondaryGuardianLabel, 
      dynamic seoDescription, 
      dynamic seoKeywords, 
      String? smallLogo, 
      String? stripeApiKey, 
      dynamic stripeAuthCode, 
      String? stripeClientId, 
      String? stripePublishableKey, 
      Student? student, 
      String? tabLabel, 
      dynamic tagLine, 
      double? taxPercent, 
      TenantAuthViewCmd? tenantAuthViewCmd, 
      String? timezone, 
      String? version, 
      dynamic waiverFormLink, 
      String? website, 
      String? workshopLabel, 
      bool? isAuthValid,}){
    _apkUrl = apkUrl;
    _authMode = authMode;
    _bankFees = bankFees;
    _bannerGradientColor1 = bannerGradientColor1;
    _bannerGradientColor2 = bannerGradientColor2;
    _bannerImage = bannerImage;
    _bigLogo = bigLogo;
    _buyButtonLabel = buyButtonLabel;
    _calendarVisibility = calendarVisibility;
    _cardFees = cardFees;
    _cloudName = cloudName;
    _cssUrl = cssUrl;
    _csymbol = csymbol;
    _customDomain = customDomain;
    _email = email;
    _environmentName = environmentName;
    _fAuthApplicationId = fAuthApplicationId;
    _fAuthTenantId = fAuthTenantId;
    _fLoginUrl = fLoginUrl;
    _fLogoutUrl = fLogoutUrl;
    _facebookAppId = facebookAppId;
    _fusionauthApiKey = fusionauthApiKey;
    _googleClientId = googleClientId;
    _isAddressNeededForEvent = isAddressNeededForEvent;
    _isDonationPublicCheckout = isDonationPublicCheckout;
    _isExternalWebsite = isExternalWebsite;
    _isMasterFranchise = isMasterFranchise;
    _isOtpAllowed = isOtpAllowed;
    _isRegAndPurchaseOnSamePage = isRegAndPurchaseOnSamePage;
    _isShowCalenderView = isShowCalenderView;
    _isShowCourses = isShowCourses;
    _isShowDonation = isShowDonation;
    _isShowGuardian = isShowGuardian;
    _isShowOnlineZoomMeeting = isShowOnlineZoomMeeting;
    _isShowRegistrationLink = isShowRegistrationLink;
    _isShowRoomName = isShowRoomName;
    _isShowSchedule = isShowSchedule;
    _isShowScheduleMenu = isShowScheduleMenu;
    _isShowSecondGuardian = isShowSecondGuardian;
    _isShowSidebar = isShowSidebar;
    _isShowStoreMenu = isShowStoreMenu;
    _isShowWorkshops = isShowWorkshops;
    _isSupportGrn = isSupportGrn;
    _isTermsAgreed = isTermsAgreed;
    _isValid = isValid;
    _isWaiverFormToBeSigned = isWaiverFormToBeSigned;
    _logo = logo;
    _name = name;
    _notificationOnesignalAppId = notificationOnesignalAppId;
    _orgGuId = orgGuId;
    _orgId = orgId;
    _page1 = page1;
    _page2 = page2;
    _page3 = page3;
    _page4 = page4;
    _page5 = page5;
    _paymentProvider = paymentProvider;
    _phone = phone;
    _plantype = plantype;
    _primaryGuardianLabel = primaryGuardianLabel;
    _promotionLabel = promotionLabel;
    _registerButtonLabel = registerButtonLabel;
    _registerSuccessMessage = registerSuccessMessage;
    _registrationLinkName = registrationLinkName;
    _restrictWeeklyCalendar = restrictWeeklyCalendar;
    _scheduleLabel = scheduleLabel;
    _secondaryGuardianLabel = secondaryGuardianLabel;
    _seoDescription = seoDescription;
    _seoKeywords = seoKeywords;
    _smallLogo = smallLogo;
    _stripeApiKey = stripeApiKey;
    _stripeAuthCode = stripeAuthCode;
    _stripeClientId = stripeClientId;
    _stripePublishableKey = stripePublishableKey;
    _student = student;
    _tabLabel = tabLabel;
    _tagLine = tagLine;
    _taxPercent = taxPercent;
    _tenantAuthViewCmd = tenantAuthViewCmd;
    _timezone = timezone;
    _version = version;
    _waiverFormLink = waiverFormLink;
    _website = website;
    _workshopLabel = workshopLabel;
    _isAuthValid = isAuthValid;
}

  PingModel.fromJson(dynamic json) {
    _apkUrl = json['apkUrl'];
    _authMode = json['authMode'];
    _bankFees = json['bankFees'];
    _bannerGradientColor1 = json['bannerGradientColor1'];
    _bannerGradientColor2 = json['bannerGradientColor2'];
    _bannerImage = json['bannerImage'];
    _bigLogo = json['bigLogo'];
    _buyButtonLabel = json['buyButtonLabel'];
    _calendarVisibility = json['calendarVisibility'];
    _cardFees = json['cardFees'];
    _cloudName = json['cloudName'];
    _cssUrl = json['cssUrl'];
    _csymbol = json['csymbol'];
    _customDomain = json['customDomain'];
    _email = json['email'];
    _environmentName = json['environmentName'];
    _fAuthApplicationId = json['fAuthApplicationId'];
    _fAuthTenantId = json['fAuthTenantId'];
    _fLoginUrl = json['fLoginUrl'];
    _fLogoutUrl = json['fLogoutUrl'];
    _facebookAppId = json['facebookAppId'];
    _fusionauthApiKey = json['fusionauthApiKey'];
    _googleClientId = json['googleClientId'];
    _isAddressNeededForEvent = json['isAddressNeededForEvent'];
    _isDonationPublicCheckout = json['isDonationPublicCheckout'];
    _isExternalWebsite = json['isExternalWebsite'];
    _isMasterFranchise = json['isMasterFranchise'];
    _isOtpAllowed = json['isOtpAllowed'];
    _isRegAndPurchaseOnSamePage = json['isRegAndPurchaseOnSamePage'];
    _isShowCalenderView = json['isShowCalenderView'];
    _isShowCourses = json['isShowCourses'];
    _isShowDonation = json['isShowDonation'];
    _isShowGuardian = json['isShowGuardian'];
    _isShowOnlineZoomMeeting = json['isShowOnlineZoomMeeting'];
    _isShowRegistrationLink = json['isShowRegistrationLink'];
    _isShowRoomName = json['isShowRoomName'];
    _isShowSchedule = json['isShowSchedule'];
    _isShowScheduleMenu = json['isShowScheduleMenu'];
    _isShowSecondGuardian = json['isShowSecondGuardian'];
    _isShowSidebar = json['isShowSidebar'];
    _isShowStoreMenu = json['isShowStoreMenu'];
    _isShowWorkshops = json['isShowWorkshops'];
    _isSupportGrn = json['isSupportGrn'];
    _isTermsAgreed = json['isTermsAgreed'];
    _isValid = json['isValid'];
    _isWaiverFormToBeSigned = json['isWaiverFormToBeSigned'];
    _logo = json['logo'];
    _name = json['name'];
    _notificationOnesignalAppId = json['notificationOnesignalAppId'];
    _orgGuId = json['orgGuId'];
    _orgId = json['orgId'];
    _page1 = json['page1'];
    _page2 = json['page2'];
    _page3 = json['page3'];
    _page4 = json['page4'];
    _page5 = json['page5'];
    _paymentProvider = json['paymentProvider'];
    _phone = json['phone'];
    _plantype = json['plantype'];
    _primaryGuardianLabel = json['primaryGuardianLabel'];
    _promotionLabel = json['promotionLabel'];
    _registerButtonLabel = json['registerButtonLabel'];
    _registerSuccessMessage = json['registerSuccessMessage'];
    _registrationLinkName = json['registrationLinkName'];
    _restrictWeeklyCalendar = json['restrictWeeklyCalendar'];
    _scheduleLabel = json['scheduleLabel'];
    _secondaryGuardianLabel = json['secondaryGuardianLabel'];
    _seoDescription = json['seoDescription'];
    _seoKeywords = json['seoKeywords'];
    _smallLogo = json['smallLogo'];
    _stripeApiKey = json['stripeApiKey'];
    _stripeAuthCode = json['stripeAuthCode'];
    _stripeClientId = json['stripeClientId'];
    _stripePublishableKey = json['stripePublishableKey'];
    _student = json['student'] != null ? Student.fromJson(json['student']) : null;
    _tabLabel = json['tabLabel'];
    _tagLine = json['tagLine'];
    _taxPercent = json['taxPercent'];
    _tenantAuthViewCmd = json['tenantAuthViewCmd'] != null ? TenantAuthViewCmd.fromJson(json['tenantAuthViewCmd']) : null;
    _timezone = json['timezone'];
    _version = json['version'];
    _waiverFormLink = json['waiverFormLink'];
    _website = json['website'];
    _workshopLabel = json['workshopLabel'];
    _isAuthValid = json['isAuthValid'];
  }
  String? _apkUrl;
  String? _authMode;
  dynamic _bankFees;
  String? _bannerGradientColor1;
  String? _bannerGradientColor2;
  String? _bannerImage;
  String? _bigLogo;
  String? _buyButtonLabel;
  int? _calendarVisibility;
  dynamic _cardFees;
  String? _cloudName;
  String? _cssUrl;
  String? _csymbol;
  dynamic _customDomain;
  String? _email;
  String? _environmentName;
  String? _fAuthApplicationId;
  String? _fAuthTenantId;
  String? _fLoginUrl;
  String? _fLogoutUrl;
  String? _facebookAppId;
  String? _fusionauthApiKey;
  String? _googleClientId;
  bool? _isAddressNeededForEvent;
  bool? _isDonationPublicCheckout;
  bool? _isExternalWebsite;
  bool? _isMasterFranchise;
  bool? _isOtpAllowed;
  bool? _isRegAndPurchaseOnSamePage;
  bool? _isShowCalenderView;
  bool? _isShowCourses;
  bool? _isShowDonation;
  bool? _isShowGuardian;
  bool? _isShowOnlineZoomMeeting;
  bool? _isShowRegistrationLink;
  bool? _isShowRoomName;
  bool? _isShowSchedule;
  bool? _isShowScheduleMenu;
  bool? _isShowSecondGuardian;
  bool? _isShowSidebar;
  bool? _isShowStoreMenu;
  bool? _isShowWorkshops;
  bool? _isSupportGrn;
  bool? _isTermsAgreed;
  bool? _isValid;
  bool? _isWaiverFormToBeSigned;
  String? _logo;
  String? _name;
  String? _notificationOnesignalAppId;
  String? _orgGuId;
  String? _orgId;
  String? _page1;
  String? _page2;
  String? _page3;
  String? _page4;
  String? _page5;
  String? _paymentProvider;
  String? _phone;
  String? _plantype;
  String? _primaryGuardianLabel;
  String? _promotionLabel;
  String? _registerButtonLabel;
  String? _registerSuccessMessage;
  String? _registrationLinkName;
  bool? _restrictWeeklyCalendar;
  String? _scheduleLabel;
  String? _secondaryGuardianLabel;
  dynamic _seoDescription;
  dynamic _seoKeywords;
  String? _smallLogo;
  String? _stripeApiKey;
  dynamic _stripeAuthCode;
  String? _stripeClientId;
  String? _stripePublishableKey;
  Student? _student;
  String? _tabLabel;
  dynamic _tagLine;
  double? _taxPercent;
  TenantAuthViewCmd? _tenantAuthViewCmd;
  String? _timezone;
  String? _version;
  dynamic _waiverFormLink;
  String? _website;
  String? _workshopLabel;
  bool? _isAuthValid;

  String? get apkUrl => _apkUrl;
  String? get authMode => _authMode;
  dynamic get bankFees => _bankFees;
  String? get bannerGradientColor1 => _bannerGradientColor1;
  String? get bannerGradientColor2 => _bannerGradientColor2;
  String? get bannerImage => _bannerImage;
  String? get bigLogo => _bigLogo;
  String? get buyButtonLabel => _buyButtonLabel;
  int? get calendarVisibility => _calendarVisibility;
  dynamic get cardFees => _cardFees;
  String? get cloudName => _cloudName;
  String? get cssUrl => _cssUrl;
  String? get csymbol => _csymbol;
  dynamic get customDomain => _customDomain;
  String? get email => _email;
  String? get environmentName => _environmentName;
  String? get fAuthApplicationId => _fAuthApplicationId;
  String? get fAuthTenantId => _fAuthTenantId;
  String? get fLoginUrl => _fLoginUrl;
  String? get fLogoutUrl => _fLogoutUrl;
  String? get facebookAppId => _facebookAppId;
  String? get fusionauthApiKey => _fusionauthApiKey;
  String? get googleClientId => _googleClientId;
  bool? get isAddressNeededForEvent => _isAddressNeededForEvent;
  bool? get isDonationPublicCheckout => _isDonationPublicCheckout;
  bool? get isExternalWebsite => _isExternalWebsite;
  bool? get isMasterFranchise => _isMasterFranchise;
  bool? get isOtpAllowed => _isOtpAllowed;
  bool? get isRegAndPurchaseOnSamePage => _isRegAndPurchaseOnSamePage;
  bool? get isShowCalenderView => _isShowCalenderView;
  bool? get isShowCourses => _isShowCourses;
  bool? get isShowDonation => _isShowDonation;
  bool? get isShowGuardian => _isShowGuardian;
  bool? get isShowOnlineZoomMeeting => _isShowOnlineZoomMeeting;
  bool? get isShowRegistrationLink => _isShowRegistrationLink;
  bool? get isShowRoomName => _isShowRoomName;
  bool? get isShowSchedule => _isShowSchedule;
  bool? get isShowScheduleMenu => _isShowScheduleMenu;
  bool? get isShowSecondGuardian => _isShowSecondGuardian;
  bool? get isShowSidebar => _isShowSidebar;
  bool? get isShowStoreMenu => _isShowStoreMenu;
  bool? get isShowWorkshops => _isShowWorkshops;
  bool? get isSupportGrn => _isSupportGrn;
  bool? get isTermsAgreed => _isTermsAgreed;
  bool? get isValid => _isValid;
  bool? get isWaiverFormToBeSigned => _isWaiverFormToBeSigned;
  String? get logo => _logo;
  String? get name => _name;
  String? get notificationOnesignalAppId => _notificationOnesignalAppId;
  String? get orgGuId => _orgGuId;
  String? get orgId => _orgId;
  String? get page1 => _page1;
  String? get page2 => _page2;
  String? get page3 => _page3;
  String? get page4 => _page4;
  String? get page5 => _page5;
  String? get paymentProvider => _paymentProvider;
  String? get phone => _phone;
  String? get plantype => _plantype;
  String? get primaryGuardianLabel => _primaryGuardianLabel;
  String? get promotionLabel => _promotionLabel;
  String? get registerButtonLabel => _registerButtonLabel;
  String? get registerSuccessMessage => _registerSuccessMessage;
  String? get registrationLinkName => _registrationLinkName;
  bool? get restrictWeeklyCalendar => _restrictWeeklyCalendar;
  String? get scheduleLabel => _scheduleLabel;
  String? get secondaryGuardianLabel => _secondaryGuardianLabel;
  dynamic get seoDescription => _seoDescription;
  dynamic get seoKeywords => _seoKeywords;
  String? get smallLogo => _smallLogo;
  String? get stripeApiKey => _stripeApiKey;
  dynamic get stripeAuthCode => _stripeAuthCode;
  String? get stripeClientId => _stripeClientId;
  String? get stripePublishableKey => _stripePublishableKey;
  Student? get student => _student;
  String? get tabLabel => _tabLabel;
  dynamic get tagLine => _tagLine;
  double? get taxPercent => _taxPercent;
  TenantAuthViewCmd? get tenantAuthViewCmd => _tenantAuthViewCmd;
  String? get timezone => _timezone;
  String? get version => _version;
  dynamic get waiverFormLink => _waiverFormLink;
  String? get website => _website;
  String? get workshopLabel => _workshopLabel;
  bool? get isAuthValid => _isAuthValid;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['apkUrl'] = _apkUrl;
    map['authMode'] = _authMode;
    map['bankFees'] = _bankFees;
    map['bannerGradientColor1'] = _bannerGradientColor1;
    map['bannerGradientColor2'] = _bannerGradientColor2;
    map['bannerImage'] = _bannerImage;
    map['bigLogo'] = _bigLogo;
    map['buyButtonLabel'] = _buyButtonLabel;
    map['calendarVisibility'] = _calendarVisibility;
    map['cardFees'] = _cardFees;
    map['cloudName'] = _cloudName;
    map['cssUrl'] = _cssUrl;
    map['csymbol'] = _csymbol;
    map['customDomain'] = _customDomain;
    map['email'] = _email;
    map['environmentName'] = _environmentName;
    map['fAuthApplicationId'] = _fAuthApplicationId;
    map['fAuthTenantId'] = _fAuthTenantId;
    map['fLoginUrl'] = _fLoginUrl;
    map['fLogoutUrl'] = _fLogoutUrl;
    map['facebookAppId'] = _facebookAppId;
    map['fusionauthApiKey'] = _fusionauthApiKey;
    map['googleClientId'] = _googleClientId;
    map['isAddressNeededForEvent'] = _isAddressNeededForEvent;
    map['isDonationPublicCheckout'] = _isDonationPublicCheckout;
    map['isExternalWebsite'] = _isExternalWebsite;
    map['isMasterFranchise'] = _isMasterFranchise;
    map['isOtpAllowed'] = _isOtpAllowed;
    map['isRegAndPurchaseOnSamePage'] = _isRegAndPurchaseOnSamePage;
    map['isShowCalenderView'] = _isShowCalenderView;
    map['isShowCourses'] = _isShowCourses;
    map['isShowDonation'] = _isShowDonation;
    map['isShowGuardian'] = _isShowGuardian;
    map['isShowOnlineZoomMeeting'] = _isShowOnlineZoomMeeting;
    map['isShowRegistrationLink'] = _isShowRegistrationLink;
    map['isShowRoomName'] = _isShowRoomName;
    map['isShowSchedule'] = _isShowSchedule;
    map['isShowScheduleMenu'] = _isShowScheduleMenu;
    map['isShowSecondGuardian'] = _isShowSecondGuardian;
    map['isShowSidebar'] = _isShowSidebar;
    map['isShowStoreMenu'] = _isShowStoreMenu;
    map['isShowWorkshops'] = _isShowWorkshops;
    map['isSupportGrn'] = _isSupportGrn;
    map['isTermsAgreed'] = _isTermsAgreed;
    map['isValid'] = _isValid;
    map['isWaiverFormToBeSigned'] = _isWaiverFormToBeSigned;
    map['logo'] = _logo;
    map['name'] = _name;
    map['notificationOnesignalAppId'] = _notificationOnesignalAppId;
    map['orgGuId'] = _orgGuId;
    map['orgId'] = _orgId;
    map['page1'] = _page1;
    map['page2'] = _page2;
    map['page3'] = _page3;
    map['page4'] = _page4;
    map['page5'] = _page5;
    map['paymentProvider'] = _paymentProvider;
    map['phone'] = _phone;
    map['plantype'] = _plantype;
    map['primaryGuardianLabel'] = _primaryGuardianLabel;
    map['promotionLabel'] = _promotionLabel;
    map['registerButtonLabel'] = _registerButtonLabel;
    map['registerSuccessMessage'] = _registerSuccessMessage;
    map['registrationLinkName'] = _registrationLinkName;
    map['restrictWeeklyCalendar'] = _restrictWeeklyCalendar;
    map['scheduleLabel'] = _scheduleLabel;
    map['secondaryGuardianLabel'] = _secondaryGuardianLabel;
    map['seoDescription'] = _seoDescription;
    map['seoKeywords'] = _seoKeywords;
    map['smallLogo'] = _smallLogo;
    map['stripeApiKey'] = _stripeApiKey;
    map['stripeAuthCode'] = _stripeAuthCode;
    map['stripeClientId'] = _stripeClientId;
    map['stripePublishableKey'] = _stripePublishableKey;
    if (_student != null) {
      map['student'] = _student?.toJson();
    }
    map['tabLabel'] = _tabLabel;
    map['tagLine'] = _tagLine;
    map['taxPercent'] = _taxPercent;
    if (_tenantAuthViewCmd != null) {
      map['tenantAuthViewCmd'] = _tenantAuthViewCmd?.toJson();
    }
    map['timezone'] = _timezone;
    map['version'] = _version;
    map['waiverFormLink'] = _waiverFormLink;
    map['website'] = _website;
    map['workshopLabel'] = _workshopLabel;
    map['isAuthValid'] = _isAuthValid;
    return map;
  }

}

/// clientId : "FRX2aOph0m7dXk9F1ZXG2Z7mHFEVCste"
/// domain : "wajooba.auth0.com"

class TenantAuthViewCmd {
  TenantAuthViewCmd({
      String? clientId, 
      String? domain,}){
    _clientId = clientId;
    _domain = domain;
}

  TenantAuthViewCmd.fromJson(dynamic json) {
    _clientId = json['clientId'];
    _domain = json['domain'];
  }
  String? _clientId;
  String? _domain;

  String? get clientId => _clientId;
  String? get domain => _domain;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['clientId'] = _clientId;
    map['domain'] = _domain;
    return map;
  }

}

/// labels : [{"name":"Courses","imageUrl":"https://d3bjn9bo4wzv3f.cloudfront.net/bottom_menu_courses.svg","position":0.0,"type":"Course"},{"name":"Schedule","imageUrl":"https://d3bjn9bo4wzv3f.cloudfront.net/bottom_menu_schedule.svg","position":1.0,"type":"Schedule"},{"name":"Study Material","imageUrl":"https://d3bjn9bo4wzv3f.cloudfront.net/bottom_menu_morefiles.svg","position":2.0,"type":"My Files"},{"name":"Profile","imageUrl":"https://d3bjn9bo4wzv3f.cloudfront.net/bottom_menu_profile.svg","position":3.0,"type":"Others"}]
/// primaryColor : "#406fbd"
/// welcomeImages : ["https://res.cloudinary.com/wajooba/image/upload/v1627119444/uiea9reh1ziromv7duvl.jpg","https://res.cloudinary.com/wajooba/image/upload/v1627119452/e1qprufy6yjwh92fjpjy.jpg","https://res.cloudinary.com/wajooba/image/upload/v1627119452/v37y9pv3kcd0y18o2jym.jpg"]

class Student {
  Student({
      List<Labels>? labels, 
      String? primaryColor, 
      List<String>? welcomeImages,}){
    _labels = labels;
    _primaryColor = primaryColor;
    _welcomeImages = welcomeImages;
}

  Student.fromJson(dynamic json) {
    if (json['labels'] != null) {
      _labels = [];
      json['labels'].forEach((v) {
        _labels?.add(Labels.fromJson(v));
      });
    }
    _primaryColor = json['primaryColor'];
    _welcomeImages = json['welcomeImages'] != null ? json['welcomeImages'].cast<String>() : [];
  }
  List<Labels>? _labels;
  String? _primaryColor;
  List<String>? _welcomeImages;

  List<Labels>? get labels => _labels;
  String? get primaryColor => _primaryColor;
  List<String>? get welcomeImages => _welcomeImages;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_labels != null) {
      map['labels'] = _labels?.map((v) => v.toJson()).toList();
    }
    map['primaryColor'] = _primaryColor;
    map['welcomeImages'] = _welcomeImages;
    return map;
  }

}

/// name : "Courses"
/// imageUrl : "https://d3bjn9bo4wzv3f.cloudfront.net/bottom_menu_courses.svg"
/// position : 0.0
/// type : "Course"

class Labels {
  Labels({
      String? name, 
      String? imageUrl, 
      double? position, 
      String? type,}){
    _name = name;
    _imageUrl = imageUrl;
    _position = position;
    _type = type;
}

  Labels.fromJson(dynamic json) {
    _name = json['name'];
    _imageUrl = json['imageUrl'];
    _position = json['position'];
    _type = json['type'];
  }
  String? _name;
  String? _imageUrl;
  double? _position;
  String? _type;

  String? get name => _name;
  String? get imageUrl => _imageUrl;
  double? get position => _position;
  String? get type => _type;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['name'] = _name;
    map['imageUrl'] = _imageUrl;
    map['position'] = _position;
    map['type'] = _type;
    return map;
  }

}