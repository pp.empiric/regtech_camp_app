class PaymentModel {
  String guId = "";
  int billingDay = 0;
  int noOfBillingCycles = 0;
  String txnAmount = "";
  String subscriptionAmount = "";
  String finalSubscriptionAmount = "";
  String oneTimePayment = "";
  String finalOneTimePayment = "";
  dynamic memberShipExpire = "";

  PaymentModel(
      {required this.guId,
      required this.billingDay,
      required this.noOfBillingCycles,
      required this.txnAmount,
      required this.subscriptionAmount,
      required this.finalSubscriptionAmount,
      required this.oneTimePayment,
      required this.finalOneTimePayment,
      required this.memberShipExpire});

  PaymentModel.fromJson(Map<String, dynamic> json) {
    guId = json['guId'];
    billingDay = json['billingDay'];
    noOfBillingCycles = json['noOfBillingCycles'];
    txnAmount = json['txnAmount'];
    subscriptionAmount = json['subscriptionAmount'];
    finalSubscriptionAmount = json['finalSubscriptionAmount'];
    oneTimePayment = json['oneTimePayment'];
    finalOneTimePayment = json['finalOneTimePayment'];
    memberShipExpire = json['membershipExpEpoch'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['guId'] = guId;
    data['billingDay'] = billingDay;
    data['noOfBillingCycles'] = noOfBillingCycles;
    data['txnAmount'] = txnAmount;
    data['subscriptionAmount'] = subscriptionAmount;
    data['finalSubscriptionAmount'] = finalSubscriptionAmount;
    data['oneTimePayment'] = oneTimePayment;
    data['finalOneTimePayment'] = finalOneTimePayment;
    data['membershipExpEpoch'] = memberShipExpire;
    return data;
  }
}
