import 'package:get/get.dart';
import 'package:regtech_camp_app/model/quiz_new_response.dart';

class Chapters {
  Chapters(
      {dynamic contentAdditionalInfo,
      String? dateCreated,
      String? guId,
      bool? isActive,
      bool? isNote,
      bool? isPublished,
      String? lastUpdated,
      String? lessonType,
      String? section,
      int? sequence,
      String? title,
      RxBool? isCompleted,
       QuizNewResponse? chapterResponse,
      RxString? cousreId}) {
    _contentAdditionalInfo = contentAdditionalInfo;
    _dateCreated = dateCreated;
    _guId = guId;
    _isActive = isActive;
    _isNote = isNote;
    _isPublished = isPublished;
    _lastUpdated = lastUpdated;
    _lessonType = lessonType;
    _section = section;
    _sequence = sequence;
    _title = title;

  }

  Chapters.fromJson(dynamic json) {
    _contentAdditionalInfo = json['contentAdditionalInfo'];
    _dateCreated = json['dateCreated'];
    _guId = json['guId'];
    _isActive = json['isActive'];
    _isNote = json['isNote'];
    _isPublished = json['isPublished'];
    _lastUpdated = json['lastUpdated'];
    _lessonType = json['lessonType'];
    _section = json['section'];
    _sequence = json['sequence'];
    _title = json['title'];
  }

  dynamic _contentAdditionalInfo;
  String? _dateCreated;
  String? _guId;
  bool? _isActive;
  bool? _isNote;
  bool? _isPublished;
  String? _lastUpdated;
  String? _lessonType;
  String? _section;
  int? _sequence;
  String? _title;
  final RxBool _isCompleted = false.obs;
  final RxString _cousreId = "".obs;
  final Rx<QuizNewResponse>? _chapterResponse=QuizNewResponse().obs;

  dynamic get contentAdditionalInfo => _contentAdditionalInfo;

  String? get dateCreated => _dateCreated;

  String? get guId => _guId;

  bool? get isActive => _isActive;

  bool? get isNote => _isNote;

  bool? get isPublished => _isPublished;

  String? get lastUpdated => _lastUpdated;

  String? get lessonType => _lessonType;

  String? get section => _section;

  int? get sequence => _sequence;

  String? get title => _title;

  RxBool? get isCompleted => _isCompleted;

  RxString? get cousreId => _cousreId;

  Rx<QuizNewResponse>? get chapterResponse=> _chapterResponse;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['contentAdditionalInfo'] = _contentAdditionalInfo;
    map['dateCreated'] = _dateCreated;
    map['guId'] = _guId;
    map['isActive'] = _isActive;
    map['isNote'] = _isNote;
    map['isPublished'] = _isPublished;
    map['lastUpdated'] = _lastUpdated;
    map['lessonType'] = _lessonType;
    map['section'] = _section;
    map['sequence'] = _sequence;
    map['title'] = _title;
    return map;
  }
}
