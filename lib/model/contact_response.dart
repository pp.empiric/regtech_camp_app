import 'custom_field_response.dart';

class ContactResponse {
  String? _guId;
  bool? _isAdminVerified;
  bool? _isEmailVerified;
  String? _phone;
  List<SavedCards>? _savedCards;
  String? _email;
  String? _lastName;
  String? _fullName;
  String? _name;

  String? get guId => _guId;

  bool? get isAdminVerified => _isAdminVerified;

  bool? get isEmailVerified => _isEmailVerified;

  String? get phone => _phone;

  List<SavedCards>? get savedCards => _savedCards;

  String? get email => _email;

  String? get lastName => _lastName;

  String? get fullName => _fullName;

  String? get name => _name;

  ContactResponse({
    String? guId,
    bool? isAdminVerified,
    bool? isEmailVerified,
    String? phone,
    List<SavedCards>? savedCards,
    String? email,
    String? lastName,
    String? fullName,
    String? name,
  }) {
    _guId = guId;
    _isAdminVerified = isAdminVerified;
    _isEmailVerified = isEmailVerified;
    _phone = phone;
    _savedCards = savedCards;
    _savedCards = savedCards;
    _email = email;
    _lastName = lastName;
    _fullName = fullName;
    _name = name;
  }

  ContactResponse.fromJson(dynamic json) {
    _guId = json['id'];
    _isAdminVerified = json['isAdminVerified'];
    _isEmailVerified = json['isEmailVerified'];
    _phone = json['phone'];
    if (json['savedCards'] != null) {
      _savedCards = [];
      json['savedCards'].forEach((v) {
        _savedCards?.add(SavedCards.fromJson(v));
      });
    }
    _email = json['email'];
    _lastName = json['lastName'];
    _fullName = json['fullName'];
    _name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _guId;
    map['isAdminVerified'] = _isAdminVerified;
    map['isEmailVerified'] = _isEmailVerified;
    map['phone'] = _phone;
    if (_savedCards != null) {
      map['savedCards'] = _savedCards?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// methodId : "8bb6dfd83ffb40e1886068bba6fbcd74"
/// paymentType : "ps_card"
/// prefCard : "visa ************1111 exp 04 2022"

class SavedCards {
  String? _methodId;
  String? _paymentType;
  String? _prefCard;

  String? get methodId => _methodId;

  String? get paymentType => _paymentType;

  String? get prefCard => _prefCard;

  SavedCards({String? methodId, String? paymentType, String? prefCard}) {
    _methodId = methodId;
    _paymentType = paymentType;
    _prefCard = prefCard;
  }

  SavedCards.fromJson(dynamic json) {
    _methodId = json['methodId'];
    _paymentType = json['paymentType'];
    _prefCard = json['prefCard'];
  }

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['methodId'] = _methodId;
    map['paymentType'] = _paymentType;
    map['prefCard'] = _prefCard;
    return map;
  }
}
