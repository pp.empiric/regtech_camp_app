import 'chapters_response.dart';

/// draw : 1
/// recordsTotal : 2
/// recordsFiltered : 2
/// data : [{"attendeesCount":0,"classColor":"bg-color-purple txt-color-white","courseSectionsListCmd":[{"chapters":[{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:09:49Z","guId":"DKEG25rdRW","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:12:49Z","lessonType":"link","section":"gYVAJj6AM1","sequence":1,"title":"Lesson 1"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:13:55Z","guId":"LWMdj1MArb","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:22:44Z","lessonType":"link","section":"gYVAJj6AM1","sequence":2,"title":"Lesson 2"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:14:05Z","guId":"ga8ADZWGOW","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:22:46Z","lessonType":"link","section":"gYVAJj6AM1","sequence":3,"title":"Lesson 3"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:14:22Z","guId":"8x6GQqEGym","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:22:49Z","lessonType":"link","section":"gYVAJj6AM1","sequence":4,"title":"Lesson 4"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:14:45Z","guId":"8YM0akydbJ","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:22:51Z","lessonType":"link","section":"gYVAJj6AM1","sequence":5,"title":"Lesson 5"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:14:57Z","guId":"XmRGkoOGe9","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:22:54Z","lessonType":"link","section":"gYVAJj6AM1","sequence":6,"title":"Lesson 6"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:15:06Z","guId":"b5YA46qGP7","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:22:57Z","lessonType":"link","section":"gYVAJj6AM1","sequence":7,"title":"Lesson 7"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:15:16Z","guId":"8jDdxBk0v2","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:22:59Z","lessonType":"link","section":"gYVAJj6AM1","sequence":8,"title":"Lesson 8"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:15:26Z","guId":"xnXdg8NGKv","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:23:02Z","lessonType":"link","section":"gYVAJj6AM1","sequence":9,"title":"Lesson 9"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:15:43Z","guId":"nD7dz58AbJ","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-16T12:26:17Z","lessonType":"link","section":"gYVAJj6AM1","sequence":10,"title":"Lesson 10"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:15:52Z","guId":"2bVGnLRA4D","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:23:08Z","lessonType":"link","section":"gYVAJj6AM1","sequence":11,"title":"Lesson 11"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:16:01Z","guId":"leZd8lRdPx","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:23:12Z","lessonType":"link","section":"gYVAJj6AM1","sequence":12,"title":"Lesson 12"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:16:10Z","guId":"ylPAZ2oAMJ","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:23:16Z","lessonType":"link","section":"gYVAJj6AM1","sequence":13,"title":"Lesson 13"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:16:19Z","guId":"QZ4dR3lG31","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:23:20Z","lessonType":"link","section":"gYVAJj6AM1","sequence":14,"title":"Lesson 14"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:16:29Z","guId":"K6Od1vvGjW","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:23:24Z","lessonType":"link","section":"gYVAJj6AM1","sequence":15,"title":"Lesson 15"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:16:40Z","guId":"NYEAomKGL5","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:23:28Z","lessonType":"link","section":"gYVAJj6AM1","sequence":16,"title":"Lesson 16"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:16:50Z","guId":"mVLdKme07x","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:23:32Z","lessonType":"link","section":"gYVAJj6AM1","sequence":17,"title":"Lesson 17"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:17:02Z","guId":"4xpAYyLAmQ","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:23:36Z","lessonType":"link","section":"gYVAJj6AM1","sequence":18,"title":"Lesson 18"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:17:13Z","guId":"1oVALJbGrQ","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:23:41Z","lessonType":"link","section":"gYVAJj6AM1","sequence":19,"title":"Lesson 19"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:17:22Z","guId":"ov6GmYmG5M","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:23:44Z","lessonType":"link","section":"gYVAJj6AM1","sequence":20,"title":"Lesson 20"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:17:32Z","guId":"gYVAJLxGM1","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:23:49Z","lessonType":"link","section":"gYVAJj6AM1","sequence":21,"title":"Lesson 21"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:17:41Z","guId":"9xP0pJQdzR","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:23:53Z","lessonType":"link","section":"gYVAJj6AM1","sequence":22,"title":"Lesson 22"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:17:54Z","guId":"7RxdE8oA3V","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:23:57Z","lessonType":"link","section":"gYVAJj6AM1","sequence":23,"title":"Lesson 23"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:18:04Z","guId":"6Bn0bQaG7R","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:24:01Z","lessonType":"link","section":"gYVAJj6AM1","sequence":24,"title":"Lesson 24"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:18:15Z","guId":"e2EAVBVAzK","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:24:06Z","lessonType":"link","section":"gYVAJj6AM1","sequence":25,"title":"Lesson 25"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:18:24Z","guId":"M4JAqLV0gW","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:24:11Z","lessonType":"link","section":"gYVAJj6AM1","sequence":26,"title":"Lesson 26"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:18:35Z","guId":"L6w039edaZ","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:24:15Z","lessonType":"link","section":"gYVAJj6AM1","sequence":27,"title":"Lesson 27"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:18:46Z","guId":"XyBAOpNd1Y","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:24:19Z","lessonType":"link","section":"gYVAJj6AM1","sequence":28,"title":"Lesson 28"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:19:41Z","guId":"Q9xABgPdvl","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:24:24Z","lessonType":"link","section":"gYVAJj6AM1","sequence":29,"title":"Lesson 29"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:19:56Z","guId":"xoYdvb6d2N","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:24:29Z","lessonType":"link","section":"gYVAJj6AM1","sequence":30,"title":"Lesson 30"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:21:00Z","guId":"K9kd9r5Gny","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:24:34Z","lessonType":"link","section":"gYVAJj6AM1","sequence":31,"title":"Lesson 31"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:21:12Z","guId":"aNZAWv10PY","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:24:39Z","lessonType":"link","section":"gYVAJj6AM1","sequence":32,"title":"Lesson 32"}],"course":"wyLA6nRAoM","dateCreated":"2021-07-14T11:09:49Z","guId":"gYVAJj6AM1","isActive":true,"lastUpdated":"2021-07-16T12:14:43Z","sequence":1,"title":"Induction Lecture"}],"dateCreated":1626260988,"dateCreatedStr":"14/07/2021","dateUpdated":1632123495,"dateUpdatedStr":"20/09/2021","donationGuId":null,"externalLink":null,"guId":"wyLA6nRAoM","id":"wyLA6nRAoM","image1":"v1626260884/il65e2qgihkvvrgggp5x.jpg","isOnlineCourse":false,"isShowOnWebsite":true,"learningPathName":null,"membershipCount":2,"name":"INDUCTION LECTURE","paymentType":"PAID","productCount":0,"programCount":0,"shortDescription":"INDUCTION LECTURE","studentProgress":0.0,"title":"INDUCTION LECTURE","value":"INDUCTION LECTURE","videoUrl":null},{"attendeesCount":0,"classColor":"bg-color-darken txt-color-white","courseSectionsListCmd":[{"chapters":[{"contentAdditionalInfo":null,"dateCreated":"2021-05-30T06:17:52Z","guId":"DKEG2EPdRW","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-17T05:29:40Z","lessonType":"link","section":"XmRGkbLde9","sequence":1,"title":"Lecture  - 1"},{"contentAdditionalInfo":null,"dateCreated":"2021-05-30T06:28:38Z","guId":"6MZ0rrR0oz","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-06-24T06:30:19Z","lessonType":"link","section":"XmRGkbLde9","sequence":1,"title":"Lecture  - 2"},{"contentAdditionalInfo":null,"dateCreated":"2021-05-30T06:28:47Z","guId":"jq4GlE108w","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-06-24T06:30:47Z","lessonType":"link","section":"XmRGkbLde9","sequence":1,"title":"Lecture  - 3"},{"contentAdditionalInfo":null,"dateCreated":"2021-05-30T06:28:53Z","guId":"LWMdjJndrb","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-06-24T06:31:28Z","lessonType":"link","section":"XmRGkbLde9","sequence":1,"title":"Lecture  - 4"},{"contentAdditionalInfo":null,"dateCreated":"2021-05-30T06:29:00Z","guId":"ga8AD8XAOW","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-06-24T06:31:58Z","lessonType":"link","section":"XmRGkbLde9","sequence":1,"title":"Lecture  - 5"},{"contentAdditionalInfo":null,"dateCreated":"2021-05-30T06:34:28Z","guId":"8x6GQNl0ym","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-06-24T06:32:19Z","lessonType":"link","section":"XmRGkbLde9","sequence":1,"title":"Lecture - 6"},{"contentAdditionalInfo":null,"dateCreated":"2021-05-31T10:31:24Z","guId":"8x6GQNk0ym","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-06-24T06:33:35Z","lessonType":"link","section":"XmRGkbLde9","sequence":1,"title":"Lecture - 7"},{"contentAdditionalInfo":null,"dateCreated":"2021-06-02T09:06:26Z","guId":"1oVAL2MGrQ","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-06-24T06:33:52Z","lessonType":"link","section":"XmRGkbLde9","sequence":8,"title":"Lecture - 8"},{"contentAdditionalInfo":null,"dateCreated":"2021-06-02T09:07:22Z","guId":"ov6GmjN05M","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-06-24T06:34:13Z","lessonType":"link","section":"XmRGkbLde9","sequence":9,"title":"Lecture - 9"},{"contentAdditionalInfo":null,"dateCreated":"2021-06-03T11:58:41Z","guId":"yJr0Xnmdq9","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-06-24T06:34:33Z","lessonType":"link","section":"XmRGkbLde9","sequence":10,"title":"Lecture - 10"},{"contentAdditionalInfo":null,"dateCreated":"2021-06-08T09:34:24Z","guId":"QZ4dRavA31","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-06-24T06:34:51Z","lessonType":"link","section":"XmRGkbLde9","sequence":11,"title":"Lecture - 11"},{"contentAdditionalInfo":null,"dateCreated":"2021-06-10T04:06:42Z","guId":"4xpAYz3dmQ","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-06-24T06:36:03Z","lessonType":"link","section":"XmRGkbLde9","sequence":12,"title":"Lecture - 12"},{"contentAdditionalInfo":null,"dateCreated":"2021-06-12T06:47:24Z","guId":"K9kd9MPdny","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-06-24T06:36:28Z","lessonType":"link","section":"XmRGkbLde9","sequence":13,"title":"Lecture - 13"},{"contentAdditionalInfo":null,"dateCreated":"2021-06-12T12:27:56Z","guId":"8EjAPlXA9q","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-06-24T06:43:46Z","lessonType":"link","section":"XmRGkbLde9","sequence":14,"title":"Lecture - 14"},{"contentAdditionalInfo":null,"dateCreated":"2021-06-16T05:22:50Z","guId":"wyLA6MV0oM","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-06-24T06:44:06Z","lessonType":"link","section":"XmRGkbLde9","sequence":15,"title":"Lecture - 15"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-02T04:55:48Z","guId":"Xzjd5ZOGvO","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-07T06:27:04Z","lessonType":"link","section":"XmRGkbLde9","sequence":16,"title":"Lecture - 16"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-03T05:38:32Z","guId":"DKEG2zZGRW","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-07T06:27:28Z","lessonType":"link","section":"XmRGkbLde9","sequence":17,"title":"Lecture - 17"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-05T08:09:56Z","guId":"b5YA48PAP7","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-07T06:27:59Z","lessonType":"link","section":"XmRGkbLde9","sequence":18,"title":"Lecture - 18"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-06T09:46:15Z","guId":"K6Od1go0jW","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-07T06:28:21Z","lessonType":"link","section":"XmRGkbLde9","sequence":19,"title":"Lecture - 19"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-07T04:22:58Z","guId":"gYVAJr6AM1","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-07T06:28:42Z","lessonType":"link","section":"XmRGkbLde9","sequence":20,"title":"Lecture - 20"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-09T07:53:04Z","guId":"QZ4dRlw031","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-09T18:10:22Z","lessonType":"link","section":"XmRGkbLde9","sequence":21,"title":"Lecture - 21"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-10T04:48:23Z","guId":"6Bn0bKEA7R","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-13T06:00:44Z","lessonType":"link","section":"XmRGkbLde9","sequence":22,"title":"Lecture - 22"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-12T05:39:37Z","guId":"aNZAWkyAPY","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-13T06:01:23Z","lessonType":"link","section":"XmRGkbLde9","sequence":23,"title":"Lecture - 23"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-13T04:19:02Z","guId":"yJr0X420q9","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-13T06:02:01Z","lessonType":"link","section":"XmRGkbLde9","sequence":24,"title":"Lecture - 24"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T04:56:58Z","guId":"Xzjd5yPdvO","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T04:57:48Z","lessonType":"link","section":"XmRGkbLde9","sequence":25,"title":"Lecture - 25"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-15T04:57:00Z","guId":"e2x0e4RGYb","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-15T04:58:19Z","lessonType":"link","section":"XmRGkbLde9","sequence":26,"title":"L-26 उभयान्वयी अव्यय"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-17T04:40:37Z","guId":"ngB0NJo05e","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-17T04:41:11Z","lessonType":"link","section":"XmRGkbLde9","sequence":27,"title":"Lecture - 27"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-17T05:30:08Z","guId":"6MZ0rmWGoz","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-21T05:11:52Z","lessonType":"link","section":"XmRGkbLde9","sequence":28,"title":"Lecture - 28"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-23T04:52:09Z","guId":"ngB0NeoG5e","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-23T04:53:01Z","lessonType":"link","section":"XmRGkbLde9","sequence":29,"title":"Lecture - 29"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-27T04:17:58Z","guId":"leZd8V90Px","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-27T04:18:14Z","lessonType":"link","section":"XmRGkbLde9","sequence":30,"title":"Lecture - 30"}],"course":"nD7dzzDdbJ","dateCreated":"2021-05-30T06:17:52Z","guId":"XmRGkbLde9","isActive":true,"lastUpdated":"2021-06-12T05:56:11Z","sequence":1,"title":"Marathi Grammar (Basic Batch)"}],"dateCreated":1622355471,"dateCreatedStr":"30/05/2021","dateUpdated":1626242928,"dateUpdatedStr":"14/07/2021","donationGuId":null,"externalLink":null,"guId":"nD7dzzDdbJ","id":"nD7dzzDdbJ","image1":"v1626242900/rdh8a4lkoxhtukwnqzvn.jpg","isOnlineCourse":false,"isShowOnWebsite":true,"learningPathName":null,"membershipCount":1,"name":"Marathi Grammar (Basic Batch)","paymentType":"PAID","productCount":0,"programCount":0,"shortDescription":"Marathi Grammar (Basic Batch - 4)","studentProgress":0.0,"title":"Marathi Grammar (Basic Batch)","value":"Marathi Grammar (Basic Batch)","videoUrl":null}]

class CourseModel {
  CourseModel({
    int? draw,
    int? recordsTotal,
    int? recordsFiltered,
    List<CourseData>? data,
  }) {
    _draw = draw;
    _recordsTotal = recordsTotal;
    _recordsFiltered = recordsFiltered;
    _data = data;
  }

  CourseModel.fromJson(dynamic json) {
    _draw = json['draw'];
    _recordsTotal = json['recordsTotal'];
    _recordsFiltered = json['recordsFiltered'];
    if (json['data'] != null) {
      _data = [];
      json['data'].forEach((v) {
        _data?.add(CourseData.fromJson(v));
      });
    }
  }

  int? _draw;
  int? _recordsTotal;
  int? _recordsFiltered;
  List<CourseData>? _data;

  int? get draw => _draw;

  int? get recordsTotal => _recordsTotal;

  int? get recordsFiltered => _recordsFiltered;

  List<CourseData>? get data => _data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['draw'] = _draw;
    map['recordsTotal'] = _recordsTotal;
    map['recordsFiltered'] = _recordsFiltered;
    if (_data != null) {
      map['data'] = _data?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// attendeesCount : 0
/// classColor : "bg-color-purple txt-color-white"
/// courseSectionsListCmd : [{"chapters":[{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:09:49Z","guId":"DKEG25rdRW","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:12:49Z","lessonType":"link","section":"gYVAJj6AM1","sequence":1,"title":"Lesson 1"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:13:55Z","guId":"LWMdj1MArb","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:22:44Z","lessonType":"link","section":"gYVAJj6AM1","sequence":2,"title":"Lesson 2"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:14:05Z","guId":"ga8ADZWGOW","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:22:46Z","lessonType":"link","section":"gYVAJj6AM1","sequence":3,"title":"Lesson 3"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:14:22Z","guId":"8x6GQqEGym","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:22:49Z","lessonType":"link","section":"gYVAJj6AM1","sequence":4,"title":"Lesson 4"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:14:45Z","guId":"8YM0akydbJ","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:22:51Z","lessonType":"link","section":"gYVAJj6AM1","sequence":5,"title":"Lesson 5"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:14:57Z","guId":"XmRGkoOGe9","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:22:54Z","lessonType":"link","section":"gYVAJj6AM1","sequence":6,"title":"Lesson 6"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:15:06Z","guId":"b5YA46qGP7","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:22:57Z","lessonType":"link","section":"gYVAJj6AM1","sequence":7,"title":"Lesson 7"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:15:16Z","guId":"8jDdxBk0v2","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:22:59Z","lessonType":"link","section":"gYVAJj6AM1","sequence":8,"title":"Lesson 8"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:15:26Z","guId":"xnXdg8NGKv","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:23:02Z","lessonType":"link","section":"gYVAJj6AM1","sequence":9,"title":"Lesson 9"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:15:43Z","guId":"nD7dz58AbJ","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-16T12:26:17Z","lessonType":"link","section":"gYVAJj6AM1","sequence":10,"title":"Lesson 10"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:15:52Z","guId":"2bVGnLRA4D","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:23:08Z","lessonType":"link","section":"gYVAJj6AM1","sequence":11,"title":"Lesson 11"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:16:01Z","guId":"leZd8lRdPx","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:23:12Z","lessonType":"link","section":"gYVAJj6AM1","sequence":12,"title":"Lesson 12"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:16:10Z","guId":"ylPAZ2oAMJ","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:23:16Z","lessonType":"link","section":"gYVAJj6AM1","sequence":13,"title":"Lesson 13"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:16:19Z","guId":"QZ4dR3lG31","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:23:20Z","lessonType":"link","section":"gYVAJj6AM1","sequence":14,"title":"Lesson 14"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:16:29Z","guId":"K6Od1vvGjW","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:23:24Z","lessonType":"link","section":"gYVAJj6AM1","sequence":15,"title":"Lesson 15"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:16:40Z","guId":"NYEAomKGL5","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:23:28Z","lessonType":"link","section":"gYVAJj6AM1","sequence":16,"title":"Lesson 16"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:16:50Z","guId":"mVLdKme07x","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:23:32Z","lessonType":"link","section":"gYVAJj6AM1","sequence":17,"title":"Lesson 17"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:17:02Z","guId":"4xpAYyLAmQ","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:23:36Z","lessonType":"link","section":"gYVAJj6AM1","sequence":18,"title":"Lesson 18"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:17:13Z","guId":"1oVALJbGrQ","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:23:41Z","lessonType":"link","section":"gYVAJj6AM1","sequence":19,"title":"Lesson 19"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:17:22Z","guId":"ov6GmYmG5M","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:23:44Z","lessonType":"link","section":"gYVAJj6AM1","sequence":20,"title":"Lesson 20"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:17:32Z","guId":"gYVAJLxGM1","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:23:49Z","lessonType":"link","section":"gYVAJj6AM1","sequence":21,"title":"Lesson 21"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:17:41Z","guId":"9xP0pJQdzR","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:23:53Z","lessonType":"link","section":"gYVAJj6AM1","sequence":22,"title":"Lesson 22"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:17:54Z","guId":"7RxdE8oA3V","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:23:57Z","lessonType":"link","section":"gYVAJj6AM1","sequence":23,"title":"Lesson 23"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:18:04Z","guId":"6Bn0bQaG7R","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:24:01Z","lessonType":"link","section":"gYVAJj6AM1","sequence":24,"title":"Lesson 24"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:18:15Z","guId":"e2EAVBVAzK","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:24:06Z","lessonType":"link","section":"gYVAJj6AM1","sequence":25,"title":"Lesson 25"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:18:24Z","guId":"M4JAqLV0gW","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:24:11Z","lessonType":"link","section":"gYVAJj6AM1","sequence":26,"title":"Lesson 26"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:18:35Z","guId":"L6w039edaZ","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:24:15Z","lessonType":"link","section":"gYVAJj6AM1","sequence":27,"title":"Lesson 27"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:18:46Z","guId":"XyBAOpNd1Y","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:24:19Z","lessonType":"link","section":"gYVAJj6AM1","sequence":28,"title":"Lesson 28"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:19:41Z","guId":"Q9xABgPdvl","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:24:24Z","lessonType":"link","section":"gYVAJj6AM1","sequence":29,"title":"Lesson 29"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:19:56Z","guId":"xoYdvb6d2N","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:24:29Z","lessonType":"link","section":"gYVAJj6AM1","sequence":30,"title":"Lesson 30"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:21:00Z","guId":"K9kd9r5Gny","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:24:34Z","lessonType":"link","section":"gYVAJj6AM1","sequence":31,"title":"Lesson 31"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:21:12Z","guId":"aNZAWv10PY","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:24:39Z","lessonType":"link","section":"gYVAJj6AM1","sequence":32,"title":"Lesson 32"}],"course":"wyLA6nRAoM","dateCreated":"2021-07-14T11:09:49Z","guId":"gYVAJj6AM1","isActive":true,"lastUpdated":"2021-07-16T12:14:43Z","sequence":1,"title":"Induction Lecture"}]
/// dateCreated : 1626260988
/// dateCreatedStr : "14/07/2021"
/// dateUpdated : 1632123495
/// dateUpdatedStr : "20/09/2021"
/// donationGuId : null
/// externalLink : null
/// guId : "wyLA6nRAoM"
/// id : "wyLA6nRAoM"
/// image1 : "v1626260884/il65e2qgihkvvrgggp5x.jpg"
/// isOnlineCourse : false
/// isShowOnWebsite : true
/// learningPathName : null
/// membershipCount : 2
/// name : "INDUCTION LECTURE"
/// paymentType : "PAID"
/// productCount : 0
/// programCount : 0
/// shortDescription : "INDUCTION LECTURE"
/// studentProgress : 0.0
/// title : "INDUCTION LECTURE"
/// value : "INDUCTION LECTURE"
/// videoUrl : null

class CourseData {
  CourseData({
    int? attendeesCount,
    String? classColor,
    List<CourseSectionsListCmd>? courseSectionsListCmd,
    int? dateCreated,
    String? dateCreatedStr,
    int? dateUpdated,
    String? dateUpdatedStr,
    dynamic donationGuId,
    dynamic externalLink,
    String? guId,
    String? id,
    String? image1,
    bool? isOnlineCourse,
    bool? isShowOnWebsite,
    dynamic learningPathName,
    int? membershipCount,
    String? name,
    String? paymentType,
    int? productCount,
    int? programCount,
    String? shortDescription,
    double? studentProgress,
    String? title,
    String? value,
    dynamic videoUrl,
  }) {
    _attendeesCount = attendeesCount;
    _classColor = classColor;
    _courseSectionsListCmd = courseSectionsListCmd;
    _dateCreated = dateCreated;
    _dateCreatedStr = dateCreatedStr;
    _dateUpdated = dateUpdated;
    _dateUpdatedStr = dateUpdatedStr;
    _donationGuId = donationGuId;
    _externalLink = externalLink;
    _guId = guId;
    _id = id;
    _image1 = image1;
    _isOnlineCourse = isOnlineCourse;
    _isShowOnWebsite = isShowOnWebsite;
    _learningPathName = learningPathName;
    _membershipCount = membershipCount;
    _name = name;
    _paymentType = paymentType;
    _productCount = productCount;
    _programCount = programCount;
    _shortDescription = shortDescription;
    _studentProgress = studentProgress;
    _title = title;
    _value = value;
    _videoUrl = videoUrl;
  }

  CourseData.fromJson(dynamic json) {
    _attendeesCount = json['attendeesCount'];
    _classColor = json['classColor'];
    if (json['courseSectionsListCmd'] != null) {
      _courseSectionsListCmd = [];
      json['courseSectionsListCmd'].forEach((v) {
        _courseSectionsListCmd?.add(CourseSectionsListCmd.fromJson(v));
      });
    }
    _dateCreated = json['dateCreated'];
    _dateCreatedStr = json['dateCreatedStr'];
    _dateUpdated = json['dateUpdated'];
    _dateUpdatedStr = json['dateUpdatedStr'];
    _donationGuId = json['donationGuId'];
    _externalLink = json['externalLink'];
    _guId = json['guId'];
    _id = json['id'];
    _image1 = json['image1'];
    _isOnlineCourse = json['isOnlineCourse'];
    _isShowOnWebsite = json['isShowOnWebsite'];
    _learningPathName = json['learningPathName'];
    _membershipCount = json['membershipCount'];
    _name = json['name'];
    _paymentType = json['paymentType'];
    _productCount = json['productCount'];
    _programCount = json['programCount'];
    _shortDescription = json['shortDescription'];
    _studentProgress = json['studentProgress'];
    _title = json['title'];
    _value = json['value'];
    _videoUrl = json['videoUrl'];
  }

  int? _attendeesCount;
  String? _classColor;
  List<CourseSectionsListCmd>? _courseSectionsListCmd;
  int? _dateCreated;
  String? _dateCreatedStr;
  int? _dateUpdated;
  String? _dateUpdatedStr;
  dynamic _donationGuId;
  dynamic _externalLink;
  String? _guId;
  String? _id;
  String? _image1;
  bool? _isOnlineCourse;
  bool? _isShowOnWebsite;
  dynamic _learningPathName;
  int? _membershipCount;
  String? _name;
  String? _paymentType;
  int? _productCount;
  int? _programCount;
  String? _shortDescription;
  double? _studentProgress;
  String? _title;
  String? _value;
  dynamic _videoUrl;

  int? get attendeesCount => _attendeesCount;

  String? get classColor => _classColor;

  List<CourseSectionsListCmd>? get courseSectionsListCmd =>
      _courseSectionsListCmd;

  int? get dateCreated => _dateCreated;

  String? get dateCreatedStr => _dateCreatedStr;

  int? get dateUpdated => _dateUpdated;

  String? get dateUpdatedStr => _dateUpdatedStr;

  dynamic get donationGuId => _donationGuId;

  dynamic get externalLink => _externalLink;

  String? get guId => _guId;

  String? get id => _id;

  String? get image1 => _image1;

  bool? get isOnlineCourse => _isOnlineCourse;

  bool? get isShowOnWebsite => _isShowOnWebsite;

  dynamic get learningPathName => _learningPathName;

  int? get membershipCount => _membershipCount;

  String? get name => _name;

  String? get paymentType => _paymentType;

  int? get productCount => _productCount;

  int? get programCount => _programCount;

  String? get shortDescription => _shortDescription;

  double? get studentProgress => _studentProgress;

  String? get title => _title;

  String? get value => _value;

  dynamic get videoUrl => _videoUrl;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['attendeesCount'] = _attendeesCount;
    map['classColor'] = _classColor;
    if (_courseSectionsListCmd != null) {
      map['courseSectionsListCmd'] =
          _courseSectionsListCmd?.map((v) => v.toJson()).toList();
    }
    map['dateCreated'] = _dateCreated;
    map['dateCreatedStr'] = _dateCreatedStr;
    map['dateUpdated'] = _dateUpdated;
    map['dateUpdatedStr'] = _dateUpdatedStr;
    map['donationGuId'] = _donationGuId;
    map['externalLink'] = _externalLink;
    map['guId'] = _guId;
    map['id'] = _id;
    map['image1'] = _image1;
    map['isOnlineCourse'] = _isOnlineCourse;
    map['isShowOnWebsite'] = _isShowOnWebsite;
    map['learningPathName'] = _learningPathName;
    map['membershipCount'] = _membershipCount;
    map['name'] = _name;
    map['paymentType'] = _paymentType;
    map['productCount'] = _productCount;
    map['programCount'] = _programCount;
    map['shortDescription'] = _shortDescription;
    map['studentProgress'] = _studentProgress;
    map['title'] = _title;
    map['value'] = _value;
    map['videoUrl'] = _videoUrl;
    return map;
  }
}

/// chapters : [{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:09:49Z","guId":"DKEG25rdRW","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:12:49Z","lessonType":"link","section":"gYVAJj6AM1","sequence":1,"title":"Lesson 1"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:13:55Z","guId":"LWMdj1MArb","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:22:44Z","lessonType":"link","section":"gYVAJj6AM1","sequence":2,"title":"Lesson 2"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:14:05Z","guId":"ga8ADZWGOW","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:22:46Z","lessonType":"link","section":"gYVAJj6AM1","sequence":3,"title":"Lesson 3"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:14:22Z","guId":"8x6GQqEGym","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:22:49Z","lessonType":"link","section":"gYVAJj6AM1","sequence":4,"title":"Lesson 4"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:14:45Z","guId":"8YM0akydbJ","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:22:51Z","lessonType":"link","section":"gYVAJj6AM1","sequence":5,"title":"Lesson 5"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:14:57Z","guId":"XmRGkoOGe9","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:22:54Z","lessonType":"link","section":"gYVAJj6AM1","sequence":6,"title":"Lesson 6"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:15:06Z","guId":"b5YA46qGP7","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:22:57Z","lessonType":"link","section":"gYVAJj6AM1","sequence":7,"title":"Lesson 7"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:15:16Z","guId":"8jDdxBk0v2","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:22:59Z","lessonType":"link","section":"gYVAJj6AM1","sequence":8,"title":"Lesson 8"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:15:26Z","guId":"xnXdg8NGKv","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:23:02Z","lessonType":"link","section":"gYVAJj6AM1","sequence":9,"title":"Lesson 9"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:15:43Z","guId":"nD7dz58AbJ","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-16T12:26:17Z","lessonType":"link","section":"gYVAJj6AM1","sequence":10,"title":"Lesson 10"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:15:52Z","guId":"2bVGnLRA4D","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:23:08Z","lessonType":"link","section":"gYVAJj6AM1","sequence":11,"title":"Lesson 11"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:16:01Z","guId":"leZd8lRdPx","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:23:12Z","lessonType":"link","section":"gYVAJj6AM1","sequence":12,"title":"Lesson 12"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:16:10Z","guId":"ylPAZ2oAMJ","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:23:16Z","lessonType":"link","section":"gYVAJj6AM1","sequence":13,"title":"Lesson 13"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:16:19Z","guId":"QZ4dR3lG31","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:23:20Z","lessonType":"link","section":"gYVAJj6AM1","sequence":14,"title":"Lesson 14"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:16:29Z","guId":"K6Od1vvGjW","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:23:24Z","lessonType":"link","section":"gYVAJj6AM1","sequence":15,"title":"Lesson 15"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:16:40Z","guId":"NYEAomKGL5","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:23:28Z","lessonType":"link","section":"gYVAJj6AM1","sequence":16,"title":"Lesson 16"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:16:50Z","guId":"mVLdKme07x","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:23:32Z","lessonType":"link","section":"gYVAJj6AM1","sequence":17,"title":"Lesson 17"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:17:02Z","guId":"4xpAYyLAmQ","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:23:36Z","lessonType":"link","section":"gYVAJj6AM1","sequence":18,"title":"Lesson 18"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:17:13Z","guId":"1oVALJbGrQ","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:23:41Z","lessonType":"link","section":"gYVAJj6AM1","sequence":19,"title":"Lesson 19"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:17:22Z","guId":"ov6GmYmG5M","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:23:44Z","lessonType":"link","section":"gYVAJj6AM1","sequence":20,"title":"Lesson 20"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:17:32Z","guId":"gYVAJLxGM1","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:23:49Z","lessonType":"link","section":"gYVAJj6AM1","sequence":21,"title":"Lesson 21"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:17:41Z","guId":"9xP0pJQdzR","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:23:53Z","lessonType":"link","section":"gYVAJj6AM1","sequence":22,"title":"Lesson 22"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:17:54Z","guId":"7RxdE8oA3V","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:23:57Z","lessonType":"link","section":"gYVAJj6AM1","sequence":23,"title":"Lesson 23"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:18:04Z","guId":"6Bn0bQaG7R","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:24:01Z","lessonType":"link","section":"gYVAJj6AM1","sequence":24,"title":"Lesson 24"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:18:15Z","guId":"e2EAVBVAzK","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:24:06Z","lessonType":"link","section":"gYVAJj6AM1","sequence":25,"title":"Lesson 25"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:18:24Z","guId":"M4JAqLV0gW","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:24:11Z","lessonType":"link","section":"gYVAJj6AM1","sequence":26,"title":"Lesson 26"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:18:35Z","guId":"L6w039edaZ","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:24:15Z","lessonType":"link","section":"gYVAJj6AM1","sequence":27,"title":"Lesson 27"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:18:46Z","guId":"XyBAOpNd1Y","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:24:19Z","lessonType":"link","section":"gYVAJj6AM1","sequence":28,"title":"Lesson 28"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:19:41Z","guId":"Q9xABgPdvl","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:24:24Z","lessonType":"link","section":"gYVAJj6AM1","sequence":29,"title":"Lesson 29"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:19:56Z","guId":"xoYdvb6d2N","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:24:29Z","lessonType":"link","section":"gYVAJj6AM1","sequence":30,"title":"Lesson 30"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:21:00Z","guId":"K9kd9r5Gny","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:24:34Z","lessonType":"link","section":"gYVAJj6AM1","sequence":31,"title":"Lesson 31"},{"contentAdditionalInfo":null,"dateCreated":"2021-07-14T11:21:12Z","guId":"aNZAWv10PY","isActive":true,"isNote":false,"isPublished":true,"lastUpdated":"2021-07-14T11:24:39Z","lessonType":"link","section":"gYVAJj6AM1","sequence":32,"title":"Lesson 32"}]
/// course : "wyLA6nRAoM"
/// dateCreated : "2021-07-14T11:09:49Z"
/// guId : "gYVAJj6AM1"
/// isActive : true
/// lastUpdated : "2021-07-16T12:14:43Z"
/// sequence : 1
/// title : "Induction Lecture"

class CourseSectionsListCmd {
  CourseSectionsListCmd({
    List<Chapters>? chapters,
    String? course,
    String? dateCreated,
    String? guId,
    bool? isActive,
    String? lastUpdated,
    int? sequence,
    String? title,
  }) {
    _chapters = chapters;
    _course = course;
    _dateCreated = dateCreated;
    _guId = guId;
    _isActive = isActive;
    _lastUpdated = lastUpdated;
    _sequence = sequence;
    _title = title;
  }

  CourseSectionsListCmd.fromJson(dynamic json) {
    if (json['chapters'] != null) {
      _chapters = [];
      json['chapters'].forEach((v) {
        _chapters?.add(Chapters.fromJson(v));
      });
    }
    _course = json['course'];
    _dateCreated = json['dateCreated'];
    _guId = json['guId'];
    _isActive = json['isActive'];
    _lastUpdated = json['lastUpdated'];
    _sequence = json['sequence'];
    _title = json['title'];
  }

  List<Chapters>? _chapters;
  String? _course;
  String? _dateCreated;
  String? _guId;
  bool? _isActive;
  String? _lastUpdated;
  int? _sequence;
  String? _title;

  List<Chapters>? get chapters => _chapters;

  String? get course => _course;

  String? get dateCreated => _dateCreated;

  String? get guId => _guId;

  bool? get isActive => _isActive;

  String? get lastUpdated => _lastUpdated;

  int? get sequence => _sequence;

  String? get title => _title;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_chapters != null) {
      map['chapters'] = _chapters?.map((v) => v.toJson()).toList();
    }
    map['course'] = _course;
    map['dateCreated'] = _dateCreated;
    map['guId'] = _guId;
    map['isActive'] = _isActive;
    map['lastUpdated'] = _lastUpdated;
    map['sequence'] = _sequence;
    map['title'] = _title;
    return map;
  }
}

/// contentAdditionalInfo : null
/// dateCreated : "2021-07-14T11:09:49Z"
/// guId : "DKEG25rdRW"
/// isActive : true
/// isNote : false
/// isPublished : true
/// lastUpdated : "2021-07-14T11:12:49Z"
/// lessonType : "link"
/// section : "gYVAJj6AM1"
/// sequence : 1
/// title : "Lesson 1"

