import 'package:get/get_rx/src/rx_types/rx_types.dart';

class RadioModel {
  var isSelected = false.obs;
  final String buttonText;
  final String text;
  final Function onClick;

  RadioModel(this.isSelected, this.buttonText, this.text, this.onClick);
}
