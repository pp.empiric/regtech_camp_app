class LoginResponse {
  String? accessToken;
  Contact? contact;
  String? email;
  bool? isNewProfile;
  String? role;
  String? fullName;

  LoginResponse();

  LoginResponse.fromJson(Map<String, dynamic> json) {
    accessToken = json['access_token'];
    contact =
        json['contact'] != null ? Contact.fromJson(json['contact']) : null;
    email = json['email'];
    isNewProfile = json['isNewProfile'];
    role = json['role'];
    fullName = json['fullName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['access_token'] = this.accessToken;
    if (this.contact != null) {
      data['contact'] = this.contact!.toJson();
    }
    data['email'] = this.email;
    data['isNewProfile'] = this.isNewProfile;
    data['role'] = this.role;
    data['fullName'] = this.fullName;
    return data;
  }
}

class Contact {
  dynamic balance;
  dynamic children;
  dynamic description;
  String? email;
  String? fullName;
  bool? hasAcceptedTerms;
  bool? hasParent;
  String? id;
  dynamic imageUrl;
  bool? isAdminVerified;
  bool? isEmailVerified;
  bool? isFirstLogin;
  bool? isLocalPicture;
  bool? isParent;
  dynamic lastName;
  String? name;
  String? phone;
  dynamic picture;
  String? role;

  Contact();

  Contact.fromJson(Map<String, dynamic> json) {
    balance = json['balance'];
    children = json['children'];
    description = json['description'];
    email = json['email'];
    fullName = json['fullName'];
    role = json['role'];
    hasAcceptedTerms = json['hasAcceptedTerms'];
    hasParent = json['hasParent'];
    id = json['id'];
    imageUrl = json['imageUrl'];
    isAdminVerified = json['isAdminVerified'];
    isEmailVerified = json['isEmailVerified'];
    isFirstLogin = json['isFirstLogin'];
    isLocalPicture = json['isLocalPicture'];
    isParent = json['isParent'];
    lastName = json['lastName'];
    name = json['name'];
    phone = json['phone'];
    picture = json['picture'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['balance'] = this.balance;
    data['children'] = this.children;
    data['description'] = this.description;
    data['email'] = this.email;
    data['fullName'] = this.fullName;
    data['hasAcceptedTerms'] = this.hasAcceptedTerms;
    data['hasParent'] = this.hasParent;
    data['id'] = this.id;
    data['role'] = this.role;
    data['imageUrl'] = this.imageUrl;
    data['isAdminVerified'] = this.isAdminVerified;
    data['isEmailVerified'] = this.isEmailVerified;
    data['isFirstLogin'] = this.isFirstLogin;
    data['isLocalPicture'] = this.isLocalPicture;
    data['isParent'] = this.isParent;
    data['lastName'] = this.lastName;
    data['name'] = this.name;
    data['phone'] = this.phone;
    data['picture'] = this.picture;
    return data;
  }
}
