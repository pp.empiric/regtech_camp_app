/// contact : {"fullName":"Jenishkumar Dholakiya","picture":"https://lh3.googleusercontent.com/a-/AOh14GjbOpS6Pxm_4Cvz_KdTDRxYfWxnx-wPUTAIW8qRl6E=s96-c"}
/// dateCreated : "2021-12-02T11:31:04Z"
/// guId : "M4JAqrkdgW"
/// note : "fgfgfgffg"

class NoteModel {
  NoteModel({
      Contact? contact, 
      String? dateCreated, 
      String? guId, 
      String? note,}){
    _contact = contact;
    _dateCreated = dateCreated;
    _guId = guId;
    _note = note;
}

  NoteModel.fromJson(dynamic json) {
    _contact = json['contact'] != null ? Contact.fromJson(json['contact']) : null;
    _dateCreated = json['dateCreated'];
    _guId = json['guId'];
    _note = json['note'];
  }
  Contact? _contact;
  String? _dateCreated;
  String? _guId;
  String? _note;

  Contact? get contact => _contact;
  String? get dateCreated => _dateCreated;
  String? get guId => _guId;
  String? get note => _note;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_contact != null) {
      map['contact'] = _contact?.toJson();
    }
    map['dateCreated'] = _dateCreated;
    map['guId'] = _guId;
    map['note'] = _note;
    return map;
  }

}

/// fullName : "Jenishkumar Dholakiya"
/// picture : "https://lh3.googleusercontent.com/a-/AOh14GjbOpS6Pxm_4Cvz_KdTDRxYfWxnx-wPUTAIW8qRl6E=s96-c"

class Contact {
  Contact({
      String? fullName, 
      String? picture,}){
    _fullName = fullName;
    _picture = picture;
}

  Contact.fromJson(dynamic json) {
    _fullName = json['fullName'];
    _picture = json['picture'];
  }
  String? _fullName;
  String? _picture;

  String? get fullName => _fullName;
  String? get picture => _picture;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['fullName'] = _fullName;
    map['picture'] = _picture;
    return map;
  }

}