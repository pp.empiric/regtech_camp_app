class LinearChartModel {
  String? title;
  String? subTitle;
  dynamic progress1;
  dynamic progress2;

  LinearChartModel(this.title, this.subTitle, this.progress1, this.progress2);
}
