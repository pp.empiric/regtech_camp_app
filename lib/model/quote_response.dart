/// data : [{"message":"Test message","type":"notice"}]
/// total : 3

class QuoteResponse {
  QuoteResponse({
    List<Quote>? data,
    int? total,
  }) {
    _data = data;
    _total = total;
  }

  QuoteResponse.fromJson(dynamic json) {
    if (json['data'] != null) {
      _data = [];
      json['data'].forEach((v) {
        _data?.add(Quote.fromJson(v));
      });
    }
    _total = json['total'];
  }

  List<Quote>? _data;
  int? _total;

  List<Quote>? get data => _data;

  int? get total => _total;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_data != null) {
      map['data'] = _data?.map((v) => v.toJson()).toList();
    }
    map['total'] = _total;
    return map;
  }
}

/// message : "Test message"
/// type : "notice"

class Quote {
  Quote({
    String? message,
    String? type,
  }) {
    _message = message;
    _type = type;
  }

  Quote.fromJson(dynamic json) {
    _message = json['message'];
    _type = json['type'];
  }

  String? _message;
  String? _type;

  String? get message => _message;

  String? get type => _type;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['message'] = _message;
    map['type'] = _type;
    return map;
  }
}
