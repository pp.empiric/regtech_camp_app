class GeneralUserDataResponse {
  bool? isOtpAllowed;
  dynamic stripeAuthCode;
  dynamic orgId;


  GeneralUserDataResponse({this.isOtpAllowed, this.stripeAuthCode, this.orgId});

  GeneralUserDataResponse.fromJson(Map<String, dynamic> json) {
    orgId = json['orgId'];
    isOtpAllowed = json['isOtpAllowed'];
    stripeAuthCode = json['stripeAuthCode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['isOtpAllowed'] = this.isOtpAllowed;
    data['stripeAuthCode'] = this.stripeAuthCode;
    data['orgId'] = this.orgId;
    return data;
  }
}




