import 'chapters_response.dart';

class SectionResponse {
  List<Chapters>? _chapters;
  String? _guId;
  bool? _isActive;
  String? _title;
  String? _course;


  List<Chapters>? get chapters => _chapters;

  String? get guId => _guId;

  bool? get isActive => _isActive;

  String? get title => _title;

  String? get course => _course;

  SectionResponse(
      {List<Chapters>? chapters,
      String? course,
      String? dateCreated,
      String? guId,
      bool? isActive,
      String? lastUpdated,
      int? sequence,
      String? title}) {
    _chapters = chapters;
    _guId = guId;
    _isActive = isActive;
    _title = title;
    _course = course;
  }

  SectionResponse.fromJson(dynamic json) {
    if (json['chapters'] != null) {
      _chapters = [];
      json['chapters'].forEach((v) {
        _chapters?.add(Chapters.fromJson(v));
      });
    }
    _guId = json['guId'];
    _isActive = json['isActive'];
    _title = json['title'];
    _course = json['course'];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_chapters != null) {
      map['chapters'] = _chapters?.map((v) => v.toJson()).toList();
    }
    map['guId'] = _guId;
    map['isActive'] = _isActive;
    map['title'] = _title;
    map['course'] = _course;
    return map;
  }
}

