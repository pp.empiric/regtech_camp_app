/// contactAssets : [{"guId":"M4JAq5lAgW","isActive":true,"isPublished":false,"title":"gh"}]

class FolderResponse {
  FolderResponse({
    List<ContactAssets>? contactAssets,
  }) {
    _contactAssets = contactAssets;
  }

  FolderResponse.fromJson(dynamic json) {
    if (json['contactAssets'] != null) {
      _contactAssets = [];
      json['contactAssets'].forEach((v) {
        _contactAssets?.add(ContactAssets.fromJson(v));
      });
    }
  }

  List<ContactAssets>? _contactAssets;

  List<ContactAssets>? get contactAssets => _contactAssets;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_contactAssets != null) {
      map['contactAssets'] = _contactAssets?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// guId : "M4JAq5lAgW"
/// isActive : true
/// isPublished : false
/// title : "gh"

class ContactAssets {
  ContactAssets(
      {String? guId,
      bool? isActive,
      bool? isPublished,
      String? title,
      String? pageNo}) {
    _guId = guId;
    _isActive = isActive;
    _isPublished = isPublished;
    _title = title;
    _pageNo = pageNo;
  }

  ContactAssets.fromJson(dynamic json) {
    _guId = json['guId'];
    _isActive = json['isActive'];
    _isPublished = json['isPublished'];
    _title = json['title'];
  }

  setPageNo(String pageNo) {
    _pageNo = pageNo;
  }

  String? _guId;
  bool? _isActive;
  bool? _isPublished;
  String? _title;
  String? _pageNo;

  String? get guId => _guId;

  bool? get isActive => _isActive;

  bool? get isPublished => _isPublished;

  String? get title => _title;

  String? get pageNo => _pageNo;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['guId'] = _guId;
    map['isActive'] = _isActive;
    map['isPublished'] = _isPublished;
    map['title'] = _title;
    return map;
  }
}
