import 'dart:io';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:downloads_path_provider_28/downloads_path_provider_28.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:html/parser.dart' as html;
import 'package:lottie/lottie.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:regtech_camp_app/model/item_category_response.dart';
import 'package:regtech_camp_app/resources/app_colors.dart';
import 'package:regtech_camp_app/resources/app_constants.dart';
import 'package:regtech_camp_app/resources/app_images.dart';
import 'package:regtech_camp_app/resources/youtubevideovalidator/youtube_video_validator.dart';
import 'package:regtech_camp_app/routes/app_routes.dart';
import 'package:regtech_camp_app/services/apis.dart';

import 'common_widget/PlayVimeoScreen.dart';
import 'common_widget/global_icon_loader.dart';
import 'common_widget/global_text.dart';
import 'common_widget/pdf_viewer_page.dart';
import 'common_widget/play_audio_screen.dart';
import 'common_widget/play_video_screen.dart';
import 'common_widget/play_youtube_screen.dart';
import 'extension/extension.dart';
import 'model/contact_response.dart';
import 'model/courses_curriculum_details_response.dart';
import 'model/general_user_data_response.dart';
import 'model/ping_response.dart';
import 'model/quiz_new_response.dart';
import 'model/quiz_question_response.dart';
import 'resources/app_texts.dart';
import 'services/app_preference.dart';

Future<int> isSafeToCallApi() async {
  final isVPN = await CheckVpnConnection.isVpnActive();
  if (isVPN) {
    return 0;
  } else {
    final connectivityResult = await Connectivity().checkConnectivity();
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      return 2;
    } else {
      return 1;
    }
  }
}

var selectedBuyCourse = ItemCategoryModel().obs;

var style = const TextStyle(
  fontSize: 17,
  fontWeight: FontWeight.w600,
  fontStyle: FontStyle.normal,
  color: Color(0xff6C6D71),
);

showSnackBar(
    String title, String message, Color color, IconData iconData) async {
  Get.snackbar(title, message,
      margin: const EdgeInsets.symmetric(horizontal: 10),
      snackPosition: SnackPosition.BOTTOM,
      colorText: Colors.white,
      backgroundColor: color,
      icon: Icon(iconData),
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      onTap: (value) => Get.back(),
      shouldIconPulse: true,
      borderRadius: 10);
}

showImageDialog(String imageUrl) {
  Get.dialog(
      Center(
        child: Scaffold(
          backgroundColor: Colors.transparent,
          body: Stack(
            children: [
              Center(
                child: SizedBox(
                  height: Get.height,
                  child: InteractiveViewer(
                      minScale: 1,
                      maxScale: 5,
                      child: FadeInImage(
                          placeholder: AssetImage(AppImages.coursePlaceHolder),
                          image: NetworkImage(imageUrl))),
                ),
              ),
              Positioned(
                  top: 0,
                  right: 0,
                  child: InkWell(
                      onTap: () {
                        Get.back();
                        Get.back();
                      },
                      child: const GlobalText(
                        "Close",
                        fontSize: 18.0,
                        color: Colors.white,
                        paddingTop: 20.0,
                        paddingRight: 10.0,
                      ))),
            ],
          ),
        ),
      ),
      barrierDismissible: false); // how to dismiss? Like Get.Dismiss().
}

bool validateStructure(String value) {
  String pattern =
      r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$';
  RegExp regExp = RegExp(pattern);
  return regExp.hasMatch(value);
}

bool validateMobile(String value) {
  String pattern = r'(^(?:[+0]9)?[0-9]{10,12}$)';
  RegExp regExp = RegExp(pattern);
  return regExp.hasMatch(value);
}

dynamic showVPNDialog(Function doOnCLick) {
  Get.dialog(
      Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Material(
            color: Colors.transparent,
            child: Container(
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(15)),
              margin: const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
              child: ListView(
                  shrinkWrap: true,
                  padding:
                      const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
                  physics: const NeverScrollableScrollPhysics(),
                  children: [
                    LottieBuilder.asset('assets/json/vpn.json',
                        width: 150, height: 150),
                    const SizedBox(height: 15),
                    GlobalText(AppTexts.vpnActive,
                        fontSize: 20,
                        fontWeight: FontWeight.w600,
                        fontStyle: FontStyle.normal),
                    const SizedBox(height: 15),
                    Container(
                        height: 48,
                        decoration: BoxDecoration(
                          color: AppColors.secondaryDartColor,
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Material(
                            color: Colors.transparent,
                            child: InkWell(
                                onTap: () async {
                                  final isVPN =
                                      await CheckVpnConnection.isVpnActive();
                                  if (!isVPN) {
                                    Get.back();
                                    doOnCLick();
                                  }
                                },
                                child: Center(
                                    child: GlobalText(AppTexts.retry,
                                        color: Colors.white,
                                        fontSize: 18,
                                        fontFamily:
                                            GoogleFonts.lato().fontFamily,
                                        fontWeight: FontWeight.w700))))),
                    const SizedBox(height: 15),
                  ]),
            ),
          ),
        ],
      ),
      barrierDismissible: false,
      barrierColor: AppColors.primaryDarkColor);
}

Future<void> showNoInternetDialog(Function doOnCLick) async {
  await Get.dialog(
      Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Material(
            color: Colors.transparent,
            child: Container(
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(15)),
              margin: const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
              child: ListView(
                  shrinkWrap: true,
                  padding:
                      const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
                  physics: const NeverScrollableScrollPhysics(),
                  children: [
                    LottieBuilder.asset('assets/json/vpn.json',
                        width: 150, height: 150),
                    const SizedBox(height: 15),
                    GlobalText(AppTexts.internetActive,
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                        fontStyle: FontStyle.normal),
                    const SizedBox(height: 15),
                    Container(
                        height: 48,
                        decoration: BoxDecoration(
                          color: AppColors.secondaryDartColor,
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Material(
                            color: Colors.transparent,
                            child: InkWell(
                                onTap: () async {
                                  final connectivityResult =
                                      await Connectivity().checkConnectivity();
                                  if (connectivityResult ==
                                          ConnectivityResult.mobile ||
                                      connectivityResult ==
                                          ConnectivityResult.wifi) {
                                    Get.back();
                                    doOnCLick();
                                  }
                                },
                                child: Center(
                                    child: GlobalText(AppTexts.retry,
                                        color: Colors.white,
                                        fontSize: 18,
                                        fontFamily:
                                            GoogleFonts.lato().fontFamily,
                                        fontWeight: FontWeight.w700))))),
                    const SizedBox(height: 15),
                  ]),
            ),
          ),
        ],
      ),
      barrierDismissible: false,
      barrierColor: AppColors.primaryDarkColor);
}

class CheckVpnConnection {
  static Future<bool> isVpnActive() async {
    bool isVpnActive;
    final List<NetworkInterface> interfaces = await NetworkInterface.list(
        includeLoopback: false, type: InternetAddressType.any);
    interfaces.isNotEmpty
        ? isVpnActive = interfaces.any((interface) =>
            interface.name.contains('tun') ||
            interface.name.contains('ppp') ||
            interface.name.contains('pptp'))
        : isVpnActive = false;
    return isVpnActive;
  }
}

initOneSignal() async {
  try {
    await OneSignal.shared.setAppId(pingModel.notificationOnesignalAppId ??
        '8cd281e9-3821-47c7-b214-f48feb870706');
    var data = await OneSignal.shared.getDeviceState();

    if (data != null) {
      oneSignalPlayerId = data.userId ?? '';
    }

    await OneSignal.shared
        .promptUserForPushNotificationPermission()
        .then((accepted) {});

    OneSignal.shared
        .setNotificationOpenedHandler((OSNotificationOpenedResult result) {});
  } catch (e) {
    if (kDebugMode) {
      print(e);
    }
  }
}

addDeviceNotificationID() {
  if (oneSignalPlayerId.isNotEmpty) {
    updateContactData('{"notificationPlayerId":$oneSignalPlayerId}');
  }
}

String accessToken = '';

Future<String> getAccessToken() async {
  if (TextUtils(accessToken).isNullOrEmpty) {
    accessToken = await AppSharedPreferences.getStringFromLocalStorage(
            KEY_ACCESS_TOKEN) ??
        "";
  }
  return accessToken;
}

showToast(String message) {
  Fluttertoast.showToast(
      msg: message,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      backgroundColor: Colors.black,
      textColor: Colors.white,
      fontSize: 16.0);
}

parseHtmlString(String htmlString) {
  var document = html.parse(htmlString);

  String? parsedString = html.parse(document.body?.text).documentElement?.text;
  return parsedString;
}

downloadSelectedDocx(Data folderData) async {
  try {
    folderData.isDeleting.value = true;

    var status = await Permission.storage.status;
    if (status.isGranted) {
      String? filename;

      if (folderData.s3url!.contains("docx")) {
        filename = DateTime.now().millisecondsSinceEpoch.toString() + ".docx";
      } else if (folderData.s3url!.contains("doc")) {
        filename = DateTime.now().millisecondsSinceEpoch.toString() + ".doc";
      } else if (folderData.s3url!.contains("ppt")) {
        filename = DateTime.now().millisecondsSinceEpoch.toString() + ".ppt";
      } else if (folderData.s3url!.contains("pptx")) {
        filename = DateTime.now().millisecondsSinceEpoch.toString() + ".pptx";
      }

      if (await File('${await getDownloadsPath()}/$filename').exists()) return;

      String url = folderData.s3url ?? '';

      var request = await HttpClient().getUrl(Uri.parse(url));
      var response = await request.close();
      var bytes = await consolidateHttpClientResponseBytes(response);
      File file = File('${await getDownloadsPath()}/$filename');
      await file.writeAsBytes(bytes);

      showToast("$filename downloaded succesfully");

      folderData.isDeleting.value = false;
    } else {
      if (await Permission.storage.request().isGranted) {
        String? filename;
        if (folderData.s3url!.contains("docx")) {
          filename = DateTime.now().millisecondsSinceEpoch.toString() + ".docx";
        } else if (folderData.s3url!.contains("doc")) {
          filename = DateTime.now().millisecondsSinceEpoch.toString() + ".doc";
        } else if (folderData.s3url!.contains("ppt")) {
          filename = DateTime.now().millisecondsSinceEpoch.toString() + ".ppt";
        } else if (folderData.s3url!.contains("pptx")) {
          filename = DateTime.now().millisecondsSinceEpoch.toString() + ".pptx";
        }

        if (await File('${await getDownloadsPath()}/$filename').exists()) {
          return;
        }

        String url = folderData.s3url ?? '';

        var request = await HttpClient().getUrl(Uri.parse(url));
        var response = await request.close();
        var bytes = await consolidateHttpClientResponseBytes(response);
        File file = File('${await getDownloadsPath()}/$filename');
        await file.writeAsBytes(bytes);

        showToast("$filename downloaded successfully");
        folderData.isDeleting.value = false;
      } else {
        folderData.isDeleting.value = false;
      }
    }
  } catch (err) {
    if (kDebugMode) {
      print(err);
    }
  }
}

downloadAssetDocx(AssetData folderData) async {
  try {
    folderData.isDeleting.value = true;

    var status = await Permission.storage.status;
    if (status.isGranted) {
      String? filename;

      if (folderData.s3url!.contains("docx")) {
        filename = DateTime.now().millisecondsSinceEpoch.toString() + ".docx";
      } else if (folderData.s3url!.contains("doc")) {
        filename = DateTime.now().millisecondsSinceEpoch.toString() + ".doc";
      } else if (folderData.s3url!.contains("ppt")) {
        filename = DateTime.now().millisecondsSinceEpoch.toString() + ".ppt";
      } else if (folderData.s3url!.contains("pptx")) {
        filename = DateTime.now().millisecondsSinceEpoch.toString() + ".pptx";
      }

      if (await File('${await getDownloadsPath()}/$filename').exists()) return;

      String url = folderData.s3url ?? '';

      var request = await HttpClient().getUrl(Uri.parse(url));
      var response = await request.close();
      var bytes = await consolidateHttpClientResponseBytes(response);
      File file = File('${await getDownloadsPath()}/$filename');
      await file.writeAsBytes(bytes);

      showToast("$filename downloaded successfully");

      folderData.isDeleting.value = false;
    } else {
      if (await Permission.storage.request().isGranted) {
        String? filename;
        if (folderData.s3url!.contains("docx")) {
          filename = DateTime.now().millisecondsSinceEpoch.toString() + ".docx";
        } else if (folderData.s3url!.contains("doc")) {
          filename = DateTime.now().millisecondsSinceEpoch.toString() + ".doc";
        } else if (folderData.s3url!.contains("ppt")) {
          filename = DateTime.now().millisecondsSinceEpoch.toString() + ".ppt";
        } else if (folderData.s3url!.contains("pptx")) {
          filename = DateTime.now().millisecondsSinceEpoch.toString() + ".pptx";
        }

        if (await File('${await getDownloadsPath()}/$filename').exists()) {
          return;
        }

        String url = folderData.s3url ?? '';

        var request = await HttpClient().getUrl(Uri.parse(url));
        var response = await request.close();
        var bytes = await consolidateHttpClientResponseBytes(response);
        File file = File('${await getDownloadsPath()}/$filename');
        await file.writeAsBytes(bytes);

        showToast("$filename downloaded succesfully");
        folderData.isDeleting.value = false;
      } else {
        folderData.isDeleting.value = false;
      }
    }
  } catch (err) {
    if (kDebugMode) {
      print(err);
    }
  }
}

showExitDialog() {
  Get.dialog(
    AlertDialog(
      title: const GlobalText(
        'Confirm',
        fontWeight: FontWeight.w700,
      ),
      content: const GlobalText(
        'Are you sure you want to Logout ?',
      ),
      actions: <Widget>[
        TextButton(
          child: GlobalText('No',
              color: AppColors.primaryDarkColor,
              fontSize: 16,
              fontWeight: FontWeight.w500),
          onPressed: () {
            Get.back();
          },
        ),
        TextButton(
          child: GlobalText('Yes',
              color: AppColors.primaryDarkColor,
              fontSize: 16,
              fontWeight: FontWeight.w500),
          onPressed: () async {
            await googleSignIn.signOut();
            await AppSharedPreferences.removeAllString();
            Get.offAllNamed(Routes.LOGIN);
          },
        )
      ],
    ),
    barrierDismissible: false,
  );
}

Future<String> getDownloadsPath() async {
  String directory = "";
  if (Platform.isAndroid) {
    Directory? _downloadsDirectory =
        await DownloadsPathProvider.downloadsDirectory;
    directory = _downloadsDirectory!.path;
  } else {
    var dirs = await getApplicationDocumentsDirectory();
    directory = dirs.path;
  }
  return directory;
}

downloadSelectedImage(dynamic folderData) async {
  try {
    folderData.isDownloading.value = true;

    var status = await Permission.storage.status;
    if (status.isGranted) {
      String filename = "";
      if (folderData.fileName!.contains("jpg")) {
        filename = DateTime.now().millisecondsSinceEpoch.toString() + ".jpg";
      } else if (folderData.fileName!.contains("png")) {
        filename = DateTime.now().millisecondsSinceEpoch.toString() + ".png";
      } else {
        filename = DateTime.now().millisecondsSinceEpoch.toString() + ".jpeg";
      }

      if (await File('${await getDownloadsPath()}/$filename').exists()) return;

      String url = folderData.s3url ?? '';

      var request = await HttpClient().getUrl(Uri.parse(url));
      var response = await request.close();
      var bytes = await consolidateHttpClientResponseBytes(response);
      File file = File('${await getDownloadsPath()}/$filename');
      await file.writeAsBytes(bytes);

      showToast("$filename downloaded successfully");

      folderData.isDownloading.value = false;
    } else {
      if (await Permission.storage.request().isGranted) {
        String filename = "";
        if (folderData.fileName!.contains("jpg")) {
          filename = DateTime.now().millisecondsSinceEpoch.toString() + ".jpg";
        } else if (folderData.fileName!.contains("png")) {
          filename = DateTime.now().millisecondsSinceEpoch.toString() + ".png";
        } else {
          filename = DateTime.now().millisecondsSinceEpoch.toString() + ".jpeg";
        }

        if (await File('${await getDownloadsPath()}/$filename').exists()) {
          return;
        }

        String url = folderData.s3url ?? '';

        var request = await HttpClient().getUrl(Uri.parse(url));
        var response = await request.close();
        var bytes = await consolidateHttpClientResponseBytes(response);
        File file = File('${await getDownloadsPath()}/$filename');
        await file.writeAsBytes(bytes);

        showToast("$filename downloaded successfully");

        folderData.isDownloading.value = false;
      } else {
        folderData.isDownloading.value = false;
      }
    }
  } catch (err) {
    if (kDebugMode) {
      print(err);
    }
  }
}

downloadSelectedPdf(dynamic folderData) async {
  try {
    folderData.isDownloading.value = true;
    var status = await Permission.storage.status;
    if (status.isGranted) {
      String filename =
          DateTime.now().millisecondsSinceEpoch.toString() + ".pdf";

      if (await File('${await getDownloadsPath()}/$filename').exists()) return;

      String url = folderData.s3url ?? '';

      var request = await HttpClient().getUrl(Uri.parse(url));
      var response = await request.close();
      var bytes = await consolidateHttpClientResponseBytes(response);
      File file = File('${await getDownloadsPath()}/$filename');
      await file.writeAsBytes(bytes);

      showToast("$filename downloaded successfully");
      folderData.isDownloading.value = false;
    } else {
      if (await Permission.storage.request().isGranted) {
        String filename =
            DateTime.now().millisecondsSinceEpoch.toString() + ".pdf";

        String dir = "";
        if (Platform.isAndroid) {
        } else {
          var dirs = await getApplicationDocumentsDirectory();
          dir = dirs.path;
        }

        if (await File('$dir/$filename').exists()) return;

        String url = folderData.s3url ?? '';

        var request = await HttpClient().getUrl(Uri.parse(url));
        var response = await request.close();
        var bytes = await consolidateHttpClientResponseBytes(response);
        File file = File('$dir/$filename');
        await file.writeAsBytes(bytes);

        showToast("$filename downloaded successfully");
        folderData.isDownloading.value = false;
      } else {
        folderData.isDownloading.value = false;
      }
    }
  } catch (err) {
    if (kDebugMode) {
      print(err);
    }
  }
}

String validateUrl(String url) {
  String value = url.replaceAll("watch?v=", "embed/");
  if (kDebugMode) {
    print(value);
  }
  return value;
}

calenderDefaultBuilder(context, date, events) {
  return Container(
      padding: const EdgeInsets.only(top: 4),
      margin: const EdgeInsets.only(left: 4, right: 4),
      alignment: Alignment.center,
      child: Column(
        children: [
          GlobalText(getDayOfWeek(date.weekday).toString(),
              color: Colors.black),
          const SizedBox(
            height: 10,
          ),
          GlobalText(date.day.toString(), color: Colors.black),
        ],
      ));
}

calenderOutSideBuilder(context, date, events) {
  return Container(
      padding: const EdgeInsets.only(top: 4),
      margin: const EdgeInsets.only(left: 4, right: 4),
      alignment: Alignment.center,
      child: Column(
        children: [
          GlobalText(getDayOfWeek(date.weekday).toString(), color: Colors.grey),
          const SizedBox(
            height: 10,
          ),
          GlobalText(date.day.toString(), color: Colors.grey),
        ],
      ));
}

calenderSelectedBuilder(context, date, events) {
  return Container(
      padding: const EdgeInsets.only(top: 4),
      margin: const EdgeInsets.only(left: 5, right: 5),
      alignment: Alignment.center,
      decoration: BoxDecoration(
          color: AppColors.primaryDarkColor,
          borderRadius: BorderRadius.circular(17)),
      child: Column(
        children: [
          GlobalText(getDayOfWeek(date.weekday).toString(),
              color: Colors.white),
          const SizedBox(
            height: 10,
          ),
          GlobalText(date.day.toString(), color: Colors.white),
        ],
      ));
}

String getDayOfWeek(int day) {
  switch (day) {
    case 0:
      return "Sun";
    case 1:
      return "Mon";
    case 2:
      return "Tue";
    case 3:
      return "Wed";
    case 4:
      return "Thu";
    case 5:
      return "Fri";
    case 6:
      return "Sat";
  }
  return "Sun";
}

Widget buildItem(AssetData folderData) {
  if (folderData.status == 'DELETED') {
    return const SizedBox();
  }

  if (folderData.fileType == 'file') {
    if (folderData.fileName == null || folderData.fileName!.isEmpty) {
      folderData.setFileName(folderData.s3url ?? ''.split('/').last);
    }
    if (folderData.fileName!.contains('.mp3') ||
        folderData.fileName!.contains('.wav')) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          PlayAudioScreen(folderData.s3url ?? '', folderData.fileName!),
          const SizedBox(height: 10)
        ],
      );
    } else if (folderData.fileName!.contains('.png') ||
        folderData.fileName!.contains('.jpg') ||
        folderData.fileName!.contains('.jpeg')) {
      return Column(
        children: [
          GestureDetector(
            onTap: () {
              showImageDialog(folderData.s3url ?? '');
            },
            child: FadeInImage(
                imageErrorBuilder: (context, object, trace) {
                  return Image.asset(AppImages.coursePlaceHolder);
                },
                placeholder: AssetImage(AppImages.coursePlaceHolder),
                image: NetworkImage('${folderData.s3url}')),
          ),
          const SizedBox(height: 10)
        ],
      );
    } else if (folderData.fileName!.contains('.pdf')) {
      return InkWell(
        onTap: () async {
          await Get.to(() => PdfViewerPage(folderData.s3url ?? ''));
        },
        child: Container(
          height: 67.834,
          margin: const EdgeInsets.only(top: 8),
          padding: const EdgeInsets.symmetric(horizontal: 18),
          decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(
              color: const Color(0xffe8e8e8),
              width: 1,
            ),
            borderRadius: BorderRadius.circular(15),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                flex: 9,
                child: GlobalText(folderData.fileName ?? '',
                    color: const Color(0xff929292),
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,
                    fontSize: 14.0,
                    textOverflow: TextOverflow.ellipsis,
                    textAlign: TextAlign.left),
              ),
              Obx(() => Flexible(
                    flex: 1,
                    child: InkWell(
                        onTap: () {
                          downloadSelectedPdf(folderData);
                        },
                        child: folderData.isDownloading.value == true
                            ? const GlobalIconLoader()
                            : Image.asset(AppImages.downArrowIcon)),
                  ))
            ],
          ),
        ),
      );
    } else if (folderData.fileName!.contains(".doc") ||
        folderData.fileName!.contains(".docx") ||
        folderData.fileName!.contains(".ppt") ||
        folderData.fileName!.contains(".pptx")) {
      return Container(
          height: 67.834,
          margin: const EdgeInsets.only(top: 8),
          padding: const EdgeInsets.symmetric(horizontal: 18),
          decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(
              color: const Color(0xffe8e8e8),
              width: 1,
            ),
            borderRadius: BorderRadius.circular(15),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                flex: 9,
                child: GlobalText(folderData.fileName ?? '',
                    color: const Color(0xff929292),
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,
                    fontSize: 14.0,
                    textOverflow: TextOverflow.ellipsis,
                    textAlign: TextAlign.left),
              ),
              Obx(() => InkWell(
                  onTap: () {
                    downloadAssetDocx(folderData);
                  },
                  child: folderData.isDownloading.value == true
                      ? const GlobalIconLoader()
                      : Image.asset(AppImages.downArrowIcon)))
            ],
          ));
    } else if (folderData.isVideo == true) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          PlayVideoScreen(folderData.s3url ?? ''),
          const SizedBox(height: 10)
        ],
      );
    } else {
      return const SizedBox();
    }
  } else if (folderData.fileType == 'video') {
    if (folderData.fileName == null || folderData.fileName!.isEmpty) {
      folderData.setFileName(folderData.s3url ?? ''.split('/').last);
    }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        PlayVideoScreen(folderData.s3url ?? ''),
        const SizedBox(height: 10)
      ],
    );
  } else if (folderData.fileType == 'link') {
    if (folderData.fileName == null || folderData.fileName!.isEmpty) {
      return const SizedBox();
    }
    if (YoutubeVideoValidator.validateUrl(folderData.fileName!)) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          PlayYouTubeScreen(folderData.fileName ?? ''),
          const SizedBox(height: 10)
        ],
      );
    } else if (folderData.fileName!.contains('vimeo') &&
        !folderData.fileName!.contains('ref=em-share')) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          PlayVimeoScreen(folderData.fileName ?? ''),
          const SizedBox(height: 10)
        ],
      );
    } else {
      return const SizedBox();
    }
  } else if (folderData.fileType == 'text') {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        GlobalText(
          parseHtmlString(folderData.notes ?? ''),
          maxLine: 500,
        ),
        const SizedBox(height: 10)
      ],
    );
  } else if (folderData.fileType == 'document') {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        GlobalText(
          parseHtmlString(folderData.notes ?? ''),
          maxLine: 500,
        ),
        const SizedBox(height: 10)
      ],
    );
  } else {
    return const SizedBox();
  }
}

String? formatPhoneNo(String? str) {
  String? result = str?.substring(str.indexOf('-') + 1);
  return result;
}

String? formatPinCode(String str) {
  String? result = str.substring(0, str.indexOf('-'));
  return result;
}

getVideoWidget(AssetData videoList, bool playVideo) {
  if (videoList.isVideo == true || videoList.fileType == 'video') {
    if (videoList.fileName == null || videoList.fileName!.isEmpty) {
      videoList.setFileName(videoList.s3url ?? ''.split('/').last);
    }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        ClipRRect(
            borderRadius: BorderRadius.circular(15),
            child: PlayVideoScreen(
              videoList.s3url ?? '',
              playVideo: playVideo,
            )),
        const SizedBox(height: 10)
      ],
    );
  } else if (videoList.fileType == 'link') {
    if (videoList.fileName != null || videoList.fileName?.isNotEmpty == true) {
      if (YoutubeVideoValidator.validateUrl(videoList.fileName!)) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            ClipRRect(
                borderRadius: BorderRadius.circular(15),
                child: PlayYouTubeScreen(
                  videoList.fileName ?? '',
                  playVideo: playVideo,
                )),
            const SizedBox(height: 10)
          ],
        );
      } else if (videoList.fileName!.contains('vimeo') &&
          !videoList.fileName!.contains('ref=em-share')) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            ClipRRect(
                borderRadius: BorderRadius.circular(15),
                child: PlayVimeoScreen(
                  videoList.fileName ?? '',
                  playVideo: playVideo,
                )),
            const SizedBox(height: 10)
          ],
        );
      } else {
        return const SizedBox();
      }
    } else {
      return const SizedBox();
    }
  } else {
    return const SizedBox();
  }
}

String getFileName(AssetData videoList) {
  if (videoList.isVideo == true || videoList.fileType == 'video') {
    return videoList.fileName ?? '';
  } else if (videoList.fileType == 'link') {
    return videoList.title?.value ?? videoList.fileName ?? '';
  } else {
    return '';
  }
}

PingModel pingModel = PingModel();
GeneralUserDataResponse userDataResponse = GeneralUserDataResponse();
ContactResponse contactResponse = ContactResponse();
String oneSignalPlayerId = '';
GoogleSignIn googleSignIn = GoogleSignIn(scopes: [
  'email',
  'profile',
]);
var dataTest = QuizNewData();
var quizTest = QuizQuestionResponse();
var quizQuestion = QuizQuestionResponse().obs;
