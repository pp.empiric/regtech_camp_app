class AppImages {
  const AppImages._();

  static const _base = 'assets/images/';

  static String _fullPath(String name) => '$_base$name';

  static final firstLogoImage = _fullPath('main_logo.png');
  static final firstSmallLogo = _fullPath('small_mainLogo.png');
  static final secondSmallLogo = _fullPath('small_vector.png');
  static final secondLogoImage = _fullPath('screen_two.png');
  static final rectTopImage = _fullPath('rect_small.png');
  static final coursesOneImg = _fullPath('course_img_ones.png');
  static final profileImage = _fullPath('profile_logo.png');
  static final courseTwoImage = _fullPath('course_img_two.png');
  static final googleLogo = _fullPath('google_logo.png');
  static final svgIconEdit = _fullPath('edit_icon.svg');
  static final svgPlusSign = _fullPath('plus_icon.svg');
  static final bgBigPurpleImage = _fullPath('bg_big_purple_image.png');
  static final curvedArrow = _fullPath('curved_arrow.svg');
  static final notification = _fullPath('notification.png');
  static final myCourseIcon = _fullPath('my_course_icon.png');
  static final eventsIcon = _fullPath('events_icon.svg');
  static final filesIcon = _fullPath('files_icon.svg');
  static final homeIcon = _fullPath('home_icon.svg');
  static final downIcon = _fullPath('down_icon.svg');
  static final powerIcon = _fullPath('power_icon.svg');
  static final bgPurpleImage = _fullPath('bg_purple_image.png');
  static final coursePlaceHolder = _fullPath('course_placeholder.png');
  static final downArrowIcon = _fullPath('down_arrow_icon.png');
  static final deleteVectorIcon = _fullPath('delete_vector_icon.png');
  static final uploadIcon = _fullPath('upload_icon.png');
  static final forwardArrowIcon = _fullPath('forward_arrow_icon.png');
  static final rightTickIcon = _fullPath('right_tick_icon.svg');
  static final deleteIcon = _fullPath('delete_icon.svg');
  static final folderIcon = _fullPath('folder_icon_final.png');
  static final checkBoxIcon = _fullPath('checkbox_icon.svg');
  static final newFileImage = _fullPath('add_file.svg');
  static final otherDownloadIcon = _fullPath('other_download.svg');
  static final newFolderImage = _fullPath('create_folder.svg');
  static final calendarBackArrow = _fullPath('calendar_backarrow.png');
  static final calendarRightArrow = _fullPath('calendar_rightarrow.png');
  static final clockIcon = _fullPath('clock_icon.svg');
  static final quizCongressImage = _fullPath('quiz_congress.png');
}
