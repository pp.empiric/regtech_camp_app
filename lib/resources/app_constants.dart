import '../utils.dart';
import 'clients.dart';

const Duration defaultTransitionDuration = Duration(milliseconds: 150);
const double minWidthPhone = 375.0;

final String COURSE_IMAGE_BASE =
    "https://res.cloudinary.com/${pingModel.cloudName ?? "dy1q4oxdv"}/image/upload/c_thumb,h_340,w_740/";
final String IMAGE_BASE =
    "https://res.cloudinary.com/${pingModel.cloudName ?? "dy1q4oxdv"}/image/upload/c_fill/";
final String QUIZ_IMAGE_BASE = "https://res.cloudinary.com/${pingModel.cloudName ?? "dy1q4oxdv"}/image/";

// -- dev
const BASE = 'https://api.wajooba.$baseApiDomain';
const BASE_URL = 'https://api.wajooba.$baseApiDomain/rest';

 // --production
// const BASE = 'https://api.onwajooba.$baseApiDomain';
// const BASE_URL = 'https://api.onwajooba.$baseApiDomain/rest';
const BASE_S3_ORIGIN = 'https://$ORG_ID.wajooba.com';

const KEY_ACCESS_TOKEN = 'accessToken';
const KEY_USER_NAME = 'userName';
const KEY_USER_EMAIL = 'userEmail';
const KEY_USER_PHONE = 'userPhone';
const KEY_USER_PROFILE_PIC = 'userPicture';
const KEY_GUID = 'GUID';
const KEY_DONATION = 'DONATION';
const String KEY_PRODUCT = 'PRODUCT';
const String KEY_PAID = 'PAID';


