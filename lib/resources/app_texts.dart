class AppTexts {
  const AppTexts._();

  static String poweredByWajooba = 'Powered by Wajooba';
  static String forgetPassword = 'Forgot password?';
  static String signIn = 'Sign In';
  static String or = 'Or';
  static String singWIthGoogle = 'Sign In with Google';
  static String singUpWIthGoogle = 'Sign Up with Google';
  static String doNotHaveAccount = 'Do not have an account?';
  static String doHaveAccount = 'Do you have an account?';
  static String signUp = 'Sign Up';
  static String bySignUp =
      'By clicking the Sign Up button,\nyou agree to The Future Academy';
  static String bySignIn =
      'By clicking the Sign In button,\nyou agree to The Future Academy';

  static String termsCondition = 'Terms & Conditions';
  static String and = ' and ';
  static String privacyPolicy = 'Privacy Policy';
  static String yourId = 'Your EMAIL';
  static String password = 'Password';

  static String firstName = 'First Name';
  static String fullName = 'Full Name';
  static String lastName = 'Last Name';
  static String email = 'Email';
  static String phoneNumber = 'Phone Number';
  static String confirmPassword = 'Confirm Password';

  static String myCourses = 'My Courses';
  static String exploreCourse = 'Explore more courses';

  static String vpnActive = 'Please turn off VPN to Continue!!';
  static String internetActive = 'Please turn on Internet to Continue!!';
  static String retry = 'Retry';

  static String profileName = 'My Profile';
  static String achievements = 'No achievements';
  static String feelFreeToAsk = 'Feel Free to Ask, We are ready to Help';
  static String callUsAt = 'Call us at';

  static String businessBreakthrough = 'Business Breakthrough';

  static String sectionTitle = 'Section One';

  static String lessonTitle = 'Lesson 01';
  static String downloadFiles = 'Download Files';
  static String uploadFiles = 'Upload Files';
  static String addFiles = 'Add Files';
  static String activityCompletion = 'I have completed this activity';
  static String submitActivity = 'Submit Activity';
  static String sendComment = 'Send Comment';
  static String typeYourMessage = 'Type your message';

  static String eventsTitle = 'Events';
  static String submit = 'Submit';
  static String cancel = 'Cancel';
  static String updateProfile = 'Update my Profile';
  static String verifyEmail = 'Verify Email';
  static String verifyPhone = 'Verify Phone';
  static String back = 'Back';
  static String enterPhoneNumber = 'Enter phone number';
  static String sendOtp = 'Send otp';
  static String continueText = 'Continue';
  static String newPassWord = 'New Password';
  static String resetPassWord = 'Reset Password';
  static String congratulations = 'Congratulations!';
  static String congratulationsText =
      'You have successfully\nreset the password!';
  static String courses = 'Courses';
}
