import 'dart:ui';

class AppColors {
  const AppColors._();

  static Color primaryDarkColor = const Color(0xff3B2E7E);
  static Color secondaryDartColor = const Color(0xffE86E25);
  static Color commonTextColor = const Color(0xff5E5E5E);
}
