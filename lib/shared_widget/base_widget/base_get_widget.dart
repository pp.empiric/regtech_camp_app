import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'theme_get_widget.dart';

abstract class BaseGetWidget<T extends GetxController> extends ThemedWidget
    implements GetView<T> {
  @override
  T get controller => GetInstance().find<T>(tag: tag);

  @override
  final String? tag;

  const BaseGetWidget({Key? key, this.tag}) : super(key: key);
}
