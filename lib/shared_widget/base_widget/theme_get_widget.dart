import 'package:flutter/material.dart';

abstract class ThemedWidget extends StatelessWidget {
  const ThemedWidget({Key? key}) : super(key: key);

  Widget body(BuildContext context);

  @override
  Widget build(BuildContext context) {
    return body(context);
  }
}