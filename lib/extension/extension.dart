extension CapExtension on String {
  String get inCaps {
    String str = this;
    try {
      if (str.isNotNullOrEmpty) {
        str = '${this[0].toUpperCase()}${substring(1)}';
      }
    } catch (e) {
      print(e);
    }
    return str;
  }
}

extension TextUtils on String {
  bool get isNullOrEmpty {
    return (isEmpty);
  }

  bool get isNotNullOrEmpty {
    return (isNotEmpty);
  }
}
extension BoolParsing on String {
  bool parseBool() {
    return toLowerCase() == 'true';
  }
}
