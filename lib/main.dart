import 'dart:io';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:responsive_framework/responsive_framework.dart';
import 'routes/app_pages.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  await GetStorage.init();
  // HttpOverrides.global = MyHttpOverrides();

  runApp(const MyApp());
}
// class MyHttpOverrides extends HttpOverrides{
//   @override
//   HttpClient createHttpClient(SecurityContext? context){
//     return super.createHttpClient(context)
//       ..badCertificateCallback = (X509Certificate cert, String host, int port)=> true;
//   }
// }

class MyApp extends StatelessWidget {
  static FirebaseAnalyticsObserver observer = FirebaseAnalyticsObserver(analytics: FirebaseAnalytics.instance);

  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
        navigatorObservers: <NavigatorObserver>[observer],
        initialRoute: AppPages.initial,
        defaultTransition: Transition.fadeIn,
        transitionDuration: const Duration(milliseconds: 150),
        getPages: AppPages.routes,
        debugShowCheckedModeBanner: false,
        title: 'RegTechCamp',
        builder: (context, child) => ResponsiveWrapper.builder(
              child,
              minWidth: 375,
              defaultScale: true,
              breakpoints: const [
                ResponsiveBreakpoint.autoScale(375, name: MOBILE),
              ],
            ));
  }
}
