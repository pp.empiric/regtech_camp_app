import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:regtech_camp_app/common_widget/global_text.dart';
import 'package:regtech_camp_app/common_widget/profile_widget.dart';
import 'package:regtech_camp_app/modules/home/controllers/home_controller.dart';
import 'package:regtech_camp_app/resources/app_colors.dart';
import 'package:regtech_camp_app/resources/app_images.dart';
import 'package:regtech_camp_app/common_widget/global_svg_button_icon_widget.dart';

class FilesOne extends StatelessWidget {
   const FilesOne({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(shrinkWrap: true, children: [
        const ProfileWidget(title: "My Files",backVisibility: false,),
        GridView.builder(
            shrinkWrap: true,
            scrollDirection: Axis.vertical,
            itemCount: 3,
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2),
            itemBuilder: (BuildContext context, int index) {
              return Container(
                width: 150,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15.0),
                    boxShadow: const [
                      BoxShadow(
                        offset: Offset(0, 1),
                        blurRadius: 11,
                        color: Color(0x47028e4f),
                        spreadRadius: 0,
                      ),
                    ],
                    color: Colors.white),
                margin: const EdgeInsets.only(top: 24, left: 15, right: 15),
                child: Padding(
                  padding: const EdgeInsets.only(top: 8.0, left: 5, right: 5),
                  child: Column(
                    //crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        margin: const EdgeInsets.only(
                          left: 120,
                        ),
                        height: 24,
                        width: 24,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(50.0),
                          color: AppColors.primaryDarkColor,
                        ),
                        child: const Center(
                          child: GlobalText(
                            '3',
                            color: Colors.white,
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                      Image.asset(AppImages.folderIcon),
                      const SizedBox(height: 5.0),
                      GlobalText(
                        'First Week Submission',
                        color: const Color(0xff5F2D2C),
                        textAlign: TextAlign.center,
                        fontFamily: GoogleFonts.lato().fontFamily,
                        fontWeight: FontWeight.w400,
                        fontStyle: FontStyle.normal,
                        fontSize: 15,
                        textOverflow: TextOverflow.ellipsis,
                        maxLine: 2,
                      ),
                      // const SizedBox(height: 5.0),
                      GlobalSvgButton(
                        onTap: () {},
                        color: const Color(0xffD24C4C),
                        assetPath: AppImages.deleteIcon,
                        padding:
                            const EdgeInsets.only(left: 120.0, bottom: 3.50),
                      ),
                      //Container(color: Colors.blue,height: 20,width: 20,),
                    ],
                  ),
                ),
              );
            }),
      ]),
    );
  }
}
