import 'dart:io';

import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:regtech_camp_app/common_widget/custom_edit_text.dart';
import 'package:regtech_camp_app/common_widget/global_edit_text.dart';
import 'package:regtech_camp_app/common_widget/global_outline_button.dart';
import 'package:regtech_camp_app/modules/login/controllers/login_controller.dart';
import 'package:regtech_camp_app/resources/app_colors.dart';
import 'package:regtech_camp_app/services/apis.dart';
import 'package:regtech_camp_app/utils.dart';

import '../../../common_widget/global_add_details_card.dart';
import '../../../common_widget/global_back_drop.dart';
import '../../../common_widget/global_button.dart';
import '../../../common_widget/global_svg_button_icon_widget.dart';
import '../../../common_widget/global_text.dart';
import '../../../resources/app_images.dart';
import '../../../resources/app_texts.dart';
import '../../../routes/app_routes.dart';
import '../../home/controllers/home_controller.dart';
import '../controllers/profile_controller.dart';

class ProfileScreen extends GetView<ProfileController> {
  final homeController = Get.put(HomeController());

  @override
  final controller = Get.put(ProfileController());
  static var style = TextStyle(
    fontSize: 16,
    fontStyle: FontStyle.normal,
    height: 2.0,
    fontWeight: FontWeight.w400,
    color: AppColors.commonTextColor,
  );

  static var titleStyle = const TextStyle(
    fontSize: 16,
    fontStyle: FontStyle.normal,
    fontWeight: FontWeight.w700,
    color: Color(0xff6C6D71),
  );

  static var subTitleStyle = const TextStyle(
    fontSize: 16,
    fontStyle: FontStyle.normal,
    fontWeight: FontWeight.w400,
    color: Color(0xff6C6D71),
  );

  ProfileScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Obx(
        () {
          return WillPopScope(
            onWillPop: () async {
              Get.offNamed(Routes.HOME);
              return true;
            },
            child: Stack(
              children: [
                ListView(
                  padding: EdgeInsets.zero,
                  physics: const ScrollPhysics(),
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  children: [
                    Stack(
                      children: [
                        Image.asset(AppImages.bgBigPurpleImage,
                            fit: BoxFit.fitWidth),
                        Container(
                          margin: const EdgeInsets.only(
                            top: 35,
                          ),
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  (controller.isEditProfile.value == false)
                                      ? InkWell(
                                          child: IconButton(
                                              onPressed: () {
                                                Get.offNamed(Routes.HOME);
                                              },
                                              icon: const Icon(
                                                  CupertinoIcons.back,
                                                  color: Colors.white,
                                                  size: 25)),
                                        )
                                      : const SizedBox(width: 30),
                                  Expanded(
                                    child: Container(
                                      alignment: Alignment.center,
                                      child: GlobalText(
                                        AppTexts.profileName,
                                        color: Colors.white,
                                        fontSize: 18,
                                        textOverflow: TextOverflow.ellipsis,
                                        fontWeight: FontWeight.w700,
                                        fontStyle: FontStyle.normal,
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  ),
                                  GlobalSvgButton(
                                    assetPath: AppImages.powerIcon,
                                    padding: const EdgeInsets.only(
                                        left: 10,
                                        right: 15,
                                        bottom: 10,
                                        top: 5),
                                    onTap: () {
                                      showExitDialog();
                                      var controller =
                                          Get.put(LogInController());
                                      controller.emailController.clear();
                                      controller.passwordController.clear();
                                      controller.isSignUp.value = false;
                                    },
                                  )
                                ],
                              ),
                              Container(
                                margin: const EdgeInsets.only(top: 10),
                                child: Stack(
                                  children: [
                                    homeController
                                                .profilePic.value.isNotEmpty ==
                                            true
                                        ? Container(
                                            margin: const EdgeInsets.symmetric(
                                                horizontal: 147),
                                            width: 82,
                                            height: 82,
                                            decoration: BoxDecoration(
                                              shape: BoxShape.circle,
                                              border: Border.all(
                                                  color: Colors.white,
                                                  width: 4),
                                              image: DecorationImage(
                                                image: NetworkImage(
                                                    homeController
                                                        .profilePic.value),
                                                fit: BoxFit.fill,
                                              ),
                                            ),
                                          )
                                        : Container(
                                            margin: const EdgeInsets.symmetric(
                                                horizontal: 147),
                                            width: 82,
                                            height: 82,
                                            decoration: BoxDecoration(
                                              shape: BoxShape.circle,
                                              border: Border.all(
                                                  color: Colors.white,
                                                  width: 2),
                                            ),
                                            child: Image.asset(
                                              AppImages.profileImage,
                                              width: 82,
                                              height: 82,
                                              fit: BoxFit.contain,
                                            ),
                                          ),
                                    Container(
                                      margin: const EdgeInsets.only(
                                          left: 201, top: 54),
                                      width: 35,
                                      height: 35,
                                      decoration: const BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: Colors.white,
                                      ),
                                      child: IconButton(
                                          padding: const EdgeInsets.all(0),
                                          alignment: Alignment.center,
                                          color: AppColors.secondaryDartColor,
                                          onPressed: () =>
                                              controller.uploadFiles(),
                                          icon: const Icon(
                                              CupertinoIcons.photo_camera),
                                          iconSize: 20),
                                    ),
                                  ],
                                ),
                              ),
                              Obx(() => (controller.isEditProfile.value ==
                                      false)
                                  ? ListView(
                                      shrinkWrap: true,
                                      physics:
                                          const NeverScrollableScrollPhysics(),
                                      children: [
                                        Container(
                                          width: 344,
                                          margin: const EdgeInsets.only(
                                            top: 38,
                                            left: 14,
                                            right: 17.0,
                                          ),
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(15.0),
                                              boxShadow: const [
                                                BoxShadow(
                                                    color: Color(0x2e000000),
                                                    offset: Offset(0, 0),
                                                    blurRadius: 10,
                                                    spreadRadius: 0)
                                              ],
                                              color: Colors.white),
                                          child: Padding(
                                            padding: const EdgeInsets.only(
                                                left: 14.0,
                                                right: 10.0,
                                                bottom: 14.0,
                                                top: 10),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: [
                                                    const GlobalText(
                                                      'Personal Info',
                                                      fontWeight:
                                                          FontWeight.w500,
                                                      fontStyle:
                                                          FontStyle.normal,
                                                      fontSize: 18,
                                                      color: Color(0xff000000),
                                                      textAlign:
                                                          TextAlign.start,
                                                    ),
                                                    GlobalSvgButton(
                                                      padding:
                                                          const EdgeInsets.all(
                                                              10),
                                                      onTap: () {
                                                        controller.isEditProfile
                                                            .value = true;
                                                      },
                                                      assetPath:
                                                          AppImages.svgIconEdit,
                                                      color: AppColors
                                                          .primaryDarkColor,
                                                    ),
                                                  ],
                                                ),
                                                if (homeController
                                                        .userName.value !=
                                                    '')
                                                  GlobalText(
                                                    homeController
                                                        .userName.value,
                                                    textStyle: style,
                                                  ),
                                                if (homeController
                                                        .profileEmail.value !=
                                                    '')
                                                  GlobalText(
                                                    homeController
                                                        .profileEmail.value,
                                                    textStyle: style,
                                                  ),
                                                if (homeController
                                                        .phoneNumber.value !=
                                                    '')
                                                  GlobalText(
                                                    homeController
                                                        .phoneNumber.value,
                                                    textStyle: style,
                                                  ),
                                              ],
                                            ),
                                          ),
                                        ),
                                        const SizedBox(
                                          height: 20.0,
                                        ),
                                        Obx(() => (controller
                                                    .updatedCustomFieldList
                                                    .isNotEmpty ==
                                                true)
                                            ? Card(
                                                margin: const EdgeInsets.only(
                                                    left: 14, right: 17.0),
                                                shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          15.0),
                                                ),
                                                child: Container(
                                                  width: Get.width,
                                                  decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              15.0),
                                                      boxShadow: const [
                                                        BoxShadow(
                                                            color: Color(
                                                                0x2e000000),
                                                            offset:
                                                                Offset(0, 0),
                                                            blurRadius: 10,
                                                            spreadRadius: 0)
                                                      ],
                                                      color: Colors.white),
                                                  child: Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            left: 12.8,
                                                            right: 14.0,
                                                            top: 10,
                                                            bottom: 11.8),
                                                    child: ListView(
                                                      shrinkWrap: true,
                                                      physics:
                                                          const NeverScrollableScrollPhysics(),
                                                      children: [
                                                        Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .spaceBetween,
                                                          children: [
                                                            const GlobalText(
                                                              'Professional Info',
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w500,
                                                              fontStyle:
                                                                  FontStyle
                                                                      .normal,
                                                              fontSize: 18,
                                                              color: Color(
                                                                  0xff000000),
                                                              textAlign:
                                                                  TextAlign
                                                                      .start,
                                                            ),
                                                            GlobalSvgButton(
                                                              padding:
                                                                  const EdgeInsets
                                                                      .all(10),
                                                              onTap: () {
                                                                controller
                                                                    .showEditProfileDialog();
                                                              },
                                                              assetPath: AppImages
                                                                  .svgIconEdit,
                                                              color: AppColors
                                                                  .primaryDarkColor,
                                                            ),
                                                          ],
                                                        ),
                                                        Obx(
                                                            () => ListView
                                                                    .builder(
                                                                  padding:
                                                                      EdgeInsets
                                                                          .zero,
                                                                  physics:
                                                                      const NeverScrollableScrollPhysics(),
                                                                  scrollDirection:
                                                                      Axis.vertical,
                                                                  shrinkWrap:
                                                                      true,
                                                                  itemBuilder:
                                                                      (context,
                                                                          index) {
                                                                    if (controller
                                                                            .updatedCustomFieldList[index]
                                                                            .value
                                                                            .value !=
                                                                        '') {
                                                                      return Column(
                                                                        crossAxisAlignment:
                                                                            CrossAxisAlignment.start,
                                                                        children: [
                                                                          const SizedBox(
                                                                              height: 15),
                                                                          GlobalText(
                                                                              controller.updatedCustomFieldList[index].name ?? '',
                                                                              textStyle: titleStyle),
                                                                          const SizedBox(
                                                                              height: 2),
                                                                          GlobalText(
                                                                              controller.updatedCustomFieldList[index].value.value.toString(),
                                                                              textStyle: subTitleStyle),
                                                                        ],
                                                                      );
                                                                    } else {
                                                                      return const SizedBox();
                                                                    }
                                                                  },
                                                                  itemCount:
                                                                      controller
                                                                          .updatedCustomFieldList
                                                                          .length,
                                                                )),
                                                      ],
                                                    ),
                                                  ),
                                                ))
                                            : GlobalAddDetails(
                                                text:
                                                    'Add Professional Details',
                                                onTap: () => controller
                                                    .showEditProfileDialog(),
                                              )),
                                        const SizedBox(
                                          height: 13.0,
                                        ),
                                        const SizedBox(
                                          height: 41,
                                        ),
                                        GlobalButton(
                                          title: 'Confirm',
                                          width: 296,
                                          margin: const EdgeInsets.only(
                                              left: 39, right: 40, bottom: 40),
                                          onTap: () {
                                            Get.offNamed(Routes.HOME);
                                          },
                                        ),
                                      ],
                                    )
                                  : ListView(
                                      shrinkWrap: true,
                                      physics:
                                          const NeverScrollableScrollPhysics(),
                                      children: [
                                        const SizedBox(height: 70),
                                        CustomEditText(
                                          hintText: AppTexts.firstName,
                                          value: homeController.userName.value,
                                          isEditText:
                                              controller.isEditProfile.value,
                                          onTextChanged: (value) {
                                            print(value);
                                            controller.userName.value = value;
                                          },
                                          controller: controller
                                              .editFirstNameController
                                            ..text =
                                                homeController.userName.value,
                                        ),
                                        const SizedBox(height: 20),
                                        CustomEditText(
                                          hintText: AppTexts.email,
                                          value:
                                              homeController.profileEmail.value,
                                          isEditText:
                                              controller.isEditProfile.value,
                                          onTextChanged: (value) {
                                            controller.profileEmail.value =
                                                value;
                                          },
                                          controller:
                                              controller.editEmailController
                                                ..text = homeController
                                                    .profileEmail.value,
                                        ),
                                        Container(
                                          height: 50,
                                          width: 296,
                                          alignment: Alignment.center,
                                          padding:
                                              const EdgeInsets.only(right: 10),
                                          margin: const EdgeInsets.only(
                                              left: 40, right: 40, top: 20),
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(10.0),
                                              border: Border.all(
                                                  color: AppColors
                                                      .primaryDarkColor,
                                                  style: BorderStyle.solid,
                                                  width: 1)),
                                          child: Row(
                                            children: [
                                              // GlobalText("gu---${homeController.phoneNumber.value}",color: Colors.pink,),
                                              CountryCodePicker(
                                                padding: EdgeInsets.zero,
                                                onChanged: (value) {
                                                  controller.code.value =
                                                      value.dialCode ?? '';
                                                },
                                                initialSelection: homeController
                                                        .phoneNumber
                                                        .value
                                                        .isNotEmpty
                                                    ? formatPinCode(
                                                        homeController
                                                            .phoneNumber.value)
                                                    : '+91',
                                                favorite: const ['+91'],
                                                textStyle: TextStyle(
                                                    color: AppColors
                                                        .primaryDarkColor),
                                                showOnlyCountryWhenClosed:
                                                    false,
                                                alignLeft: false,
                                              ),
                                              Expanded(
                                                  child: GlobalEditText(
                                                      AppTexts.phoneNumber,
                                                      Platform.isIOS
                                                          ? const TextInputType
                                                                  .numberWithOptions(
                                                              signed: true,
                                                              decimal: true)
                                                          : TextInputType
                                                              .number, (value) {
                                                controller.number.value = value;
                                              },
                                                      textFormatter: [
                                                    LengthLimitingTextInputFormatter(
                                                        16)
                                                  ],
                                                      textAlign:
                                                          TextAlign.start,
                                                      controller: controller
                                                          .editPhoneNoController
                                                        ..text = formatPhoneNo(
                                                                homeController
                                                                    .phoneNumber
                                                                    .value) ??
                                                            ''))
                                            ],
                                          ),
                                        ),
                                        const SizedBox(
                                          height: 20,
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            GlobalOutlineButton(
                                              text: AppTexts.cancel,
                                              fontSize: 22,
                                              width: 146,
                                              onTap: () {
                                                controller.isEditProfile.value =
                                                    false;
                                              },
                                            ),
                                            const SizedBox(
                                              width: 10,
                                            ),
                                            GlobalButton(
                                              title: AppTexts.submit,
                                              fontSize: 22,
                                              width: 146,
                                              onTap: () {
                                                controller
                                                    .updateContactDetails();
                                              },
                                            )
                                          ],
                                        ),
                                        const SizedBox(height: 40),
                                      ],
                                    ))
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                Obx(() => Container(
                      child: (controller.isApiCalling.value == true)
                          ? const GlobalBackDrop()
                          : const SizedBox(),
                    ))
              ],
            ),
          );
        },
      ),
    );
  }
}
