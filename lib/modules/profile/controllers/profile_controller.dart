import 'dart:convert';
import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:regtech_camp_app/common_widget/bottom_dialog.dart';
import 'package:regtech_camp_app/model/CustomFieldModel.dart';
import 'package:regtech_camp_app/model/custom_field_response.dart';

import '../../../resources/app_constants.dart';
import '../../../services/apis.dart';
import '../../../services/app_preference.dart';
import '../../../utils.dart';
import '../../home/controllers/home_controller.dart';

class ProfileController extends GetxController {
  var userName = ''.obs;
  var phoneNumber = ''.obs;
  var profileEmail = ''.obs;
  var isEditProfile = false.obs;
  var code = "+91".obs;
  var number = "".obs;
  var isApiCalling = false.obs;
  var customFieldList = <CustomField>[].obs;
  var updatedCustomFieldList = <CustomField>[].obs;
  var isCustomResponse = false.obs;
  var updatedCustomResponse = <CustomField>[].obs;

  TextEditingController editFirstNameController = TextEditingController();
  TextEditingController editPhoneNoController = TextEditingController();
  TextEditingController editEmailController = TextEditingController();

  var homeController = Get.put(HomeController());

  @override
  void onInit() {
    getCustomFields();
    super.onInit();
  }

  bool radioValidation(CustomField customField) {
    if (customField.option1 != null) {
      return true;
    } else if (customField.option2 != null) {
      return true;
    } else if (customField.option3 != null) {
      return true;
    } else if (customField.option4 != null) {
      return true;
    } else {
      return false;
    }
  }

  Future<void> getCustomFields() async {
    final type = await isSafeToCallApi();
    if (type == 0) {
      await showVPNDialog(() async {
        await getCustomFields();
      });
    } else if (type == 1) {
      await showNoInternetDialog(() async {
        await getCustomFields();
      });
    } else {
      isApiCalling.value = true;
      customFieldList.clear();

      final value = await customFieldListApi();
      customFieldList.addAll(value);
      await getUpdatedCustomField();
      customFieldList.refresh();
      isApiCalling.value = false;
    }
  }

  updateCustomField() async {
    for (var element in customFieldList) {
      if (element.isMandatory == true) {
        if (element.value.value == '') {
          showSnackBar(
              'Please  enter ${element.name}', '', Colors.red, Icons.error);
          return;
        }
      }
    }
    Get.back();
    isApiCalling.value = true;
    List<CustomFieldModel> list = [];
    for (var e in customFieldList) {
      list.add(CustomFieldModel(e.guId, e.value.value));
    }
    String body = '{"customFields":${jsonEncode(list)}}';
    await updateContactData(body);
    await getUpdatedCustomField();

    isApiCalling.value = false;
  }

  getUpdatedCustomField() async {
    updatedCustomFieldList.clear();
    updatedCustomResponse.value = await updateCustomFieldApi();
    if (updatedCustomResponse.isNotEmpty) {
      updatedCustomFieldList.addAll(updatedCustomResponse);
    }
    await resetCustomFieldValue();
  }

  resetCustomFieldValue() {
    if (updatedCustomResponse.isNotEmpty) {
      for (var temp in updatedCustomResponse) {
        for (var value in customFieldList) {
          if (temp.guId == value.guId) {
            value.value = temp.value;
          }
        }
      }
    }
  }

  updateContactDetails() async {
    isEditProfile.value = false;
    isApiCalling.value = true;

    homeController.userName.value =
        userName.value == '' ? homeController.userName.value : userName.value;
    homeController.profileEmail.value = profileEmail.value == ''
        ? homeController.profileEmail.value
        : profileEmail.value;
    phoneNumber.value =
        '${code.value}-${number.value == "" ? formatPhoneNo(homeController.phoneNumber.value) : number.value}';
    homeController.phoneNumber.value = phoneNumber.value;
    final body =
        '{"name":"${homeController.userName.value}","fullname":"","lastname":"","email":"${homeController.profileEmail.value}","picture":"${homeController.profilePic.value}","phone":"${homeController.phoneNumber.value}"}';
    print(body);
    await updateContactData(body);
    isApiCalling.value = false;
  }

  showEditProfileDialog() async {
    Get.bottomSheet(
      BottomDialogWidget(),
      enableDrag: false,
      ignoreSafeArea: true,
      isScrollControlled: true,
      isDismissible: true,
      useRootNavigator: true,
    );
  }

  Future<void> uploadFiles() async {
    try {
      final FilePickerResult? result = await FilePicker.platform.pickFiles(
        type: FileType.image,
        allowMultiple: false,
      );

      if (result == null) {
        showToast('Please select image to upload');
      } else {
        final path = result.files[0].path ?? '';
        if (path.isNotEmpty) {
          isApiCalling.value = true;
          final request = http.MultipartRequest('POST',
              Uri.parse('https://api.cloudinary.com/v1_1/dy1q4oxdv/upload'));
          request.fields['folder'] = 'm300';
          request.fields['tags'] = 'm300';
          request.fields['upload_preset'] = 'qjdp0fft';
          request.fields['context'] = 'photo=${File(path).lengthSync()}';

          request.files.add(http.MultipartFile('file',
              File(path).readAsBytes().asStream(), File(path).lengthSync(),
              filename: path.split('/').last));
          final response = await request.send();

          if (response.statusCode == 200 || response.statusCode == 201) {
            final string = await response.stream.bytesToString();
            final body = jsonDecode(string);
            if (body['url'] != null) {
              homeController.profilePic.value = body['url'];
              AppSharedPreferences.saveStringToLocalStorage(
                  KEY_USER_PROFILE_PIC, body['url']);
              isApiCalling.value = false;
              callUpdate(url: body['url']);
            } else if (body['secure_url'] != null) {
              homeController.profilePic.value = body['secure_url'];
              AppSharedPreferences.saveStringToLocalStorage(
                  KEY_USER_PROFILE_PIC, body['secure_url']);
              isApiCalling.value = false;
              callUpdate(url: body['secure_url']);
            } else {
              isApiCalling.value = false;
              showToast('Failed to upload Image');
            }
          } else {
            isApiCalling.value = false;
            showToast('Failed to upload Image');
          }
        } else {
          isApiCalling.value = false;
          showToast('Failed to upload Image');
        }
      }
    } catch (e) {
      showToast('Failed to upload Image');
    }
  }

  callUpdate({String? url, var body, isBody = false}) {
    if (isBody == false) {
      if (isEditProfile.value == false) {
        updateContactData('{"picture":"$url"}');
      }
    } else {
      updateContactData(body ?? '{}');
    }
  }
}
