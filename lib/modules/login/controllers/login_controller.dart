import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:http/http.dart' as http;
import 'package:regtech_camp_app/model/custom_field_response.dart';

import '../../../model/log_in_response.dart';
import '../../../resources/app_constants.dart';
import '../../../resources/clients.dart';
import '../../../routes/app_routes.dart';
import '../../../services/apis.dart';
import '../../../services/app_preference.dart';
import '../../../utils.dart';

class LogInController extends GetxController {
  var isSignUp = false.obs;
  var isApiCalling = false.obs;
  var loginMsg = "".obs;
  var password = ''.obs;
  var userName = ''.obs;
  var phoneNumber = ''.obs;
  var email = ''.obs;
  var confirmPassword = ''.obs;
  var code = '+91'.obs;
  var number = ''.obs;

  var emailController = TextEditingController();
  var passwordController = TextEditingController();

  Future<void> submit() async {
    if (isSignUp.value) {
      if (userName.value.isEmpty) {
        showSnackBar('Enter Full Name', '', Colors.red, Icons.error);
        return;
      }

      if (email.value.isEmpty) {
        showSnackBar('Enter Email', '', Colors.red, Icons.error);
        return;
      }

      if (!email.value.isEmail) {
        showSnackBar('Enter Valid Email', '', Colors.red, Icons.error);
        return;
      }

      if (phoneNumber.value.isEmpty) {
        showSnackBar('Enter Phone Number', '', Colors.red, Icons.error);
        return;
      }

      if (phoneNumber.value.length > 16) {
        showSnackBar('Invalid Mobile Number', '', Colors.red, Icons.error);
        return;
      }

      if (password.value.isEmpty) {
        showSnackBar('Enter Password', '', Colors.red, Icons.error);
        return;
      }
      if (password.value.length < 8) {
        showSnackBar(
            'Password must be 8 characters', '', Colors.red, Icons.error);
        return;
      }

      if (confirmPassword.value.isEmpty) {
        showSnackBar('Enter Confirm Password', '', Colors.red, Icons.error);
        return;
      }

      if (password.value != confirmPassword.value) {
        showSnackBar('Password does not match', '', Colors.red, Icons.error);
        return;
      }
    } else {
      if (email.value.isEmpty) {
        showSnackBar('Enter Email Id', '', Colors.red, Icons.error);
        return;
      }

      if (!email.value.isEmail) {
        showSnackBar('Enter Valid Email ID', '', Colors.red, Icons.error);
        return;
      }

      if (password.value.isEmpty) {
        showSnackBar('Enter Password', '', Colors.red, Icons.error);
        return;
      }

      if (password.value.length < 8) {
        showSnackBar('Password must be atleast 8 characters', '', Colors.red,
            Icons.error);
        return;
      }
      if (!validateStructure(password.value)) {
        showSnackBar('Invalid Password', '', Colors.red, Icons.error);
        return;
      }
    }

    final type = await isSafeToCallApi();
    if (type == 0) {
      await showVPNDialog(() async {
        await submit();
      });
    } else if (type == 1) {
      await showNoInternetDialog(() async {
        await submit();
      });
    } else {
      final String orgId = pingModel.orgId != null ? pingModel.orgId! : ORG_ID;
      isApiCalling.value = true;

      if (!isSignUp.value) {
        final data = <String, dynamic>{};
        data['orgId'] = orgId;
        data['email'] = email.value;
        data['password'] = password.value;
        data['authType'] = 'email';
        data['redirectUrl'] = 'https://$orgId.wajooba.$baseApiDomain';

        const url = '$BASE_URL/user/login';

        final response =
            await http.post(Uri.parse(url), body: jsonEncode(data));

        print(url);
        print(response.body);
        if (response.statusCode.toString()[0] == '2') {
          try {
            final LoginResponse userData =
                LoginResponse.fromJson(jsonDecode(response.body));

            if (userData.accessToken != null) {
              if (userData.contact != null) {
                AppSharedPreferences.saveStringToLocalStorage(
                    KEY_USER_EMAIL, userData.email ?? '');
                AppSharedPreferences.saveStringToLocalStorage(
                    KEY_ACCESS_TOKEN, userData.accessToken ?? '');
                AppSharedPreferences.saveStringToLocalStorage(KEY_USER_NAME,
                    userData.contact?.fullName ?? userData.contact?.name);
                AppSharedPreferences.saveStringToLocalStorage(
                    KEY_GUID, userData.contact?.id ?? '');
                AppSharedPreferences.saveStringToLocalStorage(
                    KEY_ACCESS_TOKEN, userData.accessToken ?? '');
                AppSharedPreferences.saveStringToLocalStorage(
                    KEY_USER_PROFILE_PIC, userData.contact?.picture ?? '');
                AppSharedPreferences.saveStringToLocalStorage(KEY_USER_PHONE,
                    userData.contact?.phone ?? phoneNumber.value);

                final type = await isSafeToCallApi();
                if (type == 0) {
                  await showVPNDialog(() async {
                    await setGeneralUser();
                  });
                } else if (type == 1) {
                  await showNoInternetDialog(() async {
                    await setGeneralUser();
                  });
                } else {
                  await setGeneralUser();
                }

                updateContactData('{"phone":"${phoneNumber.value}"}');
              } else {
                isApiCalling.value = false;
                showSnackBar('Failed to get User Contact Details', '',
                    Colors.red, Icons.error);
              }
            } else {
              isApiCalling.value = false;
              loginMsg.value = jsonDecode(response.body)["msg"];
              showSnackBar(loginMsg.value, '', Colors.red, Icons.error);
            }
          } catch (e) {
            isApiCalling.value = false;
            showSnackBar(e.toString(), '', Colors.red, Icons.error);
          }
        } else {
          isApiCalling.value = false;
          if (response.statusCode == 404) {
            showSnackBar('Wrong Credentials', '', Colors.red, Icons.error);
          } else {
            showSnackBar(
                'Failed to sign in: ${jsonDecode(response.body)["error"]["message"].toString()}',
                '',
                Colors.red,
                Icons.error);
          }
        }
      } else {
        final mapData = <String, dynamic>{};
        mapData['orgId'] = orgId;
        mapData['email'] = email.value;
        mapData['password'] = password.value;
        mapData['name'] =
            userName.value == "" ? contactResponse.name : userName.value;
        mapData['lastname'] = "";
        mapData['authType'] = 'email';
        mapData['redirectUrl'] = 'https://$orgId.wajooba.$baseApiDomain';
        mapData['phone'] =
            number.value == "" ? contactResponse.phone : phoneNumber.value;
        print(jsonEncode(mapData));

        final response = await http.post(
            Uri.parse('$BASE_URL/user/registration'),
            body: jsonEncode(mapData));

        print(response.body);
        final data = jsonDecode(response.body);

        if (data['message'] == 'User registered') {
          isSignUp.value = false;
          isApiCalling.value = false;
          await submit();
          if (loginMsg.value != 'invalid email and password') {}
        } else {
          isApiCalling.value = false;
          showSnackBar(data['message'], '', Colors.red, Icons.error);
        }
      }
    }
  }

  Future<void> setGeneralUser() async {
    final generalData = await getGeneralData();
    if (generalData.orgId == null) {
      isApiCalling.value = false;
      showSnackBar(
          'Failed to get Ping API Response', '', Colors.red, Icons.error);
      return;
    } else {
      pingModel = generalData;
      contactResponse = await getContactResponse();
      validateProfile();
    }
  }

  Future<void> validateProfile() async {
    addDeviceNotificationID();
    await updateCustomField();
  }

  Future<void> googleLogIn() async {
    final type = await isSafeToCallApi();
    if (type == 0) {
      await showVPNDialog(() async {
        await googleLogIn();
      });
    } else if (type == 1) {
      await showNoInternetDialog(() async {
        await googleLogIn();
      });
    } else {
      try {
        final GoogleSignInAccount? googleUser = await googleSignIn.signIn();

        if (googleUser != null) {
          final GoogleSignInAuthentication googleAuth =
              await googleUser.authentication;

          isApiCalling.value = true;
          final LoginResponse userData = await getGoogleUserData(
              pingModel.orgId != null ? pingModel.orgId! : ORG_ID,
              googleAuth.accessToken!,
              googleUser.email);

          if (userData.contact == null) {
            isApiCalling.value = false;
            showSnackBar(
                'Failed to get User Data', '', Colors.red, Icons.error);
            return;
          }

          AppSharedPreferences.saveStringToLocalStorage(KEY_USER_NAME,
              userData.contact?.fullName ?? userData.contact?.name ?? '');
          AppSharedPreferences.saveStringToLocalStorage(
              KEY_GUID, userData.contact?.id);
          AppSharedPreferences.saveStringToLocalStorage(
              KEY_USER_EMAIL, userData.email);
          AppSharedPreferences.saveStringToLocalStorage(
              KEY_ACCESS_TOKEN, userData.accessToken);
          AppSharedPreferences.saveStringToLocalStorage(
              KEY_USER_PROFILE_PIC, userData.contact!.picture ?? '');
          String phoneNo = phoneNumber.value;
          AppSharedPreferences.saveStringToLocalStorage(
              KEY_USER_PHONE, userData.contact?.phone ?? phoneNo);

          final type = await isSafeToCallApi();
          if (type == 0) {
            await showVPNDialog(() async {
              await setGeneralUser();
            });
          } else if (type == 1) {
            await showNoInternetDialog(() async {
              await setGeneralUser();
            });
          } else {
            await setGeneralUser();
          }

          await updateCustomField();

        }
      } catch (e) {
        showToast("failed to sign in");
      }
    }
  }

  updateCustomField() async {
    var updatedCustomResponse = await updateCustomFieldApi();
    if (updatedCustomResponse.isNotEmpty) {
      var ui = updatedCustomResponse.firstWhere(
          (element) => element.value.value == "",
          orElse: () => CustomField());
      isApiCalling.value = false;
      if (ui.guId == null) {
        Get.offNamed(Routes.HOME);
      } else {
        Get.offNamed(Routes.WELCOME);
      }
    } else {
      Get.offNamed(Routes.WELCOME);
    }
  }
}
