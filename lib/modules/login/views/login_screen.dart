import 'dart:io';

import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:regtech_camp_app/common_widget/custom_edit_text.dart';
import 'package:regtech_camp_app/common_widget/global_button.dart';
import 'package:regtech_camp_app/common_widget/global_edit_text.dart';
import 'package:regtech_camp_app/modules/login/controllers/login_controller.dart';
import 'package:regtech_camp_app/resources/app_colors.dart';
import 'package:regtech_camp_app/resources/app_images.dart';
import 'package:regtech_camp_app/shared_widget/base_widget/base_get_widget.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../common_widget/global_back_drop.dart';
import '../../../common_widget/global_text.dart';
import '../../../common_widget/global_top_widget.dart';
import '../../../resources/app_texts.dart';
import '../../../routes/app_routes.dart';

class LoginScreen extends BaseGetWidget<LogInController> {
  LoginScreen({Key? key}) : super(key: key);

  @override
  final controller = Get.put(LogInController());

  @override
  Widget body(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: [
        ListView(
          children: [
            Stack(children: [
              Image.asset(AppImages.bgBigPurpleImage,
                  height: 191, width: 375, fit: BoxFit.fill),
              const GlobalTopWidget()
            ]),
            const SizedBox(
              height: 30,
            ),
            Obx(() => controller.isSignUp.value
                ? CustomEditText(
                    hintText: AppTexts.fullName,
                    inputType: TextInputType.text,
                    onTextChanged: (value) => controller.userName.value = value)
                : const SizedBox()),
            const SizedBox(height: 20.0),
            CustomEditText(
                controller: controller.emailController,
                hintText: AppTexts.email,
                inputType: TextInputType.emailAddress,
                onTextChanged: (value) {
                  controller.email.value = value;
                },
                isEditText: true),
            Obx(() => controller.isSignUp.value
                ? buildPhoneWidget()
                : const SizedBox()),
            const SizedBox(height: 20),
            CustomEditText(
                controller: controller.passwordController,
                hintText: AppTexts.password,
                inputType: TextInputType.visiblePassword,
                obscureText: true,
                onTextChanged: (value) {
                  controller.password.value = value;
                },
                isEditText: true),
            Obx(() => (controller.isSignUp.value)
                ? Container(
                    margin: const EdgeInsets.only(top: 7, bottom: 11),
                    padding: const EdgeInsets.symmetric(horizontal: 92),
                    width: 284,
                    child: const GlobalText(
                      "Minium 8 characters (1 Upper case, 1 Special & 1 Numaric",
                      fontWeight: FontWeight.w400,
                      textAlign: TextAlign.center,
                      fontSize: 12,
                      color: Color(0xff929292),
                    ),
                  )
                : const SizedBox()),
            Obx(
              () => controller.isSignUp.value
                  ? CustomEditText(
                      hintText: AppTexts.confirmPassword,
                      inputType: TextInputType.visiblePassword,
                      obscureText: true,
                      onTextChanged: (value) {
                        controller.confirmPassword.value = value;
                      })
                  : Container(
                      padding: const EdgeInsets.only(right: 40.0, top: 13),
                      alignment: Alignment.centerRight,
                      child: GlobalText(
                        AppTexts.forgetPassword,
                        fontSize: 16,
                        textDecoration: TextDecoration.underline,
                        color: AppColors.primaryDarkColor,
                        onTap: () {
                          Get.toNamed(Routes.FORGET);
                        },
                      ),
                    ),
            ),
            const SizedBox(
              height: 30.0,
            ),
            Obx(() => GlobalButton(
                  width: 296,
                  title: controller.isSignUp.value
                      ? AppTexts.signUp
                      : AppTexts.signIn,
                  margin: const EdgeInsets.symmetric(horizontal: 40),
                  onTap: () async {
                    FocusScope.of(context).unfocus();
                    await controller.submit();
                  },
                )),
            const SizedBox(
              height: 20,
            ),
            Platform.isAndroid ?
            GlobalText(
              AppTexts.or,
              color: const Color(0xff929292),
              fontWeight: FontWeight.w400,
              fontSize: 20.0,
              textAlign: TextAlign.center,
            ) : const SizedBox(),
             SizedBox(
              height:  Platform.isAndroid ? 19.0 : 0,
            ), Platform.isAndroid ?
            Container(
              height: 51,
              width: 296,
              margin: const EdgeInsets.symmetric(horizontal: 40),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  border: Border.all(color: const Color(0xffcacaca)),
                  borderRadius: const BorderRadius.all(Radius.circular(10)),
                  gradient: const LinearGradient(
                    begin: Alignment(0.5, -3.0616171314629196e-17),
                    end: Alignment(0.5000000000000001, 1),
                    colors: [Color(0xfffbfbfb), Colors.white],
                  )),
              child: Material(
                color: Colors.transparent,
                child: InkWell(
                  onTap: () async {
                    await controller.googleLogIn();
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        AppImages.googleLogo,
                        height: 34,
                        width: 34,
                      ),
                      const SizedBox(
                        width: 15.0,
                      ),
                      Obx(() => GlobalText(
                            controller.isSignUp.value
                                ? AppTexts.singUpWIthGoogle
                                : AppTexts.singWIthGoogle,
                            color: const Color(0xffea4335),
                            fontWeight: FontWeight.w700,
                            fontStyle: FontStyle.normal,
                            fontSize: 22.0,
                            textAlign: TextAlign.end,
                          )),
                    ],
                  ),
                ),
              ),
            ) : const SizedBox(),
            const SizedBox(
              height: 34.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Obx(() => GlobalText(
                    controller.isSignUp.value
                        ? AppTexts.doHaveAccount
                        : AppTexts.doNotHaveAccount,
                    color: const Color(0xff929292),
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal)),
                const SizedBox(width: 5),
                InkWell(
                  highlightColor: Colors.transparent,
                  splashColor: Colors.transparent,
                  onTap: () {
                    controller.emailController.clear();
                    controller.passwordController.clear();
                    controller.isSignUp.value = !controller.isSignUp.value;
                  },
                  child: Obx(
                    () => GlobalText(
                        controller.isSignUp.value
                            ? AppTexts.signIn
                            : AppTexts.signUp,
                        color: AppColors.primaryDarkColor,
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        textDecoration: TextDecoration.underline,
                        fontStyle: FontStyle.normal),
                  ),
                )
              ],
            ),
            const SizedBox(
              height: 41.0,
            ),
            const GlobalText(
              'By clicking the sign Up button, \n'
              'you agree to The Future Academy',
              color: Color(0xff929292),
              fontWeight: FontWeight.w400,
              fontSize: 15.0,
              fontStyle: FontStyle.normal,
              textAlign: TextAlign.center,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                InkWell(
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  onTap: () async {
                    if (await canLaunch(
                        'https://www.wajooba.com/terms-and-conditions/index.html')) {
                      await launch(
                          'https://www.wajooba.com/terms-and-conditions/index.html');
                    }
                  },
                  child: GlobalText(AppTexts.termsCondition,
                      color: AppColors.primaryDarkColor,
                      fontSize: 15,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal),
                ),
                GlobalText(AppTexts.and,
                    color: const Color(0xff929292),
                    fontSize: 15,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal),
                InkWell(
                  onTap: () async {
                    if (await canLaunch(
                        'https://www.wajooba.com/privacy/index.html')) {
                      await launch(
                          'https://www.wajooba.com/privacy/index.html');
                    }
                  },
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  child: GlobalText(AppTexts.privacyPolicy,
                      color: AppColors.primaryDarkColor,
                      fontSize: 15,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal),
                ),
              ],
            ),
            const SizedBox(height: 29)
          ],
        ),
        Obx(() => controller.isApiCalling.value
            ? const GlobalBackDrop()
            : const SizedBox())
      ],
    ));
  }

  Widget buildPhoneWidget() {
    return Container(
      height: 50,
      width: 296,
      alignment: Alignment.center,
      padding: const EdgeInsets.only(right: 10),
      margin: const EdgeInsets.only(left: 40, right: 40, top: 20),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10.0),
          border: Border.all(
              color: AppColors.primaryDarkColor,
              style: BorderStyle.solid,
              width: 1)),
      child: Row(
        children: [
          CountryCodePicker(
            padding: EdgeInsets.zero,
            onChanged: (value) {
              controller.code.value = value.dialCode ?? '';
            },
            initialSelection: '+91',
            favorite: const ['+91'],
            textStyle: TextStyle(color: AppColors.primaryDarkColor),
            showOnlyCountryWhenClosed: false,
            alignLeft: false,
          ),
          Expanded(
              child: GlobalEditText(
            AppTexts.phoneNumber,
            Platform.isIOS
                ? const TextInputType.numberWithOptions(
                    signed: true, decimal: true)
                : TextInputType.number,
            (value) {
              controller.number.value = value;
              controller.phoneNumber.value =
                  '${controller.code.value}-${controller.number.value}';
            },
            textAlign: TextAlign.start,
          ))
        ],
      ),
    );
  }
}
