import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:regtech_camp_app/common_widget/global_button.dart';
import 'package:regtech_camp_app/common_widget/global_text.dart';
import 'package:regtech_camp_app/modules/profile/controllers/profile_controller.dart';
import 'package:regtech_camp_app/resources/app_colors.dart';
import 'package:regtech_camp_app/routes/app_routes.dart';

import '../../../resources/app_images.dart';
import '../../../utils.dart';

class WelcomeScreen extends GetView<ProfileController> {
  @override
  final controller = Get.put(ProfileController());

  WelcomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          alignment: Alignment.center,
          color: AppColors.primaryDarkColor,
          child: ListView(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 40.0, left: 16, bottom: 60),
                    child: Image.asset(AppImages.firstSmallLogo),
                  ),
                  Image.asset(AppImages.secondSmallLogo),
                  RichText(
                    text: TextSpan(
                      text: 'CERTIFIED \n',
                      style: const TextStyle(
                        height: 1.5,
                        fontSize: 17,
                        fontWeight: FontWeight.w600,
                      ),
                      children: [
                        TextSpan(
                            text: 'ANTI MONEY LAUNDERING \n', style: style),
                        TextSpan(text: 'PROFESSIONAL', style: style),
                      ],
                    ),
                  ),
                ],
              ),
              GlobalText(
                "Hi ${contactResponse.name}",
                color: Colors.white,
                fontSize: 22,
                fontFamily: GoogleFonts.poppins().fontFamily,
                fontWeight: FontWeight.w600,
                textAlign: TextAlign.center,
              ),
              const SizedBox(height: 20),
              GlobalText(
                "Welcome to CAMP",
                color: Colors.white,
                fontSize: 32,
                fontFamily: GoogleFonts.poppins().fontFamily,
                textAlign: TextAlign.center,
                fontWeight: FontWeight.w600,
              ),
              const SizedBox(height: 40),
              GlobalText(
                "Please complete your profile",
                color: Colors.white,
                fontSize: 18,
                fontFamily: GoogleFonts.poppins().fontFamily,
                textAlign: TextAlign.center,
                fontWeight: FontWeight.w400,
              ),
              const SizedBox(height: 57),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  GlobalButton(
                    width: 148,
                    title: "Skip Now",
                    onTap: () {
                      Get.offNamed(Routes.HOME);
                    },
                  ),
                  const SizedBox(
                    width: 15,
                  ),
                  GlobalButton(
                    width: 148,
                    title: "Yes",
                    onTap: () {
                      Get.offNamed(Routes.PROFILE);
                    },
                  )
                ],
              )
            ],
          )),
    );
  }
}
