import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:regtech_camp_app/model/StoreModel.dart';
import 'package:regtech_camp_app/model/offer_code_response.dart';
import 'package:regtech_camp_app/modules/checkout/views/success_full_payment_screen.dart';
import 'package:regtech_camp_app/services/apis.dart';
import 'package:regtech_camp_app/utils.dart';

class CheckOutController extends GetxController {
  var total = 0.0.obs;
  var discountAmount = 0.0.obs;
  var creditCardFees = 0.0.obs;
  var registrationFees = 0.0.obs;
  var taxableAmount = 0.0.obs;
  var offerCode = OfferCodeResponse().obs;
  var item = Get.arguments as StoreModel;
  var promoCodeTextController = TextEditingController();
  var promoLoading = false.obs;

  var isCardScreen = false.obs;

  var cardName = "".obs;
  var cardNumber = "".obs;
  var cardMonth = "".obs;
  var cardYear = "".obs;
  var cardCvv = "".obs;

  var cardNameController = TextEditingController().obs;
  var cardNumberController = TextEditingController().obs;
  var cardMonthController = TextEditingController().obs;
  var cardYearController = TextEditingController().obs;
  var cardCvvController = TextEditingController().obs;

  @override
  void onInit() {
    super.onInit();
    updatePrices();
  }

  updatePrices() {
    total.value = 0;
    discountAmount.value = 0;
    registrationFees.value = 0;
    taxableAmount.value = 0;
    creditCardFees.value = 0;
    item.discountPrimary = 0;
    item.discountSecondary = 0;
    item.discount = 0;

    double taxPercent = pingModel.taxPercent ?? 0;

    registrationFees.value += item.registrationFees;
    if (item.isChargeCreditCardFees == true) {
      var fee = pingModel.cardFees ?? 0.0;
      if (fee != 0) {
        item.normalcardfees = ((item.price * fee) / 100);
      } else {
        item.normalcardfees = 0.0;
      }

      if (item.registrationFees != 0) {
        item.regisstrationcardfees = ((item.registrationFees * fee) / 100);
      } else {
        item.regisstrationcardfees = 0.0;
      }

      item.cardfees = item.regisstrationcardfees + item.normalcardfees;
      creditCardFees.value = creditCardFees.value + item.cardfees;
    } else {
      item.normalcardfees = 0.0;
      item.regisstrationcardfees = 0.0;
      item.cardfees = 0.0;
    }

    total.value += item.registrationFees != 0
        ? item.registrationFees + item.price + item.cardfees
        : item.price + item.cardfees;

    discountAmount.value =
        offerCode.value.id != null ? getDiscountAmount().toDouble() : 0.0;
    total.value = total.value - discountAmount.value;

    num itemPrice = item.price;
    itemPrice += item.registrationFees;
    itemPrice += item.cardfees != 0.0 ? item.cardfees : 0.0;
    double itemTax = item.isTaxable == true
        ? (itemPrice - discountAmount.value) * taxPercent / 100
        : 0;
    taxableAmount.value += double.parse((itemTax).toStringAsFixed(2));
    item.tax = itemTax;
    item.categoryId = item.category != null
        ? item.category!.guId ?? ''
        : item.donationCategory != null
            ? item.donationCategory['guId'] ?? ''
            : '';
    item.offerId = offerCode.value.id ?? '';
    item.discountAmount = offerCode.value.primaryDiscountType == 'percentage'
        ? 0
        : offerCode.value.primaryDiscount ?? 0;
    item.discountPercent = offerCode.value.primaryDiscountType == 'percentage'
        ? offerCode.value.primaryDiscount ?? 0
        : 0;
    item.discount = discountAmount.value;
    total.value += taxableAmount.value;
  }

  num returnDiscount() {
    if (offerCode.value.primaryDiscountType == 'percentage') {
      if (item.isDiscountSubscriptionFees == true) {
        var amount = item.registrationFees;
        item.discountPrimary = 0;
        item.discountSecondary =
            ((offerCode.value.primaryDiscount / 100) * item.registrationFees);
        return (offerCode.value.primaryDiscount / 100) * amount;
      } else if (item.isDiscountSubscriptionFees == false) {
        if (item.membershipType == "unlimited") {
          item.discountPrimary =
              ((offerCode.value.primaryDiscount / 100) * item.price);
          item.discountSecondary = 0;
        } else {
          item.discountPrimary = 0;
          item.discountSecondary =
              ((offerCode.value.primaryDiscount / 100) * item.registrationFees);
        }
        return item.membershipType == "unlimited"
            ? item.discountPrimary
            : item.discountSecondary;
      } else {
        item.discountPrimary =
            ((offerCode.value.primaryDiscount / 100) * item.price);
        item.discountSecondary = 0;
        return item.discountPrimary;
      }
    } else if (offerCode.value.primaryDiscountType == 'amount') {
      if (total >= offerCode.value.primaryDiscount) {
        item.discountPrimary = item.price;
        item.discountSecondary = item.registrationFees;
        return offerCode.value.primaryDiscount;
      } else {
        item.discountPrimary = 0;
        item.discountSecondary = 0;
        return 0.0;
      }
    } else {
      item.discountPrimary = 0;
      item.discountSecondary = 0;
      return 0.0;
    }
  }

  num getDiscountAmount() {
    if (item.isOtherPrice == true) {
      item.discountPrimary = 0;
      item.discountSecondary = 0;
      return 0.0;
    }
    if (item.isDonationItem == true) {
      item.discountPrimary = 0;
      item.discountSecondary = 0;
      return 0.0;
    }

    if (offerCode.value.primaryItemCategory != null) {
      if (offerCode.value.primaryItemCategory == "item") {
        return returnDiscount();
      } else if (offerCode.value.primaryItemCategory == "course") {
        if (item.itemType == "course") {
          return returnDiscount();
        } else {
          item.discountPrimary = 0;
          item.discountSecondary = 0;
          return 0.0;
        }
      } else if (offerCode.value.primaryItemCategory == "product") {
        if (item.itemType == "product") {
          return returnDiscount();
        } else {
          item.discountPrimary = 0;
          item.discountSecondary = 0;
          return 0.0;
        }
      } else {
        item.discountPrimary = 0;
        item.discountSecondary = 0;
        return 0.0;
      }
    } else if (offerCode.value.primaryItem != null) {
      if (item.guId == offerCode.value.primaryItem!.guId) {
        return returnDiscount();
      } else {
        item.discountPrimary = 0;
        item.discountSecondary = 0;
        return 0.0;
      }
    } else if (offerCode.value.primaryCourse != null) {
      if (item.category != null &&
          item.category!.guId == offerCode.value.primaryCourse!.guId) {
        return returnDiscount();
      } else {
        item.discountPrimary = 0;
        item.discountSecondary = 0;
        return 0.0;
      }
    } else {
      item.discountPrimary = 0;
      item.discountSecondary = 0;
      return 0.0;
    }
  }

  appliedPromocode() async {
    if (offerCode.value.id != null) {
      promoCodeTextController.clear();
      offerCode.value = OfferCodeResponse();
      item.discountAmount = 0;
      item.discount = 0;
      item.discountSecondary = 0;
      item.discountPrimary = 0;
      item.discountPercent = 0;
      item.discount = 0;
      updatePrices();
    } else {
      if (promoCodeTextController.text.isEmpty) {
        showToast("Enter Promo Code!!");
      } else {
        promoLoading.value = true;
        var result = await applyOfferApi(promoCodeTextController.text);
        if (result.id == null) {
          showToast("Failed to apply offer Code!!");
        }
        promoLoading.value = false;
        offerCode.value = result;
        updatePrices();
      }
    }
  }

  startPayment() async {
    // if (pingModel.stripeApiKey != null &&
    //     pingModel.stripeApiKey!.isNotEmpty &&
    //     pingModel.stripePublishableKey != null &&
    //     pingModel.stripePublishableKey!.isNotEmpty) {
    //   promoLoading.value = true;
    //   if (pingModel.stripePublishableKey != null && pingModel.stripePublishableKey!.isNotEmpty) {
    //     Stripe.publishableKey = pingModel.stripePublishableKey!;
    //   }
    //
    //   if (pingModel.stripeAuthCode != null && pingModel.stripeAuthCode!.toString().isNotEmpty) {
    //     Stripe.stripeAccountId = pingModel.stripeAuthCode!;
    //   }
    //
    //   await Stripe.instance.applySettings();
    //   var data = await callStripePaymentIntentCreateApi(total.value.toInt() * 100);
    //   if (data.statusCode == 201 || data.statusCode == 200) {
    //     var responseBody = jsonDecode(data.body);
    //     if (responseBody['client_secret'] != null) {
    //       final billingDetails = BillingDetails(
    //         email: AppSharedPreferences.getStringFromLocalStorage(KEY_USER_EMAIL) ?? '',
    //         phone: AppSharedPreferences.getStringFromLocalStorage(KEY_USER_PHONE) ?? '',
    //         name: AppSharedPreferences.getStringFromLocalStorage(KEY_USER_NAME) ?? '',
    //       );
    //
    //
    //       await Stripe.instance.initPaymentSheet(
    //         paymentSheetParameters: SetupPaymentSheetParameters(
    //             customFlow: false,
    //             paymentIntentClientSecret: responseBody['client_secret'],
    //             merchantDisplayName: pingModel.name ?? "RegTechDemo",
    //             applePay: false,
    //             googlePay: false,
    //             style: ThemeMode.dark,
    //             primaryButtonColor: AppColors.primaryDarkColor,
    //             billingDetails: billingDetails,
    //             testEnv: kReleaseMode ? false : true),
    //       );
    //
    //       try {
    //         await Stripe.instance.presentPaymentSheet();
    //         var temp = await getItemInvoiceStripe(
    //             item, responseBody['id'] ?? responseBody['client_secret'] ?? '', offerCode.value.id ?? "");
    //         promoLoading.value = false;
    //         if(temp.guId != null && temp.guId!.isNotEmpty){
    //         Get.back();
    //         Get.back();
    //         Get.off(() => const SuccessFulPayment());
    //          }
    //       } on Exception catch (e) {
    //         promoLoading.value = false;
    //         if (e is StripeException) {
    //           print(e.error.localizedMessage);
    //           showToast('Error from Stripe: ${e.error.localizedMessage}');
    //         } else {
    //           showToast('Unforeseen error: $e');
    //         }
    //       }
    //     } else {
    //       promoLoading.value = false;
    //       showToast("Unable to find Client's Secret");
    //     }
    //   } else {
    //     promoLoading.value = false;
    //     showToast("Something went wrong!!");
    //   }
    // } else {
    //   showToast("Unable to find Stripe Data!!");
    // }
  }

  startPaymentOld() async {
    if (cardName.value.isEmpty) {
      showToast("Enter Valid Card Name");
      return;
    }

    if (cardNumber.value.isEmpty) {
      showToast("Enter Valid Card Number");
      return;
    }

    if (cardNumber.value.length != 19) {
      showToast("Enter Valid Card Number");
      return;
    }

    if (cardMonth.value.isEmpty) {
      showToast("Enter Valid Card Month");
      return;
    }

    if (cardMonth.value.length != 2) {
      showToast("Enter Valid Card Month");
      return;
    }

    if (cardYear.value.isEmpty) {
      showToast("Enter Valid Card Year");
      return;
    }
    if (cardYear.value.length != 4) {
      showToast("Enter Valid Card Year");
      return;
    }
    if (cardCvv.value.isEmpty) {
      showToast("Enter Valid Card CVV");
      return;
    }
    if (cardCvv.value.length != 3) {
      showToast("Enter Valid Card CVV");
      return;
    }

    if (pingModel.stripePublishableKey != null &&
        pingModel.stripePublishableKey!.isNotEmpty) {
      promoLoading.value = true;

      var data = await callStripePaymentIntentCreateApiOld(
          cardNumber.value, cardMonth.value, cardYear.value, cardCvv.value);
      if (data.statusCode == 201 || data.statusCode == 200) {
        var responseBody = jsonDecode(data.body);
        if (responseBody['id'] != null) {
          var temp = await getItemInvoiceStripe(
              item, responseBody['id'] ?? '', offerCode.value.id ?? "");
          promoLoading.value = false;
          if (temp.guId != null && temp.guId!.isNotEmpty) {
            Get.back();
            Get.back();
            Get.off(() => const SuccessFulPayment());
          }
        } else {
          promoLoading.value = false;
          showToast("Unable to find Client's Id");
        }
      } else {
        promoLoading.value = false;
        showToast("Something went wrong!!");
      }
    } else {
      showToast("Unable to find Stripe Data!!");
    }
  }
}
