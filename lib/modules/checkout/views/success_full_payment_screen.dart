import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:regtech_camp_app/common_widget/global_text.dart';
import 'package:regtech_camp_app/modules/home/controllers/home_controller.dart';
import 'package:regtech_camp_app/resources/app_images.dart';
import 'package:regtech_camp_app/utils.dart';

class SuccessFulPayment extends StatelessWidget {
  const SuccessFulPayment({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async {
          Get.back();
          final homeController = Get.put(HomeController());
           homeController.onInit2(selectedBuyCourse.value.guId ?? '');
          return true;
        },
        child: Scaffold(
            backgroundColor: const Color(0xff3b2e7e),
            body: ListView(
                padding: EdgeInsets.zero,
                shrinkWrap: true,
                physics: const ScrollPhysics(),

                children: [
              const SizedBox(height: 70),
              Row(children: [
                const SizedBox(width: 16),
                Image.asset(AppImages.firstLogoImage, height: 100, width: 100, fit: BoxFit.fill),
                Container(
                  height: 107,
                  width: 4,
                  margin: const EdgeInsets.only(
                    left: 10,
                    right: 12,
                  ),
                  decoration: const BoxDecoration(
                    color: Color(0xffe26c14),
                  ),
                ),
                Expanded(
                  child: GlobalText(selectedBuyCourse.value.title ?? '',
                      color: const Color(0xff6c6d71),
                      fontSize: 17,
                      maxLine: 4,
                      fontWeight: FontWeight.w600,
                      fontStyle: FontStyle.normal),
                ),
                const SizedBox(width: 16),
              ]),
              const SizedBox(height: 60),
              RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(children: [
                    TextSpan(
                        text: "Thank You!\n\n",
                        style: TextStyle(
                          fontFamily: GoogleFonts.poppins().fontFamily,
                          color: const Color(0xffffffff),
                          fontSize: 32,
                          fontWeight: FontWeight.w600,
                          fontStyle: FontStyle.normal,
                        )),
                    TextSpan(
                        text: "You have made payment successfully",
                        style: TextStyle(
                          fontFamily: GoogleFonts.poppins().fontFamily,
                          color: const Color(0xffffffff),
                          fontSize: 24,
                          fontWeight: FontWeight.w500,
                          fontStyle: FontStyle.normal,
                        )),
                  ])),
              const SizedBox(height: 29),
              Container(
                margin: const EdgeInsets.symmetric(horizontal: 40),
                height: 51,
                decoration: BoxDecoration(
                  color: const Color(0xffe86e25),
                  borderRadius: BorderRadius.circular(10),
                ),
                alignment: Alignment.center,
                child: InkWell(
                  borderRadius: BorderRadius.circular(10),
                  onTap: () {
                    Get.back();
                    final homeController = Get.put(HomeController());
                    homeController.onInit2(selectedBuyCourse.value.guId ?? '');
                  },
                  child: const GlobalText("Start your course",
                      color: Color(0xffffffff), fontSize: 20, fontWeight: FontWeight.w600, fontStyle: FontStyle.normal),
                ),
              )
            ])));
  }
}
