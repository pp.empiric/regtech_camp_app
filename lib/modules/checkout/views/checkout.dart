import 'dart:io';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:regtech_camp_app/common_widget/custom_edit_text.dart';
import 'package:regtech_camp_app/common_widget/global_back_drop.dart';
import 'package:regtech_camp_app/common_widget/global_button.dart';
import 'package:regtech_camp_app/common_widget/global_outline_button.dart';
import 'package:regtech_camp_app/common_widget/global_svg_button_icon_widget.dart';
import 'package:regtech_camp_app/common_widget/global_text.dart';
import 'package:regtech_camp_app/common_widget/profile_widget.dart';
import 'package:regtech_camp_app/resources/app_colors.dart';
import 'package:regtech_camp_app/resources/app_images.dart';
import 'package:regtech_camp_app/utils.dart';

import '../controllers/check_out_controller.dart';

class CustomInputFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    var text = newValue.text;

    if (newValue.selection.baseOffset == 0) {
      return newValue;
    }

    var buffer = StringBuffer();
    for (int i = 0; i < text.length; i++) {
      buffer.write(text[i]);
      var nonZeroIndex = i + 1;
      if (nonZeroIndex % 4 == 0 && nonZeroIndex != text.length) {
        buffer.write(
            ' '); // Replace this with anything you want to put after each 4 numbers
      }
    }

    var string = buffer.toString();
    return newValue.copyWith(
        text: string,
        selection: TextSelection.collapsed(offset: string.length));
  }
}

class Checkout extends StatelessWidget {
  final controller = Get.put(CheckOutController());

  Checkout({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (controller.isCardScreen.value == true) {
          controller.isCardScreen.value = false;
          return false;
        } else {
          Get.back();
          return true;
        }
      },
      child: Scaffold(
          body: Stack(
        alignment: Alignment.bottomCenter,
        children: [
          Obx(() => controller.isCardScreen.value
              ? ListView(children: [
                  ProfileWidget(
                    title: "checkout",
                    onTap: () {
                      if (controller.isCardScreen.value == true) {
                        controller.isCardScreen.value = false;
                      }
                    },
                  ),
                  Container(
                    margin: const EdgeInsets.only(left: 16, top: 19),
                    child: const GlobalText("Payment Information",
                        color: Color(0xff6c6d71),
                        fontSize: 22,
                        fontWeight: FontWeight.w700,
                        fontStyle: FontStyle.normal),
                  ),
                  Container(
                      margin:
                          const EdgeInsets.only(left: 16, right: 16, top: 12),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(15),
                        boxShadow: const [
                          BoxShadow(
                            color: Color(0x2d000000),
                            offset: Offset(0, 0),
                            blurRadius: 10,
                            spreadRadius: 0,
                          ),
                        ],
                      ),
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                                margin:
                                    const EdgeInsets.only(top: 16, left: 19),
                                child: GlobalText('Credit/Debit Card',
                                    color: AppColors.commonTextColor,
                                    fontFamily:
                                        GoogleFonts.poppins().fontFamily,
                                    fontSize: 16,
                                    fontWeight: FontWeight.w600,
                                    fontStyle: FontStyle.normal)),
                            Container(
                                alignment: Alignment.centerRight,
                                margin: const EdgeInsets.only(
                                    top: 20, bottom: 18, right: 16),
                                child: SvgPicture.asset(
                                  'assets/images/secure_transaction.svg',
                                  width: 183,
                                  height: 21,
                                )),
                            Container(
                                height: 54,
                                margin: const EdgeInsets.only(
                                    left: 15, right: 15, bottom: 15),
                                decoration: BoxDecoration(
                                  color: const Color(0xfff3f3f3),
                                  border: Border.all(
                                    color: const Color(0xffdbdbdb),
                                    width: 1,
                                  ),
                                  borderRadius: BorderRadius.circular(8),
                                ),
                                alignment: Alignment.center,
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 15),
                                child: TextFormField(
                                    textInputAction: TextInputAction.next,
                                    controller:
                                        controller.cardNameController.value,
                                    autofocus: false,
                                    onChanged: (value) {
                                      controller.cardName.value = value;
                                    },
                                    style: TextStyle(
                                        fontFeatures: const [
                                          FontFeature.tabularFigures()
                                        ],
                                        fontFamily:
                                            GoogleFonts.lato().fontFamily,
                                        fontSize: 18,
                                        fontWeight: FontWeight.w400,
                                        color: Colors.black),
                                    textAlign: TextAlign.start,
                                    keyboardType: TextInputType.name,
                                    cursorColor: AppColors.primaryDarkColor,
                                    decoration: InputDecoration(
                                        isDense: true,
                                        border: InputBorder.none,
                                        contentPadding:
                                            const EdgeInsets.fromLTRB(
                                                0, 0, 0, 0),
                                        hintStyle: TextStyle(
                                          fontFamily:
                                              GoogleFonts.lato().fontFamily,
                                          color: const Color(0xff6c6d71),
                                          fontSize: 18,
                                          fontWeight: FontWeight.w400,
                                          fontStyle: FontStyle.normal,
                                          fontFeatures: const [
                                            FontFeature.tabularFigures()
                                          ],
                                        ),
                                        hintText: 'Eric Qualls'))),
                            Container(
                                height: 54,
                                margin: const EdgeInsets.only(
                                    left: 15, right: 15, bottom: 15),
                                decoration: BoxDecoration(
                                  color: const Color(0xfff3f3f3),
                                  border: Border.all(
                                    color: const Color(0xffdbdbdb),
                                    width: 1,
                                  ),
                                  borderRadius: BorderRadius.circular(8),
                                ),
                                alignment: Alignment.center,
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 15),
                                child: TextFormField(
                                    textInputAction: TextInputAction.next,
                                    controller:
                                        controller.cardNumberController.value,
                                    autofocus: false,
                                    onChanged: (value) {
                                      controller.cardNumber.value = value;
                                    },
                                    inputFormatters: <TextInputFormatter>[
                                      FilteringTextInputFormatter.digitsOnly,
                                      CustomInputFormatter(),
                                      LengthLimitingTextInputFormatter(19)
                                    ],
                                    style: TextStyle(
                                        fontFeatures: const [
                                          FontFeature.tabularFigures()
                                        ],
                                        fontFamily:
                                            GoogleFonts.lato().fontFamily,
                                        fontSize: 18,
                                        fontWeight: FontWeight.w400,
                                        color: Colors.black),
                                    textAlign: TextAlign.start,
                                    keyboardType: Platform.isIOS
                                        ? const TextInputType.numberWithOptions(
                                        signed: true, decimal: true)
                                        : TextInputType.number,
                                    cursorColor: AppColors.primaryDarkColor,
                                    decoration: InputDecoration(
                                        isDense: true,
                                        border: InputBorder.none,
                                        contentPadding:
                                            const EdgeInsets.fromLTRB(
                                                0, 0, 0, 0),
                                        hintStyle: TextStyle(
                                          fontFamily:
                                              GoogleFonts.lato().fontFamily,
                                          color: const Color(0xff6c6d71),
                                          fontSize: 18,
                                          fontWeight: FontWeight.w400,
                                          fontStyle: FontStyle.normal,
                                          fontFeatures: const [
                                            FontFeature.tabularFigures()
                                          ],
                                        ),
                                        hintText: 'Card Number'))),
                            Container(
                              margin: const EdgeInsets.only(
                                  left: 15, right: 15, bottom: 15),
                              child: Row(children: [
                                Expanded(
                                    flex: 2,
                                    child: Container(
                                        height: 54,
                                        decoration: BoxDecoration(
                                          color: const Color(0xfff3f3f3),
                                          border: Border.all(
                                            color: const Color(0xffdbdbdb),
                                            width: 1,
                                          ),
                                          borderRadius:
                                              BorderRadius.circular(8),
                                        ),
                                        child: Row(children: [
                                          Expanded(
                                              flex: 1,
                                              child: Container(
                                                  alignment: Alignment.center,
                                                  padding: const EdgeInsets.symmetric(
                                                      horizontal: 15),
                                                  child: TextFormField(
                                                      textInputAction:
                                                          TextInputAction.done,
                                                      controller: controller
                                                          .cardMonthController
                                                          .value,
                                                      autofocus: false,
                                                      onChanged: (value) {
                                                        controller.cardMonth
                                                            .value = value;
                                                      },
                                                      inputFormatters: <TextInputFormatter>[
                                                        FilteringTextInputFormatter
                                                            .digitsOnly,
                                                        LengthLimitingTextInputFormatter(
                                                            2)
                                                      ],
                                                      style: TextStyle(
                                                          fontFeatures: const [
                                                            FontFeature
                                                                .tabularFigures()
                                                          ],
                                                          fontFamily:
                                                              GoogleFonts.lato()
                                                                  .fontFamily,
                                                          fontSize: 18,
                                                          fontWeight:
                                                              FontWeight.w400,
                                                          color: Colors.black),
                                                      textAlign:
                                                          TextAlign.start,
                                                      keyboardType:
                                                      Platform.isIOS
                                                          ? const TextInputType.numberWithOptions(
                                                          signed: true, decimal: true)
                                                          : TextInputType.number,
                                                      cursorColor: AppColors
                                                          .primaryDarkColor,
                                                      decoration:
                                                          InputDecoration(
                                                              isDense: true,
                                                              border: InputBorder
                                                                  .none,
                                                              contentPadding:
                                                                  const EdgeInsets.fromLTRB(
                                                                      0, 0, 0, 0),
                                                              hintStyle: TextStyle(
                                                                fontFamily: GoogleFonts
                                                                        .lato()
                                                                    .fontFamily,
                                                                color: const Color(
                                                                    0xff6c6d71),
                                                                fontSize: 18,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w400,
                                                                fontStyle:
                                                                    FontStyle
                                                                        .normal,
                                                                fontFeatures: const [
                                                                  FontFeature
                                                                      .tabularFigures()
                                                                ],
                                                              ),
                                                              hintText: 'MM')))),
                                          Container(
                                            height: 53,
                                            width: 1,
                                            decoration: const BoxDecoration(
                                              color: Color(0xffdbdbdb),
                                            ),
                                          ),
                                          Expanded(
                                              flex: 1,
                                              child: Container(
                                                  alignment: Alignment.center,
                                                  padding: const EdgeInsets.symmetric(
                                                      horizontal: 15),
                                                  child: TextFormField(
                                                      textInputAction:
                                                          TextInputAction.done,
                                                      controller: controller
                                                          .cardYearController
                                                          .value,
                                                      autofocus: false,
                                                      onChanged: (value) {
                                                        controller.cardYear
                                                            .value = value;
                                                      },
                                                      inputFormatters: <TextInputFormatter>[
                                                        FilteringTextInputFormatter
                                                            .digitsOnly,
                                                        LengthLimitingTextInputFormatter(
                                                            4)
                                                      ],
                                                      style: TextStyle(
                                                          fontFeatures: const [
                                                            FontFeature
                                                                .tabularFigures()
                                                          ],
                                                          fontFamily:
                                                              GoogleFonts.lato()
                                                                  .fontFamily,
                                                          fontSize: 18,
                                                          fontWeight:
                                                              FontWeight.w400,
                                                          color: Colors.black),
                                                      textAlign:
                                                          TextAlign.start,
                                                      keyboardType:
                                                      Platform.isIOS
                                                          ? const TextInputType.numberWithOptions(
                                                          signed: true, decimal: true)
                                                          : TextInputType.number,
                                                      cursorColor: AppColors
                                                          .primaryDarkColor,
                                                      decoration:
                                                          InputDecoration(
                                                              isDense: true,
                                                              border:
                                                                  InputBorder
                                                                      .none,
                                                              contentPadding:
                                                                  const EdgeInsets.fromLTRB(
                                                                      0, 0, 0, 0),
                                                              hintStyle: TextStyle(
                                                                fontFamily: GoogleFonts
                                                                        .lato()
                                                                    .fontFamily,
                                                                color: const Color(
                                                                    0xff6c6d71),
                                                                fontSize: 18,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w400,
                                                                fontStyle:
                                                                    FontStyle
                                                                        .normal,
                                                                fontFeatures: const [
                                                                  FontFeature
                                                                      .tabularFigures()
                                                                ],
                                                              ),
                                                              hintText: 'YYYY'))))
                                        ]))),
                                const SizedBox(width: 15),
                                Expanded(
                                    flex: 1,
                                    child: Container(
                                        height: 54,
                                        decoration: BoxDecoration(
                                          color: const Color(0xfff3f3f3),
                                          border: Border.all(
                                            color: const Color(0xffdbdbdb),
                                            width: 1,
                                          ),
                                          borderRadius:
                                              BorderRadius.circular(8),
                                        ),
                                        alignment: Alignment.center,
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 15),
                                        child: TextFormField(
                                            textInputAction:
                                                TextInputAction.done,
                                            controller: controller
                                                .cardCvvController.value,
                                            autofocus: false,
                                            onChanged: (value) {
                                              controller.cardCvv.value = value;
                                            },
                                            inputFormatters: <
                                                TextInputFormatter>[
                                              FilteringTextInputFormatter
                                                  .digitsOnly,
                                              LengthLimitingTextInputFormatter(
                                                  3)
                                            ],
                                            style: TextStyle(
                                                fontFeatures: const [
                                                  FontFeature.tabularFigures()
                                                ],
                                                fontFamily: GoogleFonts.lato()
                                                    .fontFamily,
                                                fontSize: 18,
                                                fontWeight: FontWeight.w400,
                                                color: Colors.black),
                                            textAlign: TextAlign.start,
                                            keyboardType: Platform.isIOS
                                                ? const TextInputType.numberWithOptions(
                                                signed: true, decimal: true)
                                                : TextInputType.number,
                                            cursorColor:
                                                AppColors.primaryDarkColor,
                                            decoration: InputDecoration(
                                                isDense: true,
                                                border: InputBorder.none,
                                                contentPadding:
                                                    const EdgeInsets.fromLTRB(
                                                        0, 0, 0, 0),
                                                hintStyle: TextStyle(
                                                  fontFamily: GoogleFonts.lato()
                                                      .fontFamily,
                                                  color:
                                                      const Color(0xff6c6d71),
                                                  fontSize: 18,
                                                  fontWeight: FontWeight.w400,
                                                  fontStyle: FontStyle.normal,
                                                  fontFeatures: const [
                                                    FontFeature.tabularFigures()
                                                  ],
                                                ),
                                                hintText: 'CVV')))),
                              ]),
                            )
                          ])),
                  const SizedBox(
                    height: 120,
                  )
                ])
              : ListView(children: [
                  ProfileWidget(
                    title: "Checkout",
                    onTap: () {
                      if (controller.isCardScreen.value == true) {
                        controller.isCardScreen.value = false;
                      } else {
                        Get.back();
                      }
                    },
                  ),
                  Container(
                    margin: const EdgeInsets.only(
                        left: 16, right: 16, bottom: 15, top: 20),
                    decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(15)),
                        boxShadow: [
                          BoxShadow(
                              color: Color(0x2e000000),
                              offset: Offset(0, 0),
                              blurRadius: 10,
                              spreadRadius: 0)
                        ],
                        color: Color(0xffffffff)),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        GlobalText(
                          controller.item.name ?? "",
                          color: Colors.black,
                          height: 1.5,
                          fontWeight: FontWeight.w500,
                          fontFamily: GoogleFonts.poppins().fontFamily,
                          fontStyle: FontStyle.normal,
                          fontSize: 15.0,
                          paddingLeft: 10.0,
                          paddingTop: 16.0,
                          paddingRight: 12.0,
                        ),
                        GlobalText(
                          '${pingModel.csymbol == "\$" ? "\$" : "₹"} ${controller.item.registrationFees != 0 ? "${controller.item.price + controller.item.registrationFees}" : "${controller.item.price}"}',
                          color: AppColors.secondaryDartColor,
                          fontWeight: FontWeight.w600,
                          fontFamily: GoogleFonts.poppins().fontFamily,
                          fontStyle: FontStyle.normal,
                          fontSize: 22.0,
                          paddingLeft: 10.0,
                        ),
                        GlobalSvgButton(
                          onTap: () {
                            Get.back();
                          },
                          color: const Color(0xffD24C4C),
                          assetPath: AppImages.deleteIcon,
                          padding: const EdgeInsets.only(
                              top: 14.0, left: 310.0, bottom: 10),
                        )
                      ],
                    ),
                  ),
                  // GlobalText(
                  //   'Add more Items',
                  //   onTap: () {
                  //     Get.back();
                  //     Get.back();
                  //   },
                  //   paddingTop: 120,
                  //   paddingRight: 20,
                  //   fontWeight: FontWeight.w600,
                  //   color: AppColors.primaryDarkColor,
                  //   textAlign: TextAlign.right,
                  // ),
                  Obx(() => Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const GlobalText(
                            'Order Summary',
                            color: Color(0xff6c6d71),
                            fontWeight: FontWeight.w700,
                            fontStyle: FontStyle.normal,
                            fontSize: 22.0,
                            paddingLeft: 16,
                          ),
                          const SizedBox(
                            height: 17.0,
                          ),
                          Column(
                            children: [
                              Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    GlobalText(
                                      'Sub-total Items (1)',
                                      textStyle: style,
                                      paddingLeft: 15,
                                      height: 2.5,
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(top: 15.0),
                                      child: GlobalText(
                                        '${pingModel.csymbol == "\$" ? "\$" : "₹"}${(controller.item.registrationFees != null ? controller.item.registrationFees + controller.item.price : controller.item.price).toStringAsFixed(2)}',
                                        paddingRight: 15,
                                        textStyle: style,
                                        height: 2,
                                        //paddingTop: 0.5,
                                      ),
                                    ),
                                  ]),
                              Container(
                                margin: const EdgeInsets.only(
                                    left: 15.0, right: 15.0, top: 13),
                                width: 342.5,
                                height: 1,
                                decoration: const BoxDecoration(
                                    color: Color(0xffdfdfdf)),
                              ),
                            ],
                          ),
                          controller.creditCardFees.value != 0
                              ? Column(
                                  children: [
                                    Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          GlobalText(
                                            'Card Fees',
                                            textStyle: style,
                                            paddingLeft: 15,
                                            height: 2.5,
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                top: 15.0),
                                            child: GlobalText(
                                              '${pingModel.csymbol == "\$" ? "\$" : "₹"}${(controller.creditCardFees.value).toStringAsFixed(2)}',
                                              paddingRight: 15,
                                              textStyle: style,
                                              height: 2,
                                              //paddingTop: 0.5,
                                            ),
                                          ),
                                        ]),
                                    Container(
                                      margin: const EdgeInsets.only(
                                          left: 15.0, right: 15.0, top: 13),
                                      width: 342.5,
                                      height: 1,
                                      decoration: const BoxDecoration(
                                          color: Color(0xffdfdfdf)),
                                    ),
                                  ],
                                )
                              : const SizedBox(),
                          controller.taxableAmount.value != 0
                              ? Column(
                                  children: [
                                    Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          GlobalText(
                                            'Taxes',
                                            textStyle: style,
                                            paddingLeft: 15,
                                            height: 2.5,
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                top: 15.0),
                                            child: GlobalText(
                                              '${pingModel.csymbol == "\$" ? "\$" : "₹"}${(controller.taxableAmount.value).toStringAsFixed(2)}',
                                              paddingRight: 15,
                                              textStyle: style,
                                              height: 2,
                                              //paddingTop: 0.5,
                                            ),
                                          ),
                                        ]),
                                    Container(
                                      margin: const EdgeInsets.only(
                                          left: 15.0, right: 15.0, top: 13),
                                      width: 342.5,
                                      height: 1,
                                      decoration: const BoxDecoration(
                                          color: Color(0xffdfdfdf)),
                                    ),
                                  ],
                                )
                              : const SizedBox(),
                          controller.discountAmount.value != 0
                              ? Column(
                                  children: [
                                    Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          GlobalText(
                                            'Discount',
                                            textStyle: style,
                                            paddingLeft: 15,
                                            height: 2.5,
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                top: 15.0),
                                            child: GlobalText(
                                              '${pingModel.csymbol == "\$" ? "\$" : "₹"}${(controller.discountAmount.value).toStringAsFixed(2)}',
                                              paddingRight: 15,
                                              textStyle: style,
                                              height: 2,
                                              //paddingTop: 0.5,
                                            ),
                                          ),
                                        ]),
                                    Container(
                                      margin: const EdgeInsets.only(
                                          left: 15.0, right: 15.0, top: 13),
                                      width: 342.5,
                                      height: 1,
                                      decoration: const BoxDecoration(
                                          color: Color(0xffdfdfdf)),
                                    ),
                                  ],
                                )
                              : const SizedBox(),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              GlobalText(
                                'Total',
                                textStyle: style,
                                paddingLeft: 15,
                                height: 2.5,
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 8.0),
                                child: GlobalText(
                                  '${pingModel.csymbol == "\$" ? "\$" : "₹"}${(controller.total.value).toStringAsFixed(2)}',
                                  height: 2.0,
                                  color: Colors.black,
                                  paddingRight: 15,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: GoogleFonts.poppins().fontFamily,
                                  fontStyle: FontStyle.normal,
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 13,
                          ),
                          Container(
                            margin:
                                const EdgeInsets.only(left: 15.0, right: 15.0),
                            width: 342.5,
                            height: 1,
                            decoration:
                                const BoxDecoration(color: Color(0xffdfdfdf)),
                          ),
                          const SizedBox(
                            height: 25,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              CustomEditText(
                                margin: EdgeInsets.zero,
                                hintText: 'Offer Code',
                                controller: controller.promoCodeTextController,
                                inputType: TextInputType.name,
                                height: 48,
                                width: 182,
                              ),
                              const SizedBox(
                                width: 12,
                              ),
                              GlobalOutlineButton(
                                text: controller.offerCode.value.id != null
                                    ? 'Remove'
                                    : 'Apply',
                                height: 48,
                                width: 148,
                                onTap: () async {
                                  await controller.appliedPromocode();
                                },
                              ),
                            ],
                          ),
                        ],
                      )),
                  const SizedBox(
                    height: 120,
                  )
                ])),
          Container(
            width: 375,
            height: 108,
            decoration: const BoxDecoration(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(25.0),
                    topRight: Radius.circular(25.0)),
                boxShadow: [
                  BoxShadow(
                      color: Color(0x473b2e7e),
                      offset: Offset(0, 0),
                      blurRadius: 11,
                      spreadRadius: 0)
                ],
                color: Color(0xffffffff)),
            child: Center(
                child: Obx(() => GlobalButton(
                      title: controller.isCardScreen.value
                          ? 'Make A Payment'
                          : 'Proceed to Checkout',
                      fontSize: 22,
                      width: 296.0,
                      height: 51,
                      bgColor: const Color(0xffE31F26),
                      onTap: () async {
                        FocusScope.of(context).unfocus();
                        if (controller.isCardScreen.value == false) {
                          controller.isCardScreen.value = true;
                        } else {
                          await controller.startPaymentOld();
                        }
                        //await controller.startPayment();
                      },
                    ))),
          ),
          Obx(() => controller.promoLoading.value
              ? const GlobalBackDrop()
              : const SizedBox())
        ],
      )),
    );
  }
}
