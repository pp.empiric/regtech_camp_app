import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:regtech_camp_app/common_widget/global_back_drop.dart';
import 'package:regtech_camp_app/common_widget/global_shimmer.dart';

import 'package:regtech_camp_app/utils.dart';
import 'package:shimmer/shimmer.dart';

import '../../../common_widget/global_text.dart';
import '../../../resources/app_colors.dart';
import '../../../resources/app_constants.dart';
import '../../../resources/app_images.dart';
import '../../../resources/app_texts.dart';
import '../../../routes/app_routes.dart';
import '../../couse_details/controllers/course_curriculum_controller.dart';
import '../../couse_details/views/course_curriculum_screen.dart';
import '../controllers/home_controller.dart';
import '../widgets/tab_indicator.dart';

class HomeScreen extends GetView<HomeController> {
  HomeScreen({ Key? key}) : super(key: key);
  @override
  final controller = Get.put(HomeController());
  final CourseCurriculumController courseCurriculumController =
      Get.put(CourseCurriculumController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
      child: Stack(
        children: [
          Stack(alignment: Alignment.bottomCenter, children: [
            Obx(() => RefreshIndicator(
                  onRefresh: controller.getAllHomeScreenApi,
                  child: ListView(
                    padding: EdgeInsets.zero,
                    physics: const AlwaysScrollableScrollPhysics(),
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    children: [
                      Stack(
                        children: [
                          Image.asset(AppImages.bgBigPurpleImage,
                              height: 191, width: 375, fit: BoxFit.fill),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Expanded(
                                child: Container(
                                  margin: const EdgeInsets.only(
                                      top: 72.3, left: 16),
                                  child: Obx(
                                    () => GlobalText(
                                        controller.greetingMsg.value,
                                        color: Colors.white,
                                        fontSize: 23,
                                        maxLine: 2,
                                        textOverflow: TextOverflow.ellipsis,
                                        fontWeight: FontWeight.w700,
                                        fontStyle: FontStyle.normal),
                                  ),
                                ),
                              ),
                              Obx(() => InkWell(
                                   highlightColor: Colors.transparent,
                                   splashColor: Colors.transparent,
                                   onTap: () => Get.toNamed(Routes.PROFILE),
                                   child: (controller.profilePic.value.isEmpty)
                                      ? Container(
                                          margin: const EdgeInsets.only(
                                              top: 60, right: 30, bottom: 15),
                                          decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            border: Border.all(
                                                color: Colors.white, width: 4),
                                          ),
                                          child: Image.asset(
                                            AppImages.profileImage,
                                            width: 54.35,
                                            height: 54.35,
                                          ),
                                        )
                                      : Container(
                                          width: 62,
                                          height: 62,
                                          margin: const EdgeInsets.only(
                                              top: 60, right: 30, bottom: 15),
                                          decoration: BoxDecoration(
                                            image: DecorationImage(
                                              image: NetworkImage(
                                                  controller.profilePic.value),
                                              fit: BoxFit.cover,
                                            ),
                                            shape: BoxShape.circle,
                                            border: Border.all(
                                              color: Colors.white,
                                              width: 4.0,
                                            ),
                                          ))))
                            ],
                          ),
                          controller.notificationList.isNotEmpty
                              ? Container(
                                  height: 90,
                                  margin: const EdgeInsets.only(top: 152),
                                  child: PageView.builder(
                                    itemCount:
                                        controller.notificationList.length,
                                    controller:
                                        controller.notificationController,
                                    physics: const ScrollPhysics(),
                                    scrollDirection: Axis.horizontal,
                                    onPageChanged: (value) {
                                      controller.notificationIndex.value =
                                          value;
                                    },
                                    itemBuilder: (context, index) => Container(
                                      margin: const EdgeInsets.only(
                                          top: 10.9,
                                          left: 15,
                                          right: 15,
                                          bottom: 10),
                                      decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.circular(15),
                                        boxShadow: [
                                          BoxShadow(
                                            color: AppColors.primaryDarkColor,
                                            offset: const Offset(0, 1),
                                            blurRadius: 14,
                                            spreadRadius: 0,
                                          ),
                                        ],
                                      ),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          const SizedBox(width: 5),
                                          Container(
                                            margin: const EdgeInsets.only(
                                                top: 10, bottom: 10, left: 5),
                                            child: Image.asset(
                                                AppImages.notification),
                                          ),
                                          Expanded(
                                            child: Container(
                                              alignment: Alignment.centerLeft,
                                              margin: const EdgeInsets.only(
                                                  top: 17,
                                                  bottom: 17,
                                                  left: 12,
                                                  right: 5),
                                              child: Obx(() => GlobalText(
                                                  parseHtmlString(controller
                                                          .notificationList[
                                                              controller
                                                                  .notificationIndex
                                                                  .value]
                                                          .message ??
                                                      ''),
                                                  color: AppColors
                                                      .primaryDarkColor,
                                                  fontSize: 13,
                                                  fontFamily:
                                                      GoogleFonts.poppins()
                                                          .fontFamily,
                                                  maxLine: 10,
                                                  textOverflow:
                                                      TextOverflow.ellipsis,
                                                  fontWeight: FontWeight.w600,
                                                  fontStyle: FontStyle.normal)),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                )
                              : const SizedBox(),
                        ],
                      ),
                      if (controller.notificationList.isNotEmpty)
                        tabIndicator(false),
                      // HomeTabBar(),
                      // const SizedBox(
                      //   height: 23,
                      // ),
                      // HomeTabPageView(),
                      // Container(
                      //   height: 50,
                      // ),
                      Stack(
                        children: [
                          Container(
                            height: 378,
                            decoration: BoxDecoration(
                              gradient: LinearGradient(
                                colors: [
                                  AppColors.secondaryDartColor,
                                  const Color(0x005f2d2c)
                                ],
                                stops: const [0, 1],
                                begin: const Alignment(-0.00, -1.00),
                                end: const Alignment(0.00, 1.00),
                              ),
                            ),
                          ),
                          ListView(
                            padding: EdgeInsets.zero,
                            physics: const NeverScrollableScrollPhysics(),
                            scrollDirection: Axis.vertical,
                            shrinkWrap: true,
                            children: [
                              Container(
                                margin:
                                    const EdgeInsets.only(top: 18, left: 16),
                                child: GlobalText(
                                  AppTexts.myCourses,
                                  fontSize: 22,
                                  fontWeight: FontWeight.w700,
                                  fontStyle: FontStyle.normal,
                                  color: Colors.white,
                                ),
                              ),
                              controller.courseList.isNotEmpty
                                  ? exploreMoreButton()
                                  : Column(
                                      children: [
                                        const SizedBox(height: 41),
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 34),
                                          child: GlobalText(
                                            'You don’t have any courses yet!',
                                            color: AppColors.primaryDarkColor,
                                            fontSize: 22,
                                            fontWeight: FontWeight.w700,
                                            fontStyle: FontStyle.normal,
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                        const SizedBox(height: 25),
                                        Container(
                                            margin: const EdgeInsets.only(
                                                left: 75, right: 142),
                                            child: SvgPicture.asset(
                                                AppImages.curvedArrow)),
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 29),
                                          child: Row(
                                            children: [
                                              GlobalText(
                                                '  Let’s\nEnroll',
                                                fontFamily:
                                                    GoogleFonts.racingSansOne()
                                                        .fontFamily,
                                                color: Colors.white,
                                                fontSize: 33,
                                                fontWeight: FontWeight.w400,
                                                fontStyle: FontStyle.normal,
                                                shadows: const [
                                                  BoxShadow(
                                                      color: Color(0xffB74400),
                                                      offset: Offset(0, 2),
                                                      blurRadius: 4,
                                                      spreadRadius: 0)
                                                ],
                                              ),
                                              const Spacer(),
                                              exploreMoreButton()
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                              if (controller.courseList.isNotEmpty)
                                ListView.builder(
                                  padding: const EdgeInsets.only(
                                      top: 22, bottom: 100),
                                  physics: const NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemCount: controller.courseList.length,
                                  itemBuilder: (context, index) => Container(
                                    margin: const EdgeInsets.only(
                                        left: 16, right: 16, bottom: 24),
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(15),
                                      boxShadow: const [
                                        BoxShadow(
                                          color: Color(0x2d000000),
                                          offset: Offset(0, 0),
                                          blurRadius: 10,
                                          spreadRadius: 0,
                                        ),
                                      ],
                                    ),
                                    child: Material(
                                      color: Colors.transparent,
                                      child: InkWell(
                                        onTap: () async
                                        {

                                          Get.to(() => CoursesCurriculumScreen(
                                              data: courseCurriculumController.course.value),
                                          );



                                          courseCurriculumController.guId.value = controller.courseList[index].guId ?? '';
                                          courseCurriculumController.course.value = controller.courseList[index];

                                          await courseCurriculumController.getCourseNewData();


                                        },
                                        borderRadius: BorderRadius.circular(15),
                                        child: ListView(
                                          physics:
                                              const NeverScrollableScrollPhysics(),
                                          shrinkWrap: true,
                                          padding: const EdgeInsets.only(
                                              left: 14,
                                              right: 14,
                                              bottom: 17,
                                              top: 14),
                                          children: [
                                            ClipRRect(
                                              borderRadius:
                                                  const BorderRadius.all(
                                                      Radius.circular(15)),
                                              child: CachedNetworkImage(
                                                  imageUrl:
                                                      '$COURSE_IMAGE_BASE${controller.courseList[index].image1}',
                                                  placeholder: (context, url) =>
                                                      Image.asset(
                                                          AppImages
                                                              .coursePlaceHolder,
                                                          fit: BoxFit.fill),
                                                  errorWidget: (context, url,
                                                          error) =>
                                                      Image.asset(
                                                          AppImages
                                                              .coursePlaceHolder,
                                                          fit: BoxFit.fill),
                                                  height: 166,
                                                  fit: BoxFit.fill),
                                            ),
                                            const SizedBox(height: 11),
                                            GlobalText(
                                                controller.courseList[index]
                                                        .title ??
                                                    '',
                                                fontSize: 16,
                                                fontWeight: FontWeight.w700,
                                                fontStyle: FontStyle.normal,
                                                maxLine: 10,
                                                textOverflow:
                                                    TextOverflow.ellipsis),
                                            const SizedBox(height: 10),
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              children: [
                                                Expanded(
                                                  child: LinearPercentIndicator(
                                                    lineHeight: 10,

                                                    percent: (controller
                                                                .courseList[
                                                                    index]
                                                                .studentProgress ??
                                                            1) /
                                                        100,
                                                    backgroundColor:
                                                        const Color(0xffE0E0E0),
                                                    progressColor: AppColors
                                                        .secondaryDartColor,
                                                  ),
                                                ),
                                                GlobalText(
                                                    "${controller.courseList[index].studentProgress} %",
                                                    maxLine: 1,
                                                    textOverflow:
                                                        TextOverflow.ellipsis,
                                                    fontWeight: FontWeight.w600,
                                                    color: AppColors
                                                        .primaryDarkColor,
                                                    fontSize: 16,
                                                    fontStyle:
                                                        FontStyle.normal),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ))
          ]),
          Obx(() => controller.isApiCalling.value
              ? const GlobalBackDrop()
              : const SizedBox())

        ],
      ),
    ));
  }
}

Widget exploreMoreButton() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.end,
    children: [
      Container(
        margin: const EdgeInsets.only(right: 10, top: 20),
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        decoration: BoxDecoration(
          color: const Color(0xffE31F26),
          borderRadius: BorderRadius.circular(10),
        ),
        child: Material(
          color: const Color(0xffE31F26),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          child: InkWell(
            borderRadius: BorderRadius.circular(10),
            onTap: () {
              Get.toNamed(Routes.COURSE);
            },
            child: Center(
                child: GlobalText(AppTexts.exploreCourse,
                    color: Colors.white,
                    maxLine: 1,
                    fontSize: 15,
                    textOverflow: TextOverflow.ellipsis,
                    fontWeight: FontWeight.w600,
                    fontStyle: FontStyle.normal)),
          ),
        ),
      ),
    ],
  );
}
