import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pie_chart/pie_chart.dart';
import 'package:regtech_camp_app/modules/home/controllers/home_controller.dart';

import '../../../common_widget/global_text.dart';
import 'linear_chat.dart';

class HomeTabPageView extends StatelessWidget {
  HomeTabPageView({Key? key}) : super(key: key);
  final homeController = Get.put(HomeController());

  @override
  Widget build(BuildContext context) {
    return Obx(() => Padding(
          padding: ((homeController.homeTabIndex.value == 0)
              ? const EdgeInsets.only(left: 16)
              : const EdgeInsets.only(right: 16)),
          child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  topLeft: (homeController.homeTabIndex.value == 0)
                      ? const Radius.circular(15)
                      : Radius.zero,
                  bottomLeft: (homeController.homeTabIndex.value == 0)
                      ? const Radius.circular(15)
                      : Radius.zero,
                  bottomRight: (homeController.homeTabIndex.value == 1)
                      ? const Radius.circular(15)
                      : Radius.zero,
                  topRight: (homeController.homeTabIndex.value == 1)
                      ? const Radius.circular(15)
                      : Radius.zero),
            ),
            child: SizedBox(
              height: 270,
              child: PageView(
                allowImplicitScrolling: false,
                controller: homeController.homeTabController,
                onPageChanged: (index) {
                  homeController.homeTabIndex.value = index;
                },
                children: [
                  Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                    Expanded(
                      child: Column(
                        children: [
                          const SizedBox(height: 23),
                          GlobalText(
                            "Weekly Progress",
                            fontFamily: GoogleFonts.poppins().fontFamily,
                            fontWeight: FontWeight.w500,
                            fontSize: 18,
                            color: const Color(0xff6C6D71),
                          ),
                          const SizedBox(height: 23),
                          PieChart(
                            dataMap: homeController.dataMap,
                            chartLegendSpacing: 35,
                            chartRadius: Get.width / 3.5,
                            colorList: const [
                              Color(0xffFFC542),
                              Color(0xffFF575F),
                              Color(0xff2A3C44),
                              Color(0xff3DD598)
                            ],
                            initialAngleInDegree: 260,
                            chartType: ChartType.ring,
                            ringStrokeWidth: 8,
                            centerText: "",
                            legendOptions: const LegendOptions(
                              showLegendsInRow: false,
                              legendPosition: LegendPosition.right,
                              showLegends: true,
                              legendShape: BoxShape.rectangle,
                              legendTextStyle: TextStyle(
                                  fontWeight: FontWeight.w400,
                                  color: Color(0xff6C6D71),
                                  fontSize: 14),
                            ),
                            chartValuesOptions: const ChartValuesOptions(
                              showChartValueBackground: true,
                              showChartValues: false,
                              showChartValuesInPercentage: false,
                              showChartValuesOutside: true,
                              decimalPlaces: 1,
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                        width: 1, height: 227, color: const Color(0xffBBBBBB))
                  ]),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      const SizedBox(height: 23),
                      GlobalText(
                        "Performance",
                        fontFamily: GoogleFonts.poppins().fontFamily,
                        fontWeight: FontWeight.w500,
                        fontSize: 18,
                        color: const Color(0xff6C6D71),
                      ),
                      const SizedBox(height: 23),
                      Expanded(child: LinearChart()),
                    ],
                  )
                ],
              ),
            ),
          ),
        ));
  }
}
