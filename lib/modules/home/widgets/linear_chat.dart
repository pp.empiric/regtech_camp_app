import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:regtech_camp_app/modules/home/controllers/home_controller.dart';

import '../../../common_widget/global_text.dart';

class LinearChart extends StatelessWidget {
  LinearChart({Key? key}) : super(key: key);
  final homeController = Get.put(HomeController());

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 200,
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          itemCount: homeController.linearChart.length,
          itemBuilder: (context, index) {
            return Column(children: [
              Container(
                  margin: const EdgeInsets.only(left: 12.5, right: 12.5),
                  height: 114,
                  width: 10,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(6),
                    color: const Color(0xff1b262b),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(6),
                          color: const Color(0xffFF575F),
                        ),
                        height:
                            100 / homeController.linearChart[index].progress1,
                      ),
                      const SizedBox(height: 3),
                      Container(
                        height:
                            100 / homeController.linearChart[index].progress2,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(6),
                          color: const Color(0xff3DD598),
                        ),
                      )
                    ],
                  )),
              const SizedBox(height: 3),
              GlobalText(homeController.linearChart[index].title ?? '',
                  color: const Color(0xff6C6D71)),
              const SizedBox(height: 3),
              GlobalText(homeController.linearChart[index].subTitle ?? '',
                  color: const Color(0xff6C6D71))
            ]);
          }),
    );
  }
}
