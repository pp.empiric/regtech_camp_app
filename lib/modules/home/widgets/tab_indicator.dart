import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:regtech_camp_app/resources/app_colors.dart';

import '../controllers/home_controller.dart';

final controller = Get.put(HomeController());

Widget tabIndicator(bool forNotice) {
  return Container(
    height: 46,
    alignment: Alignment.center,
    child: ListView.builder(
        itemBuilder: (context, index) => Obx(() => Container(
            width: 25,
            margin: const EdgeInsets.only(right: 8),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: forNotice
                    ? controller.quoteIndex.value == index
                        ? AppColors.primaryDarkColor
                        : const Color(0x00DDDDDD).withOpacity(1)
                    : controller.notificationIndex.value == index
                        ? AppColors.primaryDarkColor
                        : const Color(0x00DDDDDD).withOpacity(1)),
            child: InkWell(
                highlightColor: Colors.transparent,
                splashColor: Colors.transparent,
                onTap: () {
                  if (forNotice) {
                    controller.quoteController.animateToPage(index,
                        duration: const Duration(milliseconds: 200),
                        curve: Curves.linear);
                    controller.quoteIndex.value = index;
                  } else {
                    controller.notificationIndex.value = index;
                    controller.notificationController.animateToPage(index,
                        duration: const Duration(milliseconds: 200),
                        curve: Curves.linear);
                  }
                }))),
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        physics: const ScrollPhysics(),
        padding: const EdgeInsets.symmetric(vertical: 20),
        itemCount: forNotice
            ? controller.quoteList.length
            : controller.notificationList.length),
  );
}
