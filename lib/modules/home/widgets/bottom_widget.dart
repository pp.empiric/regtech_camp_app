// import 'package:flutter/material.dart';
// import 'package:flutter_svg/svg.dart';
// import 'package:get/get.dart';
//
// import '../../../resources/app_colors.dart';
// import '../controllers/home_controller.dart';
//
// class BottomWidget extends StatelessWidget {
//   final String? image;
//   final int? index;
//   final homeController = Get.put(HomeController());
//
//   BottomWidget({Key? key, this.image, this.index}) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return Obx(() => Container(
//           width: 60,
//           height: 60,
//           padding: const EdgeInsets.symmetric(horizontal: 18, vertical: 18),
//           decoration: homeController.selectedIndex.value == index
//               ? BoxDecoration(
//                   shape: BoxShape.circle, color: AppColors.primaryDarkColor)
//               : null,
//           child: InkWell(
//             highlightColor: Colors.transparent,
//             splashColor: Colors.transparent,
//             onTap: () {
//               if (homeController.selectedIndex.value != index) {
//                 homeController.selectedIndex.value = index!;
//               }
//             },
//             child: SvgPicture.asset(
//               image!,
//               color: homeController.selectedIndex.value == index
//                   ? Colors.white
//                   : AppColors.primaryDarkColor,
//             ),
//           ),
//         ));
//   }
// }

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:regtech_camp_app/modules/home/controllers/home_controller.dart';
import 'package:regtech_camp_app/resources/app_colors.dart';

class BottomWidget extends StatelessWidget {
  final String? image;
  final int? index;
  final HomeController homeController;

  const BottomWidget(
      {Key? key, this.image, this.index, required this.homeController})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Container(
        width: 60,
        height: 60,
        padding: const EdgeInsets.symmetric(horizontal: 18, vertical: 18),
        decoration: homeController.selectedIndex.value == index
            ? BoxDecoration(
                shape: BoxShape.circle, color: AppColors.primaryDarkColor)
            : null,
        child: InkWell(
          highlightColor: Colors.transparent,
          splashColor: Colors.transparent,
          onTap: () {
            if (homeController.selectedIndex.value != index) {
              homeController.selectedIndex.value = index!;
            }
          },
          child: SvgPicture.asset(image!,
              color: homeController.selectedIndex.value == index
                  ? Colors.white
                  : AppColors.primaryDarkColor),
        ),
      ),
    );
  }
}
