import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:regtech_camp_app/modules/home/controllers/home_controller.dart';

import '../../../common_widget/global_text.dart';
import '../../../resources/app_colors.dart';

class HomeTabBar extends StatelessWidget {
  HomeTabBar({Key? key}) : super(key: key);
  final homeController = Get.put(HomeController());

  @override
  Widget build(BuildContext context) {
    return Obx(()=>Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Card(
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(20), bottomLeft: Radius.circular(20)),
          ),
          margin: EdgeInsets.zero,
          child: Container(
            alignment: Alignment.center,
            width: 120,
            height: 34,
            decoration: homeController.homeTabIndex.value == 0
                ? BoxDecoration(
                color: AppColors.secondaryDartColor,
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(50),
                    bottomLeft: Radius.circular(50)))
                : null,
            child: InkWell(
                highlightColor: Colors.transparent,
                splashColor: Colors.transparent,
                onTap: () {
                  if (homeController.homeTabIndex.value != 0) {
                    homeController.homeTabIndex.value = 0;
                  }
                  homeController.homeTabController.animateToPage(0,
                      duration: const Duration(milliseconds: 200),
                      curve: Curves.linear);
                },
                child: GlobalText(
                  "Recent",
                  color: (homeController.homeTabIndex.value == 0)
                      ? Colors.white
                      : const Color(0xffCFCFCF),
                  fontWeight: FontWeight.w500,
                  fontSize: 16,
                  textAlign: TextAlign.center,
                  fontFamily: GoogleFonts.poppins().fontFamily,
                )),
          ),
        ),
        Card(
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topRight: Radius.circular(20),
                bottomRight: Radius.circular(20)),
          ),
          margin: EdgeInsets.zero,
          child: Container(
            width: 120,
            height: 34,
            alignment: Alignment.center,
            decoration: homeController.homeTabIndex.value == 1
                ? BoxDecoration(
                color: AppColors.secondaryDartColor,
                borderRadius: const BorderRadius.only(
                    bottomRight: Radius.circular(50),
                    topRight: Radius.circular(50)))
                : null,
            child: InkWell(
                highlightColor: Colors.transparent,
                splashColor: Colors.transparent,
                onTap: () {
                  if (homeController.homeTabIndex.value != 1) {
                    homeController.homeTabIndex.value = 1;
                  }
                  homeController.homeTabController.animateToPage(1,
                      duration: const Duration(milliseconds: 200),
                      curve: Curves.linear);
                },
                child: GlobalText(
                  "Overall",
                  color: (homeController.homeTabIndex.value == 1)
                      ? Colors.white
                      : const Color(0xffCFCFCF),
                  fontWeight: FontWeight.w500,
                  fontSize: 16,
                  textAlign: TextAlign.center,
                  fontFamily: GoogleFonts.poppins().fontFamily,
                )),
          ),
        )
      ],
    ));
  }
}
