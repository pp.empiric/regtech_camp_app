// import 'package:flutter/material.dart';
// import 'package:get/get.dart';
// import 'package:regtech_camp_app/resources/app_colors.dart';
//
// import '../../../resources/app_images.dart';
// import '../controllers/home_controller.dart';
// import 'bottom_widget.dart';
//
// final homeController = Get.put(HomeController());
//
// class BottomView extends StatelessWidget {
//   const BottomView({Key? key}) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//         width: 172,
//         margin: const EdgeInsets.only(bottom: 20),
//         height: 66,
//         decoration: BoxDecoration(
//           color: Colors.white,
//           borderRadius: BorderRadius.circular(33),
//           boxShadow: [
//             BoxShadow(
//               color: AppColors.primaryDarkColor,
//               offset: const Offset(0, 0),
//               blurRadius: 24,
//               spreadRadius: 0,
//             ),
//           ],
//         ),
//         padding: const EdgeInsets.symmetric(horizontal: 4),
//         child: Row(
//           mainAxisAlignment: MainAxisAlignment.spaceBetween,
//           children: [
//             BottomWidget(
//               image: AppImages.homeIcon,
//               index: 0,
//             ),
//             // if (homeController.categoryList.isNotEmpty == true)
//             //   BottomWidget(
//             //     image: AppImages.eventsIcon,
//             //     index: 1,
//             //   ),
//             BottomWidget(
//               image: AppImages.filesIcon,
//               index: 2,
//             ),
//           ],
//         ));
//   }
// }

import 'package:flutter/material.dart';
import 'package:regtech_camp_app/modules/home/controllers/home_controller.dart';
import 'package:regtech_camp_app/resources/app_colors.dart';
import 'package:regtech_camp_app/resources/app_images.dart';

import 'bottom_widget.dart';

class BottomView extends StatelessWidget {
  final HomeController homeController;

  const BottomView(this.homeController, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.only(bottom: 20),
        width: 160,
        height: 66,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(33),
          boxShadow: [
            BoxShadow(
              color: AppColors.primaryDarkColor,
              offset: const Offset(0, 0),
              blurRadius: 24,
              spreadRadius: 0,
            ),
          ],
        ),
        padding: const EdgeInsets.symmetric(horizontal: 4),
        child:
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          BottomWidget(
              image: AppImages.homeIcon,
              index: 0,
              homeController: homeController),
          BottomWidget(
              image: AppImages.eventsIcon,
              index: 1,
              homeController: homeController),
          // BottomWidget(
          //     image: AppImages.filesIcon,
          //     index: 2,
          //     homeController: homeController),
        ]));
  }
}
