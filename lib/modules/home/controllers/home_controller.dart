import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:regtech_camp_app/model/chapters_response.dart';
import 'package:regtech_camp_app/model/linear_chart_model.dart';
import 'package:regtech_camp_app/model/quiz_new_response.dart';
import 'package:regtech_camp_app/model/section_response.dart';
import 'package:regtech_camp_app/modules/couse_details/controllers/course_curriculum_controller.dart';
import 'package:regtech_camp_app/modules/couse_details/views/course_curriculum_screen.dart';
import 'package:regtech_camp_app/utils.dart';

import '../../../model/course_response.dart';
import '../../../model/item_category_response.dart';
import '../../../model/quote_response.dart';
import '../../../resources/app_constants.dart';
import '../../../services/apis.dart';
import '../../../services/app_preference.dart';

class HomeController extends GetxController {

  RxString greetingMsg = ''.obs;
  RxString userName = ''.obs;
  RxString profilePic = ''.obs;
  RxString profileEmail = ''.obs;
  RxString phoneNumber = ''.obs;
  var selectedIndex = 0.obs;
  var quoteIndex = 0.obs;
  var notificationIndex = 0.obs;
  var isApiCalling = false.obs;
  var paidCourseCalling = false.obs;
  var homeTabIndex = 0.obs;
  var cName = ''.obs;
  var isVideoLoading= false.obs;
  var isSectionLoading= false.obs;

  var quoteController = PageController();
  var notificationController = PageController();
  var homeTabController = PageController();

  Rx<QuizNewResponse> quizNewResponse = QuizNewResponse().obs;
  Rx<QuizNewData> quizSingleResponse = QuizNewData().obs;

  RxList<Quote> quoteList = <Quote>[].obs;
  RxList<CourseData> courseList = <CourseData>[].obs;
  RxList<Quote> notificationList = <Quote>[].obs;
  RxList<ItemCategoryModel> storeList = <ItemCategoryModel>[].obs;
  List<SectionResponse> sectionList = <SectionResponse>[].obs;
  List<SectionResponse> newSectionList = <SectionResponse>[].obs;
  List<Chapters> courseVideoList = <Chapters>[].obs;
  List<AssetData> videoList = <AssetData>[].obs;
  var linearChart = <LinearChartModel>[].obs;

  final Map<String, double> dataMap = {
    "to Start Working": 5,
    "to Start Working1": 1,
    "to Start Working2": 2,
    "to Start Working3": 2,
  };

  @override
  void onInit() {
    linearChart.add(LinearChartModel("1", "hd", 5, 2.5));
    linearChart.add(LinearChartModel("1", "hd", 3, 2));
    linearChart.add(LinearChartModel("1", "hd", 3, 2));
    linearChart.add(LinearChartModel("2", "hd", 7, 2));
    linearChart.add(LinearChartModel("2", "hd", 3.5, 2));
    linearChart.add(LinearChartModel("2", "hd", 2, 2));
    linearChart.add(LinearChartModel("2", "hd", 3.5, 2));
    getAllHomeScreenApi();

    super.onInit();
  }



  Future<void> getAllHomeScreenApi() async {
    callApis();
    getAllCourses();
    getAllStoreItems();
  }

  onInit2(String guID) async {
    isApiCalling.value = true;
    await getAllCourses();
    isApiCalling.value = false;

    final CourseCurriculumController courseCurriculumController =
        Get.put(CourseCurriculumController());

    courseCurriculumController.course.value = courseList.firstWhere(
        (element) => element.guId == guID,
        orElse: () => CourseData());

    if (courseCurriculumController.course.value.guId != null &&
        courseCurriculumController.course.value.guId!.isNotEmpty) {
        courseCurriculumController.guId.value = guID;

      courseCurriculumController.getCourseNewData().then((value) => Get.to(
            CoursesCurriculumScreen(
                data: courseCurriculumController.course.value),



          ));
    } else {
      showToast("Unable to find purchased course!!");
    }
  }

  getAllCourses() async {
    courseList.clear();
    final courses = await getCourses();
    if (courses.data?.isNotEmpty == true) {
      courses.data?.forEach((element) {
        if (element.paymentType != "PRODUCT") {
          courseList.add(element);
        }
      });
    }
    courseList.refresh();
    // isApiCalling.value = false;
  }

  Future<void> getAllStoreItems() async {
    paidCourseCalling.value = true;
    try {
      storeList.clear();
      final courses = await getItemCategory();
      if (courses.isNotEmpty) {
        for (var element in courses) {
          var list = await getStoreItems(element.guId ?? '');
          element.setStoreModel(list);
        }
        storeList.assignAll(courses);
      }
      paidCourseCalling.value = false;
    } catch (e) {
      paidCourseCalling.value = false;
    }
  }

  callApis() async {
    userName.value =
        await AppSharedPreferences.getStringFromLocalStorage(KEY_USER_NAME) ??
            '';
    profilePic.value = await AppSharedPreferences.getStringFromLocalStorage(
            KEY_USER_PROFILE_PIC) ??
        '';
    profileEmail.value =
        await AppSharedPreferences.getStringFromLocalStorage(KEY_USER_EMAIL) ??
            '';
    phoneNumber.value =
        await AppSharedPreferences.getStringFromLocalStorage(KEY_USER_PHONE) ??
            "";

    final timeNow = DateTime.now().hour;
    if (timeNow <= 12) {
      greetingMsg.value = 'Good Morning,\n$userName';
    } else if ((timeNow > 12) && (timeNow < 16)) {
      greetingMsg.value = 'Good Afternoon,\n$userName';
    } else {
      greetingMsg.value = 'Good Evening,\n$userName';
    }
  }
}
