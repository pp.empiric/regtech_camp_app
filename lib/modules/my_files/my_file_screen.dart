import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:regtech_camp_app/common_widget/global_back_drop.dart';
import 'package:regtech_camp_app/common_widget/global_text.dart';
import 'package:regtech_camp_app/common_widget/profile_widget.dart';
import 'package:regtech_camp_app/model/folder_response.dart';
import 'package:regtech_camp_app/resources/app_colors.dart';
import 'package:regtech_camp_app/resources/app_images.dart';

import 'my_file_controller.dart';

class MyFileScreen extends StatelessWidget {
  final controller = Get.put(MyFileController());

  MyFileScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(alignment: Alignment.topCenter, children: [
      ListView(
          children: [
            ProfileWidget(
              title: 'My Files',
              isFolderVisibility: true,
              backVisibility: false,
              folderWidget: controller.addFile.value ||
                      controller.isApiCalling.value
                  ? const SizedBox()
                  : Padding(
                      padding: const EdgeInsets.only(right: 10),
                      child: InkWell(
                          onTap: () async {
                            await controller.showFolderDialog();
                          },
                          child: SvgPicture.asset(AppImages.newFolderImage))),
            ),
            Obx(() => (controller.folderList.isNotEmpty == true)
                ? GridView.builder(
                    gridDelegate:
                        const SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 2,
                            mainAxisSpacing: 17,
                            crossAxisSpacing: 17,
                            childAspectRatio: .82),
                    itemBuilder: (context, index) =>
                        singleFolderUI(controller.folderList[index]),
                    scrollDirection: Axis.vertical,
                    padding: const EdgeInsets.only(
                        bottom: 100, left: 29, right: 29, top: 28),
                    physics: const NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: controller.folderList.length)
                : Container(
                    alignment: Alignment.center,
                    margin: const EdgeInsets.only(top: 200),
                    child: const GlobalText(
                        'You have not added a file into this folder.\nClick on above file icon to add file.',
                        color: Color(0xff717171),
                        fontSize: 15,
                        fontWeight: FontWeight.w400,
                        fontStyle: FontStyle.normal,
                        textAlign: TextAlign.center),
                  ))
          ],
          shrinkWrap: true,
          padding: EdgeInsets.zero,
          physics: const ScrollPhysics(),
          scrollDirection: Axis.vertical),
      Obx(() => controller.isApiCalling.value
          ? const GlobalBackDrop()
          : const SizedBox())
    ]);
  }

  Widget singleFolderUI(ContactAssets folderList) {
    //TODO assign count
    return InkWell(
      onTap: () async {
        controller.selectedFolder.value = folderList;
        await controller.getFolderDetail();
      },
      child: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(
              color: const Color(0x005f2d2c),
              width: 1,
            ),
            borderRadius: BorderRadius.circular(15),
            boxShadow: [
              BoxShadow(
                color: AppColors.primaryDarkColor,
                offset: const Offset(0, 1),
                blurRadius: 11,
                spreadRadius: 0,
              ),
            ],
          ),
          child: Column(crossAxisAlignment: CrossAxisAlignment.end, children: [
            (folderList.pageNo != "0")
                ? Container(
                    width: 24,
                    height: 24,
                    margin: const EdgeInsets.only(right: 6, top: 2),
                    decoration: BoxDecoration(
                        color: AppColors.primaryDarkColor,
                        shape: BoxShape.circle),
                    child: Center(
                        child: GlobalText(
                      folderList.pageNo ?? '',
                      color: Colors.white,
                    )),
                  )
                : const SizedBox(
                    height: 24,
                  ),
            Center(
                child:
                    Image.asset(AppImages.folderIcon, width: 73, height: 63)),
            Expanded(
              child: Container(
                margin: const EdgeInsets.symmetric(horizontal: 5),
                alignment: Alignment.center,
                child: GlobalText(folderList.title ?? '',
                    maxLine: 2,
                    textOverflow: TextOverflow.ellipsis,
                    color: AppColors.primaryDarkColor,
                    fontSize: 15,
                    fontWeight: FontWeight.w400,
                    textAlign: TextAlign.center,
                    fontStyle: FontStyle.normal),
              ),
            ),
            InkWell(
              onTap: () async {
                await controller.deleteFolder(folderList.guId ?? '');
              },
              child: Container(
                  padding: const EdgeInsets.only(right: 11, bottom: 5),
                  child: SvgPicture.asset(AppImages.deleteIcon,
                      height: 22, width: 16.5, fit: BoxFit.fill)),
            )
          ])),
    );
  }
}
