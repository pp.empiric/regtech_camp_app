import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:mime/mime.dart';
import 'package:regtech_camp_app/common_widget/custom_edit_text.dart';
import 'package:regtech_camp_app/common_widget/global_text.dart';
import 'package:regtech_camp_app/model/comments_response.dart';
import 'package:regtech_camp_app/model/courses_curriculum_details_response.dart';
import 'package:regtech_camp_app/model/folder_response.dart';
import 'package:regtech_camp_app/model/radio_model.dart';
import 'package:regtech_camp_app/resources/app_colors.dart';
import 'package:regtech_camp_app/services/apis.dart';

import '../../utils.dart';
import 'my_file_detail.dart';

class MyFileController extends GetxController {
  var isApiCalling = false.obs;
  var selectedFolder = ContactAssets().obs;
  var folderList = <ContactAssets>[].obs;
  var fileList = <Data>[].obs;
  var commentList = <Notes>[].obs;
  var addFile = false.obs;
  var sampleData = <RadioModel>[].obs;
  var messageText = ''.obs;
  var messageLoading = false.obs;
  var messageController = TextEditingController();
  Rx<CoursesCurriculumDetailsResponse> coursesCurriculumDetails =
      CoursesCurriculumDetailsResponse().obs;

  @override
  void onInit() {
    sampleData.add(RadioModel(
        true.obs,
        'File',
        'Upload video, audio, PDFs or any other format',
        () async => await chooseFile()));
    sampleData.add(RadioModel(
        false.obs, 'YouTube / Vimeo URL', 'Use the YouTube / Vimeo Video URL',
        (value) async {
      await addLink(value);
    }));
    getAllFolders();
    super.onInit();
  }

  Future<void> getAllFolders() async {
    final type = await isSafeToCallApi();
    if (type == 0) {
      await showVPNDialog(() async {
        await getAllFolders();
      });
    } else if (type == 1) {
      await showNoInternetDialog(() async {
        await getAllFolders();
      });
    } else {
      isApiCalling.value = true;
      final temp = await getFolders();
      folderList.clear();
      temp.forEach((element) async {
        coursesCurriculumDetails.value =
            await getFolderContents(element.guId ?? '');
        element.setPageNo(
            coursesCurriculumDetails.value.curriculumdata?.length.toString() ??
                '');
        folderList.add(element);
      });
      folderList.assignAll(temp);
      isApiCalling.value = false;
    }
  }

  addFolderWithName(String folderName, String isEdit) async {
    final type = await isSafeToCallApi();
    if (type == 0) {
      await showVPNDialog(() async {
        await getAllFolders();
      });
    } else if (type == 1) {
      await showNoInternetDialog(() async {
        await getAllFolders();
      });
    } else {
      isApiCalling.value = true;
      final temp = isEdit.isNotEmpty
          ? await changeFolderName(
              folderName.isNotEmpty ? folderName : 'New Folder', isEdit)
          : await addFolder(folderName.isNotEmpty ? folderName : 'New Folder');
      isApiCalling.value = false;
      if (temp.guId == null) {
        showSnackBar('Failed to Create Folder', '', Colors.red, Icons.error);
      } else {
        await getAllFolders();
        if (isEdit.isNotEmpty) {
          selectedFolder.value = folderList.firstWhere(
              (element) => element.guId == isEdit,
              orElse: () => selectedFolder.value);
        }
      }
    }
  }

  deleteFolder(String guId) async {
    final type = await isSafeToCallApi();
    if (type == 0) {
      await showVPNDialog(() async {
        await getAllFolders();
      });
    } else if (type == 1) {
      await showNoInternetDialog(() async {
        await getAllFolders();
      });
    } else {
      isApiCalling.value = true;
      await deleteFolderAPI(guId);
      isApiCalling.value = false;
      getAllFolders();
    }
  }

  sendMessage() async {
    final response = await addFolderNote(
        messageText.value, selectedFolder.value.guId.toString());
    if (response == '') {
      showSnackBar('Failed to send Message!!', '', Colors.red, Icons.error);
    } else {
      if (response['guId'] == null) {
        showSnackBar('Failed to send Message!!', '', Colors.red, Icons.error);
      } else {
        messageText.value = '';
        messageController.clear();
        await getFolderNotes();
      }
    }
  }

  @override
  Future<void> dispose() async {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    super.dispose();
  }

  showFolderDialog({ContactAssets? folderName}) async {
    String name = folderName != null ? folderName.title ?? '' : '';
    await Get.dialog(
      Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Material(
              color: Colors.transparent,
              child: Container(
                  height: 185,
                  color: Colors.white,
                  margin:
                      const EdgeInsets.symmetric(horizontal: 54, vertical: 15),
                  child: ListView(
                      shrinkWrap: true,
                      padding: EdgeInsets.zero,
                      physics: const NeverScrollableScrollPhysics(),
                      children: [
                        const SizedBox(height: 22),
                        const Center(
                          child: GlobalText('Add New Folder',
                              color: Color(0xff000000),
                              fontSize: 17,
                              fontWeight: FontWeight.w600,
                              fontStyle: FontStyle.normal),
                        ),
                        const SizedBox(height: 28),
                        SizedBox(
                          width: Get.width,
                          child: CustomEditText(
                            hintText: 'Enter Folder Name',
                            textInputAction: TextInputAction.done,
                            onTextChanged: (value) {
                              name = value;
                            },
                          ),
                        ),
                        const SizedBox(height: 17.5),
                        Container(
                          height: 1,
                          decoration: const BoxDecoration(
                            color: Color(0xffd9d9da),
                          ),
                        ),
                        SizedBox(
                          height: 45,
                          child: Row(children: [
                            Expanded(
                              flex: 1,
                              child: InkWell(
                                  onTap: () {
                                    Get.back();
                                  },
                                  child: Center(
                                    child: GlobalText('Cancel',
                                        color: AppColors.primaryDarkColor,
                                        fontSize: 17,
                                        fontWeight: FontWeight.w600,
                                        fontStyle: FontStyle.normal),
                                  )),
                            ),
                            Container(
                              height: 45,
                              width: 2,
                              decoration: const BoxDecoration(
                                color: Color(0xffd9d9da),
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: InkWell(
                                  onTap: () async {
                                    if (name.isEmpty) {
                                      showSnackBar('Enter Folder Name', '',
                                          Colors.red, Icons.error);
                                      return;
                                    }

                                    Get.back();
                                    await addFolderWithName(
                                        name,
                                        folderName != null &&
                                                folderName.guId != null
                                            ? folderName.guId!
                                            : '');
                                  },
                                  child: Center(
                                    child: GlobalText('OK',
                                        color: AppColors.primaryDarkColor,
                                        fontSize: 17,
                                        fontWeight: FontWeight.w600,
                                        fontStyle: FontStyle.normal),
                                  )),
                            )
                          ]),
                        )
                      ])),
            )
          ]),
      useSafeArea: false,
      barrierDismissible: true,
    );
  }

  getFolderDetail() async {
    final type = await isSafeToCallApi();
    if (type == 0) {
      await showVPNDialog(() async {
        await getAllFolders();
      });
    } else if (type == 1) {
      await showNoInternetDialog(() async {
        await getAllFolders();
      });
    } else {
      isApiCalling.value = true;
      final temp = await getFolderContents(selectedFolder.value.guId ?? '');
      getFolderNotes();
      fileList.clear();
      if (temp.curriculumdata != null && temp.curriculumdata!.isNotEmpty) {
        fileList.assignAll(temp.curriculumdata!);
      }
      isApiCalling.value = false;
      await Get.to(() => MyFileDetail());
    }
  }

  getFolderNotes() async {
    final type = await isSafeToCallApi();
    if (type == 0) {
      await showVPNDialog(() async {
        await getFolderNotes();
      });
    } else if (type == 1) {
      await showNoInternetDialog(() async {
        await getFolderNotes();
      });
    } else {
      messageLoading.value = true;
      commentList.clear();
      final temp = await getFolderNotesAPI(selectedFolder.value.guId ?? '');
      if (temp.notes != null && temp.notes!.isNotEmpty) {
        commentList.addAll(temp.notes!);
      }
      messageLoading.value = false;
    }
  }

  deleteFile(Data folderData) async {
    final type = await isSafeToCallApi();
    if (type == 0) {
      await showVPNDialog(() async {
        await deleteFile(folderData);
      });
    } else if (type == 1) {
      await showNoInternetDialog(() async {
        await deleteFile(folderData);
      });
    } else {
      folderData.isDeleting.value = true;
      await deleteFileApi(folderData.id ?? '');
      folderData.isDeleting.value = false;
      getFolderDetail();
    }
  }

  chooseFile() async {
    final result = await FilePicker.platform.pickFiles(
      type: FileType.custom,
      allowMultiple: false,
      allowedExtensions: [
        'jpg',
        'pdf',
        'doc',
        'docx',
        'mp4',
        'avi',
        'mkv',
        '3gp',
        'mov',
        'mp3',
        'wav',
        'm4a',
        'png',
        'ppt',
        'pptx'
      ],
    );

    if (result != null) {
      await uploadFileFunction(result);
    } else {
      showSnackBar('Please select file to upload', '', Colors.red, Icons.error);
      return;
    }
  }

  uploadFileFunction(FilePickerResult result) async {
    final type = await isSafeToCallApi();
    if (type == 0) {
      await showVPNDialog(() async {
        await uploadFileFunction(result);
      });
    } else if (type == 1) {
      await showNoInternetDialog(() async {
        await uploadFileFunction(result);
      });
    } else {
      isApiCalling.value = true;

      try {
        final data = await getS3Url(result.files.first.path!.split('/').last,
            lookupMimeType(result.files.first.path!.split('/').last)!);
        if (data == '') {
          isApiCalling.value = false;
          showSnackBar(
              'something went wrong on uploading file please try again!',
              '',
              Colors.red,
              Icons.error);
          return;
        } else {
          await uploadFile(
              File(result.files.first.path!),
              lookupMimeType(result.files.first.path!.split('/').last)!,
              data['signedUrl']);
          final response = await fileUpload(
              data['signedUrl'],
              lookupMimeType(result.files.first.path!.split('/').last)!,
              selectedFolder.value.guId ?? '',
              data['isVideo'] ?? false,
              result.files.first.path!.split('/').last);
          if (response == '') {
            isApiCalling.value = false;
            showSnackBar(
                'something went wrong on uploading file please try again!',
                '',
                Colors.red,
                Icons.error);
            return;
          } else {
            if (response['status'] == false) {
              isApiCalling.value = false;
              showSnackBar(
                  'existing record found with same fileName, folderName and chapterId',
                  '',
                  Colors.red,
                  Icons.error);
              return;
            } else {
              isApiCalling.value = false;
              addFile.value = false;
              await getFolderDetail();
            }
          }
        }
      } catch (e) {
        print(e.toString());
        isApiCalling.value = false;
        showSnackBar('something went wrong on uploading file please try again!',
            '', Colors.red, Icons.error);
      }
    }
  }

  addLink(String url) async {
    final type = await isSafeToCallApi();
    if (type == 0) {
      await showVPNDialog(() async {
        await addLink(url);
      });
    } else if (type == 1) {
      await showNoInternetDialog(() async {
        await addLink(url);
      });
    } else {
      isApiCalling.value = true;
      final data =
          await addFolderContents(selectedFolder.value.guId ?? '', url);

      if (data == '') {
        isApiCalling.value = false;
        showSnackBar('something went wrong on uploading file please try again!',
            '', Colors.red, Icons.error);
        return;
      } else {
        isApiCalling.value = false;
        addFile.value = false;
        await getFolderDetail();
      }
    }
  }
}
