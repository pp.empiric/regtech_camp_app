import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:regtech_camp_app/common_widget/PlayVimeoScreen.dart';
import 'package:regtech_camp_app/common_widget/global_back_drop.dart';
import 'package:regtech_camp_app/common_widget/global_icon_loader.dart';
import 'package:regtech_camp_app/common_widget/global_text.dart';
import 'package:regtech_camp_app/common_widget/pdf_viewer_page.dart';
import 'package:regtech_camp_app/common_widget/play_audio_screen.dart';
import 'package:regtech_camp_app/common_widget/play_video_screen.dart';
import 'package:regtech_camp_app/common_widget/play_youtube_screen.dart';
import 'package:regtech_camp_app/common_widget/profile_widget.dart';
import 'package:regtech_camp_app/common_widget/radio_item.dart';
import 'package:regtech_camp_app/model/courses_curriculum_details_response.dart';
import 'package:regtech_camp_app/resources/app_colors.dart';
import 'package:regtech_camp_app/resources/app_images.dart';
import 'package:regtech_camp_app/resources/youtubevideovalidator/youtube_video_validator.dart';

import '../../utils.dart';
import 'my_file_controller.dart';

class MyFileDetail extends GetView<MyFileController> {
  @override
  final controller = Get.put(MyFileController());

  MyFileDetail({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(children: [
        ListView(
            children: [
              Obx(() => ProfileWidget(
                    title: controller.addFile.value
                        ? 'Add File'
                        : controller.selectedFolder.value.title ?? '',
                    isFolderVisibility: true,
                    folderWidget: controller.addFile.value ||
                            controller.isApiCalling.value
                        ? const SizedBox()
                        : Padding(
                            padding: const EdgeInsets.only(right: 10),
                            child: InkWell(
                                onTap: () async {
                                  controller.addFile.value = true;
                                },
                                child:
                                    SvgPicture.asset(AppImages.newFileImage)),
                          ),
                    onTap: () {
                      if (controller.addFile.value) {
                        controller.addFile.value = false;
                      } else {
                        Get.back();
                      }
                    },
                  )),
              const SizedBox(height: 17),
              Container(
                  margin: const EdgeInsets.only(left: 14, right: 10),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Expanded(
                                child: GlobalText(
                                    controller.selectedFolder.value.title ?? '',
                                    color: AppColors.primaryDarkColor,
                                    fontSize: 23,
                                    fontWeight: FontWeight.w700,
                                    fontStyle: FontStyle.normal),
                              ),
                              InkWell(
                                  onTap: () async {
                                    await controller.showFolderDialog(
                                        folderName:
                                            controller.selectedFolder.value);
                                  },
                                  child: GlobalText(
                                    'Edit',
                                    color: AppColors.primaryDarkColor,
                                    fontSize: 16,
                                    fontWeight: FontWeight.w400,
                                    fontStyle: FontStyle.normal,
                                    textDecoration: TextDecoration.underline,
                                  ))
                            ]),
                        Obx(() => controller.addFile.value
                            ? Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                    const SizedBox(height: 29),
                                    GlobalText('Add file or link',
                                        color: AppColors.primaryDarkColor,
                                        fontSize: 18,
                                        fontWeight: FontWeight.w700,
                                        fontStyle: FontStyle.normal),
                                    const SizedBox(height: 36),
                                    RadioItem(controller.sampleData, true),
                                    RadioItem(controller.sampleData, false)
                                  ])
                            : controller.fileList.isEmpty
                                ? Container(
                                    margin: const EdgeInsets.only(top: 14),
                                    child: const GlobalText(
                                        'You have not added a file into this folder.\nClick on above file icon to add file.',
                                        color: Color(0xff717171),
                                        fontSize: 15,
                                        fontWeight: FontWeight.w400,
                                        fontStyle: FontStyle.normal))
                                : Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      ListView.builder(
                                        itemBuilder: (context, index) =>
                                            buildItem(
                                                controller.fileList[index]),
                                        padding: const EdgeInsets.only(
                                            left: 15, right: 15, top: 20),
                                        physics:
                                            const NeverScrollableScrollPhysics(),
                                        shrinkWrap: true,
                                        itemCount: controller.fileList.length,
                                      ),
                                      Obx(() => controller.messageLoading.value
                                          ? Container(
                                              alignment: Alignment.center,
                                              margin:
                                                  const EdgeInsets.symmetric(
                                                      vertical: 25),
                                              child:
                                                  const CircularProgressIndicator())
                                          : controller.commentList.isEmpty
                                              ? const SizedBox()
                                              : commentWidget()),
                                      GlobalText('Send Comment',
                                          color: AppColors.primaryDarkColor,
                                          fontSize: 16,
                                          fontWeight: FontWeight.w700,
                                          fontStyle: FontStyle.normal),
                                      const SizedBox(height: 8),
                                      Row(children: [
                                        Expanded(
                                          child: Container(
                                              height: 49,
                                              decoration: BoxDecoration(
                                                border: Border.all(
                                                  color: Colors.grey,
                                                  width: 2,
                                                ),
                                                borderRadius:
                                                    BorderRadius.circular(15),
                                              ),
                                              alignment: Alignment.center,
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                      horizontal: 15),
                                              child: TextField(
                                                onChanged: (value) {
                                                  controller.messageText.value =
                                                      value;
                                                },
                                                controller: controller
                                                    .messageController,
                                                style: TextStyle(
                                                    fontFamily:
                                                        GoogleFonts.lato()
                                                            .fontFamily,
                                                    color: Colors.black,
                                                    fontSize: 14,
                                                    fontWeight: FontWeight.w400,
                                                    fontStyle:
                                                        FontStyle.normal),
                                                decoration: const InputDecoration(
                                                    border: InputBorder.none,
                                                    enabledBorder:
                                                        InputBorder.none,
                                                    disabledBorder:
                                                        InputBorder.none,
                                                    focusedBorder:
                                                        InputBorder.none,
                                                    prefixIcon: null,
                                                    hintStyle: TextStyle(
                                                        color:
                                                            Color(0xff929292),
                                                        fontSize: 14,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontStyle:
                                                            FontStyle.normal),
                                                    hintText:
                                                        'Type your message here'),
                                                keyboardType: TextInputType.url,
                                              )),
                                        ),
                                        const SizedBox(width: 7),
                                        Container(
                                            width: 50,
                                            height: 50,
                                            decoration: BoxDecoration(
                                                shape: BoxShape.circle,
                                                color:
                                                    AppColors.primaryDarkColor),
                                            child: Material(
                                                color: Colors.transparent,
                                                child: InkWell(
                                                    onTap: () async {
                                                      if (controller.messageText
                                                          .value.isEmpty) {
                                                        showSnackBar(
                                                            'Enter Message!!',
                                                            '',
                                                            Colors.red,
                                                            Icons.error);
                                                        return;
                                                      }

                                                      await controller
                                                          .sendMessage();
                                                    },
                                                    child: Container(
                                                      width: 50,
                                                      height: 50,
                                                      alignment:
                                                          Alignment.center,
                                                      decoration: BoxDecoration(
                                                          shape:
                                                              BoxShape.circle,
                                                          color: AppColors
                                                              .primaryDarkColor),
                                                      child: Image.asset(
                                                        AppImages
                                                            .forwardArrowIcon,
                                                        height: 50,
                                                        width: 50,
                                                      ),
                                                    ))))
                                      ]),
                                      const SizedBox(height: 100)
                                    ],
                                  ))
                      ]))
            ],
            shrinkWrap: true,
            padding: EdgeInsets.zero,
            physics: const ScrollPhysics(),
            scrollDirection: Axis.vertical),
        Obx(() => controller.isApiCalling.value
            ? const GlobalBackDrop()
            : const SizedBox())
      ]),
    );
  }

  Widget buildItem(Data folderData) {
    if (folderData.status == 'DELETED') {
      return const SizedBox();
    }

    if (folderData.fileType == 'file') {
      if (folderData.fileName == null || folderData.fileName!.isEmpty) {
        folderData.fileName = folderData.s3url!.split('/').last;
      }
      if (folderData.fileName!.contains('.mp3') ||
          folderData.fileName!.contains('.wav')) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            PlayAudioScreen(folderData.s3url!, folderData.fileName!),
            Obx(() => IconButton(
                icon: folderData.isDeleting.value
                    ? const GlobalIconLoader()
                    : const Icon(Icons.delete, color: Colors.red),
                onPressed: folderData.isDeleting.value
                    ? null
                    : () async {
                        await controller.deleteFile(folderData);
                      })),
            const SizedBox(height: 10)
          ],
        );
      } else if (folderData.fileName!.contains('.png') ||
          folderData.fileName!.contains('.jpg') ||
          folderData.fileName!.contains('.jpeg')) {
        return Column(
          children: [
            GestureDetector(
              onTap: () {
                showImageDialog(folderData.s3url!);
              },
              child: FadeInImage(
                  imageErrorBuilder: (context, object, trace) {
                    return Image.asset('assets/images/course_placeholder.png');
                  },
                  placeholder: AssetImage(AppImages.coursePlaceHolder),
                  image: NetworkImage('${folderData.s3url}')),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Obx(() => IconButton(
                      icon: folderData.isDownloading.value
                          ? const GlobalIconLoader()
                          : Icon(Icons.download_rounded,
                              color: AppColors.primaryDarkColor),
                      onPressed: folderData.isDownloading.value
                          ? null
                          : () async {
                              downloadSelectedImage(folderData);
                            },
                    )),
                Obx(() => IconButton(
                      icon: folderData.isDeleting.value
                          ? const GlobalIconLoader()
                          : const Icon(Icons.delete_rounded, color: Colors.red),
                      onPressed: folderData.isDeleting.value
                          ? null
                          : () async {
                              await controller.deleteFile(folderData);
                            },
                    ))
              ],
            ),
            const SizedBox(height: 10)
          ],
        );
      } else if (folderData.fileName!.contains('.pdf')) {
        return InkWell(
          onTap: () async {
            await Get.to(() => PdfViewerPage(folderData.s3url ?? ''));
          },
          child: Column(
            children: [
              Row(
                // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                mainAxisSize: MainAxisSize.max,
                children: [
                  SvgPicture.asset(
                    'assets/images/curriculum_pdf.svg',
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 10),
                      child: GlobalText(
                        '${folderData.fileName}',
                        maxLine: 2,
                        color: AppColors.primaryDarkColor,
                        textOverflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ),
                  Obx(() => IconButton(
                        icon: folderData.isDownloading.value
                            ? const GlobalIconLoader()
                            : Icon(Icons.download_rounded,
                                color: AppColors.primaryDarkColor),
                        onPressed: folderData.isDownloading.value
                            ? null
                            : () {
                                downloadSelectedPdf(folderData);
                              },
                      )),
                  Obx(() => IconButton(
                        icon: folderData.isDeleting.value
                            ? const GlobalIconLoader()
                            : const Icon(Icons.delete_rounded,
                                color: Colors.red),
                        onPressed: folderData.isDeleting.value
                            ? null
                            : () async {
                                await controller.deleteFile(folderData);
                              },
                      ))
                ],
              ),
              const SizedBox(height: 10)
            ],
          ),
        );
      } else if (folderData.fileName!.contains('.doc') ||
          folderData.fileName!.contains('.docx') ||
          folderData.fileName!.contains('.ppt') ||
          folderData.fileName!.contains('.pptx')) {
        return InkWell(
            onTap: () async {
              downloadSelectedDocx(folderData);
            },
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(
                        left: 8.0,
                      ),
                      child: SvgPicture.asset(
                        AppImages.otherDownloadIcon,
                        color: AppColors.primaryDarkColor,
                      ),
                    ),
                    SizedBox(
                      width: Get.width / 1.5,
                      child: GlobalText(
                        folderData.fileName ?? '',
                        maxLine: 2,
                        color: const Color(0XFF0088FF),
                        paddingLeft: 10.0,
                      ),
                    ),
                    Obx(() => IconButton(
                          icon: folderData.isDeleting.value
                              ? const GlobalIconLoader()
                              : const Icon(Icons.delete_rounded,
                                  color: Colors.red),
                          onPressed: folderData.isDeleting.value
                              ? null
                              : () async {
                                  await controller.deleteFile(folderData);
                                },
                        ))
                  ],
                ),
                const SizedBox(height: 10)
              ],
            ));
      } else if (folderData.isVideo == true) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            PlayVideoScreen(folderData.s3url!),
            Obx(() => IconButton(
                icon: folderData.isDeleting.value
                    ? const GlobalIconLoader()
                    : const Icon(
                        Icons.delete,
                        color: Colors.red,
                      ),
                onPressed: folderData.isDeleting.value
                    ? null
                    : () async {
                        await controller.deleteFile(folderData);
                      })),
            const SizedBox(height: 10)
          ],
        );
      } else {
        return const SizedBox();
      }
    } else if (folderData.fileType == 'video') {
      if (folderData.fileName == null || folderData.fileName!.isEmpty) {
        folderData.fileName = folderData.s3url!.split('/').last;
      }
      return Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          PlayVideoScreen(folderData.s3url!),
          Obx(() => IconButton(
              icon: folderData.isDeleting.value
                  ? const GlobalIconLoader()
                  : const Icon(
                      Icons.delete,
                      color: Colors.red,
                    ),
              onPressed: folderData.isDeleting.value
                  ? null
                  : () async {
                      await controller.deleteFile(folderData);
                    })),
          const SizedBox(height: 10)
        ],
      );
    } else if (folderData.fileType == 'link') {
      if (folderData.fileName == null || folderData.fileName!.isEmpty) {
        return const SizedBox();
      }
      if (YoutubeVideoValidator.validateUrl(folderData.fileName!)) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            PlayYouTubeScreen(folderData.fileName!),
            Obx(() => IconButton(
                icon: folderData.isDeleting.value
                    ? const GlobalIconLoader()
                    : const Icon(
                        Icons.delete,
                        color: Colors.red,
                      ),
                onPressed: folderData.isDeleting.value
                    ? null
                    : () async {
                        await controller.deleteFile(folderData);
                      })),
            const SizedBox(height: 10)
          ],
        );
      } else {
        if (folderData.fileName!.contains('vimeo') &&
            !folderData.fileName!.contains('ref=em-share')) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              PlayVimeoScreen(folderData.fileName!),
              Obx(() => IconButton(
                  icon: folderData.isDeleting.value
                      ? const GlobalIconLoader()
                      : const Icon(Icons.delete, color: Colors.red),
                  onPressed: folderData.isDeleting.value
                      ? null
                      : () async {
                          await controller.deleteFile(folderData);
                        })),
              const SizedBox(height: 10)
            ],
          );
        } else {
          return const SizedBox();
        }
      }
    } else if (folderData.fileType == 'text') {
      return Column(
        children: [
          GlobalText(
            parseHtmlString(folderData.notes ?? ''),
            maxLine: 500,
          ),
          const SizedBox(height: 10)
        ],
      );
    } else if (folderData.fileType == 'document') {
      return Column(
        children: [
          GlobalText(
            parseHtmlString(folderData.notes ?? ''),
            maxLine: 500,
          ),
          const SizedBox(height: 10)
        ],
      );
    } else {
      return const SizedBox();
    }
  }

  Widget commentWidget() {
    final dateList = controller.commentList
        .map((element) => element.dateCreated!)
        .map((e) => e.split('T').first)
        .toSet()
        .toList();

    return ListView.builder(
      itemBuilder: (context, index) {
        final finalList = controller.commentList
            .where((p0) =>
                p0.dateCreated != null &&
                p0.dateCreated!.contains(dateList[index].split('T').first))
            .toList();
        final date = DateTime.parse(dateList[index]);
        return Column(children: [
          GlobalText(DateFormat('dd MMMM yyyy').format(date),
              color: AppColors.primaryDarkColor,
              fontSize: 15,
              fontWeight: FontWeight.w700,
              fontStyle: FontStyle.normal),
          ListView.builder(
              itemBuilder: (context, index2) => Container(
                  margin: const EdgeInsets.only(bottom: 15),
                  child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                            width: 44,
                            height: 44,
                            decoration:
                                const BoxDecoration(shape: BoxShape.circle),
                            child: Image.network(
                                finalList[index2].contact != null &&
                                        finalList[index2].contact!.imageUrl !=
                                            null
                                    ? finalList[index2]
                                        .contact!
                                        .imageUrl
                                        .toString()
                                    : '',
                                loadingBuilder:
                                    (context, child, loadingProgress) =>
                                        Image.asset(AppImages.coursePlaceHolder),
                                errorBuilder: (context, error, stackTrace) =>
                                    Image.asset(AppImages.coursePlaceHolder))),
                        const SizedBox(width: 13),
                        Expanded(
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                    width: double.infinity,
                                    decoration: BoxDecoration(
                                        color: const Color(0xfff4f3f3),
                                        borderRadius:
                                            BorderRadius.circular(15)),
                                    padding: const EdgeInsets.only(
                                        top: 10,
                                        bottom: 10,
                                        left: 17,
                                        right: 17),
                                    child: GlobalText(
                                        finalList[index2].note ?? '',
                                        color: const Color(0xff717171),
                                        fontSize: 15,
                                        textOverflow: TextOverflow.ellipsis,
                                        maxLine: 50,
                                        fontWeight: FontWeight.w400,
                                        fontStyle: FontStyle.normal)),
                                const SizedBox(height: 10),
                                GlobalText(
                                    DateFormat('hh:mm a').format(DateTime.parse(
                                        finalList[index2].dateCreated!)),
                                    color: const Color(0xff717171),
                                    fontSize: 13,
                                    fontWeight: FontWeight.w400,
                                    fontStyle: FontStyle.normal)
                              ]),
                        )
                      ])),
              itemCount: finalList.length,
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              scrollDirection: Axis.vertical,
              padding: const EdgeInsets.only(top: 9, bottom: 13)),
        ]);
      },
      itemCount: dateList.length,
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      scrollDirection: Axis.vertical,
      padding: EdgeInsets.zero,
    );
  }
}
