import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_countdown_timer/current_remaining_time.dart';
import 'package:flutter_countdown_timer/flutter_countdown_timer.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:octo_image/octo_image.dart';
import 'package:regtech_camp_app/common_widget/global_back_drop.dart';
import 'package:regtech_camp_app/common_widget/global_button.dart';
import 'package:regtech_camp_app/common_widget/global_text.dart';
import 'package:regtech_camp_app/common_widget/play_audio_screen.dart';
import 'package:regtech_camp_app/common_widget/play_video_screen.dart';
import 'package:regtech_camp_app/common_widget/profile_widget.dart';
import 'package:regtech_camp_app/modules/couse_details/controllers/course_curriculum_controller.dart';
import 'package:regtech_camp_app/modules/home/controllers/home_controller.dart';
import 'package:regtech_camp_app/modules/quiz/quiz_controller.dart';
import 'package:regtech_camp_app/resources/app_colors.dart';
import 'package:regtech_camp_app/resources/app_constants.dart';
import 'package:regtech_camp_app/resources/app_images.dart';

import '../../utils.dart';

class QuizScreen extends GetView<QuizController> {
  @override
  final controller = Get.put(QuizController());
  final courseController = Get.put(CourseCurriculumController());

  QuizScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(() => Scaffold(
          body: Stack(children: [
            WillPopScope(
              onWillPop: () async {
                if (!controller.isLoading.value &&
                    !controller.showAnswer.value &&
                    !controller.showResult.value) {
                  await Get.dialog(
                      Dialog(child: controller.confirmationDialog(false)));
                  return false;
                } else {
                  if (controller.showAnswer.value) {
                    controller.showAnswer.value = false;
                    return false;
                  } else {
                    Get.back();
                    return true;
                  }
                }
              },
              child: controller.showAnswer.value
                  ? controller.answerScreen()
                  : controller.showResult.value
                      ? controller.resultScreen()
                      : (controller.quiz.value.data?.questions?.isEmpty ==
                                  true ||
                              controller.quiz.value.data?.questions == null)
                          ? Column(
                              children: [
                                ProfileWidget(
                                  title: "Quiz",
                                  onTap: () async {
                                    if (!controller.isLoading.value &&
                                        !controller.showAnswer.value &&
                                        !controller.showResult.value) {
                                      await Get.dialog(Dialog(
                                          child: controller
                                              .confirmationDialog(false)));
                                    } else {
                                      if (controller.showAnswer.value) {
                                        controller.showAnswer.value = false;
                                      } else {
                                        Get.back();
                                      }
                                    }
                                  },
                                ),
                                const Expanded(
                                    child: Center(
                                  child: GlobalText("Question not available"),
                                ))
                              ],
                            )
                          : ListView(
                              padding: EdgeInsets.zero,
                              shrinkWrap: true,
                              scrollDirection: Axis.vertical,
                              physics: const ScrollPhysics(),
                              children: [
                                  Obx(() => ProfileWidget(
                                        title:
                                            "Question ${controller.currentIndex.value + 1}  / ${controller.quiz.value.data?.questions?.length}",
                                        onTap: () async {
                                          if (!controller.isLoading.value &&
                                              !controller.showAnswer.value &&
                                              !controller.showResult.value) {
                                            await Get.dialog(Dialog(
                                                child: controller
                                                    .confirmationDialog(
                                                        false)));
                                          } else {
                                            if (controller.showAnswer.value) {
                                              controller.showAnswer.value =
                                                  false;
                                            } else {
                                              Get.back();
                                            }
                                          }
                                        },
                                      )),
                                  Container(height: 20),
                                  if (controller.quiz.value.data?.timeAlloted !=
                                      null)
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          right: 15, bottom: 10),
                                      child: Align(
                                        alignment: Alignment.centerRight,
                                        child: CountdownTimer(
                                          controller:
                                              controller.controller.value,
                                          widgetBuilder: (context,
                                              CurrentRemainingTime? time) {
                                            if (time == null) {
                                              return const GlobalText(
                                                'Time Over',
                                                color: Colors.red,
                                                fontSize: 16,
                                              );
                                            }
                                            var remainingTime = Duration(
                                                    days: time.days ?? 0,
                                                    hours: time.hours ?? 0,
                                                    minutes: time.min ?? 0,
                                                    seconds: time.sec ?? 0)
                                                .inMilliseconds;
                                            var totalTime = Duration(
                                                    minutes: controller
                                                            .quiz
                                                            .value
                                                            .data
                                                            ?.timeAlloted ??
                                                        0)
                                                .inMilliseconds;
                                            var difference =
                                                totalTime - remainingTime;
                                            var finalTime = Duration(
                                                milliseconds: difference);
                                            controller.quizTime.value =
                                                "${finalTime.inMinutes} Min :${(finalTime.inSeconds.remainder(60))} Sec";

                                            return GlobalText(
                                              '${time.min ?? 00} min,  ${time.sec ?? 00} sec',
                                              color:
                                                  AppColors.secondaryDartColor,
                                              fontSize: 16,
                                              fontWeight: FontWeight.w700,
                                            );
                                          },
                                        ),
                                      ),
                                    ),
                                  Container(
                                      margin: const EdgeInsets.symmetric(
                                          horizontal: 25),
                                      child: HtmlWidget(
                                          controller
                                                  .quiz
                                                  .value
                                                  .data
                                                  ?.questions?[controller
                                                      .currentIndex.value]
                                                  .name ??
                                              "",
                                          textStyle: GoogleFonts.poppins(
                                              color: AppColors.primaryDarkColor,
                                              fontSize: 18,
                                              fontWeight: FontWeight.w700,
                                              fontStyle: FontStyle.normal))),
                                  Container(
                                      height: controller
                                                  .quiz
                                                  .value
                                                  .data
                                                  ?.questions?[controller
                                                      .currentIndex.value]
                                                  .imageUrl !=
                                              null
                                          ? 15
                                          : 25),
                                  controller
                                              .quiz
                                              .value
                                              .data
                                              ?.questions?[
                                                  controller.currentIndex.value]
                                              .imageUrl !=
                                          null
                                      ? imageWidget(context)
                                      : const SizedBox(),
                                  Container(
                                      height: controller
                                                  .quiz
                                                  .value
                                                  .data
                                                  ?.questions?[controller
                                                      .currentIndex.value]
                                                  .assetId !=
                                              null
                                          ? 15
                                          : 25),
                                  controller
                                              .quiz
                                              .value
                                              .data
                                              ?.questions?[
                                                  controller.currentIndex.value]
                                              .assetId !=
                                          null
                                      ? audioWidget(context)
                                      : const SizedBox(),
                                  Container(
                                      height: controller
                                                  .quiz
                                                  .value
                                                  .data
                                                  ?.questions?[controller
                                                      .currentIndex.value]
                                                  .assetId !=
                                              null
                                          ? 10
                                          : 0),
                                  Container(
                                      margin: const EdgeInsets.symmetric(
                                          horizontal: 28),
                                      child: controller.singleChoiceGroup(
                                          controller.currentIndex.value)),
                                  Container(
                                      height: controller
                                              .showAnswerButtonClicked.value
                                          ? 15
                                          : 0),
                                  if (controller.data.value.isMandatory ==
                                          true &&
                                      controller.data.value.isShowExplanation ==
                                          true)
                                    controller.showAnswerButtonClicked.value
                                        ? controller
                                                        .quiz
                                                        .value
                                                        .data
                                                        ?.questions![controller
                                                            .currentIndex.value]
                                                        .parentExplanation !=
                                                    null &&
                                                controller
                                                    .quiz
                                                    .value
                                                    .data
                                                    ?.questions![controller
                                                        .currentIndex.value]
                                                    .parentExplanation
                                                    .isNotEmpty
                                            ? Container(
                                                margin:
                                                    const EdgeInsets.symmetric(
                                                        horizontal: 28),
                                                child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.start,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      SizedBox(
                                                          width: 17,
                                                          height: 17,
                                                          child: Icon(
                                                              CupertinoIcons
                                                                  .info_circle_fill,
                                                              color: AppColors
                                                                  .primaryDarkColor,
                                                              size: 22)),
                                                      Container(width: 13),
                                                      Expanded(
                                                          child: Column(
                                                              crossAxisAlignment:
                                                                  CrossAxisAlignment
                                                                      .start,
                                                              mainAxisSize:
                                                                  MainAxisSize
                                                                      .max,
                                                              children: [
                                                            Text("Explanation",
                                                                style: GoogleFonts.poppins(
                                                                    color: AppColors
                                                                        .primaryDarkColor,
                                                                    fontSize:
                                                                        14,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w400,
                                                                    fontStyle:
                                                                        FontStyle
                                                                            .normal)),
                                                            HtmlWidget(
                                                                controller
                                                                    .quiz
                                                                    .value
                                                                    .data
                                                                    ?.questions![
                                                                        controller
                                                                            .currentIndex
                                                                            .value]
                                                                    .parentExplanation!,
                                                                textStyle: GoogleFonts.poppins(
                                                                    color: const Color(0xff717171)
                                                                        .withOpacity(
                                                                            .9),
                                                                    fontSize:
                                                                        14,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w400,
                                                                    fontStyle:
                                                                        FontStyle
                                                                            .normal))
                                                          ]))
                                                    ]))
                                            : Container()
                                        : Container(),
                                  Container(height: 39),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      controller.currentIndex.value != 0
                                          ? GlobalButton(
                                              title: "Previous",
                                              onTap: () async {
                                                if (controller.data.value
                                                        .isMandatory ==
                                                    true) {
                                                  controller
                                                      .showAnswerButtonClicked
                                                      .value = true;
                                                } else {
                                                  controller
                                                      .showAnswerButtonClicked
                                                      .value = false;
                                                }

                                                controller.currentIndex.value =
                                                    controller.currentIndex
                                                            .value -
                                                        1;
                                              },
                                            )
                                          : const SizedBox(),
                                      SizedBox(
                                          width:
                                              controller.currentIndex.value != 0
                                                  ? 10
                                                  : 0),
                                      GlobalButton(
                                        title: controller
                                                .showAnswerButtonClicked.value
                                            ? controller.quiz.value.data?.questions![controller.currentIndex.value].id ==
                                                    controller.quiz.value.data
                                                        ?.questions?.last.id!
                                                ? "Submit"
                                                : "Next"
                                            : Get.find<HomeController>()
                                                            .quizSingleResponse
                                                            .value
                                                            .isShowAnswerAfterEachQuestion ==
                                                        false &&
                                                    controller.data.value.isMandatory ==
                                                        false
                                                ? controller
                                                            .quiz
                                                            .value
                                                            .data
                                                            ?.questions![controller
                                                                .currentIndex
                                                                .value]
                                                            .id ==
                                                        controller
                                                            .quiz
                                                            .value
                                                            .data
                                                            ?.questions
                                                            ?.last
                                                            .id!
                                                    ? "Submit"
                                                    : "Next"
                                                : "Confirm",
                                        onTap: () async {
                                          if (controller
                                              .showAnswerButtonClicked.value) {
                                            if (controller
                                                    .quiz
                                                    .value
                                                    .data
                                                    ?.questions?[controller
                                                        .currentIndex.value]
                                                    .id ==
                                                controller.quiz.value.data
                                                    ?.questions?.last.id) {
                                              await Get.dialog(Dialog(
                                                  child: controller
                                                      .confirmationDialog(
                                                          true)));
                                            } else {
                                              controller.showAnswerButtonClicked
                                                  .value = false;
                                              if (controller
                                                      .currentIndex.value !=
                                                  controller.quiz.value.data!
                                                          .questions!.length -
                                                      1) {
                                                controller.currentIndex.value =
                                                    controller.currentIndex
                                                            .value +
                                                        1;
                                              }
                                            }
                                          } else {
                                            if (controller.data.value.isMandatory != null &&
                                                controller.data.value
                                                        .isMandatory ==
                                                    true &&
                                                (controller
                                                            .quiz
                                                            .value
                                                            .data
                                                            ?.questions?[
                                                                controller
                                                                    .currentIndex
                                                                    .value]
                                                            .isAttempted ==
                                                        null ||
                                                    controller
                                                            .quiz
                                                            .value
                                                            .data
                                                            ?.questions?[
                                                                controller
                                                                    .currentIndex
                                                                    .value]
                                                            .isAttempted! ==
                                                        false)) {
                                              showToast("Select an answer");
                                              return;
                                            }

                                            if (controller.data.value.isMandatory != null &&
                                                controller.data.value
                                                        .isMandatory ==
                                                    true &&
                                                Get.find<HomeController>()
                                                        .quizSingleResponse
                                                        .value
                                                        .isShowExplanation ==
                                                    true) {
                                              controller.showAnswerButtonClicked
                                                  .value = true;
                                              return;
                                            }

                                            if (Get.find<HomeController>()
                                                    .quizSingleResponse
                                                    .value
                                                    .isShowAnswerAfterEachQuestion ==
                                                true) {
                                              controller.showAnswerButtonClicked
                                                  .value = true;
                                              return;
                                            }

                                            if (controller
                                                    .quiz
                                                    .value
                                                    .data
                                                    ?.questions?[controller
                                                        .currentIndex.value]
                                                    .id ==
                                                controller.quiz.value.data
                                                    ?.questions?.last.id) {
                                              await Get.dialog(Dialog(
                                                  child: controller
                                                      .confirmationDialog(
                                                          true)));
                                            } else {
                                              controller.showAnswerButtonClicked
                                                  .value = false;
                                              if (controller
                                                      .currentIndex.value !=
                                                  controller.quiz.value.data!
                                                          .questions!.length -
                                                      1) {
                                                controller.currentIndex.value =
                                                    controller.currentIndex
                                                            .value +
                                                        1;
                                              }
                                            }
                                          }
                                        },
                                      )
                                    ],
                                  ),
                                  const SizedBox(height: 52)
                                ]),
            ),
            controller.isLoading.value
                ? const GlobalBackDrop()
                : const SizedBox()
          ]),
        ));
  }

  Widget imageWidget(BuildContext context) {
    if (controller.quiz.value.data?.questions?[controller.currentIndex.value]
                .imageUrl !=
            null &&
        controller.quiz.value.data?.questions?[controller.currentIndex.value]
            .imageUrl!.isNotEmpty) {
      var image =
          "$COURSE_IMAGE_BASE${controller.quiz.value.data?.questions?[controller.currentIndex.value].imageUrl}";
      return Container(
          margin: const EdgeInsets.symmetric(horizontal: 25),
          child: ClipRRect(
              borderRadius: BorderRadius.circular(15),
              child: OctoImage(
                  image: NetworkImage(image),
                  height: 250,
                  fit: BoxFit.fill,
                  width: double.infinity,
                  placeholderBuilder: (context) => Image.asset(
                      AppImages.coursePlaceHolder,
                      fit: BoxFit.fill,
                      height: 250,
                      width: double.infinity),
                  errorBuilder: (context, error, stackTrace) => Image.asset(
                      AppImages.coursePlaceHolder,
                      fit: BoxFit.fill,
                      height: 250,
                      width: double.infinity))));
    } else {
      return const SizedBox();
    }
  }

  Widget audioWidget(BuildContext context) {
    if (controller.quiz.value.data?.questions![controller.currentIndex.value]
            .curriculamData !=
        null) {
      var curriculumdata = controller.quiz.value.data
          ?.questions![controller.currentIndex.value].curriculamData;
      if (curriculumdata?.status == "DELETED") {
        return const SizedBox();
      }

      if (curriculumdata?.isVideo == true) {
        return Column(children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15),
            child: PlayVideoScreen(curriculumdata?.s3url ?? ''),
          )
        ]);
      }

      if (curriculumdata?.fileType != "file") {
        return const SizedBox();
      }

      if (curriculumdata?.fileName != null &&
          curriculumdata!.fileName!.contains("mp3")) {
        return PlayAudioScreen(
            curriculumdata.s3url ?? "", curriculumdata.fileName ?? '');
      } else if (curriculumdata?.fileName != null &&
              curriculumdata!.fileName!.contains("mp4") ||
          curriculumdata!.fileName!.contains("avi") ||
          curriculumdata.fileName!.contains("mk4") ||
          curriculumdata.fileName!.contains("3gp") ||
          curriculumdata.fileName!.contains("mov") ||
          curriculumdata.fileName!.contains("m3u8")) {
        return Column(children: [
          Container(
              height: 51,
              width: 136,
              decoration: BoxDecoration(
                  color: AppColors.primaryDarkColor,
                  borderRadius: BorderRadius.circular(25)),
              child: MaterialButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25)),
                  onPressed: () {
                    Get.to(() => PlayVideoScreen(curriculumdata.s3url ?? ''));
                  },
                  padding: EdgeInsets.zero,
                  child: Center(
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                        Text("Play",
                            style: GoogleFonts.mPlusRounded1c(
                                color: Colors.white,
                                fontSize: 16,
                                fontWeight: FontWeight.w900,
                                fontStyle: FontStyle.normal)),
                        Container(width: 10),
                        const Icon(CupertinoIcons.play_fill,
                            color: Colors.white),
                      ]))))
        ]);
      } else if (curriculumdata.fileName != null &&
              curriculumdata.fileName!.contains(".png") ||
          curriculumdata.fileName!.contains(".jpg") ||
          curriculumdata.fileName!.contains(".jpeg")) {
        return Container(
            margin: const EdgeInsets.symmetric(horizontal: 25),
            child: ClipRRect(
                borderRadius: BorderRadius.circular(15),
                child: OctoImage(
                    image: NetworkImage(curriculumdata.s3url ?? ""),
                    height: 250,
                    fit: BoxFit.fill,
                    width: double.infinity,
                    placeholderBuilder: (context) => Image.asset(
                        AppImages.coursePlaceHolder,
                        fit: BoxFit.fill,
                        height: 250,
                        width: double.infinity),
                    errorBuilder: (context, error, stackTrace) => Image.asset(
                        AppImages.coursePlaceHolder,
                        fit: BoxFit.fill,
                        height: 250,
                        width: double.infinity))));
      } else {
        return const SizedBox();
      }
    } else {
      return const SizedBox();
    }
  }
}
