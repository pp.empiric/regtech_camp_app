import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:regtech_camp_app/common_widget/global_button.dart';
import 'package:regtech_camp_app/common_widget/global_text.dart';
import 'package:regtech_camp_app/common_widget/profile_widget.dart';
import 'package:regtech_camp_app/model/quiz_new_response.dart';
import 'package:regtech_camp_app/modules/couse_details/controllers/course_curriculum_controller.dart';
import 'package:regtech_camp_app/modules/quiz/quiz_screen.dart';
import 'package:regtech_camp_app/resources/app_colors.dart';
import 'package:regtech_camp_app/resources/app_constants.dart';
import 'package:regtech_camp_app/resources/app_images.dart';

import '../../utils.dart';

class AssetScreen extends StatelessWidget {
  final QuizNewResponse data;
  final QuizNewData quiz;

  const AssetScreen(this.quiz, this.data, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          const ProfileWidget(
            title: "Quiz",
          ),
          data != null && quiz.additionalInfo?.isNotEmpty == true
              ? GlobalText(
                  quiz.additionalInfo ?? "",
                  color: AppColors.primaryDarkColor,
                  fontSize: 23,
                  fontWeight: FontWeight.w700,
                  fontStyle: FontStyle.normal,
                )
              : const SizedBox(),
          const SizedBox(
            height: 10,
          ),
          quiz.instructions != null && quiz.instructions?.isNotEmpty == true
              ? Container(
                  margin: const EdgeInsets.only(
                      left: 10, right: 10, top: 10, bottom: 10),
                  child: HtmlWidget(quiz.instructions ?? "",
                      textStyle: GoogleFonts.poppins(
                          color: AppColors.primaryDarkColor,
                          fontWeight: FontWeight.w600,
                          fontSize: 16)))
              : Container(),
          quiz.paragraph != null && quiz.paragraph?.isNotEmpty == true
              ? Container(
                  margin: const EdgeInsets.only(left: 10, right: 10, top: 10),
                  child: HtmlWidget(quiz.paragraph ?? "",
                      textStyle: GoogleFonts.poppins(
                          color: AppColors.secondaryDartColor,
                          fontWeight: FontWeight.w600,
                          fontSize: 16)))
              : Container(),
          quiz.imageUrl != null && quiz.imageUrl?.isNotEmpty == true
              ? Container(
                  height: 209,
                  margin: const EdgeInsets.only(left: 10, right: 10),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(25),
                      color: Colors.red),
                  child: ClipRRect(
                      child: CachedNetworkImage(
                        imageUrl: '$QUIZ_IMAGE_BASE${quiz.imageUrl}',
                        placeholder: (context, url) => Image.asset(
                            AppImages.coursePlaceHolder,
                            fit: BoxFit.fill),
                        errorWidget: (context, url, error) => Image.asset(
                            AppImages.coursePlaceHolder,
                            fit: BoxFit.fill),
                        height: 166,
                        width: Get.width,
                        fit: BoxFit.fill,
                      ),
                      borderRadius: BorderRadius.circular(25)))
              : const SizedBox(),
          (data.assetData?.isEmpty == false)
              ? Padding(
                  padding: const EdgeInsets.only(left: 10, right: 10),
                  child: ListView.builder(
                    itemBuilder: (context, index) =>
                        buildItem(data.assetData?[index] ?? AssetData()),
                    padding: const EdgeInsets.only(top: 20),
                    physics: const NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: data.assetData?.length,
                  ),
                )
              : const SizedBox(height: 50),
          // Container(
          //         alignment: Alignment.center,
          //         margin: const EdgeInsets.symmetric(vertical: 30),
          //         child: const GlobalText('No Assets Found!!',
          //             color: Color(0xff717171),
          //             fontSize: 15,
          //             fontWeight: FontWeight.w400,
          //             fontStyle: FontStyle.normal)),
          Align(
              child: GlobalButton(
                  title: "Start",
                  onTap: () async {
                    Get.off(() => QuizScreen(), arguments: null);
                  })),
          const SizedBox(height: 20,)
        ],
      ),
    );
  }
}
