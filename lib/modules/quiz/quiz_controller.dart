import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_countdown_timer/index.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';
import 'package:regtech_camp_app/common_widget/global_button.dart';
import 'package:regtech_camp_app/common_widget/global_text.dart';
import 'package:regtech_camp_app/common_widget/play_audio_screen.dart';
import 'package:regtech_camp_app/common_widget/play_video_screen.dart';
import 'package:regtech_camp_app/common_widget/play_youtube_screen.dart';
import 'package:regtech_camp_app/common_widget/profile_widget.dart';
import 'package:regtech_camp_app/model/quiz_new_response.dart';
import 'package:regtech_camp_app/model/quiz_result_request_body.dart';
import 'package:regtech_camp_app/model/quiz_result_response.dart';
import 'package:regtech_camp_app/modules/couse_details/controllers/course_curriculum_controller.dart';
import 'package:regtech_camp_app/modules/home/controllers/home_controller.dart';
import 'package:regtech_camp_app/resources/app_colors.dart';
import 'package:regtech_camp_app/resources/app_constants.dart';
import 'package:regtech_camp_app/resources/app_images.dart';
import 'package:regtech_camp_app/resources/clients.dart';
import 'package:regtech_camp_app/resources/youtubevideovalidator/youtube_video_validator.dart';
import 'package:regtech_camp_app/services/apis.dart';
import 'package:regtech_camp_app/services/app_preference.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../utils.dart';

class QuizController extends GetxController {
  var isLoading = false.obs;
  var answers = <Questions>[].obs;
  var userData = QuizResultResponse().obs;
  var showResult = false.obs;
  var currentIndex = 0.obs;
  var quiz = quizTest.obs;
  var data = dataTest.obs;
  var showAnswerButtonClicked = false.obs;
  var quizResultBody = QuizResultResponse().obs;
  var showAnswer = false.obs;
  var quizTime = "".obs;

  Rx<CountdownTimerController> controller =
      CountdownTimerController(endTime: DateTime.now().millisecondsSinceEpoch)
          .obs;

  @override
  void onInit() {
    getData();
    int endTime = DateTime.now().millisecondsSinceEpoch +
        Duration(minutes: quiz.value.data?.timeAlloted ?? 0).inMilliseconds;
    controller.value = CountdownTimerController(
        endTime: endTime,
        onEnd: () {
          calculateResult();
        });
    answers.clear();
    answers.addAll(quiz.value.data?.questions ?? []);
    super.onInit();
  }

  @override
  void dispose() {
    controller.value.dispose();
    super.dispose();
  }

  Future<void> getData() async {
    if (quiz.value.data?.questions != null &&
        quiz.value.data?.questions?.isNotEmpty == true) {
      for (var element in quiz.value.data!.questions!)  {
        if (element.assetId != null && element.assetId!.isNotEmpty) {
          String accessToken = await getAccessToken();

          String url = '$BASE/edcourse/assets/${element.assetId}';

          final response = await http.get(Uri.parse(url), headers: {
            "Authorization": "Bearer $accessToken",
          });

          if (response.statusCode == 200 || response.statusCode == 201) {
            AssetData value = AssetData.fromJson(jsonDecode(response.body));
            if (value.id != null) {
              element.setCurriculum(value);
              quiz.update((val) {});
            }
          }
        }
      }
    }
  }

  Future<void> afterResult(QuizResultResponse userData2) async {
    userData.value = userData2;
    showResult.value = true;
  }

  Future<void> calculateResult() async {
    isLoading.value = true;
    String accessToken = await getAccessToken();
    String id = await AppSharedPreferences.getStringFromLocalStorage(KEY_GUID);

    var quizResult = QuizResultRequestBody(
        id: quiz.value.data?.id,
        contact: id,
        orgId: ORG_ID,
        course: quiz.value.data?.course,
        chapter: quiz.value.data?.chapter,
        section: quiz.value.data?.section,
        duration: quiz.value.data?.timeAlloted,
        quiz: quiz.value.data?.quiz,
        startTime: quiz.value.data?.startTime,
        paperQuestionList: answers.toList());

    try {
      var body = jsonEncode(quizResult.toJson());

      String url = "$BASE/edtest/paper/${quiz.value.data?.id}";

      var response = await http.put(Uri.parse(url),
          body: body.toString(),
          headers: {
            "Authorization": "Bearer $accessToken",
            "Content-Type": "application/json"
          });

      if (jsonDecode(response.body)['data'] != null) {
        controller.value.dispose();
        QuizResultResponse userData =
            QuizResultResponse.fromJson(jsonDecode(response.body));

        await Get.find<HomeController>().getAllCourses();
        await Get.find<CourseCurriculumController>().getCourseNewData();
        await afterResult(userData);
        isLoading.value = false;
      } else {
        showToast(response.body);
        isLoading.value = false;
      }
    } catch (e) {
      print(e.toString());
      showToast("Something went wrong");
      isLoading.value = false;
    }
  }

  Widget singleChoiceGroup(int value) {
    return ListView.builder(
      itemBuilder: (context, index) => InkWell(
          onTap: () {
            if (showAnswerButtonClicked.value) {
              return;
            }
            var temp = quiz.value.data?.questions?[value].paperQuestionChoices
                ?.where((element) => element.selectedAnswer == true);
            if (temp?.isNotEmpty == true) {
              var find = quiz.value.data?.questions?[value].paperQuestionChoices
                  ?.firstWhere((element) => element.selectedAnswer == true,
                      orElse: () => PaperQuestionChoices());
              if (find ==
                  quiz.value.data?.questions?[value]
                      .paperQuestionChoices?[index]) {
                quiz.value.data?.questions?[value].paperQuestionChoices
                    ?.forEach((element) {
                  element.setAnswer(false);
                });
                quiz.value.data?.questions?[value].setAttempted(false);
                quiz.refresh();
                return;
              }
            }

            quiz.value.data?.questions?[value].setAttempted(true);
            quiz.value.data?.questions?[value].paperQuestionChoices
                ?.forEach((element) {
              element.setAnswer(false);
            });

            quiz.value.data?.questions?[value].paperQuestionChoices?[index]
                .setAnswer(true);
            quiz.refresh();
            if (answers.isNotEmpty) {
              var contain = answers.any((element) =>
                  element.id == quiz.value.data!.questions![value].id);
              if (contain) {
                answers.removeWhere((element) =>
                    element.id == quiz.value.data?.questions?[value].id);
                answers.add(quiz.value.data!.questions![value]);
                answers.refresh();
              } else {
                answers.add(quiz.value.data!.questions![value]);
                answers.refresh();
              }
            } else {
              answers.add(quiz.value.data!.questions![value]);
              answers.refresh();
            }
          },
          splashColor: Colors.transparent,
          highlightColor: Colors.transparent,
          child: Container(
              margin: const EdgeInsets.only(bottom: 23),
              decoration: BoxDecoration(
                color: showAnswerButtonClicked.value
                    ? quiz.value.data?.questions![value]
                                    .paperQuestionChoices![index].isAnswer !=
                                null &&
                            quiz.value.data?.questions![value]
                                    .paperQuestionChoices![index].isAnswer ==
                                true
                        ? const Color(0xffDEFCE7)
                        : quiz
                                    .value
                                    .data!
                                    .questions![value]
                                    .paperQuestionChoices![index]
                                    .selectedAnswer ==
                                true
                            ? const Color(0xffFFF2F2)
                            : Colors.white
                    : quiz.value.data?.questions?[value]
                                .paperQuestionChoices?[index].selectedAnswer ==
                            true
                        ? Colors.white
                        : Colors.white,
                borderRadius: BorderRadius.circular(15),
                border: Border.all(
                  color: showAnswerButtonClicked.value
                      ? quiz.value.data?.questions![value]
                                      .paperQuestionChoices![index].isAnswer !=
                                  null &&
                              quiz.value.data?.questions![value]
                                      .paperQuestionChoices![index].isAnswer ==
                                  true
                          ? const Color(0xff29BE53)
                          : quiz
                                      .value
                                      .data!
                                      .questions![value]
                                      .paperQuestionChoices![index]
                                      .selectedAnswer ==
                                  true
                              ? const Color(0xffEC2525)
                              : Colors.transparent
                      : quiz
                                  .value
                                  .data
                                  ?.questions?[value]
                                  .paperQuestionChoices?[index]
                                  .selectedAnswer ==
                              true
                          ? AppColors.primaryDarkColor
                          : Colors.transparent,
                ),
                boxShadow: const [
                  BoxShadow(
                    color: Colors.grey,
                    offset: Offset(0, 0),
                    blurRadius: 5,
                    spreadRadius: 0,
                  ),
                ],
              ),
              child: ConstrainedBox(
                  constraints: const BoxConstraints(minHeight: 65),
                  child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Expanded(
                                    child: Container(
                                        margin: const EdgeInsets.only(
                                            top: 10, bottom: 10, right: 10),
                                        child: GlobalText(
                                            quiz
                                                    .value
                                                    .data
                                                    ?.questions?[value]
                                                    .paperQuestionChoices?[
                                                        index]
                                                    .choice ??
                                                '',
                                            maxLine: 30,
                                            textAlign: TextAlign.start,
                                            textOverflow: TextOverflow.ellipsis,
                                            textStyle: GoogleFonts.poppins(
                                                color: showAnswerButtonClicked
                                                        .value
                                                    ? quiz.value.data?.questions![value].paperQuestionChoices![index].isAnswer !=
                                                                null &&
                                                            quiz
                                                                    .value
                                                                    .data
                                                                    ?.questions![
                                                                        value]
                                                                    .paperQuestionChoices![
                                                                        index]
                                                                    .isAnswer ==
                                                                true
                                                        ? const Color(
                                                            0xff29BE53)
                                                        : quiz
                                                                    .value
                                                                    .data!
                                                                    .questions![
                                                                        value]
                                                                    .paperQuestionChoices![index]
                                                                    .selectedAnswer ==
                                                                true
                                                            ? const Color(0xffEC2525)
                                                            : AppColors.primaryDarkColor
                                                    : quiz.value.data?.questions?[value].paperQuestionChoices?[index].selectedAnswer == true
                                                        ? AppColors.primaryDarkColor
                                                        : AppColors.primaryDarkColor,
                                                fontSize: 16,
                                                fontWeight: FontWeight.w500,
                                                fontStyle: FontStyle.normal)))),
                                showAnswerButtonClicked.value
                                    ? quiz
                                                    .value
                                                    .data
                                                    ?.questions![value]
                                                    .paperQuestionChoices![
                                                        index]
                                                    .isAnswer !=
                                                null &&
                                            quiz
                                                    .value
                                                    .data!
                                                    .questions![value]
                                                    .paperQuestionChoices![
                                                        index]
                                                    .isAnswer ==
                                                true
                                        ? Container(
                                            width: 26,
                                            height: 26,
                                            decoration: const BoxDecoration(
                                                shape: BoxShape.circle,
                                                color: Color(0xff29BE53)),
                                            child: const Icon(Icons.check,
                                                color: Colors.white))
                                        : const SizedBox(
                                            width: 26,
                                            height: 26,
                                          )
                                    : const SizedBox(
                                        width: 26,
                                        height: 26,
                                      )
                              ]),
                          if (quiz.value.data?.questions?[value]
                                      .paperQuestionChoices?[index].imageUrl !=
                                  "" &&
                              quiz
                                      .value
                                      .data
                                      ?.questions?[value]
                                      .paperQuestionChoices?[index]
                                      .imageUrl
                                      ?.isNotEmpty ==
                                  true)
                            Container(
                                height: 166,
                                width: 316,
                                margin: const EdgeInsets.all(13),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(15.0),
                                ),
                                child: ClipRRect(
                                  borderRadius: const BorderRadius.all(
                                      Radius.circular(15)),
                                  child: CachedNetworkImage(
                                      imageUrl:
                                          '$IMAGE_BASE${quiz.value.data?.questions?[value].paperQuestionChoices?[index].imageUrl ?? ''}',
                                      placeholder: (context, url) =>
                                          Image.asset(
                                              AppImages.coursePlaceHolder,
                                              fit: BoxFit.fill),
                                      errorWidget: (context, url, error) =>
                                          Image.asset(
                                              AppImages.coursePlaceHolder,
                                              fit: BoxFit.fill),
                                      height: 166,
                                      fit: BoxFit.fill),
                                ))
                        ],
                      ))))),
      physics: const NeverScrollableScrollPhysics(),
      scrollDirection: Axis.vertical,
      shrinkWrap: true,
      padding: EdgeInsets.zero,
      itemCount:
          quiz.value.data?.questions?[value].paperQuestionChoices?.length,
    );
  }

  confirmationDialog(bool fromSubmit) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        const SizedBox(
          height: 8.0,
        ),
        Padding(
          padding: const EdgeInsets.all(10),
          child: GlobalText(
            fromSubmit
                ? "Are you sure you want to submit test?"
                : "Are you sure you want to cancel?",
            textStyle: const TextStyle(
              fontSize: 16.0,
              fontWeight: FontWeight.w500,
            ),
          ),
        ),
        const SizedBox(
          height: 20.0,
        ),
        Row(
          children: [
            const Spacer(),
            InkWell(
              onTap: () {
                Get.back();
              },
              child: SizedBox(
                height: 40.0,
                width: fromSubmit ? 80.0 : 50.0,
                child: Center(
                  child: GlobalText(fromSubmit ? "Cancel" : "No",
                      color: AppColors.primaryDarkColor,
                      fontSize: 16.0,
                      fontWeight: FontWeight.w500),
                ),
              ),
            ),
            const SizedBox(
              width: 16.0,
            ),
            InkWell(
              onTap: () async {
                if (fromSubmit) {
                  Get.back();
                  await calculateResult();
                } else {
                  Get.back();
                  Get.back();
                }
              },
              child: SizedBox(
                height: 40.0,
                width: fromSubmit ? 80.0 : 50.0,
                child: Center(
                  child: GlobalText(
                    fromSubmit ? "Submit" : "Yes",
                    fontSize: 16.0,
                    fontWeight: FontWeight.w500,
                    color: AppColors.primaryDarkColor,
                  ),
                ),
              ),
            ),
            const SizedBox(
              width: 16.0,
            ),
          ],
        )
      ],
    );
  }

  Widget resultScreen() {
    return Center(
      child: ListView(children: [
        ProfileWidget(title: "Result"),
        data.value.displayResults == true
            ? userData.value.data?.isPass == false
                ? Lottie.asset(
                    'assets/json/failed.json',
                    width: 200,
                    height: 200,
                  )
                : SizedBox(
                    height: 250,
                    child: Image.asset(AppImages.quizCongressImage))
            : const SizedBox(),
        data.value.displayResults == true
            ? Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  GlobalText(
                    userData.value.data?.isPass == true
                        ? "Hurray! Congratulation"
                        : "FAIL",
                    color: userData.value.data?.isPass == true
                        ? AppColors.primaryDarkColor
                        : Colors.red,
                    fontWeight: FontWeight.bold,
                    maxLine: 10,
                    paddingLeft: 20,
                    paddingRight: 20,
                    paddingTop: 20,
                    fontSize: 18,
                  ),
                  GlobalText(
                    quizTime.value.isNotEmpty
                        ? "You have taken ${quizTime.value} to complete this test"
                        : "Your Test is completed!",
                    maxLine: 10,
                    paddingLeft: 30,
                    paddingRight: 30,
                    paddingTop: 10,
                    fontSize: 18,
                    textAlign: TextAlign.center,
                    color: AppColors.secondaryDartColor,
                  ),
                  GlobalText(
                    "You Score",
                    maxLine: 10,
                    paddingLeft: 20,
                    paddingRight: 20,
                    paddingTop: 20,
                    fontSize: 18,
                    fontWeight: FontWeight.w900,
                    color: AppColors.secondaryDartColor,
                  ),
                  GlobalText(
                    "${userData.value.data?.correct}/${userData.value.data?.totalQuestions} (${userData.value.data?.grade != null && userData.value.data?.grade?.isNotEmpty == true ? "${userData.value.data?.grade} Grade" : "${userData.value.data?.percentage} %"})",
                    maxLine: 10,
                    paddingLeft: 20,
                    paddingRight: 20,
                    paddingTop: 0,
                    fontSize: 20,
                    color: AppColors.primaryDarkColor,
                  ),
                ],
              )
            : Center(
                child: GlobalText(
                  quizTime.value.isNotEmpty
                      ? "You have taken ${quizTime.value} to complete this test"
                      : "Your Test is completed!",
                  maxLine: 10,
                  paddingLeft: 30,
                  paddingRight: 30,
                  paddingTop: 10,
                  fontSize: 18,
                  textAlign: TextAlign.center,
                  color: AppColors.secondaryDartColor,
                ),
              ),
        const SizedBox(
          height: 15.0,
        ),
        userData.value.data?.isPass != null &&
                userData.value.data?.isPass == true &&
                data.value.isGenerateCertificate != null &&
                data.value.isGenerateCertificate == true
            ? Container(
                margin: const EdgeInsets.symmetric(horizontal: 60),
                child: RaisedButton(
                  focusColor: AppColors.primaryDarkColor,
                  onPressed: () async {
                    await callCertificate();
                  },
                  hoverColor: AppColors.primaryDarkColor,
                  highlightColor: AppColors.primaryDarkColor,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0),
                      side: BorderSide(color: AppColors.primaryDarkColor)),
                  child: const GlobalText(
                    "Download Certificate",
                    color: Colors.white,
                  ),
                  color: AppColors.primaryDarkColor,
                ),
              )
            : const SizedBox(),
        SizedBox(
          height: data.value.isGenerateCertificate != null &&
                  data.value.isGenerateCertificate == true
              ? 10
              : 0,
        ),
        userData.value.data?.showAnswersInResult != null &&
                userData.value.data?.showAnswersInResult == true
            ? Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                    userData.value.data?.showAnswersInResult == true
                        ? GlobalButton(
                            title: 'Answers',
                            width: 150,
                            margin: const EdgeInsets.only(
                                left: 39, right: 40, bottom: 20),
                            onTap: () {
                              showAnswer.value = true;
                            },
                          )
                        : const SizedBox(),
                    GlobalButton(
                      title: 'Close',
                      width: 150,
                      margin: const EdgeInsets.only(
                          left: 39, right: 40, bottom: 40),
                      onTap: () {
                        Get.back();
                      },
                      bgColor: AppColors.primaryDarkColor,
                    )
                  ])
            : GlobalButton(
                title: 'Close',
                width: 150,
                margin: const EdgeInsets.only(left: 39, right: 40, bottom: 40),
                onTap: () {
                  Get.back();
                },
                bgColor: AppColors.primaryDarkColor,
              ),
      ]),
    );
  }

  Future<void> callCertificate() async {
    print(userData);
    isLoading.value = true;
    String uName =
        await AppSharedPreferences.getStringFromLocalStorage(KEY_USER_NAME);

    var map = <String, dynamic>{};
    map['name'] = uName;
    map['result'] = userData.value.data?.id;
    map['paper'] = userData.value.data?.paper;
    map['courseName'] =
        Get.find<CourseCurriculumController>().course.value.title;
    map['date'] = DateFormat('MM-dd-yyyy').format(DateTime.now());
    map['papername'] = quiz.value.data?.name ?? "Quiz";

    var questions = await callCertificateAPI(jsonEncode(map));

    isLoading.value = false;

    if (questions['data'] == null) {
      showToast("Something went wrong!!");
    } else {
      if (questions['data']['url'] == null) {
        showToast("Something went wrong!!");
      } else {
        var canLaunchUrl = await canLaunch(questions['data']['url']);
        if (canLaunchUrl) {
          await launch(questions['data']['url'].toString());
        }
      }
    }
  }

  Widget answerScreen() {
    return Column(children: [
      ProfileWidget(
        title: userData.value.data?.courseName,
      ),
      Align(
        alignment: Alignment.topLeft,
        child: Padding(
          padding: const EdgeInsets.only(left: 15, top: 10, bottom: 5),
          child: GlobalText(
            "Test Analysis",
            textAlign: TextAlign.left,
            fontSize: 25,
            fontWeight: FontWeight.w700,
            color: AppColors.primaryDarkColor,
          ),
        ),
      ),
      Expanded(
          child: ListView.builder(
              itemBuilder: (context, index) => Container(
                  margin: const EdgeInsets.only(bottom: 10),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 5),
                    child: Card(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        shadowColor: AppColors.primaryDarkColor,
                        elevation: 3,
                        child: Stack(children: [
                          // Container(
                          //     margin: const EdgeInsets.all(15),
                          //     child: GlobalText(
                          //       "Q.${index + 1}",
                          //       fontSize: 18,
                          //       fontWeight: FontWeight.bold,
                          //       color: AppColors.primaryDarkColor,
                          //     )),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                          margin: const EdgeInsets.only(
                                              left: 13, right: 5, top: 13),
                                          child: GlobalText(
                                            "Q.${index + 1}",
                                            fontSize: 18,
                                            fontWeight: FontWeight.bold,
                                            color: AppColors.primaryDarkColor,
                                          )),
                                      Expanded(
                                        child: Padding(
                                          padding: const EdgeInsets.symmetric(
                                              vertical: 13, horizontal: 5),
                                          child: HtmlWidget(
                                            quiz.value.data?.questions?[index]
                                                    .name ??
                                                '',
                                            textStyle: TextStyle(
                                              fontSize: 16,
                                              color: AppColors.primaryDarkColor,
                                            ),
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                  const SizedBox(height: 10),
                                  Center(
                                    child: quiz.value.data?.questions?[index]
                                                .imageUrl !=
                                            null
                                        ? CachedNetworkImage(
                                            imageUrl:
                                                "$IMAGE_BASE${quiz.value.data?.questions?[index].imageUrl}",
                                            errorWidget:
                                                (context, error, stackTrace) =>
                                                    Container(),
                                            width: Get.width - 100,
                                            height: 250,
                                            fit: BoxFit.fill,
                                          )
                                        : const SizedBox(),
                                  )
                                ],
                              ),
                              data.value.showAllChoiceInResult == true
                                  ? resultChoice(index)
                                  : resultChoice5(index),
                              SizedBox(
                                  height: data.value.isShowExplanation == true
                                      ? 10
                                      : 0),
                              data.value.isShowExplanation == true
                                  ? userData.value.data?.questions != null &&
                                          userData
                                                  .value
                                                  .data!
                                                  .questions![index]
                                                  .parentExplanation
                                                  ?.isNotEmpty ==
                                              true
                                      ? Container(
                                          margin: const EdgeInsets.symmetric(
                                              horizontal: 28),
                                          child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                SizedBox(
                                                    width: 17,
                                                    height: 17,
                                                    child: Icon(
                                                        CupertinoIcons
                                                            .info_circle_fill,
                                                        color: AppColors
                                                            .primaryDarkColor,
                                                        size: 22)),
                                                Container(width: 13),
                                                Expanded(
                                                    child: Column(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        mainAxisSize:
                                                            MainAxisSize.max,
                                                        children: [
                                                      Text("Explanation",
                                                          style: GoogleFonts.poppins(
                                                              color: AppColors
                                                                  .primaryDarkColor,
                                                              fontSize: 14,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w400,
                                                              fontStyle:
                                                                  FontStyle
                                                                      .normal)),
                                                      HtmlWidget(
                                                          userData
                                                                  .value
                                                                  .data
                                                                  ?.questions?[
                                                                      index]
                                                                  .parentExplanation ??
                                                              '',
                                                          textStyle: GoogleFonts
                                                              .poppins(
                                                                  color: const Color(
                                                                          0xff717171)
                                                                      .withOpacity(
                                                                          .9),
                                                                  fontSize: 14,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w400,
                                                                  fontStyle:
                                                                      FontStyle
                                                                          .normal))
                                                    ]))
                                              ]))
                                      : Container()
                                  : Container(),
                              SizedBox(
                                  height: data.value.isShowExplanation == true
                                      ? 10
                                      : 0),
                              SizedBox(
                                  height: data.value.isShowExplanation == true
                                      ? 10
                                      : 0),
                            ],
                          ),
                        ])),
                  )),
              padding: EdgeInsets.zero,
              physics: const ScrollPhysics(),
              shrinkWrap: true,
              itemCount: quiz.value.data?.questions?.length)),
    ]);
  }

  Widget resultChoice(int value) {
    return ListView.builder(
        padding: EdgeInsets.zero,
        itemCount:
            quiz.value.data?.questions?[value].paperQuestionChoices?.length,
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        itemBuilder: (context, index) {
          Questions? item = userData.value.data?.questions?.firstWhere(
              (element) => element.id == quiz.value.data?.questions?[value].id,
              orElse: () {
            return userData.value.data!.questions![value];
          });

          var qitem = quiz.value.data?.questions?.firstWhere(
              (element) => element.id == quiz.value.data?.questions?[value].id,
              orElse: () {
            return userData.value.data!.questions![value];
          });

          return Card(
              margin: const EdgeInsets.only(left: 20, right: 20, bottom: 15),
              shape: RoundedRectangleBorder(
                side: BorderSide(
                    width: 1,
                    color: item?.paperQuestionChoices?[index].isAnswer !=
                                null &&
                            item?.paperQuestionChoices?[index].isAnswer == true
                        ? const Color(0xff29BE53)
                        : qitem?.paperQuestionChoices?[index].selectedAnswer !=
                                    null &&
                                qitem?.paperQuestionChoices?[index]
                                        .selectedAnswer ==
                                    true
                            ? const Color(0xffEC2525)
                            : Colors.grey),
                borderRadius: BorderRadius.circular(10),
              ),
              color: item?.paperQuestionChoices?[index].isAnswer ?? false
                  ? const Color(0xffDEFCE7)
                  : qitem?.paperQuestionChoices?[index].selectedAnswer !=
                              null &&
                          qitem?.paperQuestionChoices?[index].selectedAnswer ==
                              true
                      ? const Color(0xffFFF2F2)
                      : Colors.white,
              shadowColor: Colors.grey,
              elevation: 5,
              child: Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 10, vertical: 15),
                  child: Column(children: [
                    Row(
                      children: [
                        // Container(
                        //     decoration: BoxDecoration(
                        //         border: Border.all(
                        //             color: item?.paperQuestionChoices?[index]
                        //                             .isAnswer !=
                        //                         null &&
                        //                     item?.paperQuestionChoices?[index]
                        //                             .isAnswer ==
                        //                         true
                        //                 ? AppColors.primaryDarkColor
                        //                 : Colors.grey),
                        //         borderRadius: BorderRadius.circular(5)),
                        //     child: Padding(
                        //         padding: const EdgeInsets.symmetric(
                        //             horizontal: 5, vertical: 2),
                        //         child: Text(
                        //           (index + 1).toString(),
                        //           maxLines: 50,
                        //           softWrap: true,
                        //           textAlign: TextAlign.start,
                        //           style: TextStyle(
                        //               color: item?.paperQuestionChoices?[index]
                        //                               .isAnswer !=
                        //                           null &&
                        //                       item?.paperQuestionChoices?[index]
                        //                               .isAnswer ==
                        //                           true
                        //                   ? AppColors.primaryDarkColor
                        //                   : null),
                        //         ))),
                        // const SizedBox(
                        //   width: 10,
                        // ),
                        item?.paperQuestionChoices?[index].choice != null &&
                                item?.paperQuestionChoices?[index].choice
                                        ?.isNotEmpty ==
                                    true
                            ? Expanded(
                                child: GlobalText(
                                  item?.paperQuestionChoices?[index].choice ??
                                      '',
                                  maxLine: 50,
                                  color: item?.paperQuestionChoices?[index]
                                                  .isAnswer !=
                                              null &&
                                          item?.paperQuestionChoices?[index]
                                                  .isAnswer ==
                                              true
                                      ? const Color(0xff29BE53)
                                      : qitem?.paperQuestionChoices?[index]
                                                      .selectedAnswer !=
                                                  null &&
                                              qitem
                                                      ?.paperQuestionChoices?[
                                                          index]
                                                      .selectedAnswer ==
                                                  true
                                          ? const Color(0xffEC2525)
                                          : AppColors.primaryDarkColor,
                                  textAlign: TextAlign.start,
                                ),
                              )
                            : Container(),
                        SizedBox(
                            width: item?.paperQuestionChoices?[index].choice !=
                                        null &&
                                    item?.paperQuestionChoices?[index].choice
                                            ?.isNotEmpty ==
                                        true
                                ? 10
                                : 0),
                        item?.paperQuestionChoices?[index].isAnswer ?? false
                            ? Container(
                                margin: const EdgeInsets.only(left: 2),
                                width: 20,
                                height: 20,
                                child: const Icon(
                                  Icons.check,
                                  size: 15,
                                  color: Color(0xff29BE53),
                                ),
                                decoration: const BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Colors.white),
                              )
                            : const SizedBox()
                        // qitem?.paperQuestionChoices?[index]
                        //                     .selectedAnswer !=
                        //                 null &&
                        //             qitem?.paperQuestionChoices?[index]
                        //                     .selectedAnswer ==
                        //                 true
                        //         ? Container(
                        //             margin: EdgeInsets.only(left: 2),
                        //             width: 20,
                        //             height: 20,
                        //             child: const Icon(
                        //               Icons.close,
                        //               size: 15,
                        //               color: Color(0xffEC2525),
                        //             ),
                        //             decoration: BoxDecoration(
                        //               shape: BoxShape.circle,
                        //               color: Colors.white,
                        //             ),
                        //           )
                        //         : Container()
                      ],
                    ),
                    SizedBox(
                        height:
                            item?.paperQuestionChoices?[index].imageUrl != null
                                ? item?.paperQuestionChoices![index].imageUrl
                                            ?.isNotEmpty ==
                                        true
                                    ? 5
                                    : 0
                                : 0),
                    item?.paperQuestionChoices?[index].imageUrl != null
                        ? item?.paperQuestionChoices![index].imageUrl
                                    ?.isNotEmpty ==
                                true
                            ? Image.network(
                                "$IMAGE_BASE/${item?.paperQuestionChoices?[index].imageUrl}",
                                width: item?.paperQuestionChoices?[index]
                                                .choice !=
                                            null &&
                                        item?.paperQuestionChoices?[index]
                                                .choice?.isNotEmpty ==
                                            true
                                    ? 200
                                    : MediaQuery.of(context).size.width - 100,
                                height: 150,
                              )
                            : const SizedBox()
                        : const SizedBox(),
                    SizedBox(
                        height:
                            item?.paperQuestionChoices?[index].imageUrl != null
                                ? item?.paperQuestionChoices![index].imageUrl
                                            ?.isNotEmpty ==
                                        true
                                    ? 5
                                    : 0
                                : 0)
                  ])));
        });
  }

  Widget resultChoice5(int value) {
    return ListView.builder(
        padding: EdgeInsets.zero,
        itemCount:
            quiz.value.data?.questions?[value].paperQuestionChoices?.length,
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        itemBuilder: (context, index) {
          var item = userData.value.data?.questions?.firstWhere(
              (element) => element.id == quiz.value.data?.questions?[value].id,
              orElse: () {
            return userData.value.data!.questions![value];
          });
          return item?.paperQuestionChoices?[index].isAnswer != null &&
                  item?.paperQuestionChoices?[index].isAnswer == true
              ? Card(
                  margin:
                      const EdgeInsets.only(left: 25, right: 25, bottom: 10),
                  shape: RoundedRectangleBorder(
                    side:
                        BorderSide(width: 1, color: AppColors.primaryDarkColor),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  shadowColor: Colors.grey,
                  elevation: 2,
                  child: Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 10, vertical: 10),
                      child: Column(children: [
                        Row(
                          children: [
                            Container(
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        color: AppColors.primaryDarkColor),
                                    borderRadius: BorderRadius.circular(5)),
                                child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 5, vertical: 2),
                                    child: Text(
                                      (index + 1).toString(),
                                      maxLines: 50,
                                      softWrap: true,
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                          color: AppColors.primaryDarkColor),
                                    ))),
                            const SizedBox(
                              width: 10,
                            ),
                            item?.paperQuestionChoices?[index].choice != null &&
                                    item?.paperQuestionChoices?[index].choice
                                            ?.isNotEmpty ==
                                        true
                                ? Expanded(
                                    child: Text(
                                      item?.paperQuestionChoices?[index]
                                              .choice ??
                                          '',
                                      maxLines: 50,
                                      textAlign: TextAlign.start,
                                    ),
                                  )
                                : Container(),
                            const SizedBox(width: 10),
                            Container(
                              margin: const EdgeInsets.only(left: 2),
                              width: 20,
                              height: 20,
                              child: const Icon(
                                Icons.check,
                                size: 15,
                                color: Colors.white,
                              ),
                              decoration: const BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Color(0xff29BE53)),
                            )
                          ],
                        ),
                        SizedBox(
                            height:
                                item?.paperQuestionChoices?[index].imageUrl !=
                                        null
                                    ? item?.paperQuestionChoices![index]
                                                .imageUrl?.isNotEmpty ==
                                            true
                                        ? 5
                                        : 0
                                    : 0),
                        item?.paperQuestionChoices?[index].imageUrl != null
                            ? item?.paperQuestionChoices![index].imageUrl
                                        ?.isNotEmpty ==
                                    true
                                ? Image.network(
                                    "$IMAGE_BASE/${item?.paperQuestionChoices?[index].imageUrl}",
                                    width: item?.paperQuestionChoices?[index]
                                                    .choice !=
                                                null &&
                                            item?.paperQuestionChoices?[index]
                                                    .choice?.isNotEmpty ==
                                                true
                                        ? 200
                                        : MediaQuery.of(context).size.width -
                                            100,
                                    height: 150,
                                  )
                                : const SizedBox()
                            : const SizedBox(),
                        SizedBox(
                            height:
                                item?.paperQuestionChoices?[index].imageUrl !=
                                        null
                                    ? item?.paperQuestionChoices![index]
                                                .imageUrl?.isNotEmpty ==
                                            true
                                        ? 5
                                        : 0
                                    : 0)
                      ])))
              : Container();
        });
  }

  Widget getAssetDataWidget(AssetData curriculumData) {
    if (curriculumData.status == 'DELETED') {
      return const SizedBox();
    }

    if (curriculumData.fileType == 'file' && curriculumData.isVideo == true) {
      return Column(
        children: [
          PlayVideoScreen(curriculumData.s3url ?? ''),
          const SizedBox(
            height: 15,
          )
        ],
      );
    }

    if (curriculumData.fileType == 'file' && curriculumData.isVideo == false) {
      if (curriculumData.fileName!.contains('mp3') ||
          curriculumData.fileName!.contains('wav') ||
          curriculumData.fileName!.contains('m4a')) {
        return Column(
          children: [
            PlayAudioScreen(
                curriculumData.s3url ?? '', curriculumData.fileName ?? '')
          ],
        );
      } else if (curriculumData.fileName!.contains('mp4') ||
          curriculumData.fileName!.contains('avi') ||
          curriculumData.fileName!.contains('mk4') ||
          curriculumData.fileName!.contains('3gp') ||
          curriculumData.fileName!.contains('mov') ||
          curriculumData.fileName!.contains('m3u8')) {
        return Column(
          children: [
            PlayVideoScreen(curriculumData.s3url ?? ''),
            const SizedBox(height: 15)
          ],
        );
      } else if (curriculumData.fileName!.contains('jpg') ||
          curriculumData.fileName!.contains('png') ||
          curriculumData.fileName!.contains('jpeg')) {
        return FadeInImage(
            placeholder: AssetImage(AppImages.coursePlaceHolder),
            imageErrorBuilder: (context, object, trace) {
              return Image.asset(AppImages.coursePlaceHolder);
            },
            image: NetworkImage('${curriculumData.s3url}'));
      } else {
        return const SizedBox();
      }
    } else if (curriculumData.fileType == 'text') {
      return Card(
        semanticContainer: true,
        clipBehavior: Clip.antiAliasWithSaveLayer,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(0.0),
        ),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: GlobalText(
            parseHtmlString(curriculumData.notes ?? ''),
            maxLine: 500,
          ),
        ),
      );
    } else if (curriculumData.fileType == 'document') {
      return Card(
        semanticContainer: true,
        clipBehavior: Clip.antiAliasWithSaveLayer,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(0.0),
        ),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: GlobalText(
            parseHtmlString(curriculumData.notes ?? ''),
            maxLine: 500,
          ),
        ),
      );
    } else if (curriculumData.fileType == 'video') {
      return Column(
        children: [
          PlayVideoScreen(curriculumData.s3url ?? ''),
          const SizedBox(height: 15)
        ],
      );
    } else if (curriculumData.fileType == 'link') {
      if (YoutubeVideoValidator.validateUrl(curriculumData.fileName ?? '')) {
        return PlayYouTubeScreen(curriculumData.fileName ?? '');
      } else if (curriculumData.fileName!.contains('vimeo')) {
        return Column(
          children: [
            PlayVideoScreen(curriculumData.fileName ?? ''),
            const SizedBox(height: 15)
          ],
        );
      } else {
        return const SizedBox();
      }
    }
    return const SizedBox();
  }
}
