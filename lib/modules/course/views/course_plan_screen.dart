import 'dart:convert';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:regtech_camp_app/common_widget/global_back_drop.dart';
import 'package:regtech_camp_app/common_widget/global_button.dart';
import 'package:regtech_camp_app/common_widget/global_text.dart';
import 'package:regtech_camp_app/model/StoreModel.dart';
import 'package:regtech_camp_app/model/item_category_response.dart';
import 'package:regtech_camp_app/modules/home/controllers/home_controller.dart';
import 'package:regtech_camp_app/resources/app_colors.dart';
import 'package:regtech_camp_app/resources/clients.dart';
import 'package:regtech_camp_app/routes/app_routes.dart';
import 'package:regtech_camp_app/services/apis.dart';
import 'package:regtech_camp_app/services/app_preference.dart';
import 'package:regtech_camp_app/utils.dart';
import 'package:webview_flutter/webview_flutter.dart';

import '../../../common_widget/profile_widget.dart';
import '../../../resources/app_constants.dart';
import '../../../resources/app_images.dart';

class CoursePlanScreen extends StatefulWidget {
  final ItemCategoryModel itemCategoryModel;

  const CoursePlanScreen(this.itemCategoryModel, {Key? key}) : super(key: key);

  @override
  _CoursePlanScreenState createState() => _CoursePlanScreenState();
}

class _CoursePlanScreenState extends State<CoursePlanScreen> {
  RxBool isLoading = false.obs;
  var featuredEventDescription = ''.obs;

  @override
  void initState() {
    checkFreeCourse();
    super.initState();
  }

  checkFreeCourse() async {
    if (widget.itemCategoryModel.paymentType == "FREE") {
      isLoading.value = true;
      var temp = await getFeaturedEventsDescription(
          widget.itemCategoryModel.guId ?? '9xABa920vl');
      featuredEventDescription.value = temp.description ?? '';
      isLoading.value = false;
    }
  }

  loadWebView() {
    return Obx(() => featuredEventDescription.value.isEmpty
        ? const SizedBox()
        : SizedBox(
            height: 500,
            child: WebView(
              initialUrl: Uri.dataFromString(featuredEventDescription.value,
                      mimeType: 'text/html',
                      encoding: Encoding.getByName('utf-8'))
                  .toString(),
              javascriptMode: JavascriptMode.unrestricted,
              gestureRecognizers: Set()
                ..add(
                  Factory<VerticalDragGestureRecognizer>(
                    () => VerticalDragGestureRecognizer(),
                  ),
                ),
            )));
  }

  @override
  Widget build(BuildContext context) {
    selectedBuyCourse.value = widget.itemCategoryModel;

    return Scaffold(
        body: Obx(() => Stack(children: [
              Stack(
                alignment: Alignment.bottomCenter,
                children: [
                  ListView(
                    padding: EdgeInsets.zero,
                    physics: const ScrollPhysics(),
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    children: [
                      ProfileWidget(
                        title: widget.itemCategoryModel.title ?? "",
                      ),
                      Container(
                          margin: const EdgeInsets.only(
                              top: 14, left: 14, right: 13),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15.0),
                          ),
                          child: ClipRRect(
                            borderRadius:
                                const BorderRadius.all(Radius.circular(15)),
                            child: CachedNetworkImage(
                                imageUrl:
                                    '$COURSE_IMAGE_BASE${widget.itemCategoryModel.image1 ?? ''}',
                                placeholder: (context, url) => Image.asset(
                                      AppImages.coursePlaceHolder,
                                      fit: BoxFit.fill,
                                      height: 166,
                                      width: 316,
                                    ),
                                errorWidget: (context, url, error) =>
                                    Image.asset(
                                      AppImages.coursePlaceHolder,
                                      fit: BoxFit.fill,
                                      height: 166,
                                      width: 316,
                                    ),
                                height: 166,
                                width: 316,
                                fit: BoxFit.fill),
                          )),
                      widget.itemCategoryModel.paymentType == "FREE"
                          ? const SizedBox()
                          : const Padding(
                              padding:
                                  EdgeInsets.only(top: 14, left: 14, right: 13),
                              child: GlobalText(
                                "Plans",
                                fontSize: 22,
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                      const SizedBox(
                        height: 10,
                      ),
                      widget.itemCategoryModel.paymentType == "FREE"
                          ? const SizedBox()
                          : widget.itemCategoryModel.storeModel.isEmpty
                              ? const Center(
                                  child: GlobalText(
                                  "No Plans Found!!",
                                  fontSize: 25,
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                ))
                              : ListView.builder(
                                  shrinkWrap: true,
                                  itemCount: widget
                                      .itemCategoryModel.storeModel.length,
                                  physics: const NeverScrollableScrollPhysics(),
                                  padding: EdgeInsets.zero,
                                  itemBuilder: (context, index) {
                                    return membershipItemView(widget
                                        .itemCategoryModel.storeModel[index]);
                                  }),
                      widget.itemCategoryModel.paymentType == "FREE"
                          ? loadWebView()
                          : const SizedBox(),
                      const SizedBox(
                        height: 90,
                      )
                    ],
                  ),
                  widget.itemCategoryModel.paymentType == "FREE"
                      ? bottomBuilder()
                      : const SizedBox(),
                ],
              ),
              (isLoading.value == true)
                  ? const GlobalBackDrop()
                  : const SizedBox()
            ])));
  }

  bottomBuilder() {
    return Container(
      margin: const EdgeInsets.only(bottom: 30, left: 40, right: 40, top: 10),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: const Color(0xff3b2e7e)),
      height: 50,
      alignment: Alignment.center,
      child: InkWell(
        borderRadius: BorderRadius.circular(15),
        onTap: () async {
          setState(() {
            isLoading.value = true;
          });
          String uName = await AppSharedPreferences.getStringFromLocalStorage(
              KEY_USER_NAME);
          String uEmail = await AppSharedPreferences.getStringFromLocalStorage(
              KEY_USER_EMAIL);
          String uPhone = await AppSharedPreferences.getStringFromLocalStorage(
              KEY_USER_PHONE);
          Map user = {'name': uName, 'email': uEmail, 'phone': uPhone};
          Map data = {
            'orgId': ORG_ID,
            'user': user,
            'categoryId': widget.itemCategoryModel.guId ?? '',
            'token': "",
            'paymentType': 'FREE'
          };
          String requestBody = json.encode(data);
          String url = "$BASE/public/register";

          var response = await http.post(Uri.parse(url), body: requestBody);
          var value = jsonDecode(response.body);
          if (response.statusCode == 200) {
            showToast(value['message']);
          } else {
            showToast(value['error']['message'] ?? "Failed to register.");
          }
          setState(() {
            isLoading.value = false;
          });
          Get.back();
          Get.back();
          Get.find<HomeController>()
              .onInit2(widget.itemCategoryModel.guId ?? '');
        },
        child: const GlobalText(
          "Register",
          color: Colors.white,
          fontSize: 18,
        ),
      ),
    );
  }

  Widget membershipItemView(StoreModel storeItem) {
    return Card(
      margin: const EdgeInsets.only(bottom: 10.0, left: 14, right: 14),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
      child: InkWell(
        borderRadius: BorderRadius.circular(8.0),
        onTap: () async {
          if (storeItem.isOtherPrice == true) {
            await showEnterAmountDialog(storeItem);
          } else {
            Get.toNamed(Routes.CHECKOUT, arguments: storeItem);
          }
        },
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    GlobalText(storeItem.name ?? ''),
                    GlobalText(
                      storeItem.price
                              .toStringAsFixed(1)
                              .split(".")
                              .last
                              .contains("0")
                          ? "${pingModel.csymbol == "\$" ? "\$" : "₹"}${storeItem.price.toStringAsFixed(0)}"
                          : "${pingModel.csymbol == "\$" ? "\$" : "₹"}${storeItem.price}",
                      color: AppColors.primaryDarkColor,
                      fontSize: 24,
                      paddingTop: 5.0,
                    ),
                    GlobalText(
                      "From : ${storeItem.membershipType ?? storeItem.itemType}",
                      color: AppColors.commonTextColor,
                      fontSize: 14,
                      paddingTop: 5.0,
                    ),
                    storeItem.registrationFees != 0
                        ? GlobalText(
                            storeItem.registrationFees
                                    .toStringAsFixed(1)
                                    .split(".")
                                    .last
                                    .contains("0")
                                ? "Registration Fees ${pingModel.csymbol == "\$" ? "\$" : "₹"} ${storeItem.registrationFees.toStringAsFixed(0)}"
                                : "Registration Fees ${pingModel.csymbol == "\$" ? "\$" : "₹"} ${storeItem.registrationFees}",
                            color: const Color(0XFF518453),
                            fontSize: 14,
                            paddingTop: 5.0)
                        : const SizedBox()
                  ],
                ),
              ),
              GlobalButton(
                fontSize: 15,
                width: 80,
                bgColor: const Color(0xff3b2e7e),
                title: 'Buy',
                height: 40,
                onTap: () {},
              )
            ],
          ),
        ),
      ),
    );
  }

  Future<void> showEnterAmountDialog(StoreModel storeItem) async {
    var textEditingController = TextEditingController();
    await Get.dialog(AlertDialog(
      title: TextField(
        controller: textEditingController,
        cursorColor: AppColors.primaryDarkColor,
        style: const TextStyle(fontSize: 18, color: Colors.black),
        decoration: InputDecoration(
            enabled: true,
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10.0),
              borderSide: BorderSide(color: AppColors.secondaryDartColor),
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10.0),
              borderSide: BorderSide(color: AppColors.secondaryDartColor),
            ),
            disabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10.0),
              borderSide: BorderSide(color: AppColors.secondaryDartColor),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10.0),
              borderSide: BorderSide(color: AppColors.secondaryDartColor),
            ),
            prefixIcon: null,
            hintText: "Enter Amount"),
        keyboardType: Platform.isIOS
            ? const TextInputType.numberWithOptions(signed: true, decimal: true)
            : TextInputType.number,
      ),
      actions: [
        TextButton(
            onPressed: () {
              Get.back();
            },
            child: const GlobalText("Cancel")),
        TextButton(
            onPressed: () {
              try {
                Get.back();
                String amountStr = textEditingController.text.toString().trim();
                storeItem.price = double.parse(amountStr);
                Get.toNamed(Routes.CHECKOUT, arguments: storeItem);
              } catch (e) {
                showToast('Please enter valid amount');
              }
            },
            child: const GlobalText("Add"))
      ],
    ));
  }
}
