import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:regtech_camp_app/common_widget/global_back_drop.dart';
import 'package:regtech_camp_app/common_widget/global_text.dart';
import 'package:regtech_camp_app/common_widget/profile_widget.dart';
import 'package:regtech_camp_app/modules/home/controllers/home_controller.dart';
import 'package:regtech_camp_app/resources/app_colors.dart';
import 'package:regtech_camp_app/resources/app_images.dart';
import 'package:regtech_camp_app/resources/app_texts.dart';
import '../../../resources/app_constants.dart';
import 'course_plan_screen.dart';

class CoursesScreen extends StatelessWidget {
  final homeController = Get.put(HomeController());

  CoursesScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: [
        ListView(
          physics: const ScrollPhysics(),
          shrinkWrap: true,
          padding: EdgeInsets.zero,
          children: [
            ProfileWidget(
              title: AppTexts.courses,
            ),
            Obx(() => homeController.storeList.isEmpty
                ? const SizedBox()
                : ListView.builder(
                    itemCount: homeController.storeList.length,
                    shrinkWrap: true,
                    scrollDirection: Axis.vertical,
                    physics: const ScrollPhysics(),
                    itemBuilder: (BuildContext context, int index) {
                      if (homeController.storeList[index].isShowOnWebsite ==
                              false ||
                          homeController.storeList[index].paymentType == null) {
                        return const SizedBox();
                      }
                      return Container(
                        margin: const EdgeInsets.only(bottom: 20),
                        child: InkWell(
                          onTap: () {
                            Get.to(() => CoursePlanScreen(
                                homeController.storeList[index]));
                          },
                          child: Card(
                            margin:
                                const EdgeInsets.only(left: 14.0, right: 14.0),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                            elevation: 8,
                            shadowColor: AppColors.primaryDarkColor,
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                      height: 166,
                                      width: 316,
                                      margin: const EdgeInsets.only(
                                          top: 14, left: 14, right: 13),
                                      decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(15.0),
                                      ),
                                      child: ClipRRect(
                                        borderRadius: const BorderRadius.all(
                                            Radius.circular(15)),
                                        child: CachedNetworkImage(
                                            imageUrl:
                                                '$COURSE_IMAGE_BASE${homeController.storeList[index].image1 ?? ''}',
                                            placeholder: (context, url) =>
                                                Image.asset(
                                                    AppImages.coursePlaceHolder,
                                                    fit: BoxFit.fill),
                                            errorWidget: (context, url,
                                                    error) =>
                                                Image.asset(
                                                    AppImages.coursePlaceHolder,
                                                    fit: BoxFit.fill),
                                            height: 166,
                                            fit: BoxFit.fill),
                                      )),
                                  const SizedBox(
                                    height: 11,
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        left: 14.0, bottom: 11),
                                    child: GlobalText(
                                      homeController.storeList[index].title ??
                                          '',
                                      color: Colors.black,
                                      fontSize: 16,
                                      fontWeight: FontWeight.w600,
                                      fontStyle: FontStyle.normal,
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        left: 14.0, bottom: 10),
                                    child: GlobalText(
                                      homeController.storeList[index]
                                              .shortDescription ??
                                          '',
                                      fontFamily:
                                          GoogleFonts.poppins().fontFamily,
                                      fontSize: 16,
                                      fontWeight: FontWeight.w400,
                                      textAlign: TextAlign.left,
                                    ),
                                  ),
                                ]),
                          ),
                        ),
                      );
                    })),
            const SizedBox(
              height: 20,
            ),
          ],
        ),
        Obx(() => homeController.paidCourseCalling.value
            ? const GlobalBackDrop()
            : Container())
      ],
    ));
  }
}
