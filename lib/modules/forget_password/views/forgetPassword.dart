import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:regtech_camp_app/common_widget/global_button.dart';
import 'package:regtech_camp_app/common_widget/global_top_widget.dart';
import 'package:regtech_camp_app/resources/app_colors.dart';

import '../../../common_widget/custom_edit_text.dart';
import '../../../common_widget/global_back_drop.dart';
import '../../../common_widget/global_text.dart';
import '../../../resources/app_images.dart';
import '../../../resources/app_texts.dart';
import '../../../routes/app_routes.dart';
import '../../../services/apis.dart';
import '../../../utils.dart';
import '../controllers/ForgetPasswordController.dart';

class ForgetPasswordScreen extends GetView<ForgetPasswordController> {
  @override
  final controller = Get.put(ForgetPasswordController());

  ForgetPasswordScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          ListView(
            padding: EdgeInsets.zero,
            scrollDirection: Axis.vertical,
            physics: const ScrollPhysics(),
            children: [
              Stack(children: [
                Image.asset(AppImages.bgBigPurpleImage,
                    height: 191, width: 375, fit: BoxFit.fill),
                const GlobalTopWidget()
              ]),
              GlobalText(
                controller.topText(),
                textAlign: TextAlign.center,
                fontSize: 18,
                color: const Color(0xff717171),
              ),
              const SizedBox(height: 20),
              CustomEditText(
                margin: const EdgeInsets.symmetric(horizontal: 30),
                hintText: AppTexts.verifyEmail,
                inputType: TextInputType.emailAddress,
                onTextChanged: (value) {
                  controller.email.value = value;
                },
                isEditText: true,
              ),
              const SizedBox(height: 20),
              GlobalButton(
                margin: const EdgeInsets.symmetric(horizontal: 30),
                title: AppTexts.submit,
                width: Get.width,
                onTap: () async {
                  await controller.recoverPassword();
                },
                fontSize: 22,
              ),
              const SizedBox(height: 20),
              GlobalText(
                AppTexts.back,
                fontSize: 18,
                color: AppColors.primaryDarkColor,
                textAlign: TextAlign.center,
                onTap: () {
                  Get.back();
                },
              ),
            ],
          ),
          Obx(() => controller.isApiCalling.value
              ? const GlobalBackDrop()
              : const SizedBox())
        ],
      ),
    );
  }
}
