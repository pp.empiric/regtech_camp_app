import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:regtech_camp_app/resources/app_constants.dart';
import 'package:regtech_camp_app/resources/clients.dart';
import 'package:regtech_camp_app/routes/app_routes.dart';
import 'package:regtech_camp_app/services/apis.dart';
import 'package:http/http.dart' as http;

import '../../../utils.dart';

class ForgetPasswordController extends GetxController {
  var topText = 'Verify your email or phone\n to reset your password'.obs;
  var email = ''.obs;
  var isApiCalling = false.obs;

  forgetPassword(String email) async {
    isApiCalling.value = true;
    await forgetPasswordApi(email);
    isApiCalling.value = false;
  }

  Future<void> recoverPassword() async {
    if (email.value.isEmpty) {
      showSnackBar('Enter Email Id', '', Colors.red, Icons.error);
      return;
    }

    if (!email.value.isEmail) {
      showSnackBar('Enter Valid Email ID', '', Colors.red, Icons.error);
      return;
    }

    final type = await isSafeToCallApi();
    if (type == 0) {
      await showVPNDialog(() async {
        await recoverPassword();
      });
    } else if (type == 1) {
      await showNoInternetDialog(() async {
        await recoverPassword();
      });
    } else {
      final String orgId = pingModel.orgId != null ? pingModel.orgId! : ORG_ID;

      isApiCalling.value = true;

      final user = <String, dynamic>{};
      user['orgId'] = orgId;
      user['email'] = email.value;

      const String url = '$BASE_URL/user/forgotPassword';
      final response = await http.post(Uri.parse(url), body: jsonEncode(user));

      isApiCalling.value = false;
      Get.toNamed(Routes.LOGIN);

      print(url);
      print(response);

      if (response.statusCode == 200) {
        showSnackBar(
            jsonDecode(response.body)['msg'], '', Colors.red, Icons.error);
      } else {
        showSnackBar(jsonDecode(response.body)['error']['message'], '',
            Colors.red, Icons.error);
      }
    }
  }
}
