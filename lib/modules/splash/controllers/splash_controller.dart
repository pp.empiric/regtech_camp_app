import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../model/general_user_data_response.dart';
import '../../../resources/app_constants.dart';
import '../../../routes/app_routes.dart';
import '../../../services/apis.dart';
import '../../../services/app_preference.dart';
import '../../../utils.dart';

class SplashController extends GetxController {
  var isApiCalling = false.obs;

  @override
  void onInit() {
    super.onInit();
    callPingData();
  }

  Future<void> callPingData() async {
    isApiCalling.value = true;
    final type = await isSafeToCallApi();
    if (type == 0) {
      await showVPNDialog(() async {
        await callPingData();
      });
    } else if (type == 1) {
      await showNoInternetDialog(() async {
        await callPingData();
      });
    } else {
      final pingModelResponse = await getGeneralData();
      if (pingModel.orgId == null) {
        showSnackBar(
            'Failed to get Ping API Response', '', Colors.red, Icons.error);
        isApiCalling.value = false;
        return;
      } else {
        pingModel = pingModelResponse;
        await checkLogInData();
      }
      pingModel = pingModelResponse;
      accessToken = await getAccessToken();
      userDataResponse = await getGeneralUserData();
    }
  }

  Future<void> checkLogInData() async {
    final String? isLogin =
        await AppSharedPreferences.getStringFromLocalStorage(KEY_GUID);
    if (isLogin?.isNotEmpty == true) {
      final type = await isSafeToCallApi();
      if (type == 0) {
        await showVPNDialog(() async {
          await checkLogInData();
        });
      } else if (type == 1) {
        await showNoInternetDialog(() async {
          await checkLogInData();
        });
      } else {
        contactResponse = await getContactResponse();

        AppSharedPreferences.saveStringToLocalStorage(
            KEY_USER_NAME, contactResponse.name ?? contactResponse.fullName);

        AppSharedPreferences.saveStringToLocalStorage(
            KEY_USER_EMAIL, contactResponse.email ?? '');

        AppSharedPreferences.saveStringToLocalStorage(
            KEY_USER_PHONE, contactResponse.phone);

        initOneSignal();

        isApiCalling.value = false;
        Get.offNamed(Routes.HOME);
      }
    } else {
      isApiCalling.value = false;
      Get.offNamed(Routes.LOGIN);
    }
  }
}
