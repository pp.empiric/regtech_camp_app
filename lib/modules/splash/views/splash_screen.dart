import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:regtech_camp_app/common_widget/global_text.dart';
import 'package:regtech_camp_app/modules/splash/controllers/splash_controller.dart';
import 'package:regtech_camp_app/resources/app_colors.dart';
import 'package:regtech_camp_app/resources/app_images.dart';

class SplashScreen extends GetView<SplashController> {
  SplashScreen({Key? key}) : super(key: key);

  final style = const TextStyle(
    fontSize: 32,
    fontWeight: FontWeight.w600,
    fontStyle: FontStyle.normal,
    color: Color(0xff6C6D71),
  );

  @override
  final controller = Get.put(SplashController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: 375,
        decoration: BoxDecoration(
          color: AppColors.primaryDarkColor,
        ),
        child: Padding(
          padding: const EdgeInsets.only(left: 75, right: 74, top: 74),
          child: Center(
            child: ListView(
              physics: const ScrollPhysics(),
              shrinkWrap: true,
              padding: EdgeInsets.zero,
              children: [
                Image.asset(AppImages.firstLogoImage),
                const SizedBox(
                  height: 44,
                ),
                Image.asset('assets/images/screen_two.png'),
                const SizedBox(
                  height: 34,
                ),
                RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(
                      text: ' CERTIFIED ',
                      style: const TextStyle(
                        height: 1.5,
                        fontSize: 32,
                        fontWeight: FontWeight.w600,
                      ),
                      children: [
                        TextSpan(
                          text: 'ANTI MONEY \n',
                          style: style,
                        ),
                        TextSpan(text: 'LAUNDERING \n', style: style),
                        TextSpan(text: 'PROGRAM', style: style),
                      ]),
                ),
                const SizedBox(height: 20),
                const GlobalText(
                  'Powered by Wajooba',
                  textAlign: TextAlign.center,
                  textStyle: TextStyle(
                    fontSize: 16,
                    fontStyle: FontStyle.normal,
                    color: Color(0xffFFFFFF),
                  ),
                ),
                const SizedBox(height: 25)
              ],
            ),
          ),
        ),
      ),
    );
  }
}
