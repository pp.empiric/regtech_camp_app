import 'package:flutter/material.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';

import '../../../common_widget/global_text.dart';
import '../../../model/course_response.dart';
import '../../../model/section_response.dart';
import '../controllers/course_curriculum_controller.dart';
import 'lesson_widget.dart';

class SectionCard extends StatelessWidget {
  final SectionResponse? section;
  final CourseData? data;

  SectionCard(this.section, {Key? key, this.data}) : super(key: key);

  final controller = Get.put(CourseCurriculumController());

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15),
      child: Container(
        color: Colors.white,
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          GlobalText(
            '${section?.title}',
            color: const Color(0xff333333),
            fontSize: 18,
            fontWeight: FontWeight.w400,
            fontStyle: FontStyle.normal,
            textAlign: TextAlign.start,
          ),
          ListView.builder(
              shrinkWrap: true,
              padding: EdgeInsets.zero,
              physics: const ScrollPhysics(),
              scrollDirection: Axis.vertical,
              itemCount: section?.chapters?.length,
              itemBuilder: (context, index) {
                return LessonCard(
                  section?.chapters?[index],
                  section,
                  data: data,
                );
              }),
          const SizedBox(height: 20),
        ]),
      ),
    );
  }
}
