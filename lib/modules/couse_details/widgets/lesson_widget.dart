import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:regtech_camp_app/common_widget/global_text.dart';
import 'package:regtech_camp_app/model/quiz_new_response.dart';
import 'package:regtech_camp_app/modules/home/controllers/home_controller.dart';
import 'package:regtech_camp_app/resources/app_colors.dart';

import '../../../model/chapters_response.dart';
import '../../../model/course_response.dart';
import '../../../model/section_response.dart';
import '../../../utils.dart';
import '../controllers/course_curriculum_controller.dart';

class LessonCard extends StatelessWidget {
  final Chapters? chapters;
  final SectionResponse? sections;
  final CourseData? data;

  LessonCard(this.chapters, this.sections, {Key? key, this.data})
      : super(key: key);

  final controller = Get.put(CourseCurriculumController());
  final homeController = Get.put(HomeController());

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () async {
         controller.isResult.value = false;
         controller.isChecked.value = false;
         controller.section.value = sections!;
         controller.chapter.value = chapters!;

        await controller.callDetail(chapters, sections);
        bool? completed =
            homeController.quizSingleResponse.value.hasCompleted == true;
        bool? isRepeat = (chapters?.lessonType?.contains('test') == true)
            ? homeController.quizSingleResponse.value.isRepeatTest
            : homeController.quizSingleResponse.value.isRepeatActivity;

        if (isRepeat == false && completed == true) {
          showToast("Test already completed!!");
          return;
        }
        if (chapters?.lessonType?.contains('test') == true) {
          if (homeController.quizSingleResponse.value != null) {
            if (homeController.quizSingleResponse.value
                        .isShowAnswerAfterEachQuestion ==
                    false &&
                isRepeat == false) {
              controller.isFinalTest.value = true;
            }
            await controller.startQuizDirect(
              homeController.quizSingleResponse.value,
              homeController.quizNewResponse.value);
            }
             else {
            showToast('Quiz details not found.');
          }
          print("test----${controller.isFinalTest.value}");
        } else {
          // Get.to(CourseCurriculumDetailsScreen(
          //     chapters: chapters ?? Chapters(),
          //     sections: sections,
          //     data: data));

          //     ?.then((value) {
          //   // this code is used to update completed tag after coming back
          //   // from the details screen
          //   controller.data.forEach((element) {
          //     if (element.guId == courseId) {
          //       controller.getCourseDetail(element);
          //       controller.courseSectionsListCmd.clear();
          //       controller.courseSectionsListCmd
          //           .addAll(element.courseSectionsListCmd!);
          //     }
          //   });
          //   // To update UI
          // });
        }
        // quizController.title.value = sections.title ?? '';
      },
      child: Container(
        width: Get.width,
        margin: const EdgeInsets.only(top: 10),
        padding: const EdgeInsets.symmetric(horizontal: 15),
        height: 64,
        alignment: Alignment.centerLeft,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.0),
            border: Border.all(
                color: AppColors.primaryDarkColor,
                style: BorderStyle.solid,
                width: 1)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              flex: 9,
              child: GlobalText(
                '${chapters?.title}',
                color: AppColors.primaryDarkColor,
                fontSize: 14,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
                textOverflow: TextOverflow.ellipsis,
              ),
            ),
            Obx(() => chapters?.isCompleted?.value == true
                ? Expanded(
                    flex: 1,
                    child: Icon(
                      CupertinoIcons.check_mark,
                      color: AppColors.primaryDarkColor,
                    ))
                : const SizedBox())
          ],
        ),
      ),
    );
  }
}
