import 'package:cached_network_image/cached_network_image.dart';
import 'package:dotted_decoration/dotted_decoration.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

import 'package:regtech_camp_app/common_widget/global_back_drop.dart';

import 'package:regtech_camp_app/model/quiz_new_response.dart';
import 'package:regtech_camp_app/model/section_response.dart';
import 'package:regtech_camp_app/modules/couse_details/views/result_screen.dart';
import 'package:regtech_camp_app/resources/app_colors.dart';
import 'package:regtech_camp_app/resources/app_constants.dart';

import 'package:regtech_camp_app/utils.dart';

import '../../../common_widget/global_text.dart';
import '../../../common_widget/profile_widget.dart';
import '../../../model/chapters_response.dart';
import '../../../model/course_response.dart';
import '../../../resources/app_images.dart';
import '../../../resources/app_texts.dart';
import '../../home/controllers/home_controller.dart';
import '../controllers/course_curriculum_controller.dart';

class CourseCurriculumDetailsScreen
    extends GetView<CourseCurriculumController> {
  @override
  final controller = Get.put(CourseCurriculumController());
  final homeController = Get.put(HomeController());
  final Chapters? chapters;
  final SectionResponse? sections;
  final CourseData? data;

  CourseCurriculumDetailsScreen({
    this.data,
    this.chapters,
    this.sections,
    Key? key,
  }) : super(key: key);



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Obx(() => (controller.isResult.value == true)
          ? const ResultScreen()
          : Stack(
              children: [
                ListView(
                  padding: EdgeInsets.zero,
                  physics: const ScrollPhysics(),
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  children: [
                    ProfileWidget(
                        title: controller.selectedChapter.value.title ?? '',
                        onTap: () => Get.back()),
                    const SizedBox(height: 17),
                    assetWidget()
                  ],
                ),
                Obx(() => controller.isApiCalling.value == true
                    ? const GlobalBackDrop()
                    : const SizedBox())
              ],
            )),
    );
  }

  Widget assetWidget() {
    if (homeController.quizSingleResponse.value.id == null) {
      return Container(
        margin: const EdgeInsets.symmetric(horizontal: 15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Obx(() => homeController.quizNewResponse.value.assetData != null &&
                    homeController.quizNewResponse.value.assetData!.isNotEmpty
                ? ListView.builder(
                    itemBuilder: (context, index) => buildItem(
                        homeController.quizNewResponse.value.assetData![index]),
                    padding: const EdgeInsets.only(top: 20),
                    physics: const NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount:
                    homeController.quizNewResponse.value.assetData!.length,
                  )
                : Container(
                    alignment: Alignment.center,
                    margin: const EdgeInsets.symmetric(vertical: 30),
                    child: const GlobalText('No Assets Found!!',
                        color: Color(0xff717171),
                        fontSize: 15,
                        fontWeight: FontWeight.w400,
                        fontStyle: FontStyle.normal))),
            Obx(() => controller.isCommentLoading.value
                ? Container(
                    alignment: Alignment.center,
                    margin: const EdgeInsets.symmetric(vertical: 25),
                    child: const CircularProgressIndicator())
                : controller.commentList.isEmpty == true
                    ? const SizedBox()
                    : commentWidget()),
            GlobalText('Send Comment',
                color: AppColors.primaryDarkColor,
                fontSize: 16,
                fontWeight: FontWeight.w700,
                fontStyle: FontStyle.normal),
            const SizedBox(height: 8),
            Row(children: [
              Expanded(
                child: Container(
                    height: 49,
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.grey,
                        width: 2,
                      ),
                      borderRadius: BorderRadius.circular(15),
                    ),
                    alignment: Alignment.center,
                    padding: const EdgeInsets.symmetric(horizontal: 15),
                    child: TextField(
                      onChanged: (value) {
                        controller.messageText.value = value;
                      },
                      controller: controller.messageController,
                      style: const TextStyle(
                          color: Colors.black,
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal),
                      decoration: const InputDecoration(
                          border: InputBorder.none,
                          enabledBorder: InputBorder.none,
                          disabledBorder: InputBorder.none,
                          focusedBorder: InputBorder.none,
                          prefixIcon: null,
                          hintStyle: TextStyle(
                              color: Color(0xff929292),
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal),
                          hintText: 'Type your message here'),
                      keyboardType: TextInputType.url,
                    )),
              ),
              const SizedBox(width: 7),
              Container(
                  width: 50,
                  height: 50,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: AppColors.primaryDarkColor),
                  child: Material(
                      color: Colors.transparent,
                      child: InkWell(
                          onTap: () async {
                            if (controller.messageText.value.isEmpty) {
                              showSnackBar('Enter Message!!', '', Colors.red,
                                  Icons.error);
                              return;
                            }
                            await controller.sendMessage();
                          },
                          child: Image.asset(
                            AppImages.forwardArrowIcon,
                            height: 50,
                            width: 50,
                          ))))
            ]),
            const SizedBox(height: 100)
          ],
        ),
      );
    } else {
      return Container(
        margin: const EdgeInsets.symmetric(horizontal: 15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            homeController.quizSingleResponse.value.additionalInfo != null &&
                homeController
                        .quizSingleResponse.value.additionalInfo!.isNotEmpty
                ? GlobalText(
              homeController.quizSingleResponse.value.additionalInfo!,
                    color: AppColors.primaryDarkColor,
                    fontSize: 23,
                    fontWeight: FontWeight.w700,
                    fontStyle: FontStyle.normal,
                  )
                : const SizedBox(),
            homeController.quizSingleResponse.value.imageUrl != null &&
                homeController.quizSingleResponse.value.imageUrl!.isNotEmpty
                ? Container(
                    height: 209,
                    decoration:
                        BoxDecoration(borderRadius: BorderRadius.circular(25)),
                    child: ClipRRect(
                        child: CachedNetworkImage(
                            imageUrl:
                                '$COURSE_IMAGE_BASE${homeController.quizSingleResponse.value.imageUrl}',
                            placeholder: (context, url) => Image.asset(
                                AppImages.coursePlaceHolder,
                                fit: BoxFit.fill),
                            errorWidget: (context, url, error) => Image.asset(
                                AppImages.coursePlaceHolder,
                                fit: BoxFit.fill),
                            height: 166,
                            fit: BoxFit.fill),
                        borderRadius: BorderRadius.circular(25)))
                : const SizedBox(),
            homeController.quizSingleResponse.value.paragraph != null &&
                homeController.quizSingleResponse.value.paragraph!.isNotEmpty
                ? Container(
                    padding: const EdgeInsets.only(top: 10),
                    child: HtmlWidget(
                        homeController.quizSingleResponse.value.paragraph!,
                        textStyle:
                            TextStyle(color: Colors.black.withOpacity(.9))))
                : Container(),
            Obx(() => homeController.quizNewResponse.value.assetData != null &&
                homeController.quizNewResponse.value.assetData!.isNotEmpty
                ? ListView.builder(
                    itemBuilder: (context, index) => buildItem(
                        homeController.quizNewResponse.value.assetData![index]),
                    padding: const EdgeInsets.only(top: 10),
                    physics: const NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount:
                    homeController.quizNewResponse.value.assetData!.length,
                  )
                : const SizedBox()),
            const SizedBox(
              height: 17,
            ),
            homeController.quizSingleResponse.value.activityQuestions!.isNotEmpty &&
                homeController
                        .quizSingleResponse.value.activityQuestions!.isNotEmpty
                ? controller.activityWidget()
                : const SizedBox(),
            Obx(() =>
            homeController.quizNewResponse.value.contactAssetData?.isNotEmpty ==
                            true ||
                homeController.quizSingleResponse.value
                                .isAllowUserToUploadFiles ==
                            true
                    ? Container(
                        margin: const EdgeInsets.only(bottom: 20),
                        child: GlobalText(
                          AppTexts.uploadFiles,
                          color: AppColors.primaryDarkColor,
                          fontSize: 16,
                          fontWeight: FontWeight.w700,
                          fontStyle: FontStyle.normal,
                        ),
                      )
                    : const SizedBox()),
            Obx(() =>
            homeController.quizNewResponse.value.contactAssetData?.isNotEmpty ==
                        true
                    ? userAssetWidget()
                    : const SizedBox()),
            homeController.quizSingleResponse.value.isAllowUserToUploadFiles == true
                ? Container(
                    width: 346,
                    height: 93,
                    decoration: DottedDecoration(
                      shape: Shape.box,
                      strokeWidth: 2,
                      dash: const [7, 7],
                      color: AppColors.primaryDarkColor,
                      borderRadius: const BorderRadius.all(Radius.circular(12)),
                    ),
                    child: Material(
                      color: Colors.transparent,
                      child: InkWell(
                        onTap: () async {
                          // await controller.selectFiles(
                            //   controller.quizSingleResponse.value);
                          await controller.selectFiles(
                              homeController.quizSingleResponse.value,
                              sections,
                              chapters);
                        },
                        borderRadius:
                            const BorderRadius.all(Radius.circular(12)),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Image.asset(AppImages.uploadIcon),
                            GlobalText(
                              AppTexts.addFiles,
                              color: AppColors.primaryDarkColor,
                              fontSize: 16,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal,
                            ),
                          ],
                        ),
                      ),
                    ),
                  )
                : const SizedBox(),
            const SizedBox(
              height: 34,
            ),
            InkWell(
              onTap: () {
                controller.isChecked.value = !controller.isChecked.value;
              },
              highlightColor: Colors.transparent,
              splashColor: Colors.transparent,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Obx(() => controller.isChecked.value
                      ? SvgPicture.asset(
                          AppImages.checkBoxIcon,
                          height: 26,
                          width: 26,
                        )
                      : const Icon(Icons.check_box_outline_blank,
                          color: Colors.black, size: 26)),
                  const SizedBox(width: 10),
                  GlobalText(
                    AppTexts.activityCompletion,
                    color: AppColors.primaryDarkColor,
                    fontSize: 16,
                    fontWeight: FontWeight.w700,
                    fontStyle: FontStyle.normal,
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            Center(
              child: Container(
                width: 296,
                height: 51,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: AppColors.secondaryDartColor),
                child: Material(
                  color: Colors.transparent,
                  child: InkWell(
                    borderRadius: BorderRadius.circular(10),
                    onTap: () async {
                      if (controller.isChecked.value) {
                        await controller.startQuiz(
                            homeController.quizSingleResponse.value,
                            chapters,
                            sections ?? SectionResponse());
                      } else {
                        showSnackBar('Complete Activity First', '', Colors.red,
                            Icons.error);
                      }
                    },
                    child: Center(
                        child: GlobalText(AppTexts.submitActivity,
                            color: Colors.white,
                            letterSpacing: 0.8,
                            fontSize: 22,
                            fontWeight: FontWeight.w700,
                            fontStyle: FontStyle.normal)),
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 57,
            ),
            Obx(() => controller.messageLoading.value
                ? Container(
                    alignment: Alignment.center,
                    margin: const EdgeInsets.symmetric(vertical: 25),
                    child: const CircularProgressIndicator())
                : controller.commentList.isEmpty
                    ? const SizedBox()
                    : commentWidget()),
            const SizedBox(
              height: 10,
            ),
            GlobalText('Send Comment',
                color: AppColors.primaryDarkColor,
                fontSize: 16,
                fontWeight: FontWeight.w700,
                fontStyle: FontStyle.normal),
            const SizedBox(height: 8),
            Row(children: [
              Expanded(
                child: Container(
                    height: 49,
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.grey,
                        width: 2,
                      ),
                      borderRadius: BorderRadius.circular(15),
                    ),
                    alignment: Alignment.center,
                    padding: const EdgeInsets.symmetric(horizontal: 15),
                    child: TextField(
                      onChanged: (value) {
                        controller.messageText.value = value;
                      },
                      controller: controller.messageController,
                      style: TextStyle(
                          fontFamily: GoogleFonts.lato().fontFamily,
                          color: Colors.black,
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal),
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          enabledBorder: InputBorder.none,
                          disabledBorder: InputBorder.none,
                          focusedBorder: InputBorder.none,
                          prefixIcon: null,
                          hintStyle: TextStyle(
                              fontFamily: GoogleFonts.lato().fontFamily,
                              color: const Color(0xff929292),
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal),
                          hintText: 'Type your message here'),
                      keyboardType: TextInputType.url,
                    )),
              ),
              const SizedBox(width: 7),
              Container(
                  width: 50,
                  height: 50,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: AppColors.primaryDarkColor,
                  ),
                  child: Material(
                      color: Colors.transparent,
                      child: InkWell(
                          onTap: () async {
                            if (controller.messageText.value.isEmpty) {
                              showSnackBar('Enter Message!!', '', Colors.red,
                                  Icons.error);
                              return;
                            }
                            await controller.sendMessage();
                          },
                          child: Image.asset(
                            AppImages.forwardArrowIcon,
                            height: 50,
                            width: 50,
                          ))))
            ]),
            const SizedBox(height: 100)
          ],
        ),
      );
    }
  }

  Widget commentWidget() {
    final dateList = controller.commentList
        .map((element) => element.dateCreated!)
        .map((e) => e.split('T').first)
        .toSet()
        .toList();

    return ListView.builder(
      itemBuilder: (context, index) {
        final finalList = controller.commentList
            .where((p0) =>
                p0.dateCreated != null &&
                p0.dateCreated!.contains(dateList[index].split('T').first))
            .toList();
        final date = DateTime.parse(dateList[index]);

        return Column(children: [
          GlobalText(DateFormat('dd MMMM yyyy').format(date),
              color: AppColors.primaryDarkColor,
              fontSize: 15,
              fontWeight: FontWeight.w700,
              fontStyle: FontStyle.normal),
          ListView.builder(
              itemBuilder: (context, index2) {
                return Container(
                    margin: const EdgeInsets.only(bottom: 15),
                    child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          finalList[index2].contact == null &&
                                  finalList[index2].contact!.picture == null
                              ? Container(
                                  margin:
                                      const EdgeInsets.only(top: 40, right: 20),
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    border: Border.all(
                                        color: Colors.white, width: 2),
                                  ),
                                  child: Image.asset(
                                    AppImages.profileImage,
                                    width: 44,
                                    height: 44,
                                  ),
                                )
                              : Container(
                                  width: 44,
                                  height: 44,
                                  decoration: BoxDecoration(
                                    image: DecorationImage(
                                      image: NetworkImage(
                                          finalList[index2].contact!.picture !=
                                                  null
                                              ? finalList[index2]
                                                  .contact!
                                                  .picture
                                                  .toString()
                                              : ''),
                                      fit: BoxFit.cover,
                                    ),
                                    shape: BoxShape.circle,
                                    border: Border.all(
                                      color: Colors.white,
                                      width: 2.0,
                                    ),
                                  )),
                          const SizedBox(width: 13),
                          Expanded(
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                      width: double.infinity,
                                      decoration: BoxDecoration(
                                          color: const Color(0xfff4f3f3),
                                          borderRadius:
                                              BorderRadius.circular(15)),
                                      padding: const EdgeInsets.only(
                                          top: 10,
                                          bottom: 10,
                                          left: 17,
                                          right: 17),
                                      child: GlobalText(
                                          finalList[index2].note ?? '',
                                          color: const Color(0xff717171),
                                          fontSize: 15,
                                          textOverflow: TextOverflow.ellipsis,
                                          maxLine: 50,
                                          fontWeight: FontWeight.w400,
                                          fontStyle: FontStyle.normal)),
                                  const SizedBox(height: 10),
                                  GlobalText(
                                      DateFormat('hh:mm a').format(
                                          DateTime.parse(
                                              finalList[index2].dateCreated!)),
                                      color: const Color(0xff717171),
                                      fontSize: 13,
                                      fontWeight: FontWeight.w400,
                                      fontStyle: FontStyle.normal)
                                ]),
                          )
                        ]));
              },
              itemCount: finalList.length,
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              scrollDirection: Axis.vertical,
              padding: const EdgeInsets.only(top: 9, bottom: 13)),
        ]);
      },
      itemCount: dateList.length,
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      scrollDirection: Axis.vertical,
      padding: EdgeInsets.zero,
    );
  }

  Widget userAssetWidget() {
    return ListView.builder(
      itemBuilder: (context, index) => Container(
        height: 67.834,
        margin: const EdgeInsets.only(bottom: 8),
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(
            color: const Color(0xffe8e8e8),
            width: 1,
          ),
          borderRadius: BorderRadius.circular(15),
        ),
        padding: const EdgeInsets.only(left: 10, right: 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Text(
                  homeController.quizNewResponse.value.contactAssetData![index]
                          .fileName ??
                      '',
                  style: const TextStyle(
                      color: Color(0xff929292),
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal,
                      fontSize: 14.0,
                      overflow: TextOverflow.ellipsis),
                  textAlign: TextAlign.left),
            ),
            InkWell(
                onTap: () async {
                  await controller.deleteFile(homeController
                          .quizNewResponse.value.contactAssetData?[index] ??
                      ContactAssetData());
                },
                child: Image.asset(AppImages.deleteVectorIcon))
          ],
        ),
      ),
      physics: const NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      padding: const EdgeInsets.only(bottom: 21),
      itemCount: homeController.quizNewResponse.value.contactAssetData?.length,
    );
  }
}
