import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:lottie/lottie.dart';
import 'package:regtech_camp_app/common_widget/global_button.dart';
import 'package:regtech_camp_app/common_widget/global_text.dart';
import 'package:regtech_camp_app/resources/app_colors.dart';
import 'package:regtech_camp_app/routes/app_routes.dart';

class ResultScreen extends StatelessWidget {
  const ResultScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(alignment: Alignment.center, children: [
      Container(
        width: MediaQuery.of(Get.context!).size.width,
      ),
      Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            LottieBuilder.asset("assets/json/congratulation.json",
                width: 263, height: 194),
            const SizedBox(height: 50),
            GlobalText("Hurray! Congratulations",
                textStyle: GoogleFonts.poppins(
                    color: AppColors.primaryDarkColor,
                    fontSize: 23,
                    fontWeight: FontWeight.w900,
                    fontStyle: FontStyle.normal)),
            GlobalText("\nActivity completed 👍",
                textStyle: GoogleFonts.poppins(
                    color: AppColors.secondaryDartColor,
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal)),
            const SizedBox(height: 20),
            GlobalButton(
              onTap: () {
                Get.offNamed(Routes.HOME);
              },
              title: 'Back',
              height: 51,
              width: 148,
            ),
          ])
    ]);
  }
}
