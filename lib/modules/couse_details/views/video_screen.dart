import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:regtech_camp_app/common_widget/global_text.dart';
import 'package:regtech_camp_app/model/course_response.dart';
import 'package:regtech_camp_app/model/quiz_new_response.dart';
import 'package:regtech_camp_app/modules/couse_details/views/purchased_card.dart';
import 'package:regtech_camp_app/modules/couse_details/widgets/section_card.dart';
import 'package:regtech_camp_app/modules/home/controllers/home_controller.dart';
import 'package:regtech_camp_app/resources/app_colors.dart';
import 'package:regtech_camp_app/resources/app_texts.dart';
import 'package:regtech_camp_app/utils.dart';

import '../../../common_widget/global_back_drop.dart';
import '../../../common_widget/profile_widget.dart';
import '../controllers/course_curriculum_controller.dart';

class VideoScreen extends StatelessWidget {
  final AssetData? videoList;
  final String? fileName;

  const VideoScreen(this.videoList, {Key? key, this.fileName})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        shrinkWrap: true,
        children: [
          ProfileWidget(
            title: fileName,
          ),
          getVideoWidget(videoList ?? AssetData(), true),
          Padding(
            padding: const EdgeInsets.only(left: 10),
            child: HtmlWidget(videoList?.notes ?? "",
                textStyle: GoogleFonts.poppins(
                    color: AppColors.primaryDarkColor,
                    fontWeight: FontWeight.w600,
                    fontSize: 16)),
          )
        ],
      ),
    );
  }
}
