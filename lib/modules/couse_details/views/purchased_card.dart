import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:regtech_camp_app/common_widget/PlayVimeoScreen.dart';
import 'package:regtech_camp_app/common_widget/global_text.dart';
import 'package:regtech_camp_app/common_widget/play_video_screen.dart';
import 'package:regtech_camp_app/common_widget/play_youtube_screen.dart';
import 'package:regtech_camp_app/model/quiz_new_response.dart';
import 'package:regtech_camp_app/modules/couse_details/views/video_screen.dart';
import 'package:regtech_camp_app/modules/home/controllers/home_controller.dart';
import 'package:regtech_camp_app/resources/app_colors.dart';
import 'package:regtech_camp_app/resources/youtubevideovalidator/youtube_video_validator.dart';

import '../../../utils.dart';

class PurchasedCourseCard extends StatelessWidget {
  final AssetData videoList;
  final controller = Get.put(HomeController());

  PurchasedCourseCard(this.videoList, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        if (videoList.notes?.isNotEmpty == true && videoList.notes != null) {
          Get.to(
            VideoScreen(
              videoList,
              fileName: getFileName(videoList),
            ),
          );
        }
      },
      child: Card(
        margin: (controller.videoList.last.id == videoList.id)
            ? const EdgeInsets.only(left: 15, bottom: 15, top: 5, right: 20)
            : const EdgeInsets.only(left: 15, bottom: 15, top: 5),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
        ),
        child: Container(
          width: 320,
          padding: const EdgeInsets.all(15),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(15),
            boxShadow: [
              BoxShadow(
                  color: AppColors.primaryDarkColor,
                  offset: const Offset(0, 0),
                  blurRadius: 5,
                  spreadRadius: 0),
            ],
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              getVideoWidget(
                  videoList,
                  (videoList.notes?.isEmpty == true ||
                              videoList.notes == null) ==
                          true
                      ? true
                      : false),
              const SizedBox(height: 5),
              Expanded(
                child: GlobalText(
                  getFileName(videoList),
                  fontFamily: GoogleFonts.poppins().fontFamily,
                  fontWeight: FontWeight.w600,
                  color: const Color(0xff3A3A3A),
                  fontSize: 16,
                  maxLine: 1,
                  textAlign: TextAlign.start,
                  textOverflow: TextOverflow.ellipsis,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
