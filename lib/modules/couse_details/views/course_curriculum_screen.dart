import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:regtech_camp_app/common_widget/global_shimmer.dart';
import 'package:regtech_camp_app/common_widget/global_text.dart';
import 'package:regtech_camp_app/common_widget/shimmer_effect_two.dart';
import 'package:regtech_camp_app/model/course_response.dart';
import 'package:regtech_camp_app/modules/couse_details/views/purchased_card.dart';
import 'package:regtech_camp_app/modules/couse_details/widgets/section_card.dart';
import 'package:regtech_camp_app/modules/home/controllers/home_controller.dart';
import 'package:regtech_camp_app/resources/app_colors.dart';
import 'package:regtech_camp_app/resources/app_texts.dart';
import 'package:regtech_camp_app/utils.dart';
import 'package:shimmer/shimmer.dart';

import '../../../common_widget/global_back_drop.dart';
import '../../../common_widget/profile_widget.dart';
import '../controllers/course_curriculum_controller.dart';

class CoursesCurriculumScreen extends GetView<CourseCurriculumController> {
  final CourseData? data;

  @override
  final controller = Get.put(CourseCurriculumController());

  final homeController = Get.find<HomeController>();

  CoursesCurriculumScreen({Key? key, this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(children: [
        ListView(
          shrinkWrap: true,
          children: [
            ProfileWidget(
              title: AppTexts.myCourses,
            ),
            Column(mainAxisSize: MainAxisSize.min, children: [
              Container(
                alignment: Alignment.centerLeft,
                padding:
                    const EdgeInsets.symmetric(vertical: 15, horizontal: 15),
                child: GlobalText(
                  "${data?.title}",
                  fontFamily: GoogleFonts.poppins().fontFamily,
                  fontWeight: FontWeight.w600,
                  color: const Color(0xff3A3A3A),
                  fontSize: 16,
                  maxLine: 2,
                  textOverflow: TextOverflow.ellipsis,
                ),
              ),
              Obx(() => homeController.isVideoLoading.value == true
                  ? const ShimmerEffect()
                  : Obx(() => homeController.videoList.isNotEmpty == true
                      ? SizedBox(
                          height: 250,
                          child: ListView.builder(
                              shrinkWrap: true,
                              padding: EdgeInsets.zero,
                              physics: const ScrollPhysics(),
                              scrollDirection: Axis.horizontal,
                              itemCount: homeController.videoList.length,
                              itemBuilder: (context, index) {
                                return PurchasedCourseCard(
                                    homeController.videoList[index]);
                              }),
                        )
                      : SizedBox(
                          height: 100,
                          child: Center(
                              child: GlobalText(
                            "Video not found",
                            color: AppColors.primaryDarkColor,
                          ))))),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: GlobalText(
                  parseHtmlString(
                      "Watch all the topic and videos and complete your exam.\nA mock test helps you to prepare your self to the final exam."),
                  fontFamily: GoogleFonts.poppins().fontFamily,
                  fontWeight: FontWeight.w400,
                  color: const Color(0xff333333),
                  fontSize: 16,
                ),
              ),
              const SizedBox(height: 20),
              Obx(() => homeController.isSectionLoading.value == true
                  ? const ShimmerEffectContainer()
                  : Obx(() => (homeController.sectionList.isNotEmpty == true)
                      ? ListView.builder(
                          shrinkWrap: true,
                          padding: EdgeInsets.zero,
                          physics: const ScrollPhysics(),
                          scrollDirection: Axis.vertical,
                          itemCount: homeController.sectionList.length,
                          itemBuilder: (context, index) {
                            if (homeController
                                    .sectionList[index].chapters?.isNotEmpty ==
                                true) {
                              return SectionCard(
                                homeController.sectionList[index],
                                data: data,
                              );
                            }
                            return const SizedBox();
                          })
                      : SizedBox(
                          height: 100,
                          child: Center(
                              child: GlobalText(
                            "Test not found",
                            color: AppColors.primaryDarkColor,
                          ))))),
            ]),
          ],
        ),
        Obx(() => controller.isApiCalling.value == true
            ? const GlobalBackDrop()
            : const SizedBox()),
      ]),
    );
  }
}
