import 'dart:convert';
import 'dart:io';

import 'package:downloads_path_provider_28/downloads_path_provider_28.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get.dart' hide MultipartFile;
import 'package:google_fonts/google_fonts.dart';
import 'package:mime/mime.dart';
import 'package:path_provider/path_provider.dart';
import 'package:regtech_camp_app/common_widget/global_text.dart';
import 'package:regtech_camp_app/common_widget/play_audio_screen.dart';
import 'package:regtech_camp_app/common_widget/play_video_screen.dart';
import 'package:regtech_camp_app/common_widget/play_youtube_screen.dart';
import 'package:regtech_camp_app/model/activity_response.dart';
import 'package:regtech_camp_app/model/chapters_response.dart';
import 'package:regtech_camp_app/model/comments_response.dart';
import 'package:regtech_camp_app/model/course_response.dart';
import 'package:regtech_camp_app/model/courses_curriculum_details_response.dart';
import 'package:regtech_camp_app/model/folder_response.dart';
import 'package:regtech_camp_app/model/note_model.dart';
import 'package:regtech_camp_app/model/quiz_new_response.dart';
import 'package:regtech_camp_app/model/section_response.dart';
import 'package:regtech_camp_app/modules/home/controllers/home_controller.dart';
import 'package:regtech_camp_app/modules/quiz/AssetScreen.dart';
import 'package:regtech_camp_app/modules/quiz/quiz_screen.dart';
import 'package:regtech_camp_app/resources/app_colors.dart';
import 'package:regtech_camp_app/resources/app_constants.dart';
import 'package:regtech_camp_app/resources/app_images.dart';
import 'package:regtech_camp_app/resources/clients.dart';
import 'package:regtech_camp_app/resources/youtubevideovalidator/youtube_video_validator.dart';
import 'package:regtech_camp_app/services/apis.dart';
import 'package:regtech_camp_app/services/app_preference.dart';

import '../../../utils.dart';

class CourseCurriculumController extends GetxController {
  var guId = ''.obs;
  var isApiCalling = false.obs;

  var homeController = Get.put(HomeController());

  List<CourseSectionsListCmd> newSectionList = <CourseSectionsListCmd>[].obs;

  Rx<CommentsResponse> commentsResponse = CommentsResponse().obs;
  var commentList = <NoteModel>[].obs;

  Rx<CoursesCurriculumDetailsResponse> coursesCurriculumDetails =
      CoursesCurriculumDetailsResponse().obs;
  Rx<Data> coursesCurriculumSingleResponse = Data().obs;

  RxBool isChecked = false.obs;
  RxBool isCommentSendLoading = false.obs;
  RxBool isCommentLoading = false.obs;
  RxString commentMsg = "".obs;
  var commentMsgController = TextEditingController();
  var notes = NoteModel().obs;
  var msgIndex = 0.obs;

  var directory = "".obs;
  var downloadVisibility = true.obs;
  var imageUploading = false.obs;
  Rx<CourseData> course = CourseData().obs;
  Rx<SectionResponse> section = SectionResponse().obs;
  Rx<Chapters> chapter = Chapters().obs;

  var uploadFileList = <ContactAssetData>[].obs;
  var isResult = false.obs;

  var selectedChapter = Chapters().obs;
  var selectedCourseSection = SectionResponse().obs;

  var messageText = ''.obs;
  var messageLoading = false.obs;
  var messageController = TextEditingController();
  var selectedFolder = ContactAssets().obs;
  var isFinalTest = false.obs;


  @override
  void onInit() {
    getDownloadsPath();
    super.onInit();
  }

  deleteFile(ContactAssetData folderData) async {
    final type = await isSafeToCallApi();
    if (type == 0) {
      await showVPNDialog(() async {
        await deleteFile(folderData);
      });
    } else if (type == 1) {
      await showNoInternetDialog(() async {
        await deleteFile(folderData);
      });
    } else {
      isApiCalling.value = true;
      await deleteFileApi(folderData.id ?? '');
      isApiCalling.value = false;
      await callDetail(selectedChapter.value, selectedCourseSection.value);
    }
  }

  Future<void> callDetail(Chapters? chapters, SectionResponse? sections) async {
    final type = await isSafeToCallApi();

    if (type == 0) {
      await showVPNDialog(() async {
        homeController.quizNewResponse.value = await getQuizNewApi(
            courseId: sections?.course ?? '',
            chapterId: chapters?.guId ?? '',
            type: chapters?.lessonType ?? '');
      });
    } else if (type == 1) {
      await showNoInternetDialog(() async {
        homeController.quizNewResponse.value = await getQuizNewApi(
            courseId: sections?.course ?? '',
            chapterId: chapters?.guId ?? '',
            type: chapters?.lessonType ?? '');
      });
    } else {
      try {
        isApiCalling.value = true;
        selectedChapter.value = chapters ?? Chapters();
        selectedCourseSection.value = sections!;

        homeController.quizNewResponse.value = await getQuizNewApi(
            courseId: sections.course ?? '',
            chapterId: chapters?.guId ?? '',
            type: chapters?.lessonType ?? '');

        getFolderNotes();

        if (homeController.quizNewResponse.value.contactAssetData?.isNotEmpty ==
            true) {
          uploadFileList.addAll(
              homeController.quizNewResponse.value.contactAssetData ?? []);
        }

        if (homeController.quizNewResponse.value.data != null &&
            homeController.quizNewResponse.value.data!.isNotEmpty) {
          if (homeController.quizNewResponse.value.data!.length == 1) {
            homeController.quizSingleResponse.value =
                homeController.quizNewResponse.value.data!.first;
          } else {
            homeController.quizSingleResponse.value = homeController
                .quizNewResponse.value.data!
                .firstWhere((element) => element.chapter == chapters?.guId,
                    orElse: () =>
                        homeController.quizNewResponse.value.data!.first);
          }
        }

        if (homeController.quizSingleResponse.value.activityQuestions != null &&
            homeController
                .quizSingleResponse.value.activityQuestions!.isNotEmpty) {
          for (var element
              in homeController.quizSingleResponse.value.activityQuestions!) {
            if (element.answerCount != null) {
              for(int i = 0; i < element.answerCount!; i++) {
                element.answers?.add(SAnswer(null));
              }
            } else {
              element.answers?.add(SAnswer(null));
            }
          }
        }
        isApiCalling.value = false;
      } catch (e) {
        isApiCalling.value = false;
        showSnackBar(e.toString(), '', Colors.red, Icons.error);
      }
    }
  }

  getFolderNotes() async {
    final type = await isSafeToCallApi();
    if (type == 0) {
      await showVPNDialog(() async {
        await getFolderNotes();
      });
    } else if (type == 1) {
      await showNoInternetDialog(() async {
        await getFolderNotes();
      });
    } else {
      messageLoading.value = true;
      commentList.clear();
      final temp = await getNotes(
          course.value.guId ?? '',
          selectedCourseSection.value.guId ?? '',
          selectedChapter.value.guId ?? '');
      if (temp.isNotEmpty) {
        commentList.addAll(temp);
      }
      for (var element in temp) {
        print("date------${element.dateCreated}");
      }

      messageLoading.value = false;
    }
  }

  sendMessage() async {
    final type = await isSafeToCallApi();
    if (type == 0) {
      await showVPNDialog(() async {
        await sendMessage();
      });
    } else if (type == 1) {
      await showNoInternetDialog(() async {
        await sendMessage();
      });
    } else {
      final response = await addCourseNote(
          course.value.guId ?? '',
          selectedCourseSection.value.guId ?? '',
          selectedChapter.value.guId ?? '',
          messageText.value);
      if (response == '') {
        showSnackBar('Failed to send Message!!', '', Colors.red, Icons.error);
      } else {
        if (response['guId'] == null) {
          showSnackBar('Failed to send Message!!', '', Colors.red, Icons.error);
        } else {
          messageText.value = '';
          messageController.clear();
          await getFolderNotes();
        }
      }
    }
  }

  startQuiz(
      QuizNewData item, Chapters? chapters, SectionResponse sections) async {
    if (chapters?.lessonType != null && chapters?.lessonType == 'test') {
    } else {
      isApiCalling.value = true;
      var isComplete = true;

      if (item.activityQuestions != null &&
          item.activityQuestions!.isNotEmpty) {
        for (var element in item.activityQuestions!) {
          if (element.answers != null && element.answers!.isNotEmpty) {
            for (var element2 in element.answers!) {
              if (element2.answer == null) {
                isComplete = false;
              }
            }
          }
        }
      }
      final String id =
          await AppSharedPreferences.getStringFromLocalStorage(KEY_GUID);
      final String uName =
          await AppSharedPreferences.getStringFromLocalStorage(KEY_USER_NAME);
      final String uEmail =
          await AppSharedPreferences.getStringFromLocalStorage(KEY_USER_EMAIL);
      final map = <String, dynamic>{};
      map['name'] = item.name ?? '';
      map['activity'] = item.id ?? '';
      map['contactName'] = uName;
      map['contactEmail'] = uEmail;
      map['contactId'] = id;
      map['course'] = item.course ?? course.value.id ?? '';
      map['chapter'] = item.chapter ?? chapters?.guId ?? '';
      map['section'] = item.section ?? sections.guId ?? '';
      map['orgId'] = ORG_ID;
      map['isCompleted'] = isComplete;
      map['activityQuestion'] = item.activityQuestions ?? [];
      map['courseName'] = course.value.name ?? '';
      map['chapterName'] = chapters?.title ?? '';
      map['sectionName'] = sections.title ?? [];

      final data = await setActivityApi(jsonEncode(map));
      isApiCalling.value = false;

      if (jsonDecode(data.body)['data'] != null) {
        isResult.value = true;
        await callDetail(chapters, sections);
      } else {
        showSnackBar(jsonDecode(data.body), '', Colors.red, Icons.error);
      }
    }
  }

  Future<void> selectFiles(QuizNewData activityData, SectionResponse? sections,
      Chapters? chapters) async {
    final FilePickerResult? result = await FilePicker.platform.pickFiles(
      type: FileType.custom,
      allowMultiple: true,
      allowedExtensions: [
        'jpg',
        'pdf',
        'mp4',
        'avi',
        'mkv',
        '3gp',
        'mov',
        'png'
      ],
    );

    if (result == null) {
      showSnackBar('Please select file to upload', '', Colors.red, Icons.error);
    } else {
      await uploadFiles(result.files, activityData);
    }
  }

  uploadFiles(List<PlatformFile> files, QuizNewData activityData) async {
    final type = await isSafeToCallApi();
    if (type == 0) {
      await showVPNDialog(() async {
        await uploadFiles(files, activityData);
      });
    } else if (type == 1) {
      await showNoInternetDialog(() async {
        await uploadFiles(files, activityData);
      });
    } else {
      isApiCalling.value = true;
      try {
        final name =
            '${course.value.title}-${selectedCourseSection.value.title}-${selectedChapter.value.title}';
        await addFolderWithNameTest(name, files, activityData);
      } catch (e) {
        showSnackBar('something went wrong on uploading file please try again!',
            '', Colors.red, Icons.error);
        isApiCalling.value = false;
      }
    }
  }

  addFolderWithNameTest(String folderName, List<PlatformFile> files,
      QuizNewData activityData) async {
    final type = await isSafeToCallApi();
    if (type == 0) {
      await showVPNDialog(() async {
        await addFolderWithNameTest(folderName, files, activityData);
      });
    } else if (type == 1) {
      await showNoInternetDialog(() async {
        await addFolderWithNameTest(folderName, files, activityData);
      });
    } else {
      try {
        final data = await getFolders();
        String folderGuId = '';
        if (data.isNotEmpty) {
          final response = data.firstWhere(
              (element) =>
                  element.title
                      ?.replaceAll(' ', '')
                      .compareTo(folderName.replaceAll(' ', '')) ==
                  0,
              orElse: () => ContactAssets());
          folderGuId = response.guId ?? '';
        }

        if (folderGuId.isEmpty) {
          final cAsset = await addFolder(
              folderName.isNotEmpty ? folderName : 'New Folder');
          folderGuId = cAsset.guId ?? '';
        }

        await uploadFilesApi(folderGuId, files, activityData);
      } catch (e) {
        isApiCalling.value = false;
        showSnackBar('something went wrong on uploading file please try again!',
            '', Colors.red, Icons.error);
      }
    }
  }

  getDownloadsPath() async {
    if (GetPlatform.isAndroid) {
      Directory? _downloadsDirectory =
          await DownloadsPathProvider.downloadsDirectory;
      directory.value = _downloadsDirectory!.path;
    } else {
      var dirs = await getApplicationDocumentsDirectory();
      directory.value = dirs.path;
    }
  }

  getCourseNewData() async {
    //homeController.isApiCalling.value = true;
    homeController.sectionList.clear();
    homeController.videoList.clear();
    homeController.courseVideoList.clear();
    homeController.newSectionList.clear();

    homeController.isSectionLoading.value=true;
    await getSections(course.value.guId).then((value) => {
          if (value.isNotEmpty)
            {
              value.forEach((sections) {
                sections.chapters?.forEach((chapters) async {
                   if (chapters.lessonType == "test") {
                    homeController.quizNewResponse.value = await getQuizNewApi(
                        courseId: sections.course,
                        chapterId: chapters.guId,
                        type: chapters.lessonType);

                    if (homeController.quizNewResponse.value.data?.isNotEmpty ==
                        true) {
                      if (homeController.quizNewResponse.value.data?.length ==
                          1) {
                        homeController.quizSingleResponse.value =
                            homeController.quizNewResponse.value.data!.first;
                      } else {
                        homeController.quizSingleResponse.value = homeController
                            .quizNewResponse.value.data!
                            .firstWhere(
                          (element) => element.chapter == chapters.guId,
                        );
                      }
                      chapters.isCompleted?.value = homeController.quizSingleResponse.value.hasCompleted ??
                          false;
                     // chapters.chapterResponse?.value = homeController.quizNewResponse.value;
                    }
                  }

                 }
                );

                homeController.newSectionList.add(sections);
                homeController.sectionList.add(sections);
              })
            }
        });
            homeController.isSectionLoading.value=false;

    if (homeController.newSectionList.isNotEmpty) {
      for (int i = 0; i < homeController.newSectionList.length; i++) {
        for (int j = 0;
            j < homeController.newSectionList[i].chapters!.length;
            j++) {
          if (homeController.newSectionList[i].chapters?[j].lessonType !=
              'test') {
            homeController.newSectionList[i].chapters![j].cousreId!.value =
                homeController.newSectionList[i].course ?? '';
            homeController.courseVideoList
                .add(homeController.newSectionList[i].chapters![j]);
          }
        }
      }
    }
    if (homeController.sectionList.isNotEmpty) {
      for (var sections1 in homeController.sectionList) {
        sections1.chapters
            ?.removeWhere((chapters) => chapters.lessonType != "test");
      }
    }
    var val = QuizNewResponse();
    var singleResponse;
      homeController.isVideoLoading.value=true;

    if (homeController.courseVideoList.isNotEmpty) {
      for (var chapters in homeController.courseVideoList) {
        val = await getQuizNewApi(
            courseId: chapters.cousreId?.value ?? '',
            chapterId: chapters.guId,
            type: chapters.lessonType);

        if (val.data?.isNotEmpty == true) {
          if (val.data?.length == 1) {
            singleResponse = val.data?.first ?? QuizNewData();
          } else {
            singleResponse = val.data!.firstWhere(
              (element) => element.chapter == chapters.guId,
            );
          }
        }
        if (val.assetData != null && val.assetData!.isNotEmpty) {
          for (var element in val.assetData!) {
            if (element.fileType == 'link') {
              element.title?.value = chapters.title ?? '';
              homeController.videoList.add(element);
            } else if (element.isVideo == true) {
              homeController.videoList.add(element);
            } else if (element.fileType == 'file' && element.isVideo == true) {
              homeController.videoList.add(element);
            } else if (element.fileType == 'video') {
              homeController.videoList.add(element);
            }
            element.notes = singleResponse.paragraph;
          }
        }
      }
    }
    homeController.isVideoLoading.value=false;
   // homeController.isApiCalling.value = false;
    print("ll---${homeController.courseVideoList.length}");
    print("videoList.length--${homeController.videoList.length}");
  }

  Widget activityWidget() {
    return ListView.builder(
        itemBuilder: (context, index) {
          final item =
              homeController.quizSingleResponse.value.activityQuestions![index];
          return Container(
              margin: const EdgeInsets.symmetric(vertical: 5),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    GlobalText(item.title ?? '',
                        color: AppColors.primaryDarkColor,
                        fontSize: 18,
                        fontWeight: FontWeight.w700,
                        fontStyle: FontStyle.normal),
                    Container(height: 10),
                    ListView.builder(
                      itemBuilder: (context, index) => Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15),
                              border: Border.all(
                                  color: AppColors.primaryDarkColor)),
                          margin: const EdgeInsets.symmetric(vertical: 5),
                          padding: const EdgeInsets.symmetric(horizontal: 16),
                          child: TextFormField(
                            maxLines: 1,
                            style: GoogleFonts.lato(
                                fontSize: 14,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                                color: Colors.black,
                                decoration: TextDecoration.none),
                            textInputAction: TextInputAction.next,
                            cursorColor: AppColors.primaryDarkColor,
                            keyboardType: TextInputType.text,
                            onChanged: (text) {
                              if (text.isNotEmpty) {
                                item.answers?[index] = SAnswer(text);
                              } else {
                                item.answers?[index] = SAnswer(null);
                              }
                            },
                            textAlign: TextAlign.start,
                            decoration: InputDecoration(
                                hintText: 'Add Your Answer',
                                hintStyle: GoogleFonts.lato(
                                    color: const Color(0xff929292),
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                    fontStyle: FontStyle.normal),
                                contentPadding: EdgeInsets.zero,
                                border: InputBorder.none,
                                focusedBorder: InputBorder.none,
                                enabledBorder: InputBorder.none,
                                errorBorder: InputBorder.none,
                                disabledBorder: InputBorder.none),
                          )),
                      physics: const NeverScrollableScrollPhysics(),
                      scrollDirection: Axis.vertical,
                      itemCount: item.answerCount ?? 1,
                      shrinkWrap: true,
                      padding: EdgeInsets.zero,
                    )
                  ]));
        },
        shrinkWrap: true,
        padding: const EdgeInsets.only(bottom: 15),
        itemCount:
            homeController.quizSingleResponse.value.activityQuestions?.length,
        scrollDirection: Axis.vertical,
        physics: const NeverScrollableScrollPhysics());
  }

  Future<void> uploadFilesApi(
      String s, List<PlatformFile> files, QuizNewData activityData) async {
    for (var file in files) {
      final data = await getS3Url(file.path!.split('/').last,
          lookupMimeType(file.path!.split('/').last)!);
      await uploadFile(File(file.path!),
          lookupMimeType(file.path!.split('/').last)!, data['signedUrl']);

      final map = <String, dynamic>{};
      map['type'] = 'activity';
      map['activity'] = activityData.id;
      map['title'] =
          '${course.value.title}-${selectedCourseSection.value.title}-${selectedChapter.value.title}';

      final response = await fileUpload(
          data['signedUrl'],
          lookupMimeType(file.path!.split('/').last)!,
          s,
          data['isVideo'],
          file.path!.split('/').last,
          data: map);

      if (response['status'] == false) {
        showSnackBar(
            'existing record found with same fileName, folderName and chapterId',
            '',
            Colors.red,
            Icons.error);
      }

      if (file == files.last) {
        await callDetail(selectedChapter.value, selectedCourseSection.value);
        isApiCalling.value = false;
      }
    }
  }

  Widget getAssetDataWidget(AssetData curriculumData) {
    if (curriculumData.status == 'DELETED') {
      return const SizedBox();
    }

    if (curriculumData.fileType == 'file' && curriculumData.isVideo == true) {
      return Column(
        children: [
          PlayVideoScreen(curriculumData.s3url ?? ''),
          const SizedBox(
            height: 15,
          )
        ],
      );
    }

    if (curriculumData.fileType == 'file' && curriculumData.isVideo == false) {
      if (curriculumData.fileName!.contains('mp3') ||
          curriculumData.fileName!.contains('wav') ||
          curriculumData.fileName!.contains('m4a')) {
        return Column(
          children: [
            PlayAudioScreen(
                curriculumData.s3url ?? '', curriculumData.fileName ?? '')
          ],
        );
      } else if (curriculumData.fileName!.contains('mp4') ||
          curriculumData.fileName!.contains('avi') ||
          curriculumData.fileName!.contains('mk4') ||
          curriculumData.fileName!.contains('3gp') ||
          curriculumData.fileName!.contains('mov') ||
          curriculumData.fileName!.contains('m3u8')) {
        return Column(
          children: [
            PlayVideoScreen(curriculumData.s3url ?? ''),
            const SizedBox(height: 15)
          ],
        );
      } else if (curriculumData.fileName!.contains('jpg') ||
          curriculumData.fileName!.contains('png') ||
          curriculumData.fileName!.contains('jpeg')) {
        return FadeInImage(
            placeholder: AssetImage(AppImages.coursePlaceHolder),
            imageErrorBuilder: (context, object, trace) {
              return Image.asset(AppImages.coursePlaceHolder);
            },
            image: NetworkImage('${curriculumData.s3url}'));
      } else {
        return const SizedBox();
      }
    } else if (curriculumData.fileType == 'text') {
      return Card(
        semanticContainer: true,
        clipBehavior: Clip.antiAliasWithSaveLayer,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(0.0),
        ),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: GlobalText(
            parseHtmlString(curriculumData.notes ?? ''),
            maxLine: 500,
          ),
        ),
      );
    } else if (curriculumData.fileType == 'document') {
      return Card(
        semanticContainer: true,
        clipBehavior: Clip.antiAliasWithSaveLayer,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(0.0),
        ),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: GlobalText(
            parseHtmlString(curriculumData.notes ?? ''),
            maxLine: 500,
          ),
        ),
      );
    } else if (curriculumData.fileType == 'video') {
      return Column(
        children: [
          PlayVideoScreen(curriculumData.s3url ?? ''),
          const SizedBox(height: 15)
        ],
      );
    } else if (curriculumData.fileType == 'link') {
      if (YoutubeVideoValidator.validateUrl(curriculumData.fileName ?? '')) {
        return PlayYouTubeScreen(curriculumData.fileName ?? '');
      } else if (curriculumData.fileName!.contains('vimeo')) {
        return Column(
          children: [
            PlayVideoScreen(curriculumData.fileName ?? ''),
            const SizedBox(height: 15)
          ],
        );
      } else {
        return const SizedBox();
      }
    }
    return const SizedBox();
  }

  Future<void> startQuizDirect(
      QuizNewData quiz, QuizNewResponse quizNewResponse) async {
    isApiCalling.value = true;
    try {
      String id =
          await AppSharedPreferences.getStringFromLocalStorage(KEY_GUID);
      String uName =
          await AppSharedPreferences.getStringFromLocalStorage(KEY_USER_NAME);
      String uEmail =
          await AppSharedPreferences.getStringFromLocalStorage(KEY_USER_EMAIL);
      var map = <String, dynamic>{};
      map['contact'] = id;
      map['courseName'] = course.value.title ?? '';
      map['chapterName'] = chapter.value.title ?? '';
      map['sectionName'] = section.value.title ?? '';
      map['quiz'] = quiz.id;
      map['contactName'] = uName;
      map['contactEmail'] = uEmail;
      map['startTime'] = DateTime.now().millisecondsSinceEpoch.toString();

      var questions = await initQuizAPI(jsonEncode(map));
      isApiCalling.value = false;
      dataTest = quiz;
      quizTest = questions;
      quizQuestion.value = questions;
      if (quiz.isSkipContent == true) {
        Get.to(() => QuizScreen(), arguments: null);
      } else {
        Get.to(() => AssetScreen(quiz, quizNewResponse), arguments: null);
      }
    } catch (e) {
      isApiCalling.value = false;
      showToast(e.toString());
    }
  }

  Future<void> removeFile(int index) async {
    isApiCalling.value = true;
    await deleteFolderVideo(uploadFileList[index].id ?? "");
    uploadFileList.removeAt(index);
    isApiCalling.value = false;
  }

  callUpdate(String url) {
    updateContactData('{"picture":"$url"}');
  }
}
