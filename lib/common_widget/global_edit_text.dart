import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:regtech_camp_app/resources/app_colors.dart';

class GlobalEditText extends StatefulWidget {
  final String helpText;
  final Function onTextChanged;
  final TextInputType type;
  final TextEditingController? controller;
  final TextInputAction? inputAction;
  final TextAlign textAlign;
  final bool? isEditTextLineEnabled;
  final Color? cursorType;
  final double? fontSize;
  final FontWeight? fontWight;
  final Color? textColor;
  final bool? obscureText;
  final int? maxLines;
  final int? minLines;
  final int? maxLength;
  final List<TextInputFormatter>? textFormatter;

  const GlobalEditText(
    this.helpText,
    this.type,
    this.onTextChanged, {
    Key? key,
    this.controller,
    this.inputAction,
    this.textAlign = TextAlign.center,
    this.isEditTextLineEnabled = false,
    this.cursorType,
    this.fontSize,
    this.fontWight,
    this.textColor,
    this.obscureText,
    this.maxLines,
    this.minLines,this.maxLength,this.textFormatter
  }) : super(key: key);

  @override
  _GlobalEditTextState createState() => _GlobalEditTextState();
}

class _GlobalEditTextState extends State<GlobalEditText> {
  var focusNode = FocusNode();

  @override
  Widget build(BuildContext context) => TextFormField(
        obscureText: widget.obscureText ?? false,
        textInputAction: widget.inputAction,
        controller: widget.controller,
        autofocus: false,
        onChanged: (value) {
          widget.onTextChanged(value);
        },
        style: TextStyle(
            fontFeatures: const [FontFeature.tabularFigures()],
            fontFamily: GoogleFonts.lato().fontFamily,
            fontSize: widget.fontSize ?? 18,
            fontWeight: widget.fontWight ?? FontWeight.w600,
            color: widget.textColor ?? Colors.black),
        textAlign: widget.textAlign,
inputFormatters: widget.textFormatter,
        keyboardType: widget.type,
        cursorColor: widget.cursorType ?? AppColors.primaryDarkColor,
        decoration: widget.isEditTextLineEnabled == false
            ? InputDecoration(
                isDense: true,
                border: InputBorder.none,
                hintMaxLines: 2,
                contentPadding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                hintStyle: TextStyle(
                  fontFamily: GoogleFonts.lato().fontFamily,
                  color: const Color(0xff6C6D71),
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                  fontStyle: FontStyle.normal,
                  fontFeatures: const [FontFeature.tabularFigures()],
                ),
                hintText: widget.helpText,
              )
            : InputDecoration(
                focusedBorder: const UnderlineInputBorder(
                  borderSide: BorderSide(color: Color(0xff6C6D71)),
                ),
                hintMaxLines: 2,
                contentPadding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                border: InputBorder.none,
                hintStyle: TextStyle(
                  fontFamily: GoogleFonts.lato().fontFamily,
                  color: const Color(0xff6C6D71),
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                  fontStyle: FontStyle.normal,
                  fontFeatures: const [FontFeature.tabularFigures()],
                ),
                hintText: widget.helpText,
              ),
        textAlignVertical: TextAlignVertical.center,
        maxLines: widget.maxLines ?? 1,
        minLines: widget.minLines ?? 1,
    maxLength: widget.maxLength,
      );
}
