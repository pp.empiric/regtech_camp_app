import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import 'package:regtech_camp_app/modules/profile/controllers/profile_controller.dart';
import 'package:regtech_camp_app/resources/app_colors.dart';
import 'package:regtech_camp_app/resources/app_texts.dart';

import 'custom_edit_text.dart';
import 'global_button.dart';
import 'global_edit_text.dart';
import 'global_outline_button.dart';
import 'global_text.dart';

class BottomDialogWidget extends GetView<ProfileController> {
  @override
  final controller = Get.put(ProfileController());

  BottomDialogWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Wrap(
      children: [
        Container(
            height: Get.height / 1.3,
            decoration: BoxDecoration(
              border: Border.all(color: const Color(0xffe3e3e3), width: 1),
              borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(30.0),
                topRight: Radius.circular(30.0),
              ),
              color: Colors.white,
            ),
            child: ListView(children: [
              Row(textDirection: TextDirection.ltr, children: [
                Padding(
                  padding: const EdgeInsets.only(
                    left: 16.0,
                    top: 17,
                  ),
                  child: GlobalText(
                    'Cancel',
                    color: const Color(0xff007AFF),
                    fontWeight: FontWeight.w400,
                    fontSize: 17,
                    fontStyle: FontStyle.normal,
                    onTap: () {
                      Get.back();
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 65.0, top: 16.0),
                  child: GlobalText(
                    "Update Profile",
                    fontSize: 18,
                    fontFamily: GoogleFonts.poppins().fontFamily,
                    fontWeight: FontWeight.w500,
                    color: AppColors.primaryDarkColor,
                  ),
                ),
              ]),
              Container(
                margin: const EdgeInsets.only(top: 20.0),
                width: Get.width,
                height: 1,
                decoration: const BoxDecoration(color: Color(0xffd9d9da)),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 20.0, top: 15, bottom: 15),
                child: GlobalText(
                  "Professional Details",
                  fontSize: 18,
                  fontFamily: GoogleFonts.poppins().fontFamily,
                  fontWeight: FontWeight.w500,
                  color: Colors.black,
                ),
              ),
              ListView.builder(
                  itemBuilder: (context, index) => ((controller
                                      .customFieldList[index].type ==
                                  'text' ||
                              controller.customFieldList[index].type ==
                                  'number') &&
                          controller.customFieldList[index].isDisabled == false)
                      ? CustomEditText(
                          width: Get.width,
                          margin: const EdgeInsets.only(
                              left: 20, right: 20, bottom: 15),
                          hintText: controller.customFieldList[index].name ??
                              controller.customFieldList[index].placeholder ??
                              '',
                          value: controller.customFieldList[index].value.value,
                          isEditText: true,
                          inputType:
                              controller.customFieldList[index].type == 'number'
                                  ? TextInputType.number
                                  : TextInputType.text,
                          onTextChanged: (value) {
                            controller.customFieldList[index].value.value =
                                value;
                          },
                          controller: controller
                              .customFieldList[index].controller
                            ..text =
                                (controller.customFieldList[index].value.value),
                        )
                      : (controller.customFieldList[index].type == 'textarea' &&
                              controller.customFieldList[index].isDisabled ==
                                  false)
                          ? Container(
                              alignment: Alignment.center,
                              padding:
                                  const EdgeInsets.only(top: 20, bottom: 10),
                              margin: const EdgeInsets.only(
                                  left: 20, right: 20, bottom: 20),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10.0),
                                  border: Border.all(
                                    color: AppColors.primaryDarkColor,
                                    style: BorderStyle.solid,
                                    width: 1,
                                  )),
                              child: GlobalEditText(
                                controller.customFieldList[index].name ?? '',
                                TextInputType.name,
                                (value) {
                                  controller.customFieldList[index].value
                                      .value = value;
                                },
                                controller:
                                    controller.customFieldList[index].controller
                                      ..text = (controller
                                          .customFieldList[index].value.value),
                                isEditTextLineEnabled: false,
                                maxLines: 3,
                                minLines: 2,
                              ),
                            )
                          : (controller.customFieldList[index].type ==
                                      'checkbox' &&
                                  controller
                                          .customFieldList[index].isDisabled ==
                                      false)
                              ? Obx(() => Padding(
                                    padding: const EdgeInsets.only(
                                        left: 20, right: 20, bottom: 15),
                                    child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          InkWell(
                                              onTap: () {
                                                if (controller
                                                        .customFieldList[index]
                                                        .value
                                                        .value ==
                                                    '') {
                                                  controller
                                                          .customFieldList[index]
                                                          .value
                                                          .value =
                                                      controller
                                                          .customFieldList[
                                                              index]
                                                          .name
                                                          .toString();
                                                } else {
                                                  controller
                                                      .customFieldList[index]
                                                      .value
                                                      .value = '';
                                                }
                                              },
                                              child: Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          right: 10),
                                                  child: Icon(
                                                      controller
                                                                  .customFieldList[
                                                                      index]
                                                                  .value
                                                                  .value ==
                                                              controller
                                                                  .customFieldList[
                                                                      index]
                                                                  .name
                                                          ? Icons.check_box
                                                          : Icons
                                                              .check_box_outline_blank,
                                                      color: AppColors
                                                          .primaryDarkColor,
                                                      size: 26))),
                                          GlobalText(
                                            controller.customFieldList[index]
                                                    .name ??
                                                '',
                                            color: AppColors.primaryDarkColor,
                                          )
                                        ]),
                                  ))
                              : (controller.customFieldList[index].type ==
                                          'radio' &&
                                      controller.customFieldList[index]
                                              .isDisabled ==
                                          false)
                                  ? (controller.radioValidation(controller
                                              .customFieldList[index]) ==
                                          true)
                                      ? Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 20,
                                                  right: 20,
                                                  bottom: 10),
                                              child: GlobalText(
                                                controller
                                                        .customFieldList[index]
                                                        .name ??
                                                    '',
                                                color:
                                                    AppColors.primaryDarkColor,
                                              ),
                                            ),
                                            Obx(() {
                                              if (controller
                                                      .customFieldList[index]
                                                      .value
                                                      .value ==
                                                  '') {
                                                if (controller
                                                        .customFieldList[index]
                                                        .value
                                                        .value !=
                                                    '') {
                                                  controller
                                                          .customFieldList[index]
                                                          .value
                                                          .value =
                                                      controller
                                                          .customFieldList[
                                                              index]
                                                          .value
                                                          .value;
                                                }
                                              }
                                              return Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  (controller
                                                              .customFieldList[
                                                                  index]
                                                              .option1 !=
                                                          null)
                                                      ? RadioWidget(
                                                          controller
                                                                  .customFieldList[
                                                                      index]
                                                                  .option1 ??
                                                              '',
                                                          controller
                                                                  .customFieldList[
                                                                      index]
                                                                  .option1 ??
                                                              '',
                                                          controller
                                                              .customFieldList[
                                                                  index]
                                                              .value
                                                              .value,
                                                          index,
                                                        )
                                                      : const SizedBox(),
                                                  (controller
                                                              .customFieldList[
                                                                  index]
                                                              .option2 !=
                                                          null)
                                                      ? RadioWidget(
                                                          controller
                                                                  .customFieldList[
                                                                      index]
                                                                  .option2 ??
                                                              '',
                                                          controller
                                                              .customFieldList[
                                                                  index]
                                                              .option2
                                                              .toString(),
                                                          controller
                                                              .customFieldList[
                                                                  index]
                                                              .value
                                                              .value,
                                                          index,
                                                        )
                                                      : const SizedBox(),
                                                  (controller
                                                              .customFieldList[
                                                                  index]
                                                              .option3 !=
                                                          null)
                                                      ? RadioWidget(
                                                          controller
                                                                  .customFieldList[
                                                                      index]
                                                                  .option3 ??
                                                              '',
                                                          controller
                                                              .customFieldList[
                                                                  index]
                                                              .option3
                                                              .toString(),
                                                          controller
                                                              .customFieldList[
                                                                  index]
                                                              .value
                                                              .value,
                                                          index,
                                                        )
                                                      : const SizedBox(),
                                                  (controller
                                                              .customFieldList[
                                                                  index]
                                                              .option4 !=
                                                          null)
                                                      ? RadioWidget(
                                                          controller
                                                                  .customFieldList[
                                                                      index]
                                                                  .option4 ??
                                                              '',
                                                          controller
                                                              .customFieldList[
                                                                  index]
                                                              .option4
                                                              .toString(),
                                                          controller
                                                              .customFieldList[
                                                                  index]
                                                              .value
                                                              .value,
                                                          index,
                                                        )
                                                      : const SizedBox(),
                                                ],
                                              );
                                            }),
                                          ],
                                        )
                                      : const SizedBox()
                                  : const SizedBox(),
                  padding:
                      const EdgeInsets.symmetric(horizontal: 14, vertical: 20),
                  itemCount: controller.customFieldList.length,
                  physics: const ScrollPhysics(),
                  shrinkWrap: true),
              const SizedBox(height: 15),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  GlobalOutlineButton(
                    text: AppTexts.cancel,
                    fontSize: 22,
                    width: 146,
                    onTap: () {
                      Get.back();
                      controller.resetCustomFieldValue();
                    },
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  GlobalButton(
                    title: AppTexts.submit,
                    fontSize: 22,
                    width: 146,
                    onTap: () {
                      controller.updateCustomField();
                    },
                  )
                ],
              ),
              const SizedBox(height: 15)
            ], shrinkWrap: true, physics: const ScrollPhysics())),
      ],
    );
  }

  Widget RadioWidget(
      String text, String value, String updatedValue, int index) {
    return ListTile(
      title: GlobalText(text),
      leading: Radio(
        value: value,
        groupValue: updatedValue,
        onChanged: (value) {
          controller.customFieldList[index].value.value = value.toString();
        },
        activeColor: AppColors.primaryDarkColor,
      ),
    );
  }
}
