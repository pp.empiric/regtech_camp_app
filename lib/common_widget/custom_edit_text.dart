import 'package:flutter/material.dart';

import '../resources/app_colors.dart';
import 'global_edit_text.dart';
import 'global_text.dart';

class CustomEditText extends StatelessWidget {
  final String? value;
  final Function? onTextChanged;
  final bool? isEditText;
  final String? hintText;
  final TextEditingController? controller;
  final TextInputType? inputType;
  final EdgeInsetsGeometry? margin;
  final double? width, height;
  final bool? isEditTextLineEnabled;
  final bool? obscureText;
  final EdgeInsetsGeometry? padding;
  final TextInputType? textInputType;
  final TextInputAction? textInputAction;

  const CustomEditText({
    Key? key,
    this.onTextChanged,
    this.isEditText = true,
    this.value,
    this.hintText,
    this.controller,
    this.inputType,
    this.margin,
    this.width,
    this.height,
    this.isEditTextLineEnabled,
    this.padding,
    this.obscureText,
    this.textInputType,
    this.textInputAction,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height ?? 50,
      width: width ?? 296,
      alignment: Alignment.center,
      padding: padding ?? const EdgeInsets.symmetric(horizontal: 10),
      margin: margin ?? const EdgeInsets.only(left: 40, right: 40),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10.0),
          border: Border.all(
              color: AppColors.primaryDarkColor,
              style: BorderStyle.solid,
              width: 1)),
      child: isEditText == true
          ? GlobalEditText(
              hintText ?? '',
              inputType ?? TextInputType.name,
              onTextChanged ?? () {},
              controller: controller,
              isEditTextLineEnabled: isEditTextLineEnabled ?? false,
              obscureText: obscureText,
              inputAction: textInputAction,
            )
          : GlobalText(
              value ?? '',
              color: AppColors.primaryDarkColor,
            ),
    );
  }
}
