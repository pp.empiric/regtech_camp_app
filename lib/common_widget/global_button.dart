import 'package:flutter/material.dart';
import 'package:regtech_camp_app/resources/app_colors.dart';

import '../resources/app_texts.dart';
import 'global_text.dart';

class GlobalButton extends StatelessWidget {
  final String? title;
  final GestureTapCallback? onTap;
  final double? width, height, fontSize;
  final EdgeInsetsGeometry? margin;
  final Color? bgColor;

  const GlobalButton({
    Key? key,
    this.title,
    this.onTap,
    this.width,
    this.height,
    this.fontSize,
    this.margin,
    this.bgColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      margin: margin,
      width: width ?? 180,
      height: height ?? 51,
      decoration: BoxDecoration(
        color: Colors.white,
        // border: Border.all(color: AppColors.primaryDarkColor, width: 1),
        borderRadius: BorderRadius.circular(10),
      ),
      child: Material(
        color: bgColor ?? AppColors.secondaryDartColor,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        child: InkWell(
          borderRadius: BorderRadius.circular(10),
          onTap: onTap ?? () {},
          child: Center(
              child: GlobalText(title ?? AppTexts.exploreCourse,
                  color: Colors.white,
                  fontSize: fontSize ?? 20,
                  fontWeight: FontWeight.w600,
                  fontStyle: FontStyle.normal)),
        ),
      ),
    );
  }
}
