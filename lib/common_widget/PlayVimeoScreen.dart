import 'dart:collection';
import 'dart:convert';

import 'package:better_player/better_player.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;

class PlayVimeoScreen extends StatefulWidget {
  final String url;
  final bool? playVideo;


  const PlayVimeoScreen(this.url, {Key? key, this.playVideo})
      : super(key: key);

  @override
  _PlayVimeoScreenState createState() => _PlayVimeoScreenState();
}

class _PlayVimeoScreenState extends State<PlayVimeoScreen> {
  var chewieController =
      BetterPlayerController(const BetterPlayerConfiguration()).obs;
  var init = false.obs;

  @override
  void initState() {
    callVideo();
    super.initState();
  }

  Widget build(BuildContext context) {
    return Obx(() =>
    init.value
        ? Card(
        semanticContainer: true,
        clipBehavior: Clip.antiAliasWithSaveLayer,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20.0),
        ),
        child: BetterPlayer(
          controller: chewieController.value,
        ))
        : const AspectRatio(
      aspectRatio: 16 / 9,
      child: Center(child: CircularProgressIndicator()),
    ));
  }

  @override
  void dispose() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    super.dispose();
  }

  Future<void> callVideo() async {
    final QualityLinks _quality = QualityLinks(widget.url
        .split('/')
        .last);
    final data = await _quality.getQualitiesSync();
    final _qualityValue = data[data.lastKey()];
    final BetterPlayerDataSource betterPlayerDataSource =
    BetterPlayerDataSource(
      BetterPlayerDataSourceType.network,
      _qualityValue,
    );
    chewieController.value = BetterPlayerController(
        BetterPlayerConfiguration(
            looping: false,
            autoPlay: false,
            deviceOrientationsAfterFullScreen: [
              DeviceOrientation.portraitUp,
              DeviceOrientation.portraitDown,
            ],
            fit: BoxFit.fitHeight,
            deviceOrientationsOnFullScreen: [
              DeviceOrientation.landscapeRight,
              DeviceOrientation.landscapeLeft,
            ],
            autoDetectFullscreenDeviceOrientation: true,
            autoDispose: true,
            allowedScreenSleep: false,
            controlsConfiguration: BetterPlayerControlsConfiguration(
              showControls: widget.playVideo ?? true,
              enableProgressText: true,
              enableSubtitles: false,
              enablePlayPause: true,
              enableOverflowMenu: true,
              controlBarColor: Colors.transparent,
              iconsColor: Colors.white,
              textColor: Colors.white,
              enablePip: false,
              playbackSpeedIcon: CupertinoIcons.speedometer,
              skipForwardIcon: CupertinoIcons.goforward_10,
              skipBackIcon: CupertinoIcons.gobackward_10,
              enableProgressBar: true,
              enablePlaybackSpeed: true,
              enableQualities: false,
              enableProgressBarDrag: true,
              enableAudioTracks: false,
              enableMute: true,
              loadingWidget: AspectRatio(
                aspectRatio: 16 / 9,
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              ),
              enableFullscreen: true,
            )),
        betterPlayerDataSource: betterPlayerDataSource);

    init.value = true;
  }
}

class QualityLinks {
  String videoId;

  QualityLinks(this.videoId);

  getQualitiesSync() {
    return getQualitiesAsync();
  }

  Future<SplayTreeMap> getQualitiesAsync() async {
    try {
      var url = 'https://player.vimeo.com/video/' +
          (videoId.contains('?') ? videoId
              .split("?")
              .first : videoId) + '/config';
      final response = await http.get(
          Uri.parse(url));
      final jsonData = jsonDecode(
          response.body)['request']['files']['progressive'];
      final SplayTreeMap videoList = SplayTreeMap.fromIterable(jsonData,
          key: (item) => "${item['quality']} ${item['fps']}",
          value: (item) => item['url']);
      return videoList;
    } catch (error) {
      print('=====> REQUEST ERROR: $error');
      return SplayTreeMap();
    }
  }
}
