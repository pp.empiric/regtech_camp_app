import 'package:flutter/material.dart';
import 'package:regtech_camp_app/resources/app_colors.dart';

import 'global_text.dart';

class GlobalOutlineButton extends StatelessWidget {
  final String? text;
  final double? height, width;
  final double? paddingTop, paddingBottom, paddingRight, paddingLeft;
  final VoidCallback? onTap;
  final double? fontSize;

  const GlobalOutlineButton({
    Key? key,
    this.text,
    this.width,
    this.height,
    this.paddingBottom,
    this.paddingLeft,
    this.paddingRight,
    this.paddingTop,
    this.onTap,
    this.fontSize,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      child: InkWell(
        onTap: onTap,
        child: Container(
          width: width,
          height: height ?? 51,
          decoration: BoxDecoration(
              borderRadius: const BorderRadius.all(Radius.circular(10)),
              border: Border.all(color: AppColors.secondaryDartColor, width: 1),
              color: const Color(0x00e86e25)),
          child: Center(
            child: GlobalText(
              text ?? '',
              fontStyle: FontStyle.normal,
              fontWeight: FontWeight.w600,
              fontSize: fontSize ?? 20,
              color: AppColors.secondaryDartColor,
              fontFamily: 'Poppins',
            ),
          ),
        ),
      ),
    );
  }
}
