import 'package:better_player/better_player.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:youtube_explode_dart/youtube_explode_dart.dart';

class PlayYouTubeScreen extends StatefulWidget {
  final String url;
  final bool? playVideo;

  const PlayYouTubeScreen(this.url, {Key? key, this.playVideo})
      : super(key: key);

  @override
  _PlayYouTubeScreenState createState() => _PlayYouTubeScreenState();
}

class _PlayYouTubeScreenState extends State<PlayYouTubeScreen> {
  BetterPlayerController? _chewieController;
  bool init = false;

  @override
  void initState() {
    try {
      callVideo();
    } catch (e) {
      print(e);
    }
    super.initState();
  }

  Widget build(BuildContext context) {
    return init == true
        ? Card(
            semanticContainer: true,
            clipBehavior: Clip.antiAliasWithSaveLayer,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15.0),
            ),
            child: BetterPlayer(
              controller: _chewieController!,
            ))
        : const AspectRatio(
            aspectRatio: 16 / 9,
            child: Center(child: CircularProgressIndicator()),
          );
  }

  @override
  void dispose() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    super.dispose();
  }

  Future<void> callVideo() async {
    final VideoId videoId = VideoId(
        widget.url.split(':').last.split('/').last.contains('=')
            ? widget.url.split(':').last.split('/').last.split('=').last
            : widget.url.split(':').last.split('/').last);
    final video =
        await YoutubeExplode().videos.streamsClient.getManifest(videoId);
    YoutubeExplode().close();
    final Map<String, String> data = <String, String>{};

    video.muxed.sortByVideoQuality().forEach((element) {
      data[element.qualityLabel] = element.url.toString();

      if (element == video.muxed.sortByVideoQuality().last) {
        final String? url = data.containsKey('360p')
            ? data['360p']
            : data.containsKey('720p')
                ? data['720p']
                : data.containsKey('480p')
                    ? data['480p']
                    : video.muxed.sortByVideoQuality().first.url.toString();
        final betterPlayerDataSource = BetterPlayerDataSource(
            BetterPlayerDataSourceType.network, url ?? '',
            resolutions: data);

        _chewieController = BetterPlayerController(
            BetterPlayerConfiguration(
                looping: false,
                autoPlay: false,
                deviceOrientationsAfterFullScreen: [
                  DeviceOrientation.portraitUp,
                  DeviceOrientation.portraitDown,
                ],
                deviceOrientationsOnFullScreen: [
                  DeviceOrientation.landscapeRight,
                  DeviceOrientation.landscapeLeft,
                ],
                fit: BoxFit.fitHeight,
                autoDetectFullscreenDeviceOrientation: true,
                autoDispose: true,
                allowedScreenSleep: false,
                controlsConfiguration: BetterPlayerControlsConfiguration(
                  showControls: widget.playVideo ?? true,
                  enableProgressText: true,
                  enableSubtitles: false,
                  enablePlayPause: true,
                  controlBarColor: Colors.transparent,
                  iconsColor: Colors.white,
                  textColor: Colors.white,
                  enableOverflowMenu: true,
                  enablePip: false,
                  playbackSpeedIcon: CupertinoIcons.speedometer,
                  skipForwardIcon: CupertinoIcons.goforward_10,
                  skipBackIcon: CupertinoIcons.gobackward_10,
                  enableProgressBar: true,
                  enablePlaybackSpeed: true,
                  enableQualities: true,
                  enableProgressBarDrag: true,
                  enableAudioTracks: false,
                  enableMute: true,
                  loadingWidget: AspectRatio(
                    aspectRatio: 16 / 9,
                    child: Center(
                      child: CircularProgressIndicator(),
                    ),
                  ),
                  enableFullscreen: true,
                )),
            betterPlayerDataSource: betterPlayerDataSource);
        setState(() {
          init = true;
        });
      }
    });
  }
}
