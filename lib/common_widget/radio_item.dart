import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:regtech_camp_app/model/radio_model.dart';
import 'package:regtech_camp_app/resources/app_colors.dart';

import '../utils.dart';
import 'global_text.dart';

class RadioItem extends StatelessWidget {
  final RxList<RadioModel> items;
  final bool isFileUpload;

  RadioItem(this.items, this.isFileUpload, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print(items.length);
    final item = isFileUpload ? items.first : items.last;
    var url = '';
    return Obx(() => Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            InkWell(
              onTap: () {
                items.forEach((element) {
                  element.isSelected.value = false;
                });
                if (isFileUpload) {
                  items.first.isSelected.value = true;
                } else {
                  items.last.isSelected.value = true;
                }
              },
              splashColor: Colors.transparent,
              highlightColor: Colors.transparent,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                      height: 20.0,
                      width: 20.0,
                      child: item.isSelected.value
                          ? SvgPicture.asset('assets/images/selected_radio.svg')
                          : SvgPicture.asset(
                              'assets/images/unselected_radio.svg')),
                  Flexible(
                    child: Container(
                      margin: const EdgeInsets.only(left: 10.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          GlobalText(item.buttonText,
                              maxLine: 1,
                              textOverflow: TextOverflow.ellipsis,
                              color: AppColors.primaryDarkColor,
                              fontSize: 18,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal),
                          GlobalText(item.text,
                              maxLine: 2,
                              color: const Color(0xff717171),
                              fontSize: 15,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
            isFileUpload && items.first.isSelected.value
                ? const SizedBox(height: 29)
                : !isFileUpload && items.last.isSelected.value
                    ? const SizedBox(height: 29)
                    : isFileUpload
                        ? const SizedBox(height: 20)
                        : const SizedBox(),
            isFileUpload && items.first.isSelected.value
                ? Container(
                    margin: const EdgeInsets.only(left: 48, bottom: 20),
                    decoration: BoxDecoration(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(10)),
                        color: AppColors.secondaryDartColor),
                    child: Material(
                      color: Colors.transparent,
                      child: InkWell(
                        onTap: () => item.onClick(),
                        borderRadius:
                            const BorderRadius.all(Radius.circular(10)),
                        child: Container(
                            width: 174,
                            height: 51,
                            alignment: Alignment.center,
                            child: const GlobalText('Choose File',
                                color: Colors.white,
                                fontSize: 22,
                                fontWeight: FontWeight.w700,
                                fontStyle: FontStyle.normal)),
                      ),
                    ),
                  )
                : !isFileUpload && items.last.isSelected.value
                    ? Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                            TextField(
                              onChanged: (value) {
                                url = validateUrl(value);
                              },
                              decoration: InputDecoration(
                                  border: borderStyle,
                                  enabledBorder: borderStyle,
                                  disabledBorder: borderStyle,
                                  focusedBorder: borderStyle,
                                  prefixIcon: null,
                                  hintText: 'Enter URL'),
                              keyboardType: TextInputType.url,
                            ),
                            const SizedBox(height: 15),
                            Container(
                              margin: const EdgeInsets.only(left: 20),
                              color: Colors.transparent,
                              child: Material(
                                color: Colors.transparent,
                                child: InkWell(
                                  onTap: () async {
                                    if (url.isEmpty) {
                                      showSnackBar('Enter Url!!', '',
                                          Colors.red, Icons.error);
                                      return;
                                    }
                                    await item.onClick(url);
                                  },
                                  borderRadius: const BorderRadius.all(
                                      Radius.circular(10)),
                                  child: Container(
                                      width: 174,
                                      height: 51,
                                      alignment: Alignment.center,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          color: AppColors.secondaryDartColor),
                                      child: const GlobalText('Add Link',
                                          color: Colors.white,
                                          fontSize: 22,
                                          fontWeight: FontWeight.w700,
                                          fontStyle: FontStyle.normal)),
                                ),
                              ),
                            )
                          ])
                    : const SizedBox()
          ],
        ));
  }

  OutlineInputBorder borderStyle = OutlineInputBorder(
    borderRadius: BorderRadius.circular(10.0),
    borderSide: BorderSide(color: AppColors.primaryDarkColor),
  );
}
