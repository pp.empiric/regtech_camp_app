import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:regtech_camp_app/modules/home/controllers/home_controller.dart';

import '../resources/app_images.dart';
import '../routes/app_routes.dart';
import 'global_text.dart';

class ProfileWidget extends StatelessWidget {
  final String? title;
  final String? profileImage;
  final VoidCallback? onTap;
  final bool? backVisibility;
  final bool? isFolderVisibility;
  final Widget? folderWidget;

  const ProfileWidget({
    Key? key,
    this.title,
    this.profileImage,
    this.onTap,
    this.backVisibility = true,
    this.isFolderVisibility = false,
    this.folderWidget,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Image.asset(
          AppImages.bgPurpleImage,
          height: 96,
          fit: BoxFit.fitWidth,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            (backVisibility == true)
                ? Container(
                    margin: const EdgeInsets.only(top: 40),
                    child: InkWell(
                      child: IconButton(
                          onPressed: onTap ?? () => Get.back(),
                          icon: const Icon(CupertinoIcons.back,
                              color: Colors.white, size: 25)),
                    ),
                  )
                : const SizedBox(width: 30),
            Expanded(
              child: Container(
                margin: const EdgeInsets.only(
                  top: 40,
                ),
                child: GlobalText(title ?? '',
                    color: Colors.white,
                    letterSpacing: 0.6,
                    fontSize: 16,
                    textOverflow: TextOverflow.ellipsis,
                    fontWeight: FontWeight.w700,
                    textAlign: TextAlign.center,
                    fontStyle: FontStyle.normal),
              ),
            ),
            if (isFolderVisibility == true)
              Container(
                  alignment: Alignment.center,
                  margin: const EdgeInsets.only(top: 40),
                  child: folderWidget),
            Obx(() => InkWell(
                highlightColor: Colors.transparent,
                splashColor: Colors.transparent,
                onTap: () async {
                  Get.toNamed(Routes.PROFILE,
                      arguments: Get.find<HomeController>());
                },
                child: Get.find<HomeController>().profilePic.value.isEmpty
                    ? Container(
                        margin: const EdgeInsets.only(top: 40, right: 20),
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(color: Colors.white, width: 2),
                        ),
                        child: Image.asset(
                          AppImages.profileImage,
                          width: 34,
                          height: 34,
                        ),
                      )
                    : Container(
                        width: 34,
                        height: 34,
                        margin: const EdgeInsets.only(top: 40, right: 20),
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: NetworkImage(
                                Get.find<HomeController>().profilePic.value),
                            fit: BoxFit.cover,
                          ),
                          shape: BoxShape.circle,
                          border: Border.all(
                            color: Colors.white,
                            width: 2.0,
                          ),
                        ))))
          ],
        ),
      ],
    );
  }
}
