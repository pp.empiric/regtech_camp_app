import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';

import '../resources/app_colors.dart';
import 'global_text.dart';

class PlayAudioScreen extends StatefulWidget {
  final String? url;
  final String? filename;

  const PlayAudioScreen(this.url, this.filename, {Key? key}) : super(key: key);

  @override
  _PlayAudioScreenState createState() => _PlayAudioScreenState();
}

class _PlayAudioScreenState extends State<PlayAudioScreen> {
  bool isPlaying = false;
  Duration _duration = const Duration(seconds: 10);
  Duration _position = const Duration(microseconds: 5);

  AudioPlayer? advancedPlayer;
  AudioCache? audioCache;

  @override
  void initState() {
    super.initState();
    initPlayer();
  }

  void initPlayer() {
    advancedPlayer = AudioPlayer();
    audioCache = AudioCache(fixedPlayer: advancedPlayer);

    advancedPlayer?.onDurationChanged.listen((Duration d) async {
      setState(() => _duration = d);
    });

    advancedPlayer?.onAudioPositionChanged.listen((Duration p) async {
      setState(() => _position = p);

      advancedPlayer?.onPlayerCompletion.listen((event) {
        setState(() {
          isPlaying = false;
        });
      });
    });
  }

  void seekToSecond(int second) {
    Duration newDuration = Duration(seconds: second);

    advancedPlayer?.seek(newDuration);
  }

  Widget build(BuildContext context) {
    return Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0),
      ),
      child: Container(
        color: Colors.black87,
        alignment: Alignment.center,
        child: Column(
          children: <Widget>[
            const Padding(
              padding: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 10.0),
              child: Icon(
                Icons.music_note_rounded,
                size: 40.0,
                color: Colors.white,
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 10.0),
              child: GlobalText(
                widget.filename??'',
                fontSize: 20.0,
                maxLine: 1,
                color: Colors.white,
                textAlign: TextAlign.start,
              ),
            ),
            Row(
              children: [
                ElevatedButton(
                  onPressed: () {
                    playPause(widget.url??'');
                  },
                  child: isPlaying
                      ? const Icon(
                          Icons.pause,
                          size: 20,
                        )
                      : const Icon(Icons.play_arrow, size: 20),
                  style: ElevatedButton.styleFrom(
                      shape: const CircleBorder(),
                      primary: AppColors.primaryDarkColor),
                  // color: kPrimaryColor,
                  // shape: CircleBorder(),
                ),
                const GlobalText(
                  "0:0",
                  color: Colors.white,
                ),
                SizedBox(
                  width: 200.0,
                  child: Slider.adaptive(
                    onChanged: (double value) {
                      setState(() {
                        seekToSecond(value.toInt());
                        value = value;
                      });
                    },
                    min: 0.0,
                    activeColor: AppColors.primaryDarkColor,
                    max: _duration.inSeconds.toDouble(),
                    value: _position.inSeconds.toDouble(),
                  ),
                ),
                GlobalText(
                  "${_duration.inMinutes.toString()}:${_duration.inSeconds - _duration.inMinutes * 60}",
                  color: Colors.white,
                ),
                const SizedBox(
                  height: 10.0,
                )
              ],
            ),
            const SizedBox(
              height: 10.0,
            )
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    advancedPlayer?.dispose();
    super.dispose();
  }

  void playPause(String s3url) {
    if (isPlaying) {
      setState(() {
        isPlaying = !isPlaying;
      });
      advancedPlayer?.pause();
    } else {
      setState(() {
        isPlaying = !isPlaying;
      });
      advancedPlayer?.play(widget.url??'');
    }
  }
}
