import 'package:flutter/material.dart';

import '../resources/app_colors.dart';

class GlobalIconLoader extends StatelessWidget {
  final Color? color;
  const GlobalIconLoader({Key? key, this.color}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 18.0,
      width: 18.0,
      child: CircularProgressIndicator(
        valueColor: AlwaysStoppedAnimation<Color>(color??AppColors.primaryDarkColor),
      ),
    );
  }
}
