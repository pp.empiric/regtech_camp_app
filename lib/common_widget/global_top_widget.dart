import 'package:flutter/material.dart';

import '../resources/app_images.dart';
import '../utils.dart';

class GlobalTopWidget extends StatelessWidget {
  const GlobalTopWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
      Padding(
        padding: const EdgeInsets.only(top: 50.0, left: 16, bottom: 60),
        child: Image.asset(AppImages.firstSmallLogo),
      ),
      Image.asset(AppImages.secondSmallLogo),
      RichText(
        text: TextSpan(
          text: 'CERTIFIED \n',
          style: const TextStyle(
            height: 1.5,
            fontSize: 17,
            fontWeight: FontWeight.w600,
          ),
          children: [
            TextSpan(text: 'ANTI MONEY LAUNDERING \n', style: style),
            TextSpan(text: 'PROFESSIONAL', style: style),
          ],
        ),
      ),
    ]);
  }
}
