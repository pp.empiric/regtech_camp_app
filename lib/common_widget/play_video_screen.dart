import 'package:better_player/better_player.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:regtech_camp_app/resources/app_colors.dart';

class PlayVideoScreen extends StatefulWidget {
  final String url;
  final bool? playVideo;

  const PlayVideoScreen(this.url, {Key? key, this.playVideo}) : super(key: key);

  @override
  _PlayVideoScreenState createState() => _PlayVideoScreenState();
}

class _PlayVideoScreenState extends State<PlayVideoScreen> {
  BetterPlayerController? _chewieController;

  @override
  void initState() {
    try {
      final BetterPlayerDataSource betterPlayerDataSource =
          BetterPlayerDataSource(
        BetterPlayerDataSourceType.network,
        widget.url,
      );

      _chewieController = BetterPlayerController(
          BetterPlayerConfiguration(
              looping: false,
              autoPlay: false,
              deviceOrientationsAfterFullScreen: [
                DeviceOrientation.portraitUp,
                DeviceOrientation.portraitDown,
              ],
              deviceOrientationsOnFullScreen: [
                DeviceOrientation.landscapeRight,
                DeviceOrientation.landscapeLeft,
              ],
              fit: BoxFit.fitHeight,
              autoDetectFullscreenDeviceOrientation: true,
              autoDispose: true,
              allowedScreenSleep: false,
              controlsConfiguration: BetterPlayerControlsConfiguration(
                showControls: widget.playVideo ?? true,
                enableProgressText: true,
                enableSubtitles: false,
                enablePlayPause: true,
                enableOverflowMenu: true,
                enablePip: false,
                controlBarColor: Colors.transparent,
                iconsColor: Colors.white,
                textColor: Colors.white,
                enableProgressBar: true,
                enablePlaybackSpeed: true,
                enableQualities: false,
                enableProgressBarDrag: true,
                enableAudioTracks: false,
                playbackSpeedIcon: CupertinoIcons.speedometer,
                skipForwardIcon: CupertinoIcons.goforward_10,
                skipBackIcon: CupertinoIcons.gobackward_10,
                enableMute: true,
                loadingWidget: AspectRatio(
                  aspectRatio: 16 / 9,
                  child: Center(
                    child: CircularProgressIndicator(
                      color: AppColors.primaryDarkColor,
                    ),
                  ),
                ),
                enableFullscreen: true,
              )),
          betterPlayerDataSource: betterPlayerDataSource);
    } catch (e) {
      print(e);
    }
    super.initState();
  }

  Widget build(BuildContext context) {
    return Card(
        semanticContainer: true,
        clipBehavior: Clip.antiAliasWithSaveLayer,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        child: BetterPlayer(
          controller: _chewieController!,
        ));
  }

  @override
  void dispose() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    super.dispose();
  }
}
