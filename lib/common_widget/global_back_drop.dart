import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:regtech_camp_app/resources/app_colors.dart';

class GlobalBackDrop extends StatelessWidget {
  const GlobalBackDrop({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BackdropFilter(
        filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
        child: Container(
            color: Colors.white.withOpacity(.2),
            child:  Center(
                child: CircularProgressIndicator(color:AppColors.primaryDarkColor))));
  }
}
