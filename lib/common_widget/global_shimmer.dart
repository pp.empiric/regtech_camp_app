import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:regtech_camp_app/common_widget/profile_widget.dart';
import 'package:regtech_camp_app/resources/app_colors.dart';
import 'package:regtech_camp_app/resources/app_texts.dart';
import 'package:shimmer/shimmer.dart';

import 'global_text.dart';

class ShimmerEffect extends StatelessWidget {
  const ShimmerEffect({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
        baseColor: Colors.black26,
        highlightColor: Colors.grey,
        child:
            Card(
                margin: const EdgeInsets.only(
                    left: 10, bottom: 15.0, top: 20, right: 20),
                color: Colors.grey[900],
                shape: RoundedRectangleBorder(
                  side: const BorderSide(color: Colors.white70, width: 1),
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Container(
                  width: 340,
                  height: 150,
                  margin: const EdgeInsets.only(
                      left: 10, bottom: 15, top: 10, right: 10),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(15),
                    boxShadow: [
                      BoxShadow(
                          color: AppColors.primaryDarkColor,
                          offset: const Offset(0, 0),
                          blurRadius: 5,
                          spreadRadius: 0),
                    ],
                  ),

                )));
  }
}
