import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class GlobalSvgButton extends StatelessWidget {
  final Color? color;
  final String? assetPath;
  final VoidCallback? onTap;
  final double? height;
  final double? width;
  final EdgeInsetsGeometry? padding;
  final Widget? child;

  const GlobalSvgButton(
      {Key? key, this.color, this.width, this.height, this.onTap, this.assetPath, this.child, this.padding})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: onTap,
        child: Container(
          padding: padding,
          child: SvgPicture.asset(assetPath ?? ''),
        ),
      ),
    );
  }
}
