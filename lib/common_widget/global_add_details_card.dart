import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:regtech_camp_app/modules/profile/controllers/profile_controller.dart';
import 'package:regtech_camp_app/resources/app_images.dart';

import 'global_text.dart';

class GlobalAddDetails extends StatelessWidget {

  final String? text;
  final VoidCallback? onTap;

  const GlobalAddDetails({
    this.text,
    this.onTap,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        alignment: Alignment.center,
        height: 64,
        width: 344,
        margin: const EdgeInsets.only(left: 14, right: 17.0),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15.0),
            boxShadow: const [
              BoxShadow(
                  color: Color(0x2e000000),
                  offset: Offset(0, 0),
                  blurRadius: 10,
                  spreadRadius: 0)
            ],
            color: Colors.white),
        child: Padding(
          padding: const EdgeInsets.only(left: 12.0),
          child: Row(
            children: [
              SvgPicture.asset(AppImages.svgPlusSign),
              const SizedBox(width: 10.4),
              GlobalText(
                text ?? '',
                fontSize: 18,
                fontWeight: FontWeight.w500,
                fontStyle: FontStyle.normal,
                color: const Color(0xff333333),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
