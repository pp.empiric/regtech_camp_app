import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class ShimmerEffectContainer extends StatelessWidget {
  const ShimmerEffectContainer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
        baseColor: Colors.black26,
        highlightColor: Colors.grey,
     child:Container(
       height: 70,
        width: 340,
       decoration: BoxDecoration(
         borderRadius: BorderRadius.circular(15),
         color: Colors.white,
       ),
     ));
  }
}
