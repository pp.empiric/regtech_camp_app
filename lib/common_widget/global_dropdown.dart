import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:regtech_camp_app/common_widget/global_text.dart';
import 'package:regtech_camp_app/resources/app_colors.dart';
import 'package:regtech_camp_app/resources/app_images.dart';

class GlobalDropDown extends StatelessWidget {
  final String? intialValue;
  final FontWeight? fontWeight;
  final Color? color;
  final Function(String)? onselected;
  final List<String>? listone;
  final String? value;
  final Widget? child;

  const GlobalDropDown({
    Key? key,
    this.intialValue,
    this.fontWeight,
    this.color,
    this.onselected,
    this.listone,
    this.value,
    this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 50,
        alignment: Alignment.center,
        margin: const EdgeInsets.only(left: 40, right: 40, top: 20, bottom: 20),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.0),
            border: Border.all(color: AppColors.primaryDarkColor, style: BorderStyle.solid, width: 1)),
        child: PopupMenuButton<String>(
          initialValue: '2018',
          itemBuilder: (context) {
            return listone!.map((str) {
              return PopupMenuItem(
                value: str,
                child: Container(
                    alignment: Alignment.center,
                    width: Get.width,
                    child: GlobalText(
                      str,
                      fontSize: 16,
                    )),
              );
            }).toList();
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              GlobalText(value!, fontSize: 16, fontWeight: fontWeight, color: color),
              const SizedBox(
                width: 5,
              ),
              SvgPicture.asset(AppImages.downIcon),
            ],
          ),
          onSelected: onselected,
        ));
  }
}
